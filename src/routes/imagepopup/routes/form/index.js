import React from 'react';
import { Form, Icon, Button, Spin } from 'antd';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { backendUrl } from '../../../../common/credentials'
import { createGlobalStyle } from 'styled-components';
import Dragger from 'antd/lib/upload/Dragger';
import { withToastManager } from 'react-toast-notifications';
/* eslint-disable */
const FormItem = Form.Item;

class FormComponent extends React.Component {
  state = {
    confirmDirty: false,
    // image: [],
    isLoading: false,
    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };

  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };

  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };

  addNewApi = async () => {
    const { toastManager } = this.props
    console.log("add New API", this.state)
    const {
      image,
    } = this.state;

    const { match, history } = this.props;
    const authToken = localStorage.getItem('access_token');
    this.setState({ isLoading: true });
    const self = this;
    const formData = new FormData()
    formData.append("image", image)
    axios
      .post(
        `${backendUrl}/api/uploadImage`,
        formData,
        {
          headers: {
            Authorization: `Bearer ${authToken}`,
          },
        }

      )
      .then(function (response) {
        console.log('rrrr res', response);
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        toastManager.add(
          error && error.response && error.response.data && error.response.data.message, {
          appearance: "warning",
          autoDismiss: true
        });
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 8080);
        console.log('rrrr err', error);
      });
  };

  validateForm = () => {
    const {
      image,
      error,
    } = this.state;

    if (
      image.length <= 0 ||
      image === undefined ||
      image === null
    ) {
      this.setState({
        error: {
          errorText: 'Please Select Valid popup image',
          errorType: 'add_image',
        },
      });
      return false;
    }

    return true;
  };

  handleAddData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.addNewApi();
    }
  };

  onDragChange = info => {
    const status = info.file.status;
    if (status !== 'uploading') {
      this.setState({
        image: info.file.originFileObj,
      });
    }
  };

  beforeUpload = file => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/jpg';
    const isLt2M = file.size / 1024 / 1024 < 5;
    if (!isJpgOrPng) {

      this.setState({
        error: {
          errorText: 'Please select vaild add image (JPG,PNG,JPEG )',
          errorType: 'add_image',
        },
      });
      return false;
    } else if (!isLt2M) {
      this.setState({
        error: {
          errorText: 'Image must smaller than 5MB!',
          errorType: 'add_image',
        },
      });
      return false;
    } else {
      this.setState({
        error: {
          errorText: '',
          errorType: '',
        },
      });
    }
    return isJpgOrPng && isLt2M;
  };

  render() {
    const {
      isLoading,
      apiError,
      error,
    } = this.state;
    const { errorType } = error;

    return (
      <div className="form-container">
        <GlobalStyle />
        <FormItem label="Add Image Popup">
          <Dragger
            listType="picture"
            onChange={this.onDragChange}
            showUploadList={errorType == 'add_image' ? false : true}
            beforeUpload={this.beforeUpload}
            name={'file'}
            multiple={false}
            accept=".jpg, .png, .jpeg"
          >
            <p className="ant-upload-drag-icon">
              <Icon type="inbox" />
            </p>
            <p className="ant-upload-text">Click or drag file to this area to upload</p>
            <p className="ant-upload-hint">
              Support for a single or bulk upload. Strictly prohibit from uploading company data
              or other band files
            </p>
          </Dragger>
          Note: For better user experience upload image of 1280 × 814 pixels image.
          {this.errorShow('add_image')}
        </FormItem>
        {isLoading &&
          <div className="mb-2 text-center">
            <Spin />
          </div>
        }

        <div className="text-right pt-3">
          <Button
            type="primary"
            htmlType="submit"
            className="btn-cta"
            onClick={this.handleAddData}
          >
            <Icon type="plus-circle" theme="filled" />
            Add
          </Button>
        </div>

        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}
      </div>
    );
  }
}

const WrappedFormComponent = Form.create()(withRouter(FormComponent));

export default withToastManager(WrappedFormComponent);

const GlobalStyle = createGlobalStyle`.ant-select-dropdown--multiple{display:none}`;