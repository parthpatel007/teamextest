import React from 'react';
import { Table, Icon } from 'antd';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import getAllImagePopupDetail from '../../../middleware/imagePopupMiddleware';
import Delete from './delete/index'

class ImagePopup extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            loader: true,
            listData: [],
            filterData: [],
        };
    }

    componentDidMount() {
        const { getAllImagePopupDetail } = this.props
        getAllImagePopupDetail();
        const { imagePopup } = this.props
        this.setState({
            filterData: imagePopup
        })

    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.imagePopup !== this.props.imagePopup) {
            this.setState({
                filterData: this.props.imagePopup,
                listData: this.props.imagePopup,
                loader: this.props && this.props.imagePopup && this.props.imagePopup.length > 0 ? false : true
            })

        }
    }

    render() {
        const { match } = this.props;
        const { listData } = this.state;
        const columns = [
            {
                title: 'Index',
                render: (text, record, index) => <> {index + 1} </>,
                width: 80,
            },
            {
                title: 'Image',
                dataIndex: 'image',
                key: 'image',
                render: (src) => {
                    return (
                        <div>{
                            src && <img height="130" width="auto" src={src && src.original} alt="imagePopupimg" />
                        }
                        </div>)
                },
                width: 150,
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => {
                    return (
                        <span className="d-flex">
                            <Delete recordData={text} />
                        </span>
                    );
                },
                width: 100,
            },
        ];
        return (
            <>
                <section className="container-fluid  no-breadcrumb chapter">
                    <article className="article">
                        <div className="d-flex justify-content-between mb-2">
                            <div className="d-flex">
                                <h4>Note: To replace this image click on add.</h4>
                            </div>
                            <div className="d-flex">
                                <div>
                                    <Link to={`${match.url}/add`} className="ant-btn ant-btn-primary">
                                        <Icon type="plus-circle" theme="filled" /> Add
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {/* {loader &&
                            <Skeleton height={50} count={10} />
                        } */}

                        {/* {!loader && */}
                        <Table
                            pagination={{ defaultPageSize: 15 }}
                            columns={columns}
                            dataSource={listData}
                            className="ant-table-v1"
                        // scroll={{ x: 1200 }}
                        />
                        {/* } */}
                    </article>
                </section>
            </>
        );
    }
}

const mapStateToProps = state => ({
    imagePopup: state.imagePopup.imagePopup
})
const mapDispatchToProps = dispatch => ({
    getAllImagePopupDetail: () => dispatch(getAllImagePopupDetail())
})
export default connect(mapStateToProps, mapDispatchToProps)(ImagePopup)