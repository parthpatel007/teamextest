import React from 'react';
import { Route } from 'react-router-dom';
import AddHomebanner from './routes/add/index';
import ImagePopup from './routes/index';

const Shop = ({ match }) => {
  return (
    <>
      <Route exact path={`${match.url}`} component={ImagePopup} />
      <Route path={`${match.url}/add`} component={AddHomebanner} />
    </>
  )
}
export default Shop;