import React from 'react';
import { Table, Icon, DatePicker, Form, Input, Button, Select, Tooltip, Skeleton } from 'antd';
import { BlockModal } from '../../../components/Modal';
import { ReviewCard } from '../../../components/Cards';
import styled from 'styled-components';
import { connect } from 'react-redux';
import getAllReviewDetail from '../../../middleware/reviewMiddleware';
const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const Option = Select.Option;

/* eslint-disable */

class Review extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            startDate: '',
            endDate: '',
            loader: true,
            search: '',
            store: [],
            store_id: localStorage.getItem('store_id') ? localStorage.getItem('store_id') : '',
            listData: [],
            filterData: [],
            stockFilter: 0
        };
        this.selectedStore = this.selectedStore.bind(this);
    }

    getDataRangeApi = () => {
        const { startDate, endDate, listData } = this.state;
        const filterDataHolder = [
            ...listData.filter(
                ({ createdAt }) =>
                    (new Date(createdAt).getTime() > new Date(startDate).getTime() && new Date(createdAt).getTime() < new Date(endDate).getTime())
            )
        ];
        this.setState({
            filterData: filterDataHolder,
            statusFilter: 0,
        });
    };
    componentDidMount() {
    
     const {getReviewDetails} = this.props
     getReviewDetails()


        if (this.props.customersSuccess) {
            this.setState({
                loader: false
            });
        }
        else {
            this.setState({
                loader: false
            });
        }


    }


    componentDidUpdate(prevProps, prevState) {

        const { customer } = this.props;

        console.log(prevState, "state state state state state state state ")

        console.log(prevProps, "PRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndks ")

        if (prevProps.customer !== customer) {
            console.log(" getCustomersDetail  getCustomersDetailGEEEEEEEEEEETTTTTTTTTTTTTT PACKAGES DETAILS SDDSDSDSC CALLED ", customer)
            this.setState({
                filterData: customer,
                listData: customer,
                loader: customer.length > 0 ? false : true
            })
        }

    }

    handleSearch = e => {
        const { listData } = this.state;
        const filterDataHolder = [
            ...listData.filter(
                ({ userName }) =>
                    userName.toLowerCase().startsWith(e.target.value.trim().toLowerCase())
            ),
        ];
        this.setState({
            [e.target.name]: e.target.value,
            filterData: filterDataHolder,
            stockFilter: 0,
        });
    };

    handleStatusFilter = stock => {
        const { listData, statusFilter } = this.state;

        let filterDataHolder = [];
        if (statusFilter === stock) {
            filterDataHolder = [...listData];
        } else {
            filterDataHolder = [
                ...listData.filter(
                    ({ status }) => (stock === 1 && status === 0) || (stock === 2 && status === 1)
                ),
            ];
        }
        this.setState({
            search: '',
            statusFilter: stock !== statusFilter ? stock : 0,
            filterData: filterDataHolder,
        });
    };

    handleStockFilter = stock => {
        const { listData, stockFilter } = this.state;

        let filterDataHolder = [];
        if (stockFilter === stock) {
            filterDataHolder = [...listData.filter(({ store_id }) => store_id === this.state.store_id)];
        } else {
            filterDataHolder = [
                ...listData.filter(
                    ({ status, store_id }) =>
                        ((stock === 1 && status === 0) || (stock === 2 && status === 1)) &&
                        store_id === this.state.store_id
                ),
            ];
        }

        this.setState({
            search: '',
            stockFilter: stock !== stockFilter ? stock : 0,
            filterData: filterDataHolder,
        });
    };

    handleSelect = (name, value) => {
        const { listData } = this.state;

        console.log(
            'rrrr handleSelect',
            listData,
            listData.filter(({ store_id }) => store_id === value)
        );
        const filterDataHolder = [...listData.filter(({ store_id }) => store_id === value)];
        this.setState({ [name]: value, stockFilter: 0 });
        this.getDataApiWithStoreId(value);
        if (!localStorage.getItem('store_id')) {
            localStorage.setItem('store_id', value);
        }
    };
    selectedStore() {
        const { store, store_id } = this.state;
        return (
            <SelectStore>
                <FormItem label="Choose Store" hasFeedback className="mb-0 d-flex align-items-center">
                    <Select
                        name="store_id"
                        value={store_id}
                        placeholder="Select Store"
                        onChange={e => this.handleSelect('store_id', e)}
                    >
                        {store.map((data, index) => (
                            <Option key={index} value={data._id}>
                                {data.name}
                            </Option>
                        ))}
                    </Select>
                </FormItem>
            </SelectStore>
        );
    }
    onDateChange = (date, dateString) => {
        if (date.length == 0) {
            const { listData } = this.state
            this.setState({
                filterData: listData
            })
        }
        console.log('eee', date, 'dddd', dateString);
        this.setState({ startDate: dateString[0], endDate: dateString[1] });
    };
    onRangeSearch = () => {
        this.getDataRangeApi();
    };
    render() {
        const { match } = this.props;
        const { listData, filterData, startDate, loader, search, stockFilter } = this.state;
        const dateFormat = 'YYYY-MM-DD HH:mm:ss.SSS[Z]';
        const columns = [
    
            
            {
                title:'ID',
            },
            {
                title:'Name',
            },
            {
                title:'Email',
            },
            {
                title:'Review',
            },
            {
                title:'Rating',
            },
            {
                title:'Date',
            },
            {
                title:'Product Id',
            },
         
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => {
                    console.log('razat', text);
                    return (
                        <span className="d-flex">
                            {/* <Link
                    to={{ pathname: `${match.url}/preview`, state: text }}
                    className="ant-btn ant-btn-primary ant-btn-circle"
                  >
                    <Icon type="eye" theme="filled" />
                  </Link> */}
                            {/* <Link
                    to={{ pathname: `${match.url}/edit`, state: text }}
                    className="ml-2 ant-btn ant-btn-primary ant-btn-circle"
                  >
                    <Icon type="edit" theme="filled" />
                  </Link>
                  <DeleteCustomer recordData={text} /> */}
                            <Tooltip placement="bottomLeft" title={text.status === 0 ? 'block' : 'unblock'}>
                                <span>
                                    <BlockModal
                                        recordData={text}
                                        recordName={text.userName}
                                        api={'api/changeCustomerStatus'}
                                        type={text.status === 0 ? 'block' : 'unblock'}
                                        blockTitleText="Block customer"
                                        unblockTitleText="Unblock customer"
                                        blockText="block customer"
                                        unblockText="unblock customer"
                                        blockBtnText="Block customer"
                                        unblockBtnText="Unblock customer"
                                    />
                                </span>
                            </Tooltip>
                        </span>
                    );
                },
                width: 166,
            },
        ];
        return (
            <>
                <ReviewCard
                    dateFilter={
                        <div>
                            <RangePicker
                                style={{ width: 270 }}
                                onChange={this.onDateChange}
                                format={dateFormat}
                            />
                            <Button
                                className="ml-2"
                                onClick={() => this.onRangeSearch()}
                                type="primary"
                                disabled={startDate === ''}
                            >
                                Search
                  </Button>
                            {startDate !== '' && (
                                <Button className="ml-2" onClick={() => window.location.reload()} type="danger">
                                    Reset
                                </Button>
                            )}
                        </div>
                    }
                    total={listData.length}
                    active={listData.filter(({ status }) => status === 0).length}
                    inactive={listData.filter(({ status }) => status === 1).length}
                    title="Customers"
                />
                <section className="container-fluid  no-breadcrumb chapter">
                    <article className="article">
                        <div className="d-flex justify-content-between mb-2">
                            <div className="d-flex">
                                <FormItem>
                                    <Input
                                        placeholder="Search Review"
                                        prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                                        name="search"
                                        value={search}
                                        style={{ width: 275 }}
                                        onChange={this.handleSearch}
                                    />
                                </FormItem>
                            </div>
                        
                            <div className="d-flex">
                                <div>
                                    <Button
                                        className="mx-2"
                                        onClick={() => this.handleStatusFilter(1)}
                                        type={stockFilter === 1 ? 'primary' : 'secondary'}
                                    >
                                        Active
                      </Button>
                                    <Button
                                        className="mr-2"
                                        onClick={() => this.handleStatusFilter(2)}
                                        type={stockFilter === 2 ? 'primary' : 'secondary'}
                                    >
                                        Inactive
                      </Button>

                                    {/* <Link to={`${match.url}/add`} className="ant-btn ant-btn-primary">
                        <Icon type="plus-circle" theme="filled" /> Add
                      </Link> */}
                                </div>
                            </div>
                        </div>
                        {loader &&
                            <Skeleton height={50} count={10} />
                        }

                        {/* {!loader && */}
                            <Table
                                pagination={{ defaultPageSize: 15 }}
                                columns={columns}
                                dataSource={filterData}
                                className="ant-table-v1"
                            // scroll={{ x: 1200 }}
                            />
                            {/* } */}
                    </article>
                </section>
            </>
        );
    }
}    
const mapStateToProps = state =>({
    review : state.review.review
}) 

const mapDispatchToProps = dispatch => ({
    getReviewDetails : ()=> dispatch(getAllReviewDetail())
})


export default connect(mapStateToProps, mapDispatchToProps)(Review)

const SelectStore = styled('div')`
  .ant-select {
    min-width: 200px;
  }
`;