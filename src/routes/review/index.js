import React from 'react';
import { Route } from 'react-router-dom';
import Review from './routes';


const Shop = ({ match }) => (
  <>
  <Route exact path={`${match.url}`} component={Review}/>
  <Route path={`${match.url}/add`} component={Review}/>
 </>
);

export default Shop;
