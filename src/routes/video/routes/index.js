import React from 'react';
import Skeleton from 'react-loading-skeleton';
import { Table, Icon, Form, Input, Tooltip } from 'antd';
import { VideoModal } from '../../../components/Modal';
import { VideoCard } from '../../../components/Cards';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import getAllVidoDetail from '../../../middleware/videoMiddleware';
import Delete from './delete/index';
const FormItem = Form.Item;

class Video extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            search: '',
            store: [],
            listData: [],
            filterData: [],
        };
    }

    componentDidMount() {
        const { getVideoDetail, videoState } = this.props
        getVideoDetail()
        this.setState({
            filterData: videoState
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.videoState !== this.props.videoState) {
            this.setState({
                filterData: this.props.videoState,
                listData: this.props.videoState,
            })
        }
    }

    handleSearch = e => {
        const { listData } = this.state;
        const filterDataHolder = [
            ...listData.filter(
                ({ title }) =>
                    title.toLowerCase().startsWith(e.target.value.trim().toLowerCase())
            ),
        ];
        this.setState({
            [e.target.name]: e.target.value,
            filterData: filterDataHolder,
        });
    };

    render() {
        const { match, getVideoDetail, videoLoader } = this.props;
        const { listData, filterData, search } = this.state;
        const columns = [
            {
                title: 'Url',
                dataIndex: 'url',
                key: 'url'

            },
            {
                title: 'Title',
                dataIndex: 'title',
                key: 'key'
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => {
                    return (
                        <span className="d-flex">
                            <Tooltip placement="bottomLeft" title={"view"}>
                                <span>
                                    <VideoModal
                                        VideoName={text.title}
                                        VideoUrl={text.url} />
                                </span>
                            </Tooltip>
                            <Link
                                to={{ pathname: `${match.url}/edit`, state: text }}
                                className="ml-2 ant-btn ant-btn-primary ant-btn-circle"
                            >
                                <Icon type="edit" theme="filled" />
                            </Link>
                            <Tooltip placement="bottomLeft" title="delete">
                                <span>
                                    <Delete recordData={text} getVideoDetail={getVideoDetail} />
                                </span>
                            </Tooltip>
                        </span>
                    );
                },
                width: 166,
            },
        ];
        return (
            <>
                <VideoCard
                    total={listData.length}
                    active={listData.filter(({ status }) => status === 0).length}
                    inactive={listData.filter(({ status }) => status === 1).length}
                    title="Customers"
                />
                <section className="container-fluid  no-breadcrumb chapter">
                    <article className="article">
                        <div className="d-flex justify-content-between mb-2">
                            <div className="d-flex">
                                <FormItem>
                                    <Input
                                        placeholder="Search Video"
                                        prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                                        name="search"
                                        value={search}
                                        style={{ width: 275 }}
                                        onChange={this.handleSearch}
                                    />
                                </FormItem>
                            </div>

                            <div className="d-flex">
                                <div>
                                    <Link to={`${match.url}/add`} className="ant-btn ant-btn-primary">
                                        <Icon type="plus-circle" theme="filled" /> Add
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {videoLoader ?
                            <Skeleton height={50} count={10} /> :
                            <Table
                                pagination={{ defaultPageSize: 15 }}
                                columns={columns}
                                dataSource={filterData}
                                className="ant-table-v1"
                                loading={videoLoader}
                            />
                        }
                    </article>
                </section>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        videoState: state.video.Video,
        videoLoader: state.video.videoLoader
    }
}

const mapDispatchToProps = dispatch => ({
    getVideoDetail: () => dispatch(getAllVidoDetail())
})

export default connect(mapStateToProps, mapDispatchToProps)(Video)