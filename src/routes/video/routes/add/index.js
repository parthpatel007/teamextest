import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom';
import { Icon } from 'antd';
import Form from '../form/index';

 class AddVideoModal extends Component {
    render() {
        const { match } = this.props;
        const backLink = match.url
          .split('/')
          .slice(0, -1)
          .join('/');
        return (
            <>
           
         <section className="form-card h-100">
          <div className="form-card__body p-lg-5 p-4">
          <Link to={backLink} className="ant-btn ant-btn-primary">
            <Icon type="caret-left" /> Back
          </Link>
          <section>
            <div className="form-v1-container">
              <h2>Add New Video</h2>
            </div>
            <Form />
          </section>
        </div>
      </section>
      </>
        )
    }
}

export default withRouter(AddVideoModal)