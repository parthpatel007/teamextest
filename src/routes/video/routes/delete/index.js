import React from 'react';
import { Modal, Button, Icon } from 'antd';
import axios from 'axios';
import { backendUrl } from '../../../../common/credentials';
import { withToastManager } from 'react-toast-notifications';

class Delete extends React.Component {
  state = { visible: false };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  deleteRecord = id => {

    const { toastManager } = this.props

    const authToken = localStorage.getItem('access_token');
    const self = this
    axios.delete(`${backendUrl}api/video/${id}`, {
      headers: {
        Authorization: `Bearer ${authToken}`,
      }
    })
      .then(function (response) {
        self.setState({
          visible: false,
        });
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        })
        self.props.getVideoDetail()
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };

  render() {
    const { recordData } = this.props;
    const { title, _id } = recordData;

    return (
      <div>
        <Button type="danger" className="ml-2" shape="circle" onClick={this.showModal}>
          <Icon type="delete" theme="filled" />
        </Button>
        <Modal
          title="Delete Sub Category"
          okType="danger"
          visible={this.state.visible}
          onOk={() => this.deleteRecord(_id)}
          okText="Delete"
          onCancel={this.handleCancel}

          centered
        >

          <section className="form-card">
            <div className="form-card__body ">
              <label style={{ fontSize: 16 }}>
                Do you really want to delete video <b>{title}</b>
              </label>
            </div>
          </section>
        </Modal>
      </div>
    );
  }
}

export default withToastManager(Delete)



