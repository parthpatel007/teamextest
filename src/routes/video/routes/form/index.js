
import React from 'react';
import { Form, Input, Icon, Button, Spin } from 'antd';
import { Row, Col } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { backendUrl } from '../../../../common/credentials';
import cx from 'classnames';
import { createGlobalStyle } from 'styled-components';
import { connect } from 'react-redux';
import getAllVidoDetail from '../../../../middleware/videoMiddleware';
import { withToastManager } from 'react-toast-notifications';

const FormItem = Form.Item;
class FormComponent extends React.Component {
  state = {
    videoName:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.title
        : '',

    videoUrl:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.url
        : '',

    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };

  handleTagsChange = (state, value) => {
    this.clearError();
    this.setState({ [state]: value });
  };

  handleEditor = (state, content) => {
    this.clearError();
    this.setState({ [state]: content });
  };

  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };


  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };

  handleInput = e => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSelect = (name, value) => {
    this.clearError();
    this.setState({ [name]: value });
  };


  addNewApi = async () => {
    const { toastManager } = this.props
    const {
      videoName,
      videoUrl,
    } = this.state;
    const { match, history } = this.props;
    const authToken = localStorage.getItem('access_token');

    this.setState({ isLoading: true });

    const self = this;
    let bodydata = {
      title: videoName,
      url: videoUrl
    }


    axios
      .post(
        `${backendUrl}/api/video`,
        {
          ...bodydata
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        console.log('rrrr res', response);
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
        console.log('rrrr err', error);
      });


  };


  editApi = async () => {
    const { toastManager } = this.props
    const {
      videoName,
      videoUrl,

    } = this.state;
    const { match, history } = this.props;

    const { _id } = this.props.editData;

    const authToken = localStorage.getItem('access_token');

    this.setState({ isLoading: true });

    const self = this;

    axios
      .post(
        `${backendUrl}api/video/${_id}`,
        {

          title: videoName,
          url: videoUrl,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: '*/*',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        console.log('rrrr res', response);
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
        console.log('rrrr err', error);
      });


  };

  validateForm = () => {
    const {
      videoUrl,
      videoName,
    } = this.state;
    if (videoName === '') {
      this.setState({
        error: {
          errorText: 'Please select a video name',
          errorType: 'videoName',
        },
      });
      return false;
    }
    if (videoUrl === '') {
      this.setState({
        error: {
          errorText: 'Please select a video url',
          errorType: 'videoUrl',
        },
      });
      return false;
    }

    return true;
  };


  componentDidMount() {
    const { getVideoDetail } = this.props
    getVideoDetail()

  }
  handleAddData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.addNewApi();
    }
  };

  handleEditData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.editApi();
    }
  };

  render() {

    const {
      videoUrl,
      videoName,
      isLoading,
      apiError,
      error,
    } = this.state;
    const { type = 'add', editData, match, history } = this.props;
    const { errorType } = error;
    if (type === 'edit') {
      if (!editData) {
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        return null;
      }
    }

    return (
      <div className="form-container">

        <GlobalStyle />

        <Row form>
          <Col md={12}>
            <FormItem
              label="Video Title"
              hasFeedback
              className={cx({ 'has-error': errorType === 'videoName' })}
            >
              <Input
                maxLength={120}
                name={type === 'edit' ? 'videoName' : 'videoName'}
                value={type === 'edit' ? videoName : videoName}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'videoName' : 'videoName')}
            </FormItem>

          </Col>
        </Row>

        <Row form>
          <Col md={12}>
            <FormItem
              label="Video Url"
              hasFeedback
              className={cx({ 'has-error': errorType === 'videoUrl' })}
            >
              <Input

                name={type === 'edit' ? 'videoUrl' : 'videoUrl'}
                value={type === 'edit' ? videoUrl : videoUrl}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'videoUrl' : 'videoUrl')}
            </FormItem>
          </Col>
        </Row>
        {isLoading && (
          <div className="mb-2 text-center">
            <Spin />
          </div>
        )}


        <div className="text-right pt-3">
          {type === 'add' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              onClick={this.handleAddData}
            >
              <Icon type="plus-circle" theme="filled" />
              Save
            </Button>
          )}
          {type === 'edit' && (
            <Button
              type="primary"
              htmlType="submit"
              onClick={this.handleEditData}
              className="btn-cta"
            >
              <Icon type="edit" theme="filled" /> Save
            </Button>
          )}
        </div>


        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}


      </div>
    );
  }
}

const mapStateToProps = state => ({
  video: state.video.video
})

const mapDispatchToProps = dispatch => ({
  getVideoDetail: () => dispatch(getAllVidoDetail()),
})

const WrappedFormComponent = Form.create()(withRouter(FormComponent));

export default withToastManager(connect(mapStateToProps, mapDispatchToProps)(WrappedFormComponent));

const GlobalStyle = createGlobalStyle`.ant-select-dropdown--multiple{display:none}`;
