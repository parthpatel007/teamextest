import React from 'react';
import { Form, Input, Icon, Button, } from 'antd';
import cx from 'classnames';
import { Row, Col } from 'reactstrap';
import avatarImage from '../../../../assets/images/avatar.jpg';
import { withRouter, Link } from 'react-router-dom';
import axios from 'axios';
import { backendUrl } from '../../../../common/credentials';

/* eslint-disable */


const FormItem = Form.Item;

const profileData = {
  image: avatarImage,
  firstName: '',
  lastName: '',
  email: '',
  phone: '',
  address: '',
  phoneEdit: false,
  userName: '',
  address2: '',
  latitude: '',
  longitude: '',
  city: '',
  personState: '',
  postal_code: '',
  country: '',
};
class Admin extends React.Component {
  state = {
    confirmDirty: false,
    editMode: false,
    error: {
      errorText: '',
      errorType: ''
    },
    ...profileData,
  };
  // handleSubmit = e => {
  //   e.preventDefault();
  //   this.props.form.validateFieldsAndScroll((err, values) => {
  //     if (!err) {
  //       console.log('Received values of form: ', values);
  //       this.props.history.push(DEMO.home2);
  //     }
  //   });
  // };

  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };


  validateForm = () => {
    const { firstName, lastName } = this.state;
    if (firstName == '') {
      this.setState({
        error: {
          errorText: 'Please Enter A First name',
          errorType: 'firstName',
        },
      });
      return false;
    }
    if (lastName == '') {
      this.setState({
        error: {
          errorText: 'Please Enter A Last name',
          errorType: 'lastName',
        },
      });
      return false;
    }
    return true;
  };


  async componentDidMount() {
    const authToken = localStorage.getItem('access_token')

    return await axios.get(`${backendUrl}api/profile`, {
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    }).then((response) => {
      console.log(" CUSTOMIZD ITEMS FROM MIDDDEL : ", response.data.data)
      this.setState({
        firstName: response.data.data.email,
        lastName: '',
        email: response.data.data.email,
        phone: response.data.data.phone,
        // phoneEdit: ((response && response.data && response.data.data && response.data.data.phone && response.data.data.phone.length > 0)) || (!response.data.data.phone.length ) ? false : true,
        phoneEdit: (!response.data.data.phone ) ? false : true,
        firstName: response.data.data.firstName,
        lastName: response.data.data.lastName,
        userName: response.data.data.userName
      })
    })
      .catch((error) => {
        console.log(error);
      })
  }

  componentDidUpdate(prevProps, prevState){
    if(prevState !== this.state){
      console.log(this.state, "state state")
    }
  }

  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };
  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('signup1-password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  handleSubmit = () => {

    const { email, firstName, lastName, phone } = this.state;

    console.log(this.state,"this.state this.statethis.statethis.state this.state this.state this.state")

    if (this.validateForm()) {

      const authToken = localStorage.getItem('access_token');

      axios.post(`${backendUrl}api/profile`,
        {
          firstName: firstName,
          lastName: lastName,
          userName: this.state.userName
        },
        {
          headers: {
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
        .then((response) => {
          // console.log('rrrr res', response.data.data);
          const { editMode } = this.state;
          this.setState({ editMode: !editMode });
        })
        .catch(function (error) {
          // self.setState({
          //   visible: false,
          // });
          // ErrorNotify(error.response)
          console.log('rrrr err', error);
        });
    }

  }

  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  handleInput = e => {
    console.log("inpuy change e.target.name e.target.name",e.target.name)
    this.setState({ [e.target.name]: e.target.value });
  };
  toggleEdit = () => {
    const { editMode } = this.state;
    this.setState({ editMode: !editMode });
  };

  onCancel = () => {
    this.setState({
      image: profileData.image,
      name: profileData.name,
      email: profileData.email,
      phone: profileData.phone,
    });
  };
  render() {
    const { match } = this.props;
    const { email, image, firstName, lastName, phone, editMode, address, city, country, confirmDirty, latitude, longitude, personState, postal_code, address2, error } = this.state;
    const { errorType } = error;

    return (
      <section className="form-v1-container">
        <div className="mx-auto" style={{ maxWidth: 500 }}>
          {/* <div className="mb-3 text-center">
            <img src={image} className={classes.profile} alt={firstName} />
          </div> */}

          {/* <center>
            <ChangePic />
          </center> */}
          <Row form>
            <Col md={6}>
              <FormItem label="First Name"
              className={cx({ 'has-error': errorType === 'firstName' })}>
                <Input
                  name="firstName"
                  value={firstName}
                  disabled={!editMode}
                  prefix={<Icon type="user" style={{ fontSize: 13 }} />}
                  onChange={e => this.handleInput(e)}
                />
                {this.errorShow('firstName')}
              </FormItem>
            </Col>
            <Col md={6}>
              <FormItem label="Last Name" 
              className={cx({ 'has-error': errorType === 'lastName' })}>
                <Input
                  name="lastName"
                  value={lastName}
                  disabled={!editMode}
                  prefix={<Icon type="user" style={{ fontSize: 13 }} />}
                  onChange={e => this.handleInput(e)}
                />
                {this.errorShow('lastName')}
              </FormItem>
            </Col>
          </Row>
          <Row form>
            <Col md={12}>
              <FormItem label="Email" hasFeedback>
                <Input
                  name="email"
                  value={email}
                  disabled={true}
                  prefix={<Icon type="mail" style={{ fontSize: 13 }} />}
                  type="email"
                  onChange={e => this.handleInput(e)}
                />
              </FormItem>
            </Col>
            {/* <Col md={6}>
              <FormItem label="Phone" hasFeedback>
                <Input
                  name="phone"
                  disabled={!editMode}
                  value={phone}
                  prefix={<Icon type="phone" style={{ fontSize: 13 }} />}
                  type="number"
                  onChange={e => this.handleInput(e)}
                />
              </FormItem>
            </Col>
            {this.state.phoneEdit && <Col md={6}>
              <FormItem label="Phone" hasFeedback>
                <Input
                  name="phone"
                  disabled={true}
                  value={phone}
                  prefix={<Icon type="phone" style={{ fontSize: 13 }} />}
                  type="number"
                  onChange={e => this.handleInput(e)}
                />
              </FormItem>
            </Col>} */}
          </Row>
          {/* <Row form>
            <Col md={12}>
              <FormItem label="Address" hasFeedback>
                <TextArea
                  autoSize={{ minRows: 3, maxRows: 5 }}
                  name="address"
                  value={address}
                  disabled={!editMode}
                  onChange={e => this.handleInput(e)}
                />
              </FormItem>
            </Col>
          </Row> */}
          {/* <FormItem label="Address / Location" hasFeedback>
            <Input
              disabled={!editMode}
              name="address2"
              value={address2}
              placeholder="Address 2"
              onChange={this.handleInput}
            />
          </FormItem> */}
          {/* <Row form>
            <Col md={6}>
              <FormItem
                label="City"
                hasFeedback
              >
                <Input
                  disabled={!editMode}
                  name="city"
                  value={city}
                  prefix={<i className="fas fa-city" />}
                  onChange={this.handleInput}
                />
              </FormItem>
            </Col>
            <Col md={6}>
              <FormItem
                label="State"
                hasFeedback
              >
                <Input
                  disabled={!editMode}
                  name="personState"
                  prefix={<i class="fas fa-map-marker-alt"></i>}
                  value={personState}
                  onChange={this.handleInput}
                />
              </FormItem>
            </Col>
            <Col md={6}>
              <FormItem
                label="Postal Code"
                hasFeedback
              >
                <Input
                  disabled={!editMode}
                  name="postal_code"
                  prefix={<i className="fas fa-thumbtack"></i>}
                  value={postal_code}
                  onChange={this.handleInput}
                />
              </FormItem>
            </Col>
            <Col md={6}>
              <FormItem
                label="country"
                hasFeedback
              >
                <Input
                  disabled={!editMode}
                  name="country"
                  prefix={<i className="far fa-flag"></i>}
                  value={country}
                  onChange={this.handleInput}
                />
              </FormItem>
            </Col>
          </Row> */}

          <div className="text-center pt-3">
            {editMode ? (
              <>
                <Button type="primary" htmlType="submit" className="btn-cta mr-2" onClick={this.handleSubmit}>
                  Save
                </Button>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="btn-cta"
                  onClick={() => {
                    this.onCancel();
                    this.toggleEdit();
                  }}
                >
                  Cancel
                </Button>
              </>
            ) : (
                <>
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="btn-cta mr-2"
                    onClick={this.toggleEdit}
                  >
                    Edit
                </Button>
                  {/* <ChangePassword /> */}
                  <Link
                    style={{ textDecoration: 'none' }}
                    to={`${match.url}/change-password`}
                    className="ant-btn btn-cta ant-btn-primary text-white"
                  >
                    Change Password
                </Link> 
                </>
              )}
          </div>
        </div>
      </section>
    );
  }
}

const WrappedAdmin = Form.create()(withRouter(Admin));

export default WrappedAdmin;
