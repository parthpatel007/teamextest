import React from 'react';
import { Form, Icon, Input, Button, Checkbox, Spin } from 'antd';
import { withRouter } from 'react-router-dom';
import APPCONFIG from 'constants/appConfig';
import cx from 'classnames';
import DEMO from 'constants/demoData';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import { backendUrl } from '../../../../../common/credentials';
import isLoggedIn from '../../../../../common/isLoggedIn';
import logo from '../../../../../assets/_logo.svg';
import { withToastManager } from 'react-toast-notifications';

const FormItem = Form.Item;
const validateEmail = email => {
  var re = /^(([^<>()\]\\.,;:\s@“]+(\.[^<>()\]\\.,;:\s@“]+)*)|(“.+“))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const validatePassword = password => {
  if (password.length < 5 || password.length > 15) {
    return false
  }
  return true
}

class NormalLoginForm extends React.Component {
  state = {
    email: '',
    password: '',
    isLoading: false,
    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };

  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };

  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };

  handleInput = e => {
    this.clearError();
    const { email } = this.state;
    this.setState({ [e.target.name]: e.target.value });

    if (e.target.name === 'password') {
      if (email === '') {
        this.setState({
          error: {
            errorText: 'Please Enter Email',
            errorType: 'email',
          },
        });
      } else if (!validateEmail(email)) {
        this.setState({
          error: {
            errorText: 'Please Enter A Valid Email',
            errorType: 'email',
          },
        });
      }
    }
  };

  loginApi = () => {
    const { email, password } = this.state;
    const { toastManager } = this.props
   
    if (email === '') {
      this.setState({
        error: {
          errorText: 'Please Enter Email',
          errorType: 'email',
        },
      });
      return;
    }
    if (!validateEmail(email)) {
      this.setState({
        error: {
          errorText: 'Please Enter A Valid Email',
          errorType: 'email',
        },
      });
      return;
    }

    if (password === '') {
      this.setState({
        error: {
          errorText: 'Please Enter Password',
          errorType: 'password',
        },
      });
      return;
    }
    if (!validatePassword(password)) {
      this.setState({
        error: {
          errorText: 'Enter password between 5 to 15 character',
          errorType: 'password',
        },
      });
      return;
    }

    this.setState({ isLoading: true });
    const self = this;
    axios
      .post(`${backendUrl}/api/login`, {
        username: email,
        password: password,
        role : 'ADMIN'
      })
      .then(function (response) {
        // localStorage.clear();
        // console.log(response, "login response");
        toastManager.add(
          response.data.message, {
          appearance: "success",
          autoDismiss: true
        });
        localStorage.setItem('isLoggedIn', true);
        localStorage.setItem('access_token', response.data.data.accessToken);

        self.setState({ isLoading: false });

      })
      .catch(function (error) {
       
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
        localStorage.clear();
        localStorage.setItem('isLoggedIn', false);
        // console.log('rrrr err', error);
        toastManager.add(
          error  && error.response && error.response.data &&  error.response.data.message, {
            appearance: "warning",
            autoDismiss: true
          });
      });
  };

  handleSubmit = () => {
    this.loginApi();
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { email, password, isLoading, apiError, error } = this.state;
    const { errorType} = error;
    if (isLoggedIn()) {
      return <Redirect to={'/app/dashboard'} />;
    }
    return (
      <section className="form-v1-container">
        <center>
        <img className={'mb-3'} src={logo} alt="" style={{ height: 130, width: 'auto' }} />
        </center>
        <h2>Login to Continue</h2>
        <p className="lead">Welcome back, sign in with your {APPCONFIG.brand} account</p>
        <Form onSubmit={this.handleSubmit} className="form-v1">
        <FormItem>
          <Input
            className={cx({ 'has-error': errorType === 'email' })}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     type="email"
            id="hasEmailIconPad"
            size="large"
            // style={{paddingLeft: '10px'}}
            name="email"
            value={email}
            onChange={this.handleInput}
            prefix={<Icon type="mail" style={{ fontSize: 18, marginRight: '20px' }} />}
            placeholder="Email"
          />
          {this.errorShow('email')}
        </FormItem>
        <FormItem>
          <Input.Password
            className={cx({ 'has-error': errorType === 'password' })}
            id="hasPasswordIconPad"
            iconRender={visible => (visible ? <Icon type="EyeTwoTone" style={{ fontSize: 20 }} /> :<Icon type="EyeInvisibleOutlined " style={{ fontSize: 20 }} />)}
            size="large"
            prefix={<Icon type="lock" style={{ fontSize: 18 }} />}
            maxLength={15}
            name="password"
            value={password}
            onChange={this.handleInput}
            type="password"
            placeholder="Password"
          />
          {this.errorShow('password')}
        </FormItem>
        <FormItem className="form-v1__remember">
          {getFieldDecorator('login1-remember', {
            valuePropName: 'checked',
            initialValue: true,
          })(<Checkbox>Remember me</Checkbox>)}
        </FormItem>
        {isLoading && (
          <div className="mb-2 text-center">
            <Spin />
          </div>
        )}
        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            className="btn-cta btn-block"
            onClick={this.handleSubmit}
            disabled={isLoading}
          >
            Log in
          </Button>
        </FormItem>
        </Form>
        {/* <p className="additional-info">
          Don't have an account yet? <a href={DEMO.signUp}>Sign up</a>
        </p> */}
        {apiError && (
          <div className="callout callout-info">
            <p>{apiError}</p>
          </div>
        )}
        <p className="additional-danger">
          Forgot your password? <a href={DEMO.forgotPassword}>Reset password</a>
        </p>
      </section>
    );
  }
}

const WrappedNormalLoginForm = Form.create()(withRouter(NormalLoginForm));

export default withToastManager(WrappedNormalLoginForm);
