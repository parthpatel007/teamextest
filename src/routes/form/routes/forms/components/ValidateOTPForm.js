import logo from '../../../../../assets/_logo.svg';
import React from 'react';
import { Form, Icon, notification, Input, Button, } from 'antd';
import { Spin } from 'antd';
import { withRouter } from "react-router-dom";
import axios from 'axios';
import { backendUrl } from '../../../../../common/credentials';
import { Redirect } from 'react-router-dom';
import isLoggedIn from '../../../../../common/isLoggedIn';
const FormItem = Form.Item;

class NormalValidateForm extends React.Component {

  state = {
    apiError: '',
    isLoading: false
  }


  handleSubmit = (e) => {

    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        // console.log('Received values of form: ', values);
        const forgotOTP = values.resetOTP;
        this.verifyOTP(forgotOTP);
        // console.log(forgotOTP);
        // this.props.history.push(DEMO.home2);
      }
    });
  }

  verifyOTP(requestedOTP) {

    const { match, history } = this.props;

    const forgotEmail = localStorage.getItem('forgotEmail');

    this.setState({ isLoading: true });

    axios.post(`${backendUrl}api/verifyotp`, {
      otp: requestedOTP,
      email: forgotEmail
    })
      .then((response) => {

        if (response.data.data.statusCode === 200 && response.data.message === "Success") {

          //Redirect to Validate otp form 
          const ValidateOtpLink = match.url
            .split('/')
            .slice(0, -1)
            .join('/');
          history.push(ValidateOtpLink + '/create-password');

        }

        // console.log(response);
      })
      .catch((error) => {

        this.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => this.setState({ apiError: '' }), 3000);
        // localStorage.clear();
        // localStorage.setItem('isLoggedIn', false);
        console.log(error);

      });

  }



  resendOTP(e) {
    e.preventDefault();
    const forgotEmail = localStorage.getItem('forgotEmail');
    this.setState({ isLoading: true });
    axios.post(`${backendUrl}api/requestotp`, {
      email: forgotEmail
    })
      .then((response) => {
        console.log(response);
        notification['success']({
          message: 'OTP Sent Successfully',
        });
        this.setState({ isLoading: false });
      })
      .catch((error) => {
        this.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error') ? 'Network Error' : error.response.data.message,
        });
        setTimeout(() => this.setState({ apiError: '' }), 3000);
      });
  }

  render() {
    const { isLoading, apiError } = this.state;
    const { getFieldDecorator } = this.props.form;
    if (isLoggedIn()) {
      return <Redirect to={'/app/dashboard'} />;
    }

    return (
      <section className="form-v1-container">
        <center>
          <a href={'/'}>
            <img className={'mb-5'} src={logo} alt="" style={{ height: 170, width: 'auto' }} />
          </a>
        </center>
        <h2>Validate OTP</h2>
        <p className="additional-info col-lg-10 mx-lg-auto mb-3">Enter the otp it will help you to change your password.</p>
        <Form onSubmit={this.handleSubmit} className="form-v1">
          <FormItem className="mb-3">
            {getFieldDecorator('resetOTP', {
              rules: [
                { required: true, message: 'Please input your OTP!' },
                { min: 5, message: 'Please enter valid 6 digit otp' }
              ],
            })(
              <Input id="validateOtp" type={'number'} maxLength={6} min={0} className={'w-100'} size="large" prefix={<Icon type="lock" style={{ fontSize: 20 }} />} placeholder="OTP" onInput={(e) => e.target.value = e.target.value.slice(0, 6)} />
            )}
          </FormItem>
          {isLoading && (
            <div className="mb-2 text-center">
              <Spin />
            </div>
          )}
          <FormItem>
            <Button type="primary" htmlType="submit" className="btn-cta btn-block">
              Check OTP
            </Button>
          </FormItem>
          <p className="additional-danger">
            {/* eslint-disable-next-line */}
            <a
              onClick={(e) => this.resendOTP(e)}
            >
              Resend OTP
            </a>
          </p>
          <p className="additional-danger">
            Click here to <a href={'#/login'}>Login</a>
          </p>

          {apiError && (
            <div className="callout callout-info">
              <p>{apiError}</p>
            </div>
          )}

        </Form>
      </section>
    );
  }
}

const WrappedValidateOtpForm = Form.create()(withRouter(NormalValidateForm));
export default WrappedValidateOtpForm;