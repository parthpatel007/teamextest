import React from 'react';
import { Form, Icon, Input, Button, Spin, notification } from 'antd';
import { withRouter } from "react-router-dom";
import axios from 'axios';
import { backendUrl } from '../../../../../common/credentials';
import logo from '../../../../../assets/_logo.svg';
import { withToastManager } from 'react-toast-notifications';


const FormItem = Form.Item;
class NormalForm extends React.Component {
  state = {
    apiError: '',
    isLoading: false
  }


  handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values.resetPassword1Email);
        const forgotEmail = values.resetPassword1Email;
        this.forgotPasswordApi(forgotEmail);
        // this.props.history.push(DEMO.home2);
      }
    });
  }

  forgotPasswordApi(requestedEmail) {

    const { match, history } = this.props;
    const {toastManager} = this.props
    this.setState({ isLoading: true });

    axios.post(`${backendUrl}/api/requestotp`, {
      email: requestedEmail,
      role : 'ADMIN'
    })
      .then((response) => {
        console.log(response);
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        localStorage.setItem('forgotEmail', requestedEmail);

        //Redirect to Validate otp form 
        const ValidateOtpLink = match.url
          .split('/')
          .slice(0, -1)
          .join('/');
        notification['success']({
          message: 'OTP Sent Successfully',
        });
        history.push(ValidateOtpLink + '/validate-otp');

      })
      .catch((error) => {
        this.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error') ? 'Network Error' : error.response.data.message,
        });
        setTimeout(() => this.setState({ apiError: '' }), 3000);
          toastManager.add(
          error  && error.response && error.response.data &&  error.response.data.message, {
            appearance: "warning",
            autoDismiss: true
          });
       });

  }

  render() {

    const { isLoading, apiError } = this.state;

    const { getFieldDecorator } = this.props.form;

    return (
      <section className="form-v1-container">
        <center>
          <a href={'/'}>
            <img className={'mb-5'} src={logo} alt="" style={{ height: 170, width: 'auto' }} />
          </a>
        </center>
        <h2>Forgot Password?</h2>
        <p className="additional-info col-lg-10 mx-lg-auto mb-3">Enter the email address you used when you joined and we’ll send you instructions to reset your password.</p>
        <Form onSubmit={this.handleSubmit} className="form-v1">
          <FormItem className="mb-3">
            {getFieldDecorator('resetPassword1Email', {
              rules: [
                { type: 'email', message: 'The input is not valid E-mail!' },
                { required: true, message: 'Please input your email!' }
              ],
            })(
              <Input size="large" prefix={<Icon type="mail" style={{ fontSize: 20 }} />} placeholder="Email" />
            )}
          </FormItem>


          {isLoading && (
            <div className="mb-2 text-center">
              <Spin />
            </div>
          )}


          <FormItem>
            <Button type="primary" htmlType="submit" className="btn-cta btn-block">
              Send Reset Instructions
            </Button>
          </FormItem>

          <p className="additional-danger">
            Click here to <a href={'#/login'}>Login</a>
          </p>


          {apiError && (
            <div className="callout callout-info">
              <p>{apiError}</p>
            </div>
          )}
        </Form>
      </section>
    );
  }
}

const WrappedNormalForm = Form.create()(withRouter(NormalForm));
export default withToastManager(WrappedNormalForm);
