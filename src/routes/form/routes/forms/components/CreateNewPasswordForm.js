import React from 'react';
import { Form, Icon, Input, Button} from 'antd';
import { withRouter } from "react-router-dom";
import cx from 'classnames';
import axios from 'axios';
import { backendUrl } from '../../../../../common/credentials';
import { Redirect } from 'react-router-dom';
import isLoggedIn from '../../../../../common/isLoggedIn';
import logo from '../../../../../assets/_logo.svg';
import { withToastManager } from 'react-toast-notifications';
const FormItem = Form.Item;

const validatePassword = password => {
  if(password.length < 5 || password.length > 16 ){
    return false
  }
  return true
}


class NormalValidateForm extends React.Component {

state = {
    confirmDirty: false,
    new_password: '',
    confirmPassword: '',
    isLoading: false,
    apiError: '',
    apiSuccess: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };



  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };



  clearInputs = () => {
    this.setState({ new_password: '', confirmPassword: '' });
  };


  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };


  createNewPasswordApi(requestedOTP){

   

    const { confirmPassword } = this.state;
    const { toastManager} = this.props;
    const forgotEmail = localStorage.getItem('forgotEmail');

    this.setState({ isLoading: true });

    axios.post(`${backendUrl}api/forgotpassword`,{
        email: forgotEmail,
        password: confirmPassword
      })
      .then((response) => {
          
        if (response.data.data.statusCode === 200)
        {
          localStorage.setItem('isLoggedIn', true);
          localStorage.setItem('access_token', response.data.data.accessToken);
          window.location.reload();

        }

 
        // console.log(response);
      })
      .catch((error) => {

        this.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => this.setState({ apiError: '' }), 3000);
        // localStorage.clear();
        // localStorage.setItem('isLoggedIn', false);
        console.log(error);
        toastManager.add(
          error  && error.response && error.response.data &&  error.response.data.message, {
            appearance: "warning",
            autoDismiss: true
          });
      }); 

  }



  validateForm = () => {
    const { new_password, confirmPassword } = this.state;
    if (new_password === '') {
      this.setState({
        error: {
          errorText: 'Please Enter New Password',
          errorType: 'new_password',
        },
      });
      return false;
    }
    if (!validatePassword(new_password)) {
      this.setState({
        error: {
          errorText: 'Enter password between 5 to 16 character',
          errorType: 'new_password',
        },
      });
      return false;
    }
    if (confirmPassword === '') {
      this.setState({
        error: {
          errorText: 'Please Enter Confirm Password',
          errorType: 'confirmPassword',
        },
      });
      return false;
    }
    if (!validatePassword(confirmPassword)) {
      this.setState({
        error: {
          errorText: 'Enter password between 5 to 16 character',
          errorType: 'confirmPassword',
        },
      });
      return false;
    }
    if (confirmPassword !== new_password) {
      this.setState({
        error: {
          errorText: 'Password Not Match',
          errorType: 'confirmPassword',
        },
      });
      return false;
    }
    return true;
  };


  handleSubmit = (e) => {

    e.preventDefault();

    if (this.validateForm()) {
        this.clearError();
        this.createNewPasswordApi();
    }
  };


  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };



  render() {

    const {
        new_password,
        confirmPassword,
        error,
       
      } = this.state;

    const { errorType } = error;

  

    if (isLoggedIn()) {
        return <Redirect to={'/app/dashboard'} />;
    }

    return (
      <section className="form-v1-container">

<center>
          <a href={'/'}>
            <img className={'mb-5'} src={logo} alt="" style={{ height: 170, width: 'auto' }} />
          </a>
        </center>

        <h2>Change Password</h2>
        {/* <p className="additional-info col-lg-10 mx-lg-auto mb-3">Enter the new Password.</p> */}
        <Form onSubmit={this.handleSubmit} className="form-v1">
          {/* <FormItem 
          className="mb-3">
            {getFieldDecorator('newPass', {
              rules: [
                { required: true, message: 'Please input your New Password!' }
              ],
            })(
              <Input 
              size="large" prefix={<Icon type="lock" style={{ fontSize: 13 }} />} placeholder="New Password" />
            )}
          </FormItem>
          <FormItem className="mb-3">
            {getFieldDecorator('confirmNewPass', {
              rules: [
                { required: true, message: 'Please input your New Confirm Password!' }
              ],
            })(
              <Input size="large" prefix={<Icon type="lock" style={{ fontSize: 13 }} />} placeholder="Confirm New Password" />
            )}
          </FormItem> */}



          <FormItem
              label="New Password"
              hasFeedback
              className={cx({ 'has-error': errorType === 'new_password' })}
            >
              <Input.Password
                name="new_password"
                maxLength={16}
                id="password"
                type="password"
                prefix={<Icon type="lock" style={{ fontSize: 20 }} />}
                iconRender={visible => (visible ? <Icon type="EyeTwoTone" style={{ fontSize: 20 }} /> :<Icon type="EyeInvisibleOutlined " style={{ fontSize: 20 }} />)}
                value={new_password}
                onChange={e => this.handleInput(e)}
              />
              {this.errorShow('new_password')}
            </FormItem>
    
            <FormItem
              label="Confirm Password"
              hasFeedback
              className={cx({ 'has-error': errorType === 'confirmPassword' })}
            >
              <Input.Password
                name="confirmPassword"
                type="password"
                id="cnpassword"
                maxLength={16}
                prefix={<Icon type="lock" style={{ fontSize: 20 }} />}
                iconRender={visible => (visible ? <Icon type="EyeTwoTone" style={{ fontSize: 20 }} /> :<Icon type="EyeInvisibleOutlined " style={{ fontSize: 20 }} />)}
                value={confirmPassword}
                onChange={e => this.handleInput(e)}
              />
              {this.errorShow('confirmPassword')}
            </FormItem>




          <FormItem>
            <Button type="primary" htmlType="submit" className="btn-cta btn-block">
              Change Password
            </Button>
          </FormItem>
          <p className="additional-danger">
          Click here to <a href={'#/login'}>Login</a>
          </p>
        </Form>
      </section>
    );
  }
}

const WrappedValidateOtpForm = Form.create()(withRouter(NormalValidateForm));


export default withToastManager(WrappedValidateOtpForm);
