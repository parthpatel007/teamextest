import React from 'react';
import { Modal, Button, Icon } from 'antd';
import { createGlobalStyle } from 'styled-components';
import axios from 'axios';
import { backendUrl } from '../../../../common/credentials';
import { withToastManager } from 'react-toast-notifications';

class Delete extends React.Component {
  state = { visible: false };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = e => {
    this.setState({
      visible: false,
    });
  };
  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };

  deleteRecord = id => {
    const { toastManager } = this.props
    const authToken = localStorage.getItem('access_token');
    var self = this;
    axios.delete(`${backendUrl}/api/franchise/${id}`, {
      headers: {
        Authorization: `Bearer ${authToken}`,
      }
    })
      .then(function (response) {
        self.setState({
          visible: false,
        });
        toastManager.add(
          response.data && response.data.data && response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        })
        self.props.getFranchiseDetails()
      })
      .catch(function (error) {
        console.log('rrrr err', error);
        toastManager.add(
          error.data && error.data.data && error.data.data.data && error.data.data.data.customMessage, {
          appearance: "error",
          autoDismiss: true
        })
      });
  };

  render() {
    const { recordData } = this.props;
    const { franchiseName, _id } = recordData;
    return (
      <div>
        <Button type="danger" className="ml-2" shape="circle" onClick={this.showModal}>
          <Icon type="delete" theme="filled" />
        </Button>
        <Modal
          title="Delete Sub Category"
          okType="danger"
          visible={this.state.visible}
          onOk={() => this.deleteRecord(_id)}
          okText="Delete"
          onCancel={this.handleCancel}
          // footer={null}
          // className="custom-modal-v1"
          centered
        >
          {/* <GlobalStyle /> */}
          <section className="form-card">
            <div className="form-card__body ">
              <label style={{ fontSize: 16 }}>
                Do you really want to delete franchise <b>{franchiseName}</b>?
              </label>
            </div>
          </section>
        </Modal>
      </div>
    );
  }
}

export default withToastManager(Delete)