
import React from 'react';
import { Form, Input, Icon, Button, Spin } from 'antd';
import { Row, Col } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { backendUrl } from '../../../../common/credentials'
import cx from 'classnames';
import { createGlobalStyle } from 'styled-components';
import { connect } from 'react-redux';
import getAllFranchiseDetail from '../../../../middleware/franchiseMiddleware';
import { withToastManager } from 'react-toast-notifications';


const FormItem = Form.Item;
const validateEmail = email => {
  var re = /^(([^<>()\]\\.,;:\s@“]+(\.[^<>()\]\\.,;:\s@“]+)*)|(“.+“))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const validateState = email => {
  var re = /^[a-zA-Z_ ]+$/;
  return re.test(String(email).toLowerCase());
};

class FormComponent extends React.Component {
  state = {
    franchiseName:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.franchiseName
        : '',
    franchiseNumber:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.franchiseNumber
        : '',
    address:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.address
        : '',
    city:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.city
        : '',
    pincode:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.pinCode
        : '',
    district:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.district
        : '',
    state:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.state
        : '',
    contact:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.contactNo
        : '',
    email:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.email
        : '',

    isLoading: false,
    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };

  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };

  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };
  handleInput = e => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };

  addNewApi = async () => {
    const { toastManager } = this.props
    const {
      franchiseNumber,
      franchiseName,
      address,
      city,
      pincode,
      district,
      state,
      contact,
      email,
    } = this.state;
    const { match, history } = this.props;
    const authToken = localStorage.getItem('access_token');
    this.setState({ isLoading: true });
    const self = this;
    axios
      .post(
        `${backendUrl}/api/franchise`,
        {
          franchiseNumber: franchiseNumber,
          franchiseName: franchiseName,
          address: address,
          city: city,
          pinCode: pincode,
          district: district,
          state: state,
          contactNo: contact,
          email: email,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        console.log('rrrr res', response);
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
        console.log('rrrr err', error);
      });
  };

  editApi = async () => {
    const { toastManager } = this.props
    const {
      franchiseNumber,
      franchiseName,
      address,
      city,
      pincode,
      district,
      state,
      contact,
      email,
    } = this.state;

    const { match, history } = this.props;
    const { _id } = this.props.editData;
    const authToken = localStorage.getItem('access_token');
    this.setState({ isLoading: true });
    const self = this;
    axios
      .post(
        `${backendUrl}/api/franchise/${_id}`,
        {
          franchiseNumber: franchiseNumber,
          franchiseName: franchiseName,
          address: address,
          city: city,
          pinCode: pincode,
          district: district,
          state: state,
          contactNo: contact,
          email: email,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: '*/*',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        console.log('rrrr res', response);
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
        console.log('rrrr err', error);
      });


  };

  validateForm = () => {
    const {
      franchiseNumber,
      franchiseName,
      address,
      city,
      pincode,
      district,
      state,
      contact,
      email
    } = this.state;
    if (franchiseNumber === '') {
      this.setState({
        error: {
          errorText: 'Please Enter a Franchise Number',
          errorType: 'franchiseNumber',
        },
      });
      return false;
    }

    if (franchiseName === '') {
      this.setState({
        error: {
          errorText: 'Please Enter a Franchise Name',
          errorType: 'franchiseName',
        },
      });
      return false;
    }

    if (address === '') {
      this.setState({
        error: {
          errorText: 'Please Enter a Address',
          errorType: 'address',
        },
      });
      return false;
    }

    if (city === '') {
      this.setState({
        error: {
          errorText: 'Please Enter a City',
          errorType: 'city',
        },
      });
      return false;
    }
    else if (!validateState(city)) {
      this.setState({
        error: {
          errorText: 'Please Enter A Valid City',
          errorType: 'city',
        },
      });
      return false;
    }

    if (district === '') {
      this.setState({
        error: {
          errorText: 'Please Enter a District',
          errorType: 'district',
        },
      });
      return false;
    }
    else if (!validateState(district)) {
      this.setState({
        error: {
          errorText: 'Please Enter A Valid District',
          errorType: 'district',
        },
      });
      return false;
    }

    if (pincode === '') {
      this.setState({
        error: {
          errorText: 'Please Enter a Pincode',
          errorType: 'pincode',
        },
      });
      return false;
    }

    if (state === '') {
      this.setState({
        error: {
          errorText: 'Please Enter a State',
          errorType: 'state',
        },
      });
      return false;
    }
    else if (!validateState(state)) {
      this.setState({
        error: {
          errorText: 'Please Enter A Valid State',
          errorType: 'state',
        },
      });
      return false;
    }

    if (email === '') {
      this.setState({
        error: {
          errorText: 'Please Enter Email',
          errorType: 'email',
        },
      });
    } else if (!validateEmail(email)) {
      this.setState({
        error: {
          errorText: 'Please Enter A Valid Email',
          errorType: 'email',
        },
      });
      return false;
    }

    if (contact === '') {
      this.setState({
        error: {
          errorText: 'Please Enter a Contact',
          errorType: 'contact',
        },
      });
      return false;
    }
    return true;
  };

  componentDidMount() {
    const { getFranchiseDetail } = this.props;
    getFranchiseDetail()
  }
  handleAddData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.addNewApi();
    }
  };

  handleEditData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.editApi();
    }
  };

  render() {
    const {
      franchiseNumber,
      franchiseName,
      address,
      city,
      pincode,
      district,
      state,
      contact,
      email,
      isLoading,
      apiError,
      error,
    } = this.state;
    const { type = 'add', editData, match, history } = this.props;
    const { errorType } = error;
    if (type === 'edit') {
      if (!editData) {
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        return null;
      }
    }
    return (
      <div className="form-container">
        <GlobalStyle />
        <Row form>
          <Col md={6}>
            <FormItem
              label="Franchise Number"
              hasFeedback
              className={cx({ 'has-error': errorType === 'franchiseNumber' })}
            >
              <Input
                maxLength={10}
                type="number"
                pattern={"[a-zA-Z ]{2,}"}
                onInput={(e) => e.target.value = e.target.value.slice(0, 6)}
                name={type === 'edit' ? 'franchiseNumber' : 'franchiseNumber'}
                value={type === 'edit' ? franchiseNumber : franchiseNumber}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'franchiseNumber' : 'franchiseNumber')}
            </FormItem>

          </Col>
          <Col md={6}>
            <FormItem
              label="Franchise Name"
              hasFeedback
              className={cx({ 'has-error': errorType === 'franchiseName' })}
            >
              <Input
                maxLength={50}
                type="text"
                pattern={"[a-zA-Z ]{2,}"}
                name={type === 'edit' ? 'franchiseName' : 'franchiseName'}
                value={type === 'edit' ? franchiseName : franchiseName}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'franchiseName' : 'franchiseName')}
            </FormItem>
          </Col>
        </Row>

        <Row form>
          <Col md={6}>
            <FormItem
              label="Address :"
              hasFeedback
              className={cx({ 'has-error': errorType === 'address' })}
            >
              <Input
                maxLength={350}
                name={type === 'edit' ? 'address' : 'address'}
                value={type === 'edit' ? address : address}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'address' : 'address')}
            </FormItem>

          </Col>

          <Col md={6}>
            <FormItem
              label="City"
              hasFeedback
              className={cx({ 'has-error': errorType === 'city' })}
            >
              <Input
                type={'text'}
                maxLength={20}
                name={type === 'edit' ? 'city' : 'city'}
                value={type === 'edit' ? city : city}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'city' : 'city')}
            </FormItem>

          </Col>

        </Row>

        <Row form>
          <Col md={6}>
            <FormItem
              label="District"
              hasFeedback
              className={cx({ 'has-error': errorType === 'district' })}
            >
              <Input
                maxLength={30}
                type="text"
                name={type === 'edit' ? 'district' : 'district'}
                value={type === 'edit' ? district : district}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'district' : 'district')}
            </FormItem>

          </Col>

          <Col md={6}>
            <FormItem
              minLength={6}
              type={'number'}
              label="Pincode"
              hasFeedback
              className={cx({ 'has-error': errorType === 'pincode' })}
            >
              <Input
                maxLength={6}
                min="0"
                onInput={(e) => e.target.value = e.target.value.slice(0, 6)}
                type={'number'}
                name={type === 'edit' ? 'pincode' : 'pincode'}
                value={type === 'edit' ? pincode : pincode}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'pincode' : 'pincode')}
            </FormItem>
          </Col>
        </Row>
        <Row form>
          <Col md={6}>
            <FormItem
              label="State"
              hasFeedback
              className={cx({ 'has-error': errorType === 'state' })}
            >
              <Input
                maxLength={20}
                name={type === 'edit' ? 'state' : 'state'}
                value={type === 'edit' ? state : state}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'state' : 'state')}
            </FormItem>
          </Col>
          <Col md={6}>
            <FormItem
              label="Email id"
              hasFeedback
              className={cx({ 'has-error': errorType === 'email' })}
            >
              <Input
                type="email"
                maxLength={50}
                name={type === 'edit' ? 'email' : 'email'}
                value={type === 'edit' ? email : email}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'email' : 'email')}
            </FormItem>
          </Col>
        </Row>
        <Row form>
          <Col md={12}>
            <FormItem
              label="Contact Number"
              hasFeedback
              className={cx({ 'has-error': errorType === 'contact' })}
            >
              <Input
                maxLength={10}
                type={'tel'}
                name={type === 'edit' ? 'contact' : 'contact'}
                value={type === 'edit' ? contact : contact}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'contact' : 'contact')}
            </FormItem>
          </Col>
        </Row>
        {isLoading && (
          <div className="mb-2 text-center">
            <Spin />
          </div>
        )}
        <div className="text-right pt-3">
          {type === 'add' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              onClick={this.handleAddData}
            >
              <Icon type="plus-circle" theme="filled" />
              Save
            </Button>
          )}
          {type === 'edit' && (
            <Button
              type="primary"
              htmlType="submit"
              onClick={this.handleEditData}
              className="btn-cta"
            >
              <Icon type="edit" theme="filled" /> Save
            </Button>
          )}
        </div>
        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  franchise: state.franchise.franchise
})

const mapDispatchToProps = dispatch => ({
  getFranchiseDetail: () => dispatch(getAllFranchiseDetail())
})

const WrappedFormComponent = Form.create()(withRouter(FormComponent));

export default withToastManager(connect(mapStateToProps, mapDispatchToProps)(WrappedFormComponent));

const GlobalStyle = createGlobalStyle`.ant-select-dropdown--multiple{display:none}`;