import React from 'react';
import Skeleton from 'react-loading-skeleton';
import { Table, Icon, Form, Input } from 'antd';
import { FranchiseModal } from '../../../components/Modal';
import { FrenchiseCard } from '../../../components/Cards';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import Delete from './delete/index'
import getAllFranchiseDetail from '../../../middleware/franchiseMiddleware';

const FormItem = Form.Item;

class Frenchies extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            store: [],
            listData: [],
            filterData: []
        };
    }
    componentDidMount() {
        const { getFranchiseDetails, franchiseState } = this.props
        getFranchiseDetails()
        this.setState({
            filterData: franchiseState
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.franchiseState !== this.props.franchiseState) {
            this.setState({
                filterData: this.props.franchiseState,
                listData: this.props.franchiseState,
            })
        }
    }

    handleSearch = e => {
        const { listData } = this.state;
        const filterDataHolder = [
            ...listData.filter(
                ({ franchiseName }) =>
                    franchiseName.toLowerCase().startsWith(e.target.value.trim().toLowerCase())
            ),
        ];
        this.setState({
            [e.target.name]: e.target.value,
            filterData: filterDataHolder,
        });
    };

    handleStatusFilter = stock => {
        const { listData, statusFilter } = this.state;
        let filterDataHolder = [];
        if (statusFilter === stock) {
            filterDataHolder = [...listData];
        } else {
            filterDataHolder = [
                ...listData.filter(
                    ({ status }) => (stock === 1 && status === 0) || (stock === 2 && status === 1)
                ),
            ];
        }
        this.setState({
            search: '',
            statusFilter: stock !== statusFilter ? stock : 0,
            filterData: filterDataHolder,
        });
    };

    render() {
        const { match, getFranchiseDetails, franchiseLoader } = this.props;
        const { filterData, search } = this.state;
        const columns = [
            {
                title: 'Frenchise Id',
                dataIndex: 'franchiseNumber',
                width: 90,
            },
            {
                title: 'Frenchise Name',
                dataIndex: 'franchiseName',
                width: 90,
            },
            {
                title: 'Address',
                dataIndex: 'address',
                width: 180,
            },
            {
                title: 'City',
                dataIndex: 'city'
            },
            {
                title: 'Pin Code',
                dataIndex: "pinCode"
            },
            {
                title: 'District',
                dataIndex: 'district'
            },
            {
                title: 'State',
                dataIndex: 'state'

            },
            {
                title: 'Contact No.',
                dataIndex: 'contactNo'
            },
            {
                title: 'Email',
                dataIndex: 'email',
                width: 150
            },

            {
                title: 'Action',
                key: 'action',

                render: (text, record) => {
                    console.log('razat', text);
                    return (
                        <span className="d-flex">
                            <Link
                                to={{ pathname: `${match.url}/edit`, state: text }}
                                className="ml-2 ant-btn ant-btn-primary ant-btn-circle"
                            >
                                <Icon type="edit" theme="filled" />
                            </Link>
                            <FranchiseModal
                                FranchiseNumber={text.franchiseNumber}
                                FranchiseName={text.franchiseName}
                                Address={text.address}
                                City={text.city}
                                PinCode={text.pinCode}
                                District={text.district}
                                State={text.state}
                                ContactNo={text.contactNo}
                                Email={text.email}
                            />
                            <Delete recordData={text} getFranchiseDetails={getFranchiseDetails} />
                        </span>
                    );
                },
                width: 180,
            },
        ];
        return (
            <>
                <FrenchiseCard />
                <section className="container-fluid  no-breadcrumb chapter">
                    <article className="article">
                        <div className="d-flex justify-content-between mb-2">
                            <div className="d-flex">
                                <FormItem>
                                    <Input
                                        placeholder="Search Franchise"
                                        prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                                        name="search"
                                        value={search}
                                        style={{ width: 275 }}
                                        onChange={this.handleSearch}
                                    />
                                </FormItem>
                            </div>

                            <div className="d-flex">
                                <div>
                                    <Link to={`${match.url}/add`} className="ant-btn ant-btn-primary">
                                        <Icon type="plus-circle" theme="filled" /> Add
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {franchiseLoader ?
                            <Skeleton height={50} count={10} /> :
                            <Table
                                pagination={{ defaultPageSize: 15 }}
                                columns={columns}
                                dataSource={filterData}
                                className="ant-table-v1"
                                loading={franchiseLoader}
                            />
                        }
                    </article>
                </section>
            </>
        );
    }
}

const mapStateToProps = state => ({
    franchiseState: state.franchise.Franchise,
    franchiseLoader: state.franchise.franchiseLoader
})
const mapDispatchToProps = dispatch => ({
    getFranchiseDetails: () => dispatch(getAllFranchiseDetail())
})
export default connect(mapStateToProps, mapDispatchToProps)(Frenchies)