import React from 'react';
import { Route } from 'react-router-dom';
import Frenchies from './route';
import AddFranchise from './route/add/index'
import EditRecord from './route/edit';

const Shop = ({ match }) => (
  <>
  <Route exact path={`${match.url}`} component={Frenchies}/>
  <Route path={`${match.url}/add`} component={AddFranchise}/>
  <Route path={`${match.url}/edit`} component={EditRecord}/>
 </>
);

export default Shop; 