import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import Login from './routes/Login';
import LoginV2 from './routes/LoginV2';
import SignUp from './routes/SignUp';
import SignUpV2 from './routes/SignUpV2';
import ForgotPassword from './routes/ForgotPassword';
import ForgotPasswordV2 from './routes/ForgotPasswordV2';
import ValidteOtp from './routes/ValidateOtp';
import CreateNewPassword from './routes/CreateNewPassword';
import './styles.scss';

const SignOut = () => {
  localStorage.clear();
  return <Redirect to={'/login'} />;
};

const Page = ({ match }) => (
  <div>
    {console.log("login login login componenet component login login login componenet component ",match.url )}
    <Route path={`${match.url}signOut`} component={SignOut} />
    <Route path={`${match.url}login`} component={Login} />
    <Route path={`${match.url}login-v2`} component={LoginV2} />
    <Route path={`${match.url}sign-up`} component={SignUp} />
    <Route path={`${match.url}sign-up-v2`} component={SignUpV2} />
    <Route path={`${match.url}forgot-password`} component={ForgotPassword} />
    <Route path={`${match.url}validate-otp`} component={ValidteOtp} />
    <Route path={`${match.url}create-password`} component={CreateNewPassword} />
    <Route path={`${match.url}forgot-password-v2`} component={ForgotPasswordV2} />
  </div>
);

export default Page;
