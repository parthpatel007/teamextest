import React from 'react';
import QueueAnim from 'rc-queue-anim';
import CreateNewPasswordForm from '../../../routes/form/routes/forms/components/CreateNewPasswordForm';

const FormCard = () => (
  <section className="form-card-page form-card row no-gutters">
    {/* <div className="form-card__img form-card__img--left col-lg-6" style={{backgroundImage: "url('assets/images-demo/covers/bruno-martins-442125-unsplash.jpg')"}}></div> */}
    <div className="form-card__body col-lg-3 p-5 px-lg-8 d-flex align-items-center justify-content-center"></div>
    <div className="form-card__body col-lg-6 p-5 px-lg-8 d-flex align-items-center justify-content-center">
      <CreateNewPasswordForm/>
    </div>
    <div className="form-card__body col-lg-3 p-5 px-lg-8 d-flex align-items-center justify-content-center"></div>
  </section>
)

const Page = () => (
  <QueueAnim type="bottom" className="ui-animate">
    <div key="9">
      <FormCard />
    </div>
  </QueueAnim>
)

export default Page;
