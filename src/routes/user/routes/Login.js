import React from 'react';
import QueueAnim from 'rc-queue-anim';
import LoginForm1 from 'routes/form/routes/forms/components/LoginForm1';

const FormCard = () => (
  <section className="form-card-page form-card row no-gutters">
    {/* <div className="form-card__img form-card__img--left col-lg-6 bg-gray-100 d-flex align-items-center justify-content-center">
      <div>
        <img src={logo} alt="" style={{ height: 300, width: 'auto' }} />
      </div>
    </div> */}
    <div className="form-card__body col-lg-3 p-5 px-lg-8 d-flex align-items-center">  
    </div>
    <div className="form-card__body col-lg-6 p-5 px-lg-8 d-flex align-items-center">  
      <LoginForm1 />
    </div>
    <div className="form-card__body col-lg-3 p-5 px-lg-8 d-flex align-items-center">  
    </div>
  </section>
);

const Page = (props) => (
  <QueueAnim type="bottom" className="ui-animate">
    {console.log("login login match   login componenet component ",props )}
    <div key="1">
      <FormCard />
    </div>
  </QueueAnim>
);

export default Page;
