
import React from 'react';
import { Form, Input, Icon, Button, Spin } from 'antd';
import { Row, Col } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { backendUrl } from '../../../../common/credentials'
import cx from 'classnames';
import { createGlobalStyle } from 'styled-components';
import { connect } from 'react-redux';
import Dragger from 'antd/lib/upload/Dragger';
import getAllTestimonialDetail from '../../../../middleware/testimonialMiddleware';
import { withToastManager } from 'react-toast-notifications';

const FormItem = Form.Item;

class FormComponent extends React.Component {

  state = {
    testimonialMessage: this.props.type === 'edit' ? this.props && this.props.editData && this.props.editData.testimonialMessage : '',
    testimonialName:this.props.type === 'edit' ? this.props && this.props.editData && this.props.editData.testimonialName : '',
    designation: this.props.type === 'edit' ? this.props.editData && this.props.editData && this.props.editData.designation : '',
    profileImage: null,
    productImages: [],
    categories: [],
    store: [],
    editProductImages: this.props.type === 'edit' ? this.props && this.props.editData && this.props.editData.profileImage : [],
    editProductImagesObjects: [],
    isLoading: false,
    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };

  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };

  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };
  handleInput = e => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };

  addNewApi = async () => {

    const {
      testimonialMessage,
      testimonialName,
      designation,
      profileImage,
    } = this.state;
    const { match, history, toastManager } = this.props;
    const authToken = localStorage.getItem('access_token');

    this.setState({ isLoading: true });

    const self = this;
    const formData = new FormData()
    formData.append("testimonialMessage", testimonialMessage)
    formData.append("testimonialName", testimonialName)
    formData.append("designation", designation)
    formData.append("profileImage", profileImage)
    axios
      .post(
        `${backendUrl}/api/addTestimonial`,
        formData,
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        toastManager.add(
          error && error.data && error.data.message, {
          appearance: "warning",
          autoDismiss: true
        });
        self.setState({
          isLoading: false,
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
        console.log('rrrr err', error);
      });
  };


  editApi = async () => {
    console.log("Edit Testimonial Data")
    const {
      testimonialMessage,
      testimonialName,
      designation,
      editProductImages,
    } = this.state;
    const { match, history } = this.props;
    const { _id } = this.props.editData;
    const authToken = localStorage.getItem('access_token');
    this.setState({ isLoading: true });

    const self = this;
    const formData = new FormData()
    formData.append("testimonialMessage", testimonialMessage)
    formData.append("testimonialName", testimonialName)
    formData.append("designation", designation)
    formData.append("profileImage", editProductImages)

    console.log(editProductImages, "editProductImageseditProductImages API")

    axios
      .post(
        `${backendUrl}/api/updateTestimonial/${_id}`,
        formData,
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: '*/*',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        console.log('rrrr res', response);
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        self.setState({
          isLoading: false,
          apiError:'Network Error'
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
        console.log('rrrr err', error);
      });
  };

  validateForm = () => {
    const {
      testimonialMessage,
      testimonialName,
      designation,
      editProductImages,
      profileImage
    } = this.state;
    if (testimonialName === '') {
      this.setState({
        error: {
          errorText: 'Please enter testimonial Name',
          errorType: 'testimonialName',
        },
      });
      return false;
    }
    if (designation === '') {
      this.setState({
        error: {
          errorText: 'Please select a designation',
          errorType: 'designation',
        },
      });
      return false;
    }
    if (testimonialMessage === '') {
      this.setState({
        error: {
          errorText: 'Please select a Message',
          errorType: 'testimonialMessage',
        },
      });
      return false;
    }
    // if (
    //   profileImage && profileImage.length <= 0 ||
    //   profileImage === undefined ||
    //   profileImage === null
    // ) {
    //   this.setState({
    //     error: {
    //       errorText: 'Please Select Profile Image',
    //       errorType: 'add_image',
    //     },
    //   });
    //   return false;
    // }
    if (this.props.type === 'edit') {
      if (!this.state.editProductImages && editProductImages.length === 0) {
        this.setState({
          error: {
            errorText: 'Add testimonial Image',
            errorType: 'edit_image',
          },
        });
        return false;
      }
    }
    else {
      if (profileImage.length === 0) {
        this.setState({
          error: {
            errorText: 'Please select a testimonial Image',
            errorType: 'add_image',
          },
        });
        return false;
      }
    }
    return true;
  };


  componentDidMount() {
    const { getTestimonialDetail } = this.props;
    getTestimonialDetail()
  }
  handleAddData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.addNewApi();
    }
  };

  handleEditData = () => {
    this.clearError();
    console.log("1222334434")
    // this.editApi();
    if (this.validateForm()) {
      this.editApi();
    }
  };

  handleEditProductImage = () => {
    const { editData} = this.props
    const { editProductImages } = this.state
    // const tempArr = [...editProductImages]
    // tempArr.splice(index, 1)
    
    delete editData.profileImage;

    console.log(editData, "editDataeditDataeditData")

    this.setState({
      editProductImages: ''
    })
  }

  onDragChange = (info) => {
    const status = info.file.status;
    if (status !== 'uploading') {
      this.setState({
        profileImage: info.file.originFileObj,
      });
    }
  }


  beforeUpload = (file) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/jpg';
    const { type } = this.props
    const isLt2M = file.size / 1024 / 1024 < 5;
    if (!isJpgOrPng) {
      if (type === "edit") {
        this.setState({
          error: {
            errorText: 'Please select vaild edit image (JPG,PNG,JPEG )',
            errorType: 'edit_image',
          },
        });
        return false
      }
      else {
        this.setState({
          error: {
            errorText: 'Please select vaild add image (JPG,PNG,JPEG )',
            errorType: 'add_image',
          },
        });
        return false
      }
    }
    else if (!isLt2M) {
      if (type === "edit") {
        this.setState({
          error: {
            errorText: 'Image must smaller than 5MB!',
            errorType: 'edit_image',
          },
        });
        return false
      }
      else {
        this.setState({
          error: {
            errorText: 'Image must smaller than 5MB!',
            errorType: 'add_image',
          },
        });
        return false
      }
    }
    else {
      this.setState({
        error: {
          errorText: '',
          errorType: '',
        },
      });
    }
    return isJpgOrPng && isLt2M;
  }


  onEditDragChange = (info) => {
    const status = info.file.status;
    if (status !== 'uploading') {
      if (info.fileList.length <= 4) {
        this.setState({
          editProductImages: info.file.originFileObj
        });
      }
      else if (info.file.originFileObj.length > 4) {
        console.log("inside filelist lengt > 4")
        this.setState({
          editProductImages: info.file.originFileObj.splice(4)
        });
      }
      this.setState({
        editProductImagesObjects: info.file.originFileObj
      });
    }
  }

  render() {

    const {

      testimonialMessage,
      testimonialName,
      designation,
      isLoading,
      apiError,
      error,
      editProductImages,
    } = this.state;
    const { type = 'add', editData, match, history } = this.props;
    console.log(editProductImages , "Edit Data Testimonial Data")
    const { errorType } = error;
    if (type === 'edit') {
      if (!editData) {
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        return null;
      }
    }

    return (
      <div className="form-container">
        <GlobalStyle />
        <Row form>
          <Col md={6}>
            <FormItem
              label="Testimonial Name"
              hasFeedback
              className={cx({ 'has-error': errorType === 'testimonialName' })}
            >
              <Input
                maxLength={30}
                name={type === 'edit' ? 'testimonialName' : 'testimonialName'}
                value={type === 'edit' ? testimonialName : testimonialName}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'testimonialName' : 'testimonialName')}
            </FormItem>

          </Col>


          <Col md={6}>
            <FormItem
              label="Designation"
              hasFeedback
              className={cx({ 'has-error': errorType === 'designation' })}
            >
              <Input

                maxLength={20}

                name={type === 'edit' ? 'designation' : 'designation'}
                value={designation}

                onChange={this.handleInput}
              />
              {this.errorShow('designation')}
            </FormItem>
          </Col>

        </Row>

        <Row Form>
          <Col md={12} height={10} className="h-50">
            <FormItem
              label="Testimonial Message"
              hasFeedback
              className={cx({ 'has-error': errorType === 'testimonialMessage' })}
            >
              <Input

                maxLength={150}
                style={{ height: "50px" }}
                name={type === 'edit' ? 'testimonialMessage' : 'testimonialMessage'}
                value={testimonialMessage}
                onChange={this.handleInput}
              />
              {this.errorShow('testimonialMessage')}
            </FormItem>
          </Col>
        </Row>


        {type === 'add' && (
          <FormItem label="Add Testimonial  Image">
            <Dragger
              listType="picture"
              onChange={this.onDragChange}
              showUploadList={errorType == 'add_image' ? false : true}
              beforeUpload={this.beforeUpload}
              name={'file'}
              multiple={false}
              accept=".png, .jpg, .jpeg"
            >
              <p className="ant-upload-drag-icon">
                <Icon type="inbox" />
              </p>
              <p className="ant-upload-text">Click or drag file to this area to upload</p>
              <p className="ant-upload-hint">
                Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                band files
              </p>
            </Dragger>
            Note: For better user experience upload image of 1280*1280 pixels (square shape) image.
            {this.errorShow('add_image')}
          </FormItem>
        )}

        {type === 'edit' && (
          <>
            <FormItem
              className="mt-5"
              label="Testimonial Image">
              <Row className="pt-5" className="text-center" justify={'space-between'}>
                    {editProductImages ?
                    <Col span={4} className="text-center">
                      <Button type="danger" className="ml-2" shape="circle" onClick={() => this.handleEditProductImage()}>
                        <Icon type="close-circle" theme="filled" />
                      </Button>
                      <br />
                      <img
                        src={editProductImages}

                        style={{ width: 100, height: 100, objectFit: 'contain' }}
                      />
                      <br />
                    </Col>                  
                  : <Col span={12} className="text-center">No Image</Col>}
              </Row>
            </FormItem>
          </>
        )}
        {type === 'edit' && (
          <FormItem label="Upload Image">
            {/* <DragAndDrop /> */}
            <Dragger
              listType="picture"
              onChange={this.onEditDragChange}
              showUploadList={errorType == 'edit_image' ? false : true}
              beforeUpload={this.beforeUpload}
              name={'file'}
              multiple={false}
              accept=".png, .jpg, .jpeg"
            >
              <p className="ant-upload-drag-icon">
                <Icon type="inbox" />
              </p>
              <p className="ant-upload-text">Click or drag file to this area to upload</p>
              <p className="ant-upload-hint">
                Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                band files
              </p>
            </Dragger>
            Note: For better user experience upload image of 1280*1280 pixels (square shape) image.
            {this.errorShow('edit_image')}
          </FormItem>
        )}
        {isLoading && (
          <div className="mb-2 text-center">
            <Spin />
          </div>
        )}
        <div className="text-right pt-3">
          {type === 'add' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              onClick={this.handleAddData}
            >
              <Icon type="plus-circle" theme="filled" />
              Save
            </Button>
          )}
          {type === 'edit' && (
            <Button
              type="primary"
              htmlType="submit"
              onClick={this.handleEditData}
              className="btn-cta"
            >
              <Icon type="edit" theme="filled" /> Save
            </Button>
          )}
        </div>
        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  testimonial: state.testimonial.testimonial
})

const mapDispatchToProps = dispatch => ({
  getTestimonialDetail: () => dispatch(getAllTestimonialDetail())
})

const WrappedFormComponent = Form.create()(withRouter(FormComponent));

export default withToastManager(connect(mapStateToProps, mapDispatchToProps)(WrappedFormComponent));

const GlobalStyle = createGlobalStyle`.ant-select-dropdown--multiple{display:none}`;
