import React from 'react';
import { Table, Icon, DatePicker, Form, Input, Button, Select, Tooltip, Skeleton } from 'antd';
import { BlockModal } from '../../../components/Modal';
import { TestimonialCard } from '../../../components/Cards';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import getAllTestimonialDetail from '../../../middleware/testimonialMiddleware';
import Delete from './delete';
const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const Option = Select.Option;

/* eslint-disable */


class TestiMonial extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            startDate: '',
            endDate: '',
            loader: true,
            search: '',
            store: [],
            store_id: localStorage.getItem('store_id') ? localStorage.getItem('store_id') : '',
            listData: [],
            filterData: [],
            stockFilter: 0
        };
        this.selectedStore = this.selectedStore.bind(this);
    }

    getDataRangeApi = () => {
        const { startDate, endDate, listData } = this.state;
        const filterDataHolder = [
            ...listData.filter(
                ({ createdAt }) =>
                    (new Date(createdAt).getTime() > new Date(startDate).getTime() && new Date(createdAt).getTime() < new Date(endDate).getTime())
            )
        ];
        this.setState({
            filterData: filterDataHolder,
            statusFilter: 0,
        });
    };
    componentDidMount() {
        const {getTestimonialDetails, testimonial, productLoader} = this.props
        getTestimonialDetails()
        // console.log(productLoader, "testimonialtestimonialtestimonialtestimonialtestimonial")
        this.setState({
            listData:testimonial,
            loader:productLoader,
        })
        // if (this.props.customersSuccess) {
        //     this.setState({
        //         loader: productLoader
        //     });
        // }
        // else {
        //     this.setState({
        //         loader: productLoader
        //     });
        // }

    }


    componentDidUpdate(prevProps, prevState) {

        const { testimonial, productLoader } = this.props;

        console.log(prevState, "state state state state state state state ")

        console.log(prevProps, "PRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndks ")

        if (prevProps.testimonial !== testimonial) {
            this.setState({
                filterData: testimonial,
                listData: testimonial,
                loader:productLoader
            })
        }

    }

    handleSearch = e => {
        const { listData } = this.state;
        console.log(listData , "listDatalistDatalistDatalistData New Data")
        const filterDataHolder = [
            ...listData.filter(
                ({ testimonialName }) =>
                    testimonialName.toLowerCase().startsWith(e.target.value.trim().toLowerCase())
            ),
        ];
        this.setState({
            [e.target.name]: e.target.value,
            filterData: filterDataHolder,
            stockFilter: 0,
        });
    };

    handleStatusFilter = stock => {
        const { listData, statusFilter } = this.state;

        let filterDataHolder = [];
        if (statusFilter === stock) {
            filterDataHolder = [...listData];
        } else {
            filterDataHolder = [
                ...listData.filter(
                    ({ status }) => (stock === 1 && status === 0) || (stock === 2 && status === 1)
                ),
            ];
        }
        this.setState({
            search: '',
            statusFilter: stock !== statusFilter ? stock : 0,
            filterData: filterDataHolder,
        });
    };

    handleStockFilter = stock => {
        const { listData, stockFilter } = this.state;

        let filterDataHolder = [];
        if (stockFilter === stock) {
            filterDataHolder = [...listData.filter(({ store_id }) => store_id === this.state.store_id)];
        } else {
            filterDataHolder = [
                ...listData.filter(
                    ({ status, store_id }) =>
                        ((stock === 1 && status === 0) || (stock === 2 && status === 1)) &&
                        store_id === this.state.store_id
                ),
            ];
        }

        this.setState({
            search: '',
            stockFilter: stock !== stockFilter ? stock : 0,
            filterData: filterDataHolder,
        });
    };

    handleSelect = (name, value) => {
        const { listData } = this.state;

        console.log(
            'rrrr handleSelect',
            listData,
            listData.filter(({ store_id }) => store_id === value)
        );
        const filterDataHolder = [...listData.filter(({ store_id }) => store_id === value)];
        this.setState({ [name]: value, stockFilter: 0 });
        this.getDataApiWithStoreId(value);
        if (!localStorage.getItem('store_id')) {
            localStorage.setItem('store_id', value);
        }
    };
    selectedStore() {
        const { store, store_id } = this.state;
        return (
            <SelectStore>
                <FormItem label="Choose Store" hasFeedback className="mb-0 d-flex align-items-center">
                    <Select
                        name="store_id"
                        value={store_id}
                        placeholder="Select Store"
                        onChange={e => this.handleSelect('store_id', e)}
                    >
                        {store.map((data, index) => (
                            <Option key={index} value={data._id}>
                                {data.name}
                            </Option>
                        ))}
                    </Select>
                </FormItem>
            </SelectStore>
        );
    }
    onDateChange = (date, dateString) => {
        if (date.length == 0) {
            const { listData } = this.state
            this.setState({
                filterData: listData
            })
        }
        console.log('eee', date, 'dddd', dateString);
        this.setState({ startDate: dateString[0], endDate: dateString[1] });
    };
    onRangeSearch = () => {
        this.getDataRangeApi();
    };
    render() {
        const { match, testimonial, getTestimonialDetails} = this.props;
        const { listData, loader, search} = this.state;
        console.log(listData, "listData Testimonial")
        const columns = [
            {
                title:'Name',
                render: (text) => {
                    return <div>{text.testimonialName}</div>
                },
                width:150
            },
            {
                title:'Message',
                render: (text) => {
                    return <div className="text-messages-wrap">{text.testimonialMessage}</div>
                },
                width:500
            },
            {
                title:'Designation',
                render: (text) => {
                    return <div>{text.designation}</div>
                },
                width:150
            },
            {
                title:'Profile Image',
                render:(text) =>{
                    return <img className="profile-img" src={text.profileImage} alt={text.profileImage} title="img" />
                },
                width:150
            },
          
            {
                title: 'Action',
                key: 'action',
                render: (text) => {
                    return (
                        <span className="d-flex">
                            <Link
                                    to={{ pathname: `${match.url}/preview`, state: text }}
                                    className="ant-btn ant-btn-primary ant-btn-circle"
                                >
                                <Icon type="eye" theme="filled" />
                            </Link>
                            <Tooltip placement="bottomLeft" title={'edit'}>
                                <Link
                                to={{ pathname:`${match.url}/edit`, state: text }}
                                className="ml-2 ant-btn ant-btn-primary ant-btn-circle"
                                >
                                <Icon type="edit" theme="filled" />
                                </Link>
                            </Tooltip>
                            <Tooltip placement="bottomLeft" title="delete">
                                <span>
                                    <Delete recordData={text} getTestimonialDetails={getTestimonialDetails} />
                                </span>
                            </Tooltip>
                  
                            {/* <Link
                    to={{ pathname: `${match.url}/edit`, state: text }}
                    className="ml-2 ant-btn ant-btn-primary ant-btn-circle"
                  >
                    <Icon type="edit" theme="filled" />
                  </Link>
                  <DeleteCustomer recordData={text} /> */}
                            {/* <Tooltip placement="bottomLeft" title={text.status === 0 ? 'block' : 'unblock'}>
                                <span>
                                    <BlockModal
                                        recordData={text}
                                        recordName={text.userName}
                                        api={'api/changeCustomerStatus'}
                                        type={text.status === 0 ? 'block' : 'unblock'}
                                        blockTitleText="Block customer"
                                        unblockTitleText="Unblock customer"
                                        blockText="block customer"
                                        unblockText="unblock customer"
                                        blockBtnText="Block customer"
                                        unblockBtnText="Unblock customer"
                                    />
                                </span>
                            </Tooltip> */}
                        </span>
                    );
                },
                width: 200,
            },
        ];
        return (
            <>
                <section className="container-fluid  no-breadcrumb chapter">
                    <article className="article">
                        <div className="d-flex justify-content-between mb-2">
                            <div className="d-flex">
                                <FormItem>
                                    <Input
                                        placeholder="Search Testimonial"
                                        prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                                        name="search"
                                        value={search}
                                        style={{ width: 275 }}
                                        onChange={this.handleSearch}
                                    />
                                </FormItem>
                            </div>
                            
                            <div className="d-flex">
                                <div>
                                    {/* <Button
                                        className="mx-2"
                                        onClick={() => this.handleStatusFilter(1)}
                                        type={stockFilter === 1 ? 'primary' : 'secondary'}
                                    >
                                        Active
                                    </Button>
                                    <Button
                                        className="mr-2"
                                        onClick={() => this.handleStatusFilter(2)}
                                        type={stockFilter === 2 ? 'primary' : 'secondary'}
                                    >
                                        Inactive
                                    </Button> */}

                                    <Link to={`${match.url}/add`} className="ant-btn ant-btn-primary">
                        <Icon type="plus-circle" theme="filled" /> Add
                      </Link>
                                </div>
                            </div>
                        </div>
                        {loader ? <Skeleton height={50} count={10} /> : (

                            <Table
                            pagination={{ defaultPageSize: 15 }}
                            columns={columns}
                            dataSource={testimonial}
                            className="ant-table-v1"
                            loading={loader}
                            />
                        )}                            
                    </article>
                </section>
            </>
        );
    }
}        
const mapStateToProps = state => ({
    testimonial : state.testimonial.testimonial,
    productLoader: state.testimonial.productLoader
})

const mapDispatchToProps = dispatch => ({
    getTestimonialDetails : ()=> dispatch(getAllTestimonialDetail())
})

export default connect(mapStateToProps,mapDispatchToProps)(TestiMonial)

const SelectStore = styled('div')`
  .ant-select {
    min-width: 200px;
  }
`;