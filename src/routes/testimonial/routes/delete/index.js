import React from 'react';
import { Modal, Button, Icon } from 'antd';
import { createGlobalStyle } from 'styled-components';
import axios from 'axios';
import { backendUrl } from '../../../../../src/common/credentials';
import { withToastManager } from 'react-toast-notifications';

class Delete extends React.Component {
  state = { visible: false };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  deleteRecord = id => {
    var self = this;
    const{toastManager} = this.props;

    console.log("delete id:" + id);

    const authToken = localStorage.getItem('access_token');

    console.log(authToken);

    axios.delete(`${backendUrl}/api/deleteTestimonial/${id}`, {
      headers: {
        Authorization: `Bearer ${authToken}`,
      }
    })
      .then(function(response) {
        self.setState({
          visible: false,
        });
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        self.props.getTestimonialDetails()
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };

  render() {
    const { recordData } = this.props;
    const { testimonialName, _id } = recordData;
    return (
      <div>
        <Button type="danger" className="ml-2" shape="circle" onClick={this.showModal}>
          <Icon type="delete" theme="filled" />
        </Button>
        <Modal
          title="Delete Testimonial"
          okType="danger"
          visible={this.state.visible}
          onOk={() => this.deleteRecord(_id)}
          okText="Delete"
          onCancel={this.handleCancel}
          // footer={null}
          // className="custom-modal-v1"
          centered
        >
          {/* <GlobalStyle /> */}
          <section className="form-card">
            <div className="form-card__body ">
              <label style={{ fontSize: 16 }}>
                Do you really want to delete testimonial <b>{testimonialName}</b>
              </label>
            </div>
          </section>
        </Modal>
      </div>
    );
  }
}
export default withToastManager(Delete)
