import React from 'react';
import { Route } from 'react-router-dom';
import TestiMonial from './routes';
import AddForm from "./routes/add/index"
import EditForm from "./routes/edit/index"



const Shop = ({ match }) => (
  <>
  <Route exact path={`${match.url}`} component={TestiMonial}/>
  <Route path={`${match.url}/add`} component={AddForm}/>
  <Route path={`${match.url}/edit`} component={EditForm}/>

 </>
);

export default Shop; 