import React from 'react';
import { Route } from 'react-router-dom';
import AddDownload from './routes/add/index'
import Download from './routes/index';


const Shop = ({ match }) => (
  <>
    <Route exact path={`${match.url}`} component={Download} />
    <Route path={`${match.url}/add`} component={AddDownload} />

  </>
);

export default Shop; 