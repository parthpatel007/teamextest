import React from 'react';
import { Form, Input, Icon, Button, Spin } from 'antd';
import { Row, Col } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { backendUrl } from '../../../../common/credentials'
import cx from 'classnames';
import { createGlobalStyle } from 'styled-components';
import { connect } from 'react-redux';
import Dragger from 'antd/lib/upload/Dragger';
import { withToastManager } from 'react-toast-notifications';
import getAllDownloadDetail from '../../../../middleware/downloadMiddleware';
import TextArea from 'antd/lib/input/TextArea';
const FormItem = Form.Item;

class FormComponent extends React.Component {
  state = {
    name: '',
    description: '',
    productImages: [],
    file: [],
    isLoading: false,
    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };

  handleTagsChange = (state, value) => {
    this.clearError();
    this.setState({ [state]: value });
    console.log(`rrrr selected  ${value} `);
  };

  handleEditor = (state, content) => {
    this.clearError();
    this.setState({ [state]: content });
  };

  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };

  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };

  handleInput = e => {
    this.clearError();
    console.log("handleinput", e.target.value)
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSelect = (name, value) => {
    this.clearError();
    this.setState({ [name]: value });
  };

  addNewApi = async () => {
    const { toastManager } = this.props
    const {
      name,
      description,
      productImages,
      file
    } = this.state;

    const { match, history } = this.props;
    const authToken = localStorage.getItem('access_token');
    this.setState({ isLoading: true });

    const self = this;
    const formData = new FormData()
    formData.append("coverImage", productImages)
    formData.append("file", file)
    console.log("swkja nakjan saksna sasn", this.state.productImages)
    formData.append("name", name)
    formData.append("description", description)
    axios
      .post(
        `${backendUrl}/api/downloadImage`,
        formData,
        {
          headers: {
            Authorization: `Bearer ${authToken}`,
          },
        }

      )
      .then(function (response) {
        console.log('rrrr res', response);
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        toastManager.add(
          error && error.response && error.response.data && error.response.data.message, {
          appearance: "warning",
          autoDismiss: true
        });
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 8080);
        console.log('rrrr err', error);
      });
  };

  validateForm = () => {
    const {
      name,
      description
    } = this.state;

    if (name === '') {
      this.setState({
        error: {
          errorText: 'Please select a name',
          errorType: 'name',
        },
      });
      return false;
    }

    if (description === '') {
      this.setState({
        error: {
          errorText: 'Please select a description',
          errorType: 'description',
        },
      });
      return false;
    }
    if (
      this.state.productImages.length <= 0 ||
      this.state.productImages === undefined ||
      this.state.productImages === null
    ) {
      this.setState({
        error: {
          errorText: 'Please Select Valid Cover image',
          errorType: 'add_image',
        },
      });
      return false;
    }
    if (
      this.state.file.length <= 0 ||
      this.state.file === undefined ||
      this.state.file === null
    ) {
      this.setState({
        error: {
          errorText: 'Please Select Valid File',
          errorType: 'add_file',
        },
      });
      return false;
    }
    return true;
  };

  componentDidMount() {
    const { getDownloadDetails } = this.props;
    getDownloadDetails();
  }
  handleAddData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.addNewApi();
    }
  };

  onDragChange = info => {
    const status = info.file.status;
    if (status !== 'uploading') {
      this.setState({
        productImages: info.file.originFileObj,
      });
    }
  };

  onDragChangeFile = info => {
    const status = info.file.status;
    if (status !== 'uploading') {
      this.setState({
        file: info.file,
      });
    }
  };

  beforeUpload = file => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    const isLt2M = file.size / 1024 / 1024 < 5;
    if (!isJpgOrPng) {

      this.setState({
        error: {
          errorText: 'Please select vaild add image (JPG,PNG,JPEG )',
          errorType: 'add_image',
        },
      });
      return false;
    } else if (!isLt2M) {
      this.setState({
        error: {
          errorText: 'Image must smaller than 5MB!',
          errorType: 'add_image',
        },
      });
      return false;
    } else {
      this.setState({
        error: {
          errorText: '',
          errorType: '',
        },
      });
    }
    return isJpgOrPng && isLt2M;
  };

  beforeUploadFile = file => {
    const isPdfOrPngOrXlsx = file.type === 'application/pdf' || file.type === 'application/doc' || file.type === 'application/xlsx';
    const isLt2M = file.size / 1024 / 1024 < 10;
    if (!isPdfOrPngOrXlsx) {
      this.setState({
        error: {
          errorText: 'Please select vaild add file (PDF,DOC,XLSX)',
          errorType: 'add_file',
        },
      });
      return false;
    } else if (!isLt2M) {
      this.setState({
        error: {
          errorText: 'File must smaller than 10MB!',
          errorType: 'add_file',
        },
      });
      return false;
    } else {
      this.setState({
        error: {
          errorText: '',
          errorType: '',
        },
      });
    }
    return isPdfOrPngOrXlsx && isLt2M;
  };

  render() {
    const {
      name,
      description,
      isLoading,
      apiError,
      error,
    } = this.state;
    const { errorType } = error;
    return (
      <div className="form-container">
        <GlobalStyle />
        <Row form>
          <Col md={12}>
            <FormItem
              label="File Name"
              hasFeedback
              className={cx({ 'has-error': errorType === 'name' })}
            >
              <Input
                maxLength={150}
                name={'name'}
                value={name}
                onChange={this.handleInput}
              />
              {this.errorShow('name')}
            </FormItem>
          </Col>

          <Col md={12}>
            <FormItem
              label="File Description"
              hasFeedback
              className={cx({ 'has-error': errorType === 'description' })}
            >
              <TextArea
                name={'description'}
                value={description}
                onChange={this.handleInput}

              />
              {this.errorShow('description')}
            </FormItem>
          </Col>
        </Row>
        <FormItem label="Add File Cover Image">
          <Dragger
            listType="picture"
            onChange={this.onDragChange}
            showUploadList={errorType === 'add_image' ? false : true}
            beforeUpload={this.beforeUpload}
            name={'file'}
            multiple={false}
          >
            <p className="ant-upload-drag-icon">
              <Icon type="inbox" />
            </p>
            <p className="ant-upload-text">Click or drag file to this area to upload</p>
            <p className="ant-upload-hint">
              Support for a single or bulk upload. Strictly prohibit from uploading company data
              or other band files
            </p>
          </Dragger>
          {this.errorShow('add_image')}
        </FormItem>

        <FormItem label="Add File">
          <Dragger
            listType="picture"
            onChange={this.onDragChangeFile}
            showUploadList={errorType === 'add_file' ? false : true}
            beforeUpload={this.beforeUploadFile}
            name={'file'}
            multiple={false}
          >
            <p className="ant-upload-drag-icon">
              <Icon type="inbox" />
            </p>
            <p className="ant-upload-text">Click or drag file to this area to upload</p>
            <p className="ant-upload-hint">
              Support for a single or bulk upload. Strictly prohibit from uploading company data
              or other band files
            </p>
          </Dragger>
          {/* Note: For better user experience upload image of 1336*800 pixels (square shape) image. */}
          {this.errorShow('add_file')}
        </FormItem>

        {isLoading &&
          <div className="mb-2 text-center">
            <Spin />
          </div>
        }
        <div className="text-right pt-3">
          <Button
            type="primary"
            htmlType="submit"
            className="btn-cta"
            onClick={this.handleAddData}
          >
            <Icon type="plus-circle" theme="filled" />
            Add
          </Button>
        </div>
        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  download: state.banner.download
});

const mapDispatchToProps = dispatch => ({
  getDownloadDetails: () => dispatch(getAllDownloadDetail()),
});

const WrappedFormComponent = Form.create()(withRouter(FormComponent));
export default withToastManager(connect(mapStateToProps, mapDispatchToProps)(WrappedFormComponent));
const GlobalStyle = createGlobalStyle`.ant-select-dropdown--multiple{display:none}`;