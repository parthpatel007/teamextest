import React from 'react';
import { Table, Icon, Form, Input } from 'antd';
import Delete from './delete/index'
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import getAllDownloadDetail from '../../../middleware/downloadMiddleware';

const FormItem = Form.Item;

class Download extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            loader: true,
            search: '',
            listData: [],
            filterData: [],
        };
    }

    componentDidMount() {
        const { getAllDownloadDetail } = this.props
        getAllDownloadDetail()
        const { download } = this.props
        this.setState({
            filterData: download
        })
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.download !== this.props.download) {
            this.setState({
                filterData: this.props.download,
                listData: this.props.download,
                loader: this.props && this.props.download && this.props.download.length > 0 ? false : true
            })

        }
    }

    handleSearch = e => {
        const { listData } = this.state;
        const filterDataHolder = [
            ...listData.filter(
                ({ name }) =>
                    name.toLowerCase().startsWith(e.target.value.trim().toLowerCase())
            ),
        ];
        this.setState({
            [e.target.name]: e.target.value,
            filterData: filterDataHolder,
            stockFilter: 0,
        });
    };
    render() {
        const { match } = this.props;
        const { filterData, search, } = this.state;

        const columns = [
            {
                title: 'Index',
                render: (text, record, index) => <> {index + 1} </>,
                width: 80,
            },
            {
                title: 'File Name',
                dataIndex: 'name'
            },
            {
                title: 'File Descripation',
                render: (value) => (
                    <p
                        className=" textSize"
                        dangerouslySetInnerHTML={{ __html: value.description }}
                    ></p>
                ),
            },
            {
                title: 'File Download Link',
                dataIndex: 'file',
                render: (file) => {
                    return (
                        <div style={{ marginLeft: '50px' }}>
                            {
                                file.ext === "pdf" ?

                                    <a href={file.original} target="_blank" rel="noopener noreferrer">
                                        <Icon type="download" style={{ fontSize: 25 }} /></a>
                                    :
                                    <a href={file.original} target="_blank" download rel="noopener noreferrer">
                                        <Icon type="download" style={{ fontSize: 25 }} /></a>
                            }
                        </div>
                    )
                }
            },

            {
                title: 'File Image',
                dataIndex: 'coverImage',
                key: 'coverImage',
                render: (src) => {
                    return (
                        <div>{
                            <img height="40" width="40" src={src && src.original} alt="dowmloadimg" />
                        }
                        </div>)
                },
                width: 120,
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => {
                    return (
                        <span className="d-flex">
                            <Delete recordData={text} />
                        </span>
                    );
                },
                width: 100,
            },
        ];
        return (
            <>
                <section className="container-fluid  no-breadcrumb chapter">
                    <article className="article">
                        <div className="d-flex justify-content-between mb-2">
                            <div className="d-flex">
                                <FormItem>
                                    <Input
                                        placeholder="Search File Name"
                                        prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                                        name="search"
                                        value={search}
                                        style={{ width: 275 }}
                                        onChange={this.handleSearch}
                                    />
                                </FormItem>
                            </div>
                            <div className="d-flex">
                                <div>
                                    <Link to={`${match.url}/add`} className="ant-btn ant-btn-primary">
                                        <Icon type="plus-circle" theme="filled" /> Add
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <Table
                            pagination={{ defaultPageSize: 15 }}
                            columns={columns}
                            dataSource={filterData}
                            className="ant-table-v1"
                        />
                    </article>
                </section>
            </>
        );
    }
}

const mapStateToProps = state => ({
    download: state.download.download
})
const mapDispatchToProps = dispatch => ({
    getAllDownloadDetail: () => dispatch(getAllDownloadDetail())
})
export default connect(mapStateToProps, mapDispatchToProps)(Download)