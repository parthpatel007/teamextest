import React from 'react';
import Skeleton from 'react-loading-skeleton';
import { Table, Icon, Form, Input } from 'antd';
import Delete from './delete/index'
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import getAllBannerDetail from '../../../middleware/bannerMiddleware';

const FormItem = Form.Item;

class Homebanner extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            loader: true,
            search: '',
            listData: [],
            filterData: [],
        };
    }

    componentDidMount() {
        const { getBannerDetail, banner } = this.props
        getBannerDetail()
        this.setState({
            filterData: banner
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.banner !== this.props.banner) {
            this.setState({
                filterData: this.props.banner,
                listData: this.props.banner
            })
        }
    }

    handleSearch = e => {
        const { listData } = this.state;
        const filterDataHolder = [
            ...listData.filter(
                ({ title }) =>
                    title.toLowerCase().startsWith(e.target.value.trim().toLowerCase())
            ),
        ];
        this.setState({
            [e.target.name]: e.target.value,
            filterData: filterDataHolder,
        });
    };

    render() {
        const { match, getBannerDetail, bannerLoader } = this.props;
        const { filterData, search } = this.state;

        const columns = [
            {
                title: 'Index',
                render: (text, record, index) => <> {index + 1} </>,
                width: 80,
            },
            {
                title: 'Title',
                dataIndex: 'title'
            },
            {
                title: 'SubTitle',
                dataIndex: 'subTitle',
            },
            {
                title: 'Category',
                dataIndex: 'categoryName',
                width: "100"
            },
            {
                title: 'Image',
                dataIndex: 'bgImage',
                key: 'bgImage',
                render: (src) => {
                    return (
                        <div>{
                            src && src.length > 0 && <img height="100" width="auto" src={src && src[0] && src[0].original} alt="homeimg" />
                        }
                        </div>)
                },
                width: 120,
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => {
                    return (
                        <span className="d-flex">
                            <Delete recordData={text} getBannerDetail={getBannerDetail} />
                        </span>
                    );
                },
                width: 100,
            },
        ];
        return (
            <>
                <section className="container-fluid  no-breadcrumb chapter">
                    <article className="article">
                        <div className="d-flex justify-content-between mb-2">
                            <div className="d-flex">
                                <FormItem>
                                    <Input
                                        placeholder="Search Banner"
                                        prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                                        name="search"
                                        value={search}
                                        style={{ width: 275 }}
                                        onChange={this.handleSearch}
                                    />
                                </FormItem>
                            </div>
                            <div className="d-flex">
                                <div>
                                    <Link to={`${match.url}/add`} className="ant-btn ant-btn-primary">
                                        <Icon type="plus-circle" theme="filled" /> Add
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {bannerLoader ?
                            <Skeleton height={50} count={10} /> :
                            <Table
                                pagination={{ defaultPageSize: 15 }}
                                columns={columns}
                                dataSource={filterData}
                                className="ant-table-v1"
                                loading={bannerLoader}
                            />
                        }
                    </article>
                </section>
            </>
        );
    }
}

const mapStateToProps = state => ({
    banner: state.banner.banner,
    bannerLoader: state.banner.bannerLoader
})
const mapDispatchToProps = dispatch => ({
    getBannerDetail: () => dispatch(getAllBannerDetail())
})
export default connect(mapStateToProps, mapDispatchToProps)(Homebanner)