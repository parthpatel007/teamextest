import React from 'react';
import { Form, Input, Icon, Button, Select, Spin } from 'antd';
import { Row, Col } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { backendUrl } from '../../../../common/credentials'
import cx from 'classnames';
import { createGlobalStyle } from 'styled-components';
import { connect } from 'react-redux';
import getAllCategoryDetail from '../../../../middleware/categoryMiddleware';
import Dragger from 'antd/lib/upload/Dragger';
import { withToastManager } from 'react-toast-notifications';
import getAllBannerDetail from '../../../../middleware/bannerMiddleware';
/* eslint-disable */
const FormItem = Form.Item;

const Option = Select.Option;

class FormComponent extends React.Component {
  state = {
    subtitle: '',
    productImages: [],
    categories: [],
    category_id: '',
    title: '',
    isLoading: false,
    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };

  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };

  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };

  handleInput = e => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSelect = (name, value) => {
    this.clearError();
    this.setState({ [name]: value });
  };

  addNewApi = async () => {
    const { toastManager } = this.props
    const {
      category_id = '',
      title,
      subtitle,
      productImages,
    } = this.state;

    const { match, history } = this.props;

    const authToken = localStorage.getItem('access_token');

    this.setState({ isLoading: true });
    const self = this;

    const formData = new FormData()

    formData.append("bgImage", productImages)
    formData.append("title", title)
    formData.append("subTitle", subtitle)
    formData.append("category", category_id)
    axios
      .post(
        `${backendUrl}/api/homeBanner`,
        formData,
        {
          headers: {
            Authorization: `Bearer ${authToken}`,
            "content-type": "multipart/form-data",
          },
        }

      )
      .then(function (response) {
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        toastManager.add(
          error && error.response && error.response.data && error.response.data.message, {
          appearance: "warning",
          autoDismiss: true
        });
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 8080);
        console.log('rrrr err', error);
      });
  };

  validateForm = () => {
    const {
      category_id,
      title,
      subtitle,
    } = this.state;

    if (category_id === '') {
      this.setState({
        error: {
          errorText: 'Please select a category',
          errorType: 'category_id',
        },
      });
      return false;
    }


    if (title === '') {
      this.setState({
        error: {
          errorText: 'Please select a category',
          errorType: 'title',
        },
      });
      return false;
    }

    if (subtitle === '') {
      this.setState({
        error: {
          errorText: 'Please select a category',
          errorType: 'subtitle',
        },
      });
      return false;
    }

    if (this.props.type === 'edit') {
      if (
        this.state.editProductImages.length <= 0 &&
        this.state.editProductImagesObjects.length <= 0
      ) {
        console.log('inside image of invalida');
        this.setState({
          error: {
            errorText: 'Please Select Valid product image',
            errorType: 'edit_image',
          },
        });
        return false;
      }
    } else {
      if (
        this.state.productImages.length <= 0 ||
        this.state.productImages === undefined ||
        this.state.productImages === null
      ) {
        this.setState({
          error: {
            errorText: 'Please Select Valid product image',
            errorType: 'add_image',
          },
        });
        return false;
      }
    }
    return true;
  };

  componentDidMount() {
    const { getCategoriesDetails } = this.props;
    getCategoriesDetails();
  }
  handleAddData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.addNewApi();
    }
  };

  handleEditProductImage = index => {
    const { editProductImages } = this.state;
    const tempArr = [...editProductImages];

    tempArr.splice(index, 1);

    this.setState({
      editProductImages: tempArr,
    });
  };

  onDragChange = info => {
    const status = info.file.status;
    if (status !== 'uploading') {
      this.setState({
        productImages: info.file.originFileObj,
      });
    }
  };

  beforeUpload = file => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png' || file.type === "image/jpg";
    const isLt2M = file.size / 1024 / 1024 < 5;
    if (!isJpgOrPng) {

      this.setState({
        error: {
          errorText: 'Please select vaild add image (JPG,PNG,JPEG )',
          errorType: 'add_image',
        },
      });
      return false;
    } else if (!isLt2M) {
      this.setState({
        error: {
          errorText: 'Image must smaller than 5MB!',
          errorType: 'add_image',
        },
      });
      return false;
    } else {
      this.setState({
        error: {
          errorText: '',
          errorType: '',
        },
      });
    }
    return isJpgOrPng && isLt2M;
  };

  render() {
    const {
      category_id,
      title,
      subtitle,
      isLoading,
      apiError,
      error,
    } = this.state;

    const { category, type = 'add' } = this.props;
    const { errorType } = error;

    return (
      <div className="form-container">
        <GlobalStyle />

        <Row form>
          <Col md={12}>
            {type === 'add' && (

              <FormItem
                label="Category"
                hasFeedback
                className={cx({ 'has-error': errorType === 'category_id' })}>
                <Select
                  className="w-100"
                  name="category_id"
                  value={category_id}
                  placeholder="Select Category"
                  onChange={e => this.handleSelect('category_id', e)}
                >
                  {category && category.length > 0 && category.map((data, index) => {
                    return (
                      <Option key={index} value={data._id}>
                        {data.categoryName}
                      </Option>
                    );
                  })}
                </Select>
                {this.errorShow('category_id')}
              </FormItem>
            )}
          </Col>

        </Row>
        <Row form>
          <Col md={6}>
            <FormItem
              label="Title"
              hasFeedback
              className={cx({ 'has-error': errorType === 'title' })}
            >
              <Input
                maxLength={150}
                name={'title'}
                value={title}
                onChange={this.handleInput}
              />
              {this.errorShow('title')}
            </FormItem>
          </Col>

          <Col md={6}>
            <FormItem
              label="Sub Title"
              hasFeedback
              className={cx({ 'has-error': errorType === 'subtitle' })}
            >
              <Input
                maxLength={150}
                name={'subtitle'}
                value={subtitle}
                onChange={this.handleInput}
              />
              {this.errorShow('subtitle')}
            </FormItem>
          </Col>
        </Row>
        <FormItem label="Add Banner Image">
          <Dragger
            listType="picture"
            onChange={this.onDragChange}
            showUploadList={errorType == 'add_image' ? false : true}
            beforeUpload={this.beforeUpload}
            name={'file'}
            multiple={false}
            accept=".png, .jpg, .jpeg"
          >
            <p className="ant-upload-drag-icon">
              <Icon type="inbox" />
            </p>
            <p className="ant-upload-text">Click or drag file to this area to upload</p>
            <p className="ant-upload-hint">
              Support for a single or bulk upload. Strictly prohibit from uploading company data
              or other band files
            </p>
          </Dragger>
          Note: For better user experience upload image of  2559 X1333 pixels (square shape) image.
          {this.errorShow('add_image')}
        </FormItem>
        {isLoading &&
          <div className="mb-2 text-center">
            <Spin />
          </div>
        }

        <div className="text-right pt-3">
          <Button
            type="primary"
            htmlType="submit"
            className="btn-cta"
            onClick={this.handleAddData}
          >
            <Icon type="plus-circle" theme="filled" />
            Add
          </Button>
        </div>

        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  subCategory: state.subCategory.subCategory,
  category: state.category.category,
  banner: state.banner.banner
});

const mapDispatchToProps = dispatch => ({
  getBannerDetail: () => dispatch(getAllBannerDetail()),
  getCategoriesDetails: () => dispatch(getAllCategoryDetail()),
});

const WrappedFormComponent = Form.create()(withRouter(FormComponent));

export default withToastManager(connect(mapStateToProps, mapDispatchToProps)(WrappedFormComponent));

const GlobalStyle = createGlobalStyle`.ant-select-dropdown--multiple{display:none}`;