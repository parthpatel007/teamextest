import React from 'react';
import { HomeBannerDetailCard } from '../../../../components/Cards';
import { Link, withRouter } from 'react-router-dom';
import { Icon } from 'antd';
const BannerPreview = props => {
  
  const { match, history, location } = props;
  const { state } = location;

  console.log('STRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATESTRTATE', state)
  
  const backLink = match.url
    .split('/')
    .slice(0, -1)
    .join('/');
  
  
  return (
    <section>
      <div className="form-card__body p-lg-5 p-4">
        <Link to={backLink} className="ant-btn ant-btn-primary">
          <Icon type="caret-left" /> Back
        </Link>
      <HomeBannerDetailCard/>
      </div>
    </section>
  );
};

export default withRouter(BannerPreview);
