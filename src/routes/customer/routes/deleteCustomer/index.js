import React from 'react';
import { Modal, Button, Icon } from 'antd';
import axios from 'axios';
import { backendUrl } from '../../../../common/credentials';
import { createGlobalStyle } from 'styled-components';
export default class DeleteProduct extends React.Component {
  state = { visible: false };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = () => {
    this.setState({
      visible: false,
    });
  };
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  deleteApi = id => {
    const authToken = localStorage.getItem('access_token');
    axios
      .delete(`${backendUrl}/delete-customer/${id}`, {
        headers: {
          Authorization: authToken,
        },
      })
      .then(function (response) {
        this.handleOk()
        if (response) {
          window.location.reload();
        }
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };
  render() {
    const { recordData } = this.props;
    const { name, _id } = recordData;

    return (
      <div>
        <Button type="danger" className="ml-2" shape="circle" onClick={this.showModal}>
          <Icon type="delete" theme="filled" />
        </Button>
        <Modal
          title="Delete Customer"
          okType="danger"
          visible={this.state.visible}
          onOk={() => { this.deleteApi(_id) }}
          okText="Delete"
          onCancel={this.handleCancel}
          // footer={null}
          // className="custom-modal-v1"
          centered
        >
          <GlobalStyle />
          <section className="form-card">
            <div className="form-card__body ">
              <h4>
                Do you really want to delete customer <b>{name}</b>
              </h4>
            </div>
          </section>
        </Modal>
      </div>
    );
  }
}
const GlobalStyle = createGlobalStyle`
.ant-modal-title{
  font-size: 22px;
}
.ant-modal{  
width: 380px !important;
}
`;
