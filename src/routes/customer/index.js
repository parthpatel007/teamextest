import React from 'react';
import { Route } from 'react-router-dom';
import Customers from './routes';
import AddCustomers from './routes/add';
import EditCustomers from './routes/editCustomer';
// import AccountPreview from './routes/accountPreview/';

const Shop = ({ match }) => (
  <>
    <Route exact path={`${match.url}`} component={Customers} />
    {/* <Route path={`${match.url}/preview`} component={AccountPreview} /> */}
    <Route path={`${match.url}/add`} component={AddCustomers} />
    <Route path={`${match.url}/edit`} component={EditCustomers} />
 
  </>
);

export default Shop;
