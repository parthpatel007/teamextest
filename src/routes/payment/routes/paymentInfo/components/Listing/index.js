import Skeleton from 'react-loading-skeleton';
import React from 'react';
import { Table } from 'antd';
import avatarImage from '../../../../../../assets/images/avatar.jpg';
import classes from './style.module.scss';
import { Link } from 'react-router-dom';
import getAllPaymentDetail from '../../../../../../middleware/paymentMiddleware';
import { connect } from 'react-redux';

const data = [
  {
    key: '1',
    name: 'Jhone',
    email: 'Jhone@gmail.com',
    phone: '7104505987',
    image: avatarImage,
    method: 'Visa',
  },
  {
    key: '1',
    name: 'Eliate',
    email: 'Guest@gmail.com',
    phone: '9104505987',
    image: avatarImage,
    method: 'COD',
  },
];


class Article extends React.Component {
// const Article = props => {

state = {
  filterData: [],
  loader: true,
      listData: [] ,
}


  componentDidMount() {
    // this.getDataApi();
    const { getPaymentDetails, payment } = this.props;
  
    getPaymentDetails();
  
    this.setState({
      filterData: payment,
      listData: payment,
      // loader: payment.length >= 0 ? false : true
    });

    if(this.props.paymentSuccess){
      this.setState({
        loader: false
      });
    }
    else{
      this.setState({
        loader: true
      });
    }
  
  }
  
  componentDidUpdate(prevProps, prevState) {
  
    const { payment } = this.props;
  
    if (prevProps.payment !== payment) {
      this.setState({
        filterData: payment,
        listData: payment,
        loader: payment.length >= 0 ? false : true
      })
    }
  
  }
  

  render(){

  const { match } = this.props;
  const { filterData, loader } = this.state
//   username
// email
// mobile
// payment method
// amount
  const columns = [
    // {
    //   title: 'Person Image',
    //   dataIndex: 'image',
    //   key: 'image',
    //   render: src => (
    //     <div>
    //       <img className={classes.productImage} src={src} />
    //     </div>
    //   ),
    // },
    {
      title: 'Username',
      key: 'name',
      render: text => text && text.addedBy && text.addedBy.userName
    },
    {
      title: 'Email',
      render: text => text && text.addedBy &&  text.addedBy.email,
      key: 'email',
    },
    {
      title: 'Mobile',
      render: text => text && text.addedBy && text.addedBy.phone, 
      key: 'phone',
    },

    {
      title: 'Payment method',
      render: text => text && text.cardType, 
      key: 'method',
    },
    {
      title: 'Amount',
      render: text => text && '₹' +text.totalAmount, 
      key: 'method',
    },
    // {
    //   title: 'Action',
    //   key: 'action',
    //   render: (text, record) => (
    //     <span className="d-flex">
    //       {/* <EditCustomer recordData={text} /> */}
    //       <Link to={`${match.url}/edit`} className="ant-btn ant-btn-primary">
    //         <Icon type="edit" theme="filled" /> Edit
    //       </Link>
    //       <DeleteCustomer recordData={text} />
    //     </span>
    //   ),
    //   width: 200,
    // },
  ];
  return (
    <article className="article">
      <div className="d-flex justify-content-between">
        <h2 className="main-title  pt-0">Payment Listing</h2>
      </div>
      {loader &&
              <Skeleton height={50} count={10}/> 
              }
            {/* </SkeletonTheme> */}

            {!loader&&   
              <Table columns={columns} dataSource={this.state.filterData} className="ant-table-v1" scroll={{ x: 700 }} />
            }
    </article>
  );
  }
};


const mapStateToProps = state => ({
  payment: state.payment.payment,
  paymentSuccess: state.payment.paymentSuccess
})

const mapDispatchToProps = dispatch => ({
  getPaymentDetails: () => dispatch(getAllPaymentDetail())
})

export default connect(mapStateToProps, mapDispatchToProps)(Article);
