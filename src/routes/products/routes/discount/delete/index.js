import React from 'react';
import { Modal, Button, Icon } from 'antd';
import axios from 'axios';
import { backendUrl } from '../../../../../common/credentials';
import { withToastManager } from 'react-toast-notifications';

class Delete extends React.Component {
  state = { visible: false };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  deleteRecord = id => {
    const { toastManager } = this.props
    const self = this
    const authToken = localStorage.getItem('access_token');
    axios
      .delete(`${backendUrl}api/discounts/${id}`, {
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
      })
      .then(function (response) {
        self.setState({
          visible: false,
        });
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        self.props.getDiscountDetails()
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };

  render() {
    const { recordData } = this.props;
    const { _id, discountName } = recordData;
    return (
      <div>
        <Button type="danger" shape="circle" className="ml-2" onClick={this.showModal}>
          <Icon type="delete" theme="filled" />
        </Button>
        <Modal
          title="Delete Discount"
          okType="danger"
          visible={this.state.visible}
          onOk={() => this.deleteRecord(_id)}
          okText="Delete"
          onCancel={this.handleCancel}
          centered
        >
          <section className="form-card">
            <div className="form-card__body ">
              <label style={{ fontSize: 16 }}>
                Do you really want to delete discount <b>{discountName}</b>
              </label>
            </div>
          </section>
        </Modal>
      </div>
    );
  }
}
export default withToastManager(Delete)