import React from 'react';
import { Table, Icon, Form, Button, Input, Tooltip } from 'antd';
import Skeleton from 'react-loading-skeleton';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import { DiscountStatistics } from '../../../../components/Cards';
import { BlockModal } from '../../../../components/Modal';
import getAllDiscountDetail from '../../../../middleware/discountMiddleware';
import Delete from './delete';

const FormItem = Form.Item;
const expDateFormat = 'DD/MM/YYYY';

class SubCategories extends React.Component {
  state = {
    listData: [],
    search: '',
    categories: [],
    statusFilter: 0,
    subcategories: [],
    filterData: [],
  };

  componentDidMount() {
    const { getDiscountDetails } = this.props;
    const { discount } = this.props;
    getDiscountDetails();
    this.setState({
      filterData: discount,
      listData: discount,
    })
  }

  componentDidUpdate(prevProps, prevState) {
    const { discount } = this.props;
    if (prevProps.discount !== discount) {
      this.setState({
        filterData: discount,
        listData: discount,
      })
    }
  }
  handleSearch = e => {
    const { listData } = this.state;
    const filterDataHolder = [
      ...listData.filter(({ discountName }) =>
        discountName.toLowerCase().startsWith(
          e.target.value.trim().toLowerCase()
        )
      ),
    ];
    this.setState({
      [e.target.name]: e.target.value,
      filterData: filterDataHolder,
      statusFilter: 0,
    });
  };


  handleStatusFilter = stock => {
    const { listData, statusFilter } = this.state;
    let filterDataHolder = [];
    if (statusFilter === stock) {
      filterDataHolder = [...listData];
    } else {
      filterDataHolder = [
        ...listData.filter(
          ({ status }) => (stock === 1 && status === 0) || (stock === 2 && status === 1)
        ),
      ];
    }
    this.setState({
      search: '',
      statusFilter: stock !== statusFilter ? stock : 0,
      filterData: filterDataHolder,
    });
  };

  render() {
    const { match, getDiscountDetails, discountLoader } = this.props;
    const { listData, filterData, search, statusFilter } = this.state;
    const columns = [
      {
        title: 'Date',
        key: 'createdAt',
        dataIndex: 'createdAt',
        render: text => moment(text).format('DD-MM-YYYY'),
        width: 135,
      },
      {
        title: 'Offer Name',
        dataIndex: 'discountName',
        key: 'discountName',
      },
      {
        title: 'Type',
        dataIndex: 'type',
        key: 'discountName',
      },

      {
        title: 'Offer',
        key: 'offer',
        render: (text) => {
          if (text.measure === 'price') {
            return '₹' + text.offer;
          }
          else {
            return '%' + text.offer;
          }
        }
      },
      {
        title: 'Expiry Date',
        dataIndex: 'expiryDate',
        key: 'expiryDate',
        render: (text, row) => {
          return moment(text).format(expDateFormat)
        },
      },
      {
        title: 'Status',
        key: 'status',
        render: (text, record) => (
          <b className={`text-${text.status === 0 ? 'success' : 'danger'}`}>
            {text.status === 0 ? 'Active' : 'Inactive'}
          </b>
        ),
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span className="d-flex">

            <Tooltip placement="bottomLeft" title={"edit"}>
              <Link
                to={{ pathname: `${match.url}/edit`, state: text }}
                className="ant-btn ant-btn-primary ant-btn-circle"
              >
                <Icon type="edit" theme="filled" />
              </Link>
            </Tooltip>
            <Tooltip placement="bottomLeft" title="delete">
              <span>
                <Delete recordData={text} getDiscountDetails={getDiscountDetails} />
              </span>
            </Tooltip>

            <Tooltip placement="bottomLeft" title={text.status === 0 ? 'block' : 'unblock'}>
              <span>
                <BlockModal
                  recordData={text}
                  recordName={text.discountName}
                  api={'api/changeDiscountStatus'}
                  type={text.status === 0 ? 'block' : 'unblock'}
                  blockTitleText="Block Discount"
                  unblockTitleText="Unblock Discount"
                  blockText="block Discount"
                  unblockText="unblock Discount"
                  blockBtnText="Block Discount"
                  unblockBtnText="Unblock Discount"
                />
              </span>
            </Tooltip>

          </span>
        ),
        width: 140,
      },
    ];

    return (
      <>
        <DiscountStatistics
          inactive={listData.filter(({ status }) => status === 1).length}
        />

        <section className="container-fluid container-mw-xxl no-breadcrumb chapter">
          <article className="article pb-0">
            <div className="d-flex justify-content-between mb-2">
              <div className="d-flex">
                <FormItem>
                  <Input
                    placeholder="Search discount"
                    prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                    name="search"
                    value={search}
                    onChange={this.handleSearch}
                  />
                </FormItem>
              </div>
              <div>
                <Button
                  className="mx-2"
                  onClick={() => this.handleStatusFilter(1)}
                  type={statusFilter === 1 ? 'primary' : 'secondary'}
                >
                  Active
                </Button>
                <Button
                  className="mr-2"
                  onClick={() => this.handleStatusFilter(2)}
                  type={statusFilter === 2 ? 'primary' : 'secondary'}
                >
                  Inactive
                </Button>
                <Link to={`${match.url}/add`} className="ml-2 ant-btn ant-btn-primary">
                  <Icon type="plus-circle" theme="filled" /> Add
                </Link>
              </div>
            </div>
            {discountLoader ?
              <Skeleton height={50} count={10} /> :
              <Table
                columns={columns}
                dataSource={filterData}
                className="ant-table-v1"
                scroll={{ x: 1000 }}
                loading={discountLoader}
              />
            }
          </article>
        </section>
      </>
    );
  }
}

const mapStateToProps = state => ({
  discount: state.discount.discount,
  discountLoader: state.discount.discountLoader,
})

const mapDispatchToProps = dispatch => ({
  getDiscountDetails: () => dispatch(getAllDiscountDetail())
})

export default connect(mapStateToProps, mapDispatchToProps)(SubCategories);