import React from 'react';
import { Form, Input, Icon, Button, Select, Spin, Radio, InputNumber, DatePicker } from 'antd';
import { Row, Col } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { backendUrl } from '../../../../../common/credentials';
import cx from 'classnames';
import moment from 'moment'
import { connect } from 'react-redux';
import getAllCategoryDetail from '../../../../../middleware/categoryMiddleware';
import getAllProductDetail from '../../../../../middleware/productMiddleware';
import { getAllSubCategoryDetail, getAllSubCategoryDetailCategoryWise } from '../../../../../middleware/subCategoryMiddleware';
import getAllPackageListing from '../../../../../middleware/packageMiddleware';
import getAllProductCategoryWiseDetail from '../../../../../middleware/productCategoryWIseMiddleware';
import getAllProductSubCategoryWiseDetail from '../../../../../middleware/productSubCategoryWiseMiddleware';


/* eslint-disable */

const RadioGroup = Radio.Group;
const Option = Select.Option;
const FormItem = Form.Item;

const validateProdcut = email => {
  var re = /^[a-zA-Z_ ]+$/;
  return re.test(String(email).toLowerCase());
};
const dateFormat = 'YYYY-MM-DD';

const defaultState = {
  name: '',
  category_id: '',
  category_name: '',
  sub_category_id: '',
  package_id: '',
  expiry_date: '',
  product_id: '',
  sub_category_name: '',
  store_id: '',
  offer_price: 0,
  offerType: 'price',
};


class FormComponent extends React.Component {


  state = {
    min_off_amount: this.props.type === 'edit' ? this.props.editData && this.props.editData.minimumOrderAmount : 0,
    offer_pro_order_type: this.props.type === 'edit' ? this.props.editData && this.props.editData.type : 'on product',
    editSubCategoryId: this.props.type === 'edit' ? this.props.editData && this.props.editData.subCategoryId && this.props.editData.subCategoryId._id : '',
    editCategoryId: this.props.type === 'edit' ? this.props.editData && this.props.editData.categoryId && this.props.editData.categoryId._id : '',
    editProductId: this.props.type === 'edit' ? this.props.editData && this.props.editData.productId && this.props.editData.productId._id : '',
    editpackageId: this.props.type === 'edit' ? this.props.editData && this.props.editData.packageId && this.props.editData.packageId._id : '',
    ...(this.props.type === 'edit' ? this.props.editData : defaultState),
    isLoading: false,
    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };


  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };


  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };


  handleInput = e => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };


  handleSelect = (name, value) => {
    // this.clearError();
    console.log("THE NAME SELECTED :", name, "VALUE SELECTED:", value)
    this.setState({ [name]: value });
  };

  handleCategoriesSelect = (name, value) => {
    this.clearError();
    const filterRecord = this.state.categories.filter(data => data._id === value);
    const catName =
      filterRecord.length > 0 && filterRecord[0].title ? filterRecord[0].title : 'none';

    this.setState({ [name]: value, category_name: catName });
  };



  handleMinOfferAmount = (value) => {

    this.setState({ min_off_amount: value });

  }

  handleOfferPrice = (value) => {
    const { type } = this.props;
    if (type === 'edit') {
      this.setState({ offer: value });
    }
    if (type !== 'edit') {
      this.setState({ offer_price: value });
    }
  };


  handleSubCategoriesSelect = (name, value) => {
    this.clearError();
    const filterRecord = this.state.subcategories.filter(data => data._id === value);
    const subcatName =
      filterRecord.length > 0 && filterRecord[0].title ? filterRecord[0].title : 'none';
    this.setState({ [name]: value, sub_category_name: subcatName });
  };


  handleRadio = (name, value) => {
    this.clearError();
    this.setState({ [name]: value });
  };


  addNewApi = () => {

    const {
      category_id,
      package_id,
      sub_category_id,
      product_id,
      offer_pro_order_type,
      min_off_amount,
      name,
      offer_price,
      expiry_date,
      offerType,
    } = this.state;


    const { match, history } = this.props;

    const authToken = localStorage.getItem('access_token');

    this.setState({ isLoading: true });

    const self = this;

    const onMeal = {
      categoryId: category_id,
      subCategoryId: sub_category_id,
      productId: product_id,
    }

    const onPackage = {
      packageId: package_id,
    }

    const onOrder = {
      minimumOrderAmount: min_off_amount
    }


    const bodyData = {
      discountName: name,
      expiryDate: expiry_date,
      measure: offerType,
      type: offer_pro_order_type,
      offer: offer_price,
    }


    if (offer_pro_order_type === 'on product') {

      axios
        .post(
          `${backendUrl}api/discounts`, {
          ...bodyData,
          ...onMeal
        },
          {
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
              Authorization: `Bearer ${authToken}`,
            },
          }
        )
        .then(function (response) {
          console.log('rrrr res', response);
          history.push(
            match.url
              .split('/')
              .slice(0, -1)
              .join('/')
          );
          self.setState({ isLoading: false });
        })
        .catch(function (error) {
          console.log('rrrr err', error);
          self.setState({ isLoading: false });
          self.setState({
            isLoading: false,
            apiError: error.toString().includes('Network Error')
              ? 'Network Error'
              : error.response.data.message,
          });
          setTimeout(() => self.setState({ apiError: '' }), 3000);
        });

    }
    else {
      axios
        .post(
          `${backendUrl}api/discounts`, {
          ...bodyData,
          ...onOrder
        },
          {
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
              Authorization: `Bearer ${authToken}`,
            },
          }
        )
        .then(function (response) {
          console.log('rrrr res', response);
          history.push(
            match.url
              .split('/')
              .slice(0, -1)
              .join('/')
          );
          self.setState({ isLoading: false });
        })
        .catch(function (error) {
          console.log('rrrr err', error);
          self.setState({ isLoading: false });
          self.setState({
            isLoading: false,
            apiError: error.toString().includes('Network Error')
              ? 'Network Error'
              : error.response.data.message,
          });
          setTimeout(() => self.setState({ apiError: '' }), 3000);
        });

    }



  };
  editApi = () => {
    const {
      editCategoryId,
      editSubCategoryId,
      editProductId,
      offer_pro_order_type,
      min_off_amount,
      discountName,
      offer,
      expiryDate,
      measure,
    } = this.state;

    const { _id } = this.props.editData;

    const { match, history } = this.props;

    const authToken = localStorage.getItem('access_token');

    this.setState({ isLoading: true });
    const self = this;
    const onMeal = {
      categoryId: editCategoryId,
      subCategoryId: editSubCategoryId,
      productId: editProductId,
    }

    const onOrder = {
      minimumOrderAmount: min_off_amount
    }
    const bodyData = {
      discountName: discountName,
      expiryDate: expiryDate,
      measure: measure,
      type: offer_pro_order_type,
      offer: offer,
    }

    if (offer_pro_order_type === 'on product') {

      axios
        .post(
          `${backendUrl}api/discounts/${_id}`,
          {
            ...bodyData,
            ...onMeal
          },
          {
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
              Authorization: `Bearer ${authToken}`,
            },
          }
        )
        .then(function (response) {
          console.log('rrrr res', response);
          history.push(
            match.url
              .split('/')
              .slice(0, -1)
              .join('/')
          );
          self.setState({ isLoading: false });
        })
        .catch(function (error) {
          console.log('rrrr err', error);
          self.setState({ isLoading: false });
          self.setState({
            isLoading: false,
            apiError: error.toString().includes('Network Error')
              ? 'Network Error'
              : error.response.data.message,
          });
          setTimeout(() => self.setState({ apiError: '' }), 3000);
        });

    }
    else {

      axios
        .post(
          `${backendUrl}api/discounts/${_id}`,
          {
            ...bodyData,
            ...onOrder
          },
          {
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
              Authorization: `Bearer ${authToken}`,
            },
          }
        )
        .then(function (response) {
          console.log('rrrr res', response);
          history.push(
            match.url
              .split('/')
              .slice(0, -1)
              .join('/')
          );
          self.setState({ isLoading: false });
        })
        .catch(function (error) {
          console.log('rrrr err', error);
          self.setState({ isLoading: false });
          self.setState({
            isLoading: false,
            apiError: error.toString().includes('Network Error')
              ? 'Network Error'
              : error.response.data.message,
          });
          setTimeout(() => self.setState({ apiError: '' }), 3000);
        });
    }
  };

  getStoreApi = () => {
    var self = this;
    const authToken = localStorage.getItem('access_token');
    axios
      .get(`${backendUrl}/get-store`, {
        headers: {
          Authorization: authToken,
        },
      })
      .then(function (response) {
        console.log('rrrr res', response.data.data);
        if (response) {
          self.setState({
            store: response.data.data,
          });
        }
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };


  getCategoriesApi = () => {
    var self = this;
    const authToken = localStorage.getItem('access_token');
    axios
      .get(`${backendUrl}/get-category`, {
        headers: {
          Authorization: authToken,
        },
      })
      .then(function (response) {
        console.log('rrrr res', response.data.data);
        if (response) {
          self.setState({
            categories: response.data.data,
          });
        }
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };


  getSubCategoriesApi = () => {
    var self = this;
    const authToken = localStorage.getItem('access_token');
    axios
      .get(`${backendUrl}/get-subcategory`, {
        headers: {
          Authorization: authToken,
        },
      })
      .then(function (response) {
        console.log('rrrr res', response.data.data);
        if (response) {
          self.setState({
            subcategories: response.data.data,
          });
        }
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };

  componentDidMount() {
    const { type, getCategoryDetail, getSubCategoryDetail, getProductsDetails } = this.props;
    const { offer_pro_order_type } = this.state;

    if (offer_pro_order_type == 'on product') {
      getCategoryDetail();
      getSubCategoryDetail();
      getProductsDetails();
      if (type === 'edit') {
        getCategoryDetail();
        this.props.getAllSubCategoryDetailCategoryWiseFunction(this.state.editCategoryId)
        this.props.getProductsSubCategoryWise(this.state.editCategoryId, this.state.editSubCategoryId)
      }
    }
  }


  componentDidUpdate(prevProps, prevState) {

    const {
      getProductsCategoryWise,
      getProductsSubCategoryWise,
      getAllSubCategoryDetailCategoryWiseFunction, getPackagesDetail, type, product } = this.props
    const { offer_pro_order_type, category_id, sub_category_id, editCategoryId, editSubCategoryId, product_id, editProductId } = this.state;
    if (type === 'edit') {
      if (prevState.offer_pro_order_type !== offer_pro_order_type) {
        if (offer_pro_order_type == 'on product') {
          getPackagesDetail()
        }
      }
      else {
        if (offer_pro_order_type == 'on product') {
          if (prevState.editCategoryId !== this.state.editCategoryId) {
            getAllSubCategoryDetailCategoryWiseFunction(editCategoryId)
            if (editCategoryId !== '' && editSubCategoryId == '' && editProductId == '') {
              getProductsCategoryWise(editCategoryId)
              this.setState({
                filterData: product,
                listData: product,
              })
            }
            if (editCategoryId !== '' && editSubCategoryId !== '' && editProductId == '') {
              // getProductsSubCategoryWise(defaultStoreId, defaultSuperCatId, categoryId, subCategoryId)
              this.setState({
                editSubCategoryId: '',
                filterData: product,
                listData: product,
              })
              getProductsCategoryWise(editCategoryId)
            }
            if (editCategoryId !== '' && editSubCategoryId !== '' && editProductId !== '') {
              // getProductsSubCategoryWise(defaultStoreId, defaultSuperCatId, categoryId, subCategoryId)
              this.setState({
                editSubCategoryId: '',
                editProductId: '',
                filterData: product,
                listData: product,
              })
              getProductsCategoryWise(editCategoryId)
            }
          }

          if (prevState.editSubCategoryId !== editSubCategoryId) {
            if (editCategoryId !== '' && editSubCategoryId !== '') {
              getProductsSubCategoryWise(editCategoryId, editSubCategoryId)
              this.setState({
                filterData: product,
                listData: product,
              })
            }
          }

          if (prevProps.product !== product) {
            this.setState({
              filterData: product,
              listData: product,
            })
          }

        }
      }

    }
    else {


      if (prevState.offer_pro_order_type !== offer_pro_order_type) {

        if (offer_pro_order_type == 'on product') {
          getPackagesDetail()
        }
      }
      else {
        if (offer_pro_order_type == 'on product') {

          if (prevState.category_id !== category_id) {
            getAllSubCategoryDetailCategoryWiseFunction(category_id)

            if (category_id !== '' && sub_category_id == '' && product_id == '') {
              getProductsCategoryWise(category_id)
              this.setState({
                filterData: product,
                listData: product,
              })
            }
            if (category_id !== '' && sub_category_id !== '' && product_id == '') {
              this.setState({
                sub_category_id: '',
                filterData: product,
                listData: product,
              })
              getProductsCategoryWise(category_id)
            }
            if (category_id !== '' && sub_category_id !== '' && product_id !== '') {
              this.setState({
                sub_category_id: '',
                product_id: '',
                filterData: product,
                listData: product,
              })
              getProductsCategoryWise(category_id)
            }
          }

          if (prevState.sub_category_id !== sub_category_id) {
            if (category_id !== '' && sub_category_id !== '' && product_id == '') {
              getProductsSubCategoryWise(category_id, sub_category_id)
              this.setState({
                filterData: product,
                listData: product,
              })
            }

            if (category_id !== '' && sub_category_id !== '' && product_id !== '') {
              this.setState({
                product_id: '',
                filterData: product,
                listData: product,
              })
              getProductsSubCategoryWise(category_id, sub_category_id)
            }
          }

          if (prevProps.product !== product) {
            this.setState({
              filterData: product,
              listData: product,
            })
          }

        }
      }
    }
  }
  validateForm = () => {
    const {
      name,
      offer,
      offerType,
      offer_price,
      offer_pro_order_type,
      sub_category_id,
      category_id,
      product_id,
      packageId,
      editSubCategoryId,
      editCategoryId,
      measure,
      expiryDate,
      editProductId,
      expiry_date,
      discountName,
      package_id,
      min_off_amount,
    } = this.state;

    const { type } = this.props;

    if (type === 'edit') {
      if (offer_pro_order_type == 'on product') {
        if (editCategoryId === '') {
          this.setState({
            error: {
              errorText: 'Please select category',
              errorType: 'category_id',
            },
          });
          return false;
        }
        if (editSubCategoryId === '') {
          this.setState({
            error: {
              errorText: 'Please select subcategory',
              errorType: 'sub_category_id',
            },
          });
          return false;
        }
        if (editProductId === '') {
          this.setState({
            error: {
              errorText: 'Please select product',
              errorType: 'product_id',
            },
          });
          return false;
        }
        if (discountName === '') {
          this.setState({
            error: {
              errorText: 'Please enter discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (!validateProdcut(discountName)) {
          this.setState({
            error: {
              errorText: 'Please enter vaild  discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (expiryDate === '') {
          this.setState({
            error: {
              errorText: 'Please select expiry date',
              errorType: 'expiryDate',
            },
          });
          return false;
        }
        if (measure === '') {
          this.setState({
            error: {
              errorText: 'Please select offer type',
              errorType: 'offerType',
            },
          });
          return false;
        }
        // return true;

      }
      else if (offer_pro_order_type == 'on order') {

        if (min_off_amount === undefined || min_off_amount === 0 || min_off_amount === '' || min_off_amount === null || parseInt(min_off_amount) < 0 || parseInt(min_off_amount) > 1000000) {
          this.setState({
            error: {
              errorText: 'Please enter valid minimum offer amount',
              errorType: 'min_off_amount',
            },
          });
          return false;
        }
        if (parseInt(min_off_amount) < 0 || parseInt(min_off_amount) > 1000000) {
          this.setState({
            error: {
              errorText: 'Please Enter valid minimum offer amount',
              errorType: 'min_off_amount',
            },
          });
          return false;
        }

        if (discountName === '') {
          this.setState({
            error: {
              errorText: 'Please enter discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (!validateProdcut(discountName)) {
          this.setState({
            error: {
              errorText: 'Please enter vaild  discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (expiryDate === '') {
          this.setState({
            error: {
              errorText: 'Please select expiry date',
              errorType: 'expiryDate',
            },
          });
          return false;
        }
        if (offer === "" || offer === 0 || offer === null || offer === undefined) {
          this.setState({
            error: {
              errorText: 'Please enter offer price',
              errorType: 'offer',
            },
          });
          return false;
        }
        if (measure === '') {
          this.setState({
            error: {
              errorText: 'Please select offer type',
              errorType: 'offerType',
            },
          });
          return false;
        }
        // return true;

      }
      else {

        console.log("inside edit on ")

        if (packageId === undefined || packageId === 0 || packageId === '') {
          this.setState({
            error: {
              errorText: 'Please select package',
              errorType: 'package_id',
            },
          });
          return false;
        }
        if (discountName === '') {
          this.setState({
            error: {
              errorText: 'Please enter discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (!validateProdcut(discountName)) {
          this.setState({
            error: {
              errorText: 'Please enter vaild  discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (expiryDate === '') {
          this.setState({
            error: {
              errorText: 'Please select expiry date',
              errorType: 'expiryDate',
            },
          });
          return false;
        }
        if (offer === "" || offer === 0 || offer === null || offer === undefined) {
          this.setState({
            error: {
              errorText: 'Please enter offer price',
              errorType: 'offer',
            },
          });
          return false;
        }
        if (measure === '') {
          this.setState({
            error: {
              errorText: 'Please select offer type',
              errorType: 'offerType',
            },
          });
          return false;
        }
        // return true;

      }

      if (measure == 'price') {

        if (offer === "" || offer == 0 || offer === null || offer === undefined) {
          this.setState({
            error: {
              errorText: 'Please enter offer price',
              errorType: 'offer',
            },
          });
          return false;
        }

        if (parseInt(offer) < 0 || parseInt(offer) > 1000000) {
          this.setState({
            error: {
              errorText: 'Please enter valid offer price',
              errorType: 'offer',
            },
          });
          return false;
        }

      }
      else {

        if (offer === "" || offer == 0 || offer === null || offer === undefined) {
          this.setState({
            error: {
              errorText: 'Please enter offer percentage',
              errorType: 'offer',
            },
          });
          return false;
        }

        if (parseInt(offer) < 0 || parseInt(offer) > 100) {
          this.setState({
            error: {
              errorText: 'Please enter valid offer percentage',
              errorType: 'offer',
            },
          });
          return false;
        }
      }
      return true

    }
    else {

      if (offer_pro_order_type == 'on product') {

        if (category_id === '') {
          console.log("instide category print", offer_pro_order_type, "type", type, "category_id", category_id)

          this.setState({
            error: {
              errorText: 'Please select category',
              errorType: 'category_id',
            },
          });
          return false;
        }
        if (sub_category_id === '') {
          this.setState({
            error: {
              errorText: 'Please select subcategory',
              errorType: 'sub_category_id',
            },
          });
          return false;
        }
        if (product_id === '') {
          this.setState({
            error: {
              errorText: 'Please select product',
              errorType: 'product_id',
            },
          });
          return false;
        }
        if (name === '') {
          this.setState({
            error: {
              errorText: 'Please enter discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (!validateProdcut(name)) {
          this.setState({
            error: {
              errorText: 'Please enter vaild  discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (expiry_date === '') {
          this.setState({
            error: {
              errorText: 'Please select expiry date',
              errorType: 'expiryDate',
            },
          });
          return false;
        }
        if (offer_price == 0) {
          this.setState({
            error: {
              errorText: 'Please enter offer price',
              errorType: 'offer_price',
            },
          });
          return false;
        }
      }
      else if (offer_pro_order_type == 'on order') {


        if (min_off_amount === 0) {
          this.setState({
            error: {
              errorText: 'Please enter minimum offer amount',
              errorType: 'min_off_amount',
            },
          });
          return false;
        }
        if (name === '') {
          this.setState({
            error: {
              errorText: 'Please enter discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (!validateProdcut(name)) {
          this.setState({
            error: {
              errorText: 'Please enter vaild  discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (expiry_date === '') {
          this.setState({
            error: {
              errorText: 'Please select expiry date',
              errorType: 'expiryDate',
            },
          });
          return false;
        }
        if (offer_price == 0) {
          this.setState({
            error: {
              errorText: 'Please enter offer price',
              errorType: 'offer_price',
            },
          });
          return false;
        }
        if (offerType === '') {
          this.setState({
            error: {
              errorText: 'Please select offer type',
              errorType: 'offerType',
            },
          });
          return false;
        }
        // return true;
      }
      else {


        if (package_id === '') {
          this.setState({
            error: {
              errorText: 'Please select package',
              errorType: 'package_id',
            },
          });
          return false;
        }
        if (name === '') {
          this.setState({
            error: {
              errorText: 'Please enter discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (!validateProdcut(name)) {
          this.setState({
            error: {
              errorText: 'Please enter vaild  discount name',
              errorType: 'name',
            },
          });
          return false;
        }
        if (expiry_date === '') {
          this.setState({
            error: {
              errorText: 'Please select expiry date',
              errorType: 'expiryDate',
            },
          });
          return false;
        }
        if (offer_price == 0) {
          this.setState({
            error: {
              errorText: 'Please enter offer price',
              errorType: 'offer_price',
            },
          });
          return false;
        }
        if (offerType === '') {
          this.setState({
            error: {
              errorText: 'Please select offer type',
              errorType: 'offerType',
            },
          });
          return false;
        }
        // return true;
      }
      if (offerType == 'price') {

        if (offer_price === "" || offer_price == 0 || offer_price === null || offer_price === undefined) {
          this.setState({
            error: {
              errorText: 'Please enter offer price',
              errorType: 'offer_price',
            },
          });
          return false;
        }

        if (parseInt(offer_price) < 0 || parseInt(offer_price) > 1000000) {
          this.setState({
            error: {
              errorText: 'Please enter valid offer price',
              errorType: 'offer_price',
            },
          });
          return false;
        }

      }
      else {

        if (offer_price === "" || offer_price == 0 || offer_price === null || offer_price === undefined) {
          this.setState({
            error: {
              errorText: 'Please enter offer percentage',
              errorType: 'offer_price',
            },
          });
          return false;
        }

        if (parseInt(offer_price) < 0 || parseInt(offer_price) > 100) {
          this.setState({
            error: {
              errorText: 'Please enter valid offer percentage',
              errorType: 'offer_price',
            },
          });
          return false;
        }
      }
      return true
    }
  };

  handleAddData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.addNewApi();
    }
  };


  handleEditData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.editApi();
    }
  };

  disabledDate = current => {
    // Can not select days before today and today
    return current && current < moment().endOf('day');
  }

  onEditDateChange = (date, dateString) => {
    console.log('eee', date, 'dddd', dateString);
    // dateString
    const dem = new Date(dateString).getTime()
    this.setState({ expiryDate: dem });
  };

  onDateChange = (date, dateString) => {
    console.log('eee', date, 'dddd', dateString);
    // dateString
    const dem = new Date(dateString).getTime()
    this.setState({ expiry_date: dem });
  };


  render() {

    const { type = 'add', subCategory, product, category, editData, match, history } = this.props;

    const {
      name,
      offer,
      offerType,
      offer_price,
      isLoading,
      offer_pro_order_type,
      sub_category_id,
      category_id,
      product_id,
      editSubCategoryId,
      editCategoryId,
      measure,
      editProductId,
      expiryDate,
      discountName,
      min_off_amount,
      apiError,
      error,
    } = this.state;


    const { errorType, errorText } = error;

    if (type === 'edit') {
      if (!editData) {
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        return null;
      }
    }

    return (

      <div className="form-container">


        <FormItem
          label="Offer Product / Order Type : "
          hasFeedback
          className={cx({ 'has-error': errorType === 'offer_pro_order_type' })}
        >
          <Select
            className="w-100"
            name="offer_pro_order_type"
            disabled={type === 'edit' ? true : false}
            value={type === 'edit' ? offer_pro_order_type : offer_pro_order_type}
            placeholder="Select  Offer Product / Order Type"
            onChange={e => this.handleSelect(type === 'edit' ? 'offer_pro_order_type' : 'offer_pro_order_type', e)}
          >
            <Option key={0} value={'on product'}>{'On Product'}</Option>
            <Option key={1} value={'on order'}>{'On Order'}</Option>
          </Select>
          {this.errorShow('offer_pro_order_type')}
        </FormItem>


        {offer_pro_order_type == 'on product' && (<Row form>
          <Col md={6}>
            <FormItem
              label="Category"
              hasFeedback
              className={cx({ 'has-error': errorType === 'category_id' })}
            >
              <Select
                className="w-100"
                name="category_id"
                // value={category_id}
                disabled={offer_pro_order_type === '' ? true : false}
                defaultValue={type === 'edit' ? editCategoryId : category_id}
                placeholder="Select Category"
                onChange={e => this.handleSelect(type === 'edit' ? 'editCategoryId' : 'category_id', e)}>

                {type === 'edit' && (
                  offer_pro_order_type !== '' && (

                    category && category.map((data, index) => {
                      if (data.status === 1) {
                        return null;
                      }
                      return (
                        <Option key={index} value={data._id}>
                          {data.categoryName}
                        </Option>
                      );
                    })
                  )
                )}

                {type !== 'edit' && (
                  offer_pro_order_type !== '' && (

                    category && category.map((data, index) => {
                      if (data.status === 1) {
                        return null;
                      }
                      return (
                        <Option key={index} value={data._id}>
                          {data.categoryName}
                        </Option>
                      );
                    })

                  )
                )}

              </Select>
              {this.errorShow('category_id')}

            </FormItem>
          </Col>

          <Col md={6}>
            <FormItem
              label="Sub Category"
              hasFeedback
              className={cx({ 'has-error': errorType === 'sub_category_id' })}
            >
              <Select
                className="w-100"
                name="sub_category_id"
                value={type === 'edit' ? editSubCategoryId : sub_category_id}
                disabled={category_id == '' ? true : false}
                placeholder="Select Category"
                onChange={e => this.handleSelect(type === 'edit' ? 'editSubCategoryId' : 'sub_category_id', e)}>


                {type === 'edit' && (
                  editCategoryId !== '' && (
                    subCategory.map((data, index) => {
                      if (data.status === 1) {
                        return null;
                      }
                      return (
                        <Option key={index} value={data._id}>
                          {data.subCategoryName}
                        </Option>
                      );
                    })
                  )
                )}

                {type !== 'edit' && (
                  category_id !== '' && (
                    subCategory.map((data, index) => {
                      return (
                        <Option key={index} value={data._id}>
                          {data.subCategoryName}
                        </Option>
                      );
                    })
                  )
                )}
              </Select>
              {this.errorShow('sub_category_id')}
            </FormItem>
          </Col>
        </Row>
        )}

        {offer_pro_order_type == 'on product' && (
          <Row form>
            <Col md={12}>
              <FormItem
                label="Product"
                hasFeedback
                className={cx({ 'has-error': errorType === 'product_id' })}
              >
                <Select
                  className="w-100"
                  name="product_id"
                  disabled={sub_category_id === '' ? true : false}
                  value={type === 'edit' ? editProductId : product_id}
                  placeholder="Select Product"
                  onChange={e => this.handleSelect(type === 'edit' ? 'editProductId' : 'product_id', e)}>

                  {type === 'edit' && (
                    editSubCategoryId !== '' && (
                      product.map((data, index) => {
                        if (data.status === 1) {
                          return null;
                        }
                        return (
                          <Option key={index} value={data._id}>
                            {data.productName}
                          </Option>
                        );
                      })
                    )
                  )}

                  {type !== 'edit' && (
                    sub_category_id !== '' && (
                      product.map((data, index) => {
                        return (
                          <Option key={index} value={data._id}>
                            {data.productName}
                          </Option>
                        );
                      })

                    )
                  )}

                </Select>
                {this.errorShow('product_id')}
              </FormItem>
            </Col>
          </Row>)}


        {offer_pro_order_type == 'on order' && (

          <Row form>
            <Col md={12}>
              <FormItem
                label="Minimum Offer Amount"
                className={cx({ 'has-error': errorType === 'min_off_amount' })}
                hasFeedback
              >
                <InputNumber
                  style={{ width: '100%' }}
                  name="min_off_amount"
                  min="0"
                  value={min_off_amount}
                  onChange={this.handleMinOfferAmount} />
                {this.errorShow('min_off_amount')}
              </FormItem>
            </Col>
          </Row>
        )}
        <Row form>
          <Col md={6}>
            <FormItem
              label="Offer Name"
              className={cx({ 'has-error': errorType === 'name' })}
              hasFeedback>
              <Input
                maxLength={25}
                name={type === 'edit' ? "discountName" : "name"}
                value={type === 'edit' ? discountName : name}
                onChange={this.handleInput} />
              {this.errorShow('name')}
            </FormItem>
          </Col>

          <Col md={6}>
            <FormItem
              label="Expiry Date"
              className={cx({ 'has-error': errorType === 'expiryDate' })}
              hasFeedback
            >
              {type === 'edit' && (
                <DatePicker
                  defaultValue={moment(expiryDate, dateFormat)}
                  style={{ width: '100%' }}
                  format={dateFormat}
                  disabledDate={this.disabledDate}
                  onChange={this.onEditDateChange} />
              )}

              {type !== 'edit' && (
                <DatePicker
                  style={{ width: '100%' }}
                  format={dateFormat}
                  disabledDate={this.disabledDate}
                  onChange={this.onDateChange} />
              )}
              {this.errorShow('expiryDate')}
            </FormItem>
          </Col>
        </Row>

        <Row form>
          <Col md={6}>
            <FormItem label="Offer Type" hasFeedback>
              <RadioGroup
                onChange={e => this.handleRadio(type === 'edit' ? 'measure' : 'offerType', e.target.value)}
                value={type === 'edit' ? measure : offerType}>
                <Radio value="price">₹ (Price)</Radio>
                <Radio value="percentage">% (Percentage)</Radio>
              </RadioGroup>
            </FormItem>
          </Col>
          <Col md={6}>
            <FormItem
              label={`Offer ${type === 'edit' ? measure : offerType === 'price' ? 'Price' : 'Percentage'}`}
              className={cx({ 'has-error': errorType === 'offer_price' })}
              hasFeedback
            >
              <Input
                type={'number'}
                name={type === 'edit' ? "offer" : "offer_price"}
                value={type === 'edit' ? offer : offer_price}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? "offer" : "offer_price")}
            </FormItem>
          </Col>
        </Row>



        {isLoading && (
          <div className="mb-2 text-center">
            <Spin />
          </div>
        )}

        <div className="text-right pt-3">
          {type === 'add' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              disabled={isLoading}
              onClick={this.handleAddData}
            >
              <Icon type="plus-circle" theme="filled" />
              Add
            </Button>
          )}
          {type === 'edit' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              disabled={isLoading}
              onClick={this.handleEditData}
            >
              <Icon type="edit" theme="filled" /> Save
            </Button>
          )}
        </div>
        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}
      </div>


    );
  }
}


const mapStateToProps = state => ({
  category: state.category.category,
  subCategory: state.subCategory.subCategory,
  product: state.product.product,
  packages: state.packages.packages
})

const mapDispatchToProps = dispatch => ({
  getProductsDetails: () => dispatch(getAllProductDetail()),
  getProductsCategoryWise: (categoryId) => dispatch(getAllProductCategoryWiseDetail(categoryId)),
  getProductsSubCategoryWise: (categoryId, subCategoryId) => dispatch(getAllProductSubCategoryWiseDetail(categoryId, subCategoryId)),
  getCategoryDetail: () => dispatch(getAllCategoryDetail()),
  getSubCategoryDetail: () => dispatch(getAllSubCategoryDetail()),
  getAllSubCategoryDetailCategoryWiseFunction: (category) => dispatch(getAllSubCategoryDetailCategoryWise(category)),
  getPackagesDetail: () => dispatch(getAllPackageListing()),
})

const WrappedFormComponent = Form.create()(withRouter(FormComponent));

export default connect(mapStateToProps, mapDispatchToProps)(WrappedFormComponent);
