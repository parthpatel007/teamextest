import React from 'react';
import { Form, Input, Icon, Button, Spin } from 'antd';
import cx from 'classnames';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { backendUrl } from '../../../../../common/credentials';
import CKEditor from '../../../../../components/CKEditor';
import { withToastManager } from 'react-toast-notifications';

/* eslint-disable */
const FormItem = Form.Item;

const validateProdcut = email => {
  var re = /^[a-zA-Z_ ]+$/;  
  return re.test(String(email).toLowerCase());
};


const defaultState = {
  categoryName: '',
  category_image: '',
  description: '',
};


class FormComponent extends React.Component {


  state = {
    ...(this.props.type === 'edit' ? this.props.editData : defaultState),
    isLoading: false,
    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };


  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };


  handleInput = e => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };


  handleEditor = (state, content) => {
    this.clearError();
    this.setState({ [state]: content });
  };


  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };


  addNewApi = () => {
    const {toastManager} = this.props
    const { categoryName, description } = this.state;
    const { match, history } = this.props;
    this.setState({ isLoading: true });
    const self = this;
    const authToken = localStorage.getItem('access_token');
    axios
      .post(
        `${backendUrl}/api/categories`,
        {
          categoryName: categoryName,
          description: description
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        console.log('Add Category Response', response);
  
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        console.log('Add Category Error', error);
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error' : error.toString().includes('Duplicate Entry') ? "Category Already exist" : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
      });
  };



  editApi = () => {
    

    const { categoryName, description } = this.state;
    const { _id } = this.props.editData;
    const { match, history } = this.props;
    const { toastManager } = this.props
    this.setState({ isLoading: true });
    const self = this;
    const authToken = localStorage.getItem('access_token');
    axios
      .post(
        `${backendUrl}/api/categories/${_id}`,
        {
          categoryName: categoryName,
          description: description
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        console.log('Update Category Response', response);
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
  
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        console.log('Update Category Response', error);
        toastManager.add(
          error  && error.response && error.response.data &&  error.response.data.message, {
            appearance: "warning",
            autoDismiss: true
          });
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : 'Somthing went wrong',
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
      });
  };



  validateForm = () => {
    const { categoryName, description } = this.state;
    if (categoryName.trim() == '') {
      this.setState({
        error: {
          errorText: 'Please enter category name',
          errorType: 'categoryName',
        },
      });
      return false;
    }
    if (!validateProdcut(categoryName)) {
      this.setState({
        error: {
          errorText: 'Please enter vaild category name',
          errorType: 'categoryName',
        },
      });
      return false;
    }
    if (description === '') {
      this.setState({
        error: {
          errorText: 'Please enter description',
          errorType: 'description',
        },
      });
      return false;
    }
    return true;
  };



  handleAddData = () => {
    // this.clearError();
    if (this.validateForm()) {
      this.addNewApi();
    }
  };



  handleEditData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.editApi();
    }
  };



  render() {


    const { type = 'add', editData, match, history } = this.props;

    const { categoryName, description, isLoading, apiError, error } = this.state;

    const { errorType } = error;

    if (type === 'edit') {

      if (!editData) {
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        return null;
      }

    }
    return (

      <div className="form-container">

        <FormItem
          label="Category Name"
          hasFeedback
          className={cx({ 'has-error': errorType === 'categoryName' })}
        >

          <Input maxLength={20} name="categoryName" value={categoryName} onChange={this.handleInput} />
          {this.errorShow('categoryName')}

        </FormItem>
        <FormItem label="Description" className={cx({ 'has-error': errorType === 'description' })}>
          <CKEditor name="description" value={description} onUpdate={this.handleEditor} />
          {this.errorShow('description')}
        </FormItem>


        {isLoading && (
          <div className="mb-2 text-center">
            <Spin />
          </div>
        )}


        <div className="text-right pt-3">

          {type === 'add' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              onClick={this.handleAddData}
              disabled={isLoading}
            >
              <Icon type="plus-circle" theme="filled" />
              Add
            </Button>
          )}


          {type === 'edit' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              onClick={this.handleEditData}
              disabled={isLoading}
            >
              <Icon type="edit" theme="filled" /> Save
            </Button>
          )}


        </div>


        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}


      </div>
    );
  }
}

const WrappedFormComponent = Form.create()(withRouter(FormComponent));

export default withToastManager(WrappedFormComponent);
