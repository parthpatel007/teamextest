import React from 'react';
import { Table, Tooltip, Icon, Form, Input, Button, DatePicker } from 'antd';
import Delete from './delete';
import { Link } from 'react-router-dom';
import { BlockModal } from '../../../../components/Modal';
import { InfoModal } from '../../../../components/Modal';
import { CategoryStatistics } from '../../../../components/Cards';
import ReactHtmlParser from 'react-html-parser';
import getAllCategoryDetail from '../../../../middleware/categoryMiddleware';
import { connect } from 'react-redux';
import Skeleton from 'react-loading-skeleton';

/* eslint-disable */
const { RangePicker } = DatePicker;
const FormItem = Form.Item;
class SubCategories extends React.Component {

  state = { startDate: '', loader: true, endDate: '', listData: [], search: '', filterData: [], statusFilter: 0 };


  getDataRangeApi = () => {
    const { startDate, endDate, listData } = this.state;
    const filterDataHolder = [
      ...listData.filter(
        ({ createdAt }) =>
          (new Date(createdAt).getTime() > new Date(startDate).getTime() && new Date(createdAt).getTime() < new Date(endDate).getTime())
      )
    ];
    this.setState({
      filterData: filterDataHolder,
      statusFilter: 0,
    });
  };


  componentDidMount() {

    const { getCategoriesDetails, category } = this.props;

    getCategoriesDetails();

    this.setState({
      filterData: category,
      listData: category,

    });
  }

  componentDidUpdate(prevProps, prevState) {

    const { category } = this.props;

    if (prevProps.category !== category) {
      this.setState({
        filterData: category,
        listData: category
      })
    }

  }


  handleSearch = e => {
    const { listData } = this.state;
    const filterDataHolder = [
      ...listData.filter(({ categoryName }) =>
        categoryName.toLowerCase().startsWith(
          e.target.value.trim().toLowerCase()
          // && store_id === this.state.store_id
        )
      ),
    ];
    this.setState({
      [e.target.name]: e.target.value,
      filterData: filterDataHolder,
      statusFilter: 0,
    });
  };



  handleStatusFilter = stock => {
    const { listData, statusFilter } = this.state;

    let filterDataHolder = [];
    if (statusFilter === stock) {
      filterDataHolder = [...listData];
    } else {
      filterDataHolder = [
        ...listData.filter(
          ({ status }) => (stock === 1 && status === 0) || (stock === 2 && status === 1)
        ),
      ];
    }
    this.setState({
      search: '',
      statusFilter: stock !== statusFilter ? stock : 0,
      filterData: filterDataHolder,
    });
  };


  onDateChange = (date, dateString) => {
    const { listData } = this.state
    // if(startDate !== ''){
    //   this.setState({

    //   });
    // }
    if (date.length == 0) {
      this.setState({ filterData: listData })
    }
    console.log('eee', date, 'dddd', dateString);
    this.setState({ startDate: dateString[0], endDate: dateString[1] });
  };


  onRangeSearch = () => {
    this.getDataRangeApi();
  };


  render() {


    const { match, category, getCategoriesDetails, categoryLoader } = this.props;
    const { filterData, startDate, search, statusFilter } = this.state;
    const dateFormat = 'YYYY-MM-DD';

    const columns = [
      {
        title: 'Index',
        render: (text, record, index) => <> {index + 1} </>,
        width: 50,
      },
      {
        title: 'Name',
        dataIndex: 'categoryName',
        key: 'title',
        width: 100,
      },
      {
        title: 'Description',
        key: 'description',
        render: (text, record) => {
          if ((text && text.description && text.description.length) >= 35) {
            return (ReactHtmlParser(`${text.description.substring(0, 35)}...`))
          }
          else {
            return (ReactHtmlParser(`${text.description}`))
          }
        },
        width: 150,
      },
      {
        title: 'Status',
        key: 'name',
        render: (text, record) => (
          <b className={`text-${text.status === 0 ? 'success' : 'danger'}`}>
            {text.status === 0 ? 'Active' : 'Inactive'}
          </b>
        ),
        width: 80,
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span className="d-flex">
            <Tooltip placement="bottomLeft" title={"view"}>
              <span>
                <InfoModal
                  InfoData={text}
                  Name={text.categoryName}
                  Status={text.status}
                  Description={text.description}
                // api={text.status === 0 ? 'block-category' : 'unblock-category'}
                // type={text.status === 0 ? 'block' : 'unblock'}
                // blockTitleText="Inactive"
                // unblockTitleText="Active"
                // blockText="inactive"
                // unblockText="actie"
                // blockBtnText="Inactive"
                // unblockBtnText="Active"
                />
              </span>
            </Tooltip>

            <Tooltip placement="bottomLeft" title={"edit"}>
              <Link
                to={{ pathname: `${match.url}/edit`, state: text }}
                className="ml-2 ant-btn ant-btn-primary ant-btn-circle"
              >
                <Icon type="edit" theme="filled" />
              </Link>
            </Tooltip>
            <Tooltip placement="bottomLeft" title="delete">
              <span>
                <Delete recordData={text} getCategoriesDetails={getCategoriesDetails} />
              </span>
            </Tooltip>

            <Tooltip placement="bottomLeft" title={text.status === 0 ? 'block' : 'unblock'}>
              <span>
                <BlockModal
                  recordData={text}
                  recordName={text.categoryName}
                  api={'api/changeCategoryStatus'}
                  type={text.status === 0 ? 'block' : 'unblock'}
                  blockTitleText="Block Category"
                  unblockTitleText="Unblock Category"
                  blockText="block Category"
                  unblockText="unblock Category"
                  blockBtnText="Block Category"
                  unblockBtnText="Unblock Category"
                />
              </span>
            </Tooltip>
          </span>
        ),
        width: 90,
      },
    ];
    return (
      <>
        <CategoryStatistics
          filter={
            <div>
              <RangePicker
                style={{ width: 270 }}
                onChange={this.onDateChange}
                format={dateFormat}
              />
              <Button
                className="ml-2"
                onClick={() => this.onRangeSearch()}
                type="primary"
                disabled={startDate === ''}
              >
                Search
              </Button>
              {startDate !== '' && (
                <Button className="ml-2" onClick={() => window.location.reload()} type="danger">
                  Reset
                </Button>
              )}
            </div>
          }
          // total={listData.length}
          // active={listData.filter(({ status }) => status === 0).length}
          // inactive={listData.filter(({ status }) => status === 1).length}
          total={category && category.length}
          active={category && category.filter(({ status }) => status === 0).length}
          inactive={category && category.filter(({ status }) => status === 1).length}
        />

        <section className="container-fluid container-mw-xxl no-breadcrumb chapter">
          <article className="article pb-0">
            <div>{/* <h2 className="main-title pt-0">Categories</h2> */}</div>
            <div className="d-flex justify-content-between">
              <div className="d-flex">
                <FormItem>
                  <Input
                    placeholder="Search category"
                    prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                    name="search"
                    value={search}
                    onChange={this.handleSearch}
                  />
                </FormItem>
              </div>
              <div>
                <Button
                  className="mx-2"
                  onClick={() => this.handleStatusFilter(1)}
                  type={statusFilter === 1 ? 'primary' : 'secondary'}
                >
                  Active
                </Button>
                <Button
                  className="mr-2"
                  onClick={() => this.handleStatusFilter(2)}
                  type={statusFilter === 2 ? 'primary' : 'secondary'}
                >
                  Inactive
                </Button>

                <Link to={`${match.url}/add`} className="ant-btn ant-btn-primary">
                  <Icon type="plus-circle" theme="filled" /> Add
                </Link>
              </div>
            </div>

            {/* <SkeletonTheme color="#F" highlightColor="#444"> */}
            {categoryLoader ?
              <Skeleton height={50} count={10} /> :
              <Table
                columns={columns}
                dataSource={filterData}
                className="ant-table-v1"
                scroll={{ x: 700 }}
                loading={categoryLoader}
              />
            }
          </article>
        </section>
      </>
    );
  }
}

const mapStateToProps = state => ({
  category: state.category.category,
  categoryLoader: state.category.categoryLoader
})

const mapDispatchToProps = dispatch => ({
  getCategoriesDetails: () => dispatch(getAllCategoryDetail())
})

export default connect(mapStateToProps, mapDispatchToProps)(SubCategories);
