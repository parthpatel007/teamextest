import React from 'react';
import { Modal, Button, Icon } from 'antd';
import axios from 'axios';
import { backendUrl } from '../../../../../common/credentials';
import { withToastManager } from 'react-toast-notifications';

class Delete extends React.Component {
  state = { visible: false };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  deleteRecord = id => {
    const { toastManager } = this.props

    const authToken = localStorage.getItem('access_token');
    const self = this
    axios.delete(`${backendUrl}/api/subCategories/${id}`, {
      headers: {
        Authorization: `Bearer ${authToken}`,
      }
    })
      .then(function (response) {
        // console.log('rrrr res', response.data.data);
        self.setState({
          visible: false,
        });
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        self.props.getAllSubCategoryDetail()
      })
      .catch(function (error) {
        console.log(error, "error delete subcategory")
        toastManager.add(
          error && error.response && error.response.data && error.response.data.message, {
          appearance: "warning",
          autoDismiss: true
        });
        self.setState({
          visible: false,
        });
      });
  };

  render() {
    const { recordData } = this.props;
    const { subCategoryName, _id } = recordData;

    return (
      <div>
        <Button type="danger" className="ml-2" shape="circle" onClick={this.showModal}>
          <Icon type="delete" theme="filled" />
        </Button>
        <Modal
          title="Delete Sub Category"
          okType="danger"
          visible={this.state.visible}
          onOk={() => this.deleteRecord(_id)}
          okText="Delete"
          onCancel={this.handleCancel}
          // footer={null}
          // className="custom-modal-v1"
          centered
        >
          {/* <GlobalStyle /> */}
          <section className="form-card">
            <div className="form-card__body ">
              <label style={{ fontSize: 16 }}>
                Do you really want to delete sub category <b>{subCategoryName}</b>?
              </label>
            </div>
          </section>
        </Modal>
      </div>
    );
  }
}

export default withToastManager(Delete)