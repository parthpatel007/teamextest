import React from 'react';
import { Table, Tooltip, Icon, Button, Form, Input } from 'antd';
import ReactHtmlParser from 'react-html-parser';
import Skeleton from 'react-loading-skeleton';
import Delete from './delete';
import { Link } from 'react-router-dom';
import { SubCategoryStatistics } from '../../../../components/Cards';
import { BlockModal, InfoModal } from '../../../../components/Modal';
import { getAllSubCategoryDetail } from '../../../../middleware/subCategoryMiddleware';
import { connect } from 'react-redux';
import getAllCategoryDetail from '../../../../middleware/categoryMiddleware';
const FormItem = Form.Item;
/* eslint-disable */

class SubCategories extends React.Component {
  state = {
    listData: [],
    search: '',
    categories: [],
    statusFilter: 0,
    subcategories: [],
    filterData: [],
  };

  componentDidMount() {
    const { getSubCategoriesDetails, subCategory } = this.props;
    getSubCategoriesDetails();
    this.setState({
      filterData: subCategory,
      listData: subCategory,

    });
  }
  componentDidUpdate(prevProps, prevState) {
    const { subCategory } = this.props
    if (prevProps.subCategory !== subCategory) {
      this.setState({
        filterData: subCategory,
        listData: subCategory
      })
    }
  }

  handleSearch = e => {
    const { listData } = this.state;
    const filterDataHolder = [
      ...listData.filter(({ subCategoryName }) =>
        subCategoryName.toLowerCase().startsWith(
          e.target.value.trim().toLowerCase()
        )
      ),
    ];
    this.setState({
      [e.target.name]: e.target.value,
      filterData: filterDataHolder,
      statusFilter: 0,
    });
  };

  handleStatusFilter = stock => {
    const { listData, statusFilter } = this.state;
    let filterDataHolder = [];
    if (statusFilter === stock) {
      filterDataHolder = [...listData];
    } else {
      filterDataHolder = [
        ...listData.filter(
          ({ status }) => (stock === 1 && status === 0) || (stock === 2 && status === 1)
        ),
      ];
    }
    this.setState({
      search: '',
      statusFilter: stock !== statusFilter ? stock : 0,
      filterData: filterDataHolder,
    });
  };

  render() {
    const { match, getSubCategoriesDetails, subCategoryLoader } = this.props;
    const {
      listData,
      filterData,
      search,
      statusFilter
    } = this.state;
    const columns = [
      {
        title: 'Index',
        render: (text, record, index) => <> {index + 1} </>,
        width: 50,
      },
      {
        title: 'Name',
        dataIndex: 'subCategoryName',
        key: 'subCategoryName',
        width: 150
      },
      {
        title: 'Category',
        key: 'Category',
        render: (text, row) => {
          return text && text.categoryId && text.categoryId.categoryName
        },
        width: 150,
      },
      {
        title: 'Description',
        key: 'description',
        render: (text, record) => {
          if ((text && text.description && text.description.length) >= 35) {
            return (ReactHtmlParser(`${text.description.substring(0, 35)}...`))
          }
          else {
            return (ReactHtmlParser(`${text.description}`))
          }
        },
        width: 200
      },
      {
        title: 'Status',
        key: 'name',
        render: (text, record) => (
          <b className={`text-${text.status === 0 ? 'success' : 'danger'}`}>
            {text.status === 0 ? 'Active' : 'Inactive'}
          </b>
        ),
        width: 120
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span className="d-flex">
            <Tooltip placement="bottomLeft" title={"view"}>
              <span>
                <InfoModal
                  InfoData={text}
                  Name={text.subCategoryName}
                  Status={text.status}
                  Description={text.description}
                />
              </span>
            </Tooltip>

            <Tooltip placement="bottomLeft" title={"edit"}>
              <Link
                to={{ pathname: `${match.url}/edit`, state: text }}
                className="ml-2 ant-btn ant-btn-primary ant-btn-circle"
              >
                <Icon type="edit" theme="filled" />
              </Link>
            </Tooltip>
            <Tooltip placement="bottomLeft" title="delete">
              <span>
                <Delete recordData={text} getAllSubCategoryDetail={getSubCategoriesDetails} />
              </span>
            </Tooltip>

            <Tooltip placement="bottomLeft" title={text.status === 0 ? 'block' : 'unblock'}>
              <span>
                <BlockModal
                  recordData={text}
                  recordId={text._id}
                  recordName={text.subCategoryName}
                  api={'api/changeSubCategoryStatus'}
                  type={text.status === 0 ? 'block' : 'unblock'}
                  blockTitleText="Block Sub Category"
                  unblockTitleText="Unblock Sub Category"
                  blockText="block Sub Category"
                  unblockText="unblock Sub Category"
                  blockBtnText="Block Sub Category"
                  unblockBtnText="Unblock Sub Category"
                />
              </span>
            </Tooltip>
          </span>
        ),
        width: 165,
      },
    ];

    return (
      <>
        <SubCategoryStatistics
          total={listData.length}
          active={listData.filter(({ status }) => status === 0).length}
          inactive={listData.filter(({ status }) => status === 1).length}
        />
        <section className="container-fluid container-mw-xxl no-breadcrumb chapter">
          <article className="article pb-0">
            <div className="d-flex justify-content-between mb-2">
              <div>
                <FormItem>
                  <Input
                    placeholder="Search subcategory"
                    prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                    name="search"
                    value={search}
                    onChange={this.handleSearch}
                  />
                </FormItem>
              </div>
              <div>
                <Button
                  className="mx-2"
                  onClick={() => this.handleStatusFilter(1)}
                  type={statusFilter === 1 ? 'primary' : 'secondary'}
                >
                  Active
                </Button>
                <Button
                  className="mr-2"
                  onClick={() => this.handleStatusFilter(2)}
                  type={statusFilter === 2 ? 'primary' : 'secondary'}
                >
                  Inactive
                </Button>
                <Link to={`${match.url}/add`} className="ant-btn ant-btn-primary">
                  <Icon type="plus-circle" theme="filled" /> Add
                </Link>
              </div>
            </div>
            {subCategoryLoader ?
              <Skeleton height={50} count={10} />
              :
              <Table
                columns={columns}
                dataSource={filterData}
                className="ant-table-v1"
                scroll={{ x: 700 }}
                loading={subCategoryLoader}
              />}
          </article>
        </section>
      </>
    );
  }
}

const mapStateToProps = state => ({
  subCategory: state.subCategory.subCategory,
  category: state.category.category,
  subCategoryLoader: state.subCategory.subCategoryLoader
})

const mapDispatchToProps = dispatch => ({
  getSubCategoriesDetails: () => dispatch(getAllSubCategoryDetail()),
  getCategoriesDetails: () => dispatch(getAllCategoryDetail())
})

export default connect(mapStateToProps, mapDispatchToProps)(SubCategories);