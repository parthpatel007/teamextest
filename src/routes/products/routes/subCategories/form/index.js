import React from 'react';
import { Form, Input, Icon, Button, Spin, Select } from 'antd';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { backendUrl } from '../../../../../common/credentials';
import cx from 'classnames';
import { connect } from 'react-redux';
import getAllCategoryDetail from '../../../../../middleware/categoryMiddleware';
import { withToastManager } from 'react-toast-notifications';

const FormItem = Form.Item;
const Option = Select.Option;

const validateProdcut = email => {
  var re = /^[a-zA-Z_ ]+$/;
  return re.test(String(email).toLowerCase());
};


const defaultState = {
  subCategoryName: '',
  description: '',
  categoryId: '',
};


class FormComponent extends React.Component {

  state = {
    categories: [],
    ...(this.props.type === 'edit' ? this.props.editData : defaultState),
    isLoading: false,
    editCategoryId: this.props.type === 'edit' ? this.props.editData && this.props.editData.categoryId && this.props.editData.categoryId._id : '',
    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };



  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };


  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };


  handleInput = e => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };


  handleSelect = (name, value) => {
    this.setState({ [name]: value });
  };



  addNewApi = () => {
    const { subCategoryName, description, categoryId } = this.state;
    const { match, history } = this.props;
    const { toastManager } = this.props
    const authToken = localStorage.getItem('access_token');
    this.setState({ isLoading: true });
    const self = this;
    axios
      .post(
        `${backendUrl}/api/subCategories`,
        {
          subCategoryName: subCategoryName,
          description: description,
          categoryId: categoryId,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        console.log('rrrr res', response);
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        console.log('rrrr err', error);
        toastManager.add(
          error && error.response && error.response.data && error.response.data.message, {
          appearance: "warning",
          autoDismiss: true
        });
        self.setState({ isLoading: false });
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
      });
  };


  editApi = () => {
    const { subCategoryName, description, categoryId, editCategoryId } = this.state;
    const { _id } = this.props.editData;
    const { toastManager } = this.props
    console.log("sub category:" + _id)
    console.log("category:" + categoryId)

    const { match, history } = this.props;
    const authToken = localStorage.getItem('access_token');
    this.setState({ isLoading: true });
    const self = this;
    axios
      .post(
        `${backendUrl}/api/subCategories/${_id}`,
        {
          categoryId: editCategoryId,
          subCategoryName: subCategoryName,
          description: description
        },
        {
          headers: {
            Authorization: `Bearer ${authToken}`
          },
        }
      )
      .then(function (response) {
        console.log('rrrr res', response);
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        console.log('rrrr err', error);
        toastManager.add(
          error && error.response && error.response.data && error.response.data.message, {
          appearance: "warning",
          autoDismiss: true
        });
        self.setState({ isLoading: false });
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : 'Somthing went wrong',
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
      });
  };


  getCategoriesApi = () => {
    var self = this;
    const authToken = localStorage.getItem('access_token');
    axios
      .get(`${backendUrl}/get-category`, {
        headers: {
          Authorization: authToken,
        },
      })
      .then(function (response) {
        console.log('rrrr res', response.data.data);
        if (response) {
          self.setState({
            categories: response.data.data,
          });
        }
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };


  validateForm = () => {
    const { subCategoryName, description, categoryId } = this.state;
    if (categoryId === '') {
      this.setState({
        error: {
          errorText: 'Please select a category',
          errorType: 'categoryId',
        },
      });
      return false;
    }
    if (subCategoryName === '') {
      this.setState({
        error: {
          errorText: 'Please enter sub category name',
          errorType: 'subCategoryName',
        },
      });
      return false;
    }

    if (!validateProdcut(subCategoryName)) {
      this.setState({
        error: {
          errorText: 'Please enter  vaild sub category name',
          errorType: 'subCategoryName',
        },
      });
      return false;
    }

    if (description === '') {
      this.setState({
        error: {
          errorText: 'Please enter description',
          errorType: 'description',
        },
      });
      return false;
    }

    return true;
  };


  handleAddData = () => {
    if (this.validateForm()) {
      this.addNewApi();
    }
  };


  handleEditData = () => {
    if (this.validateForm()) {
      this.editApi();
    }
  };


  componentDidMount() {

    const { getCategoriesDetails } = this.props;

    getCategoriesDetails();

    // this.getCategoriesApi();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.category) {
      this.setState({
        categories: nextProps.category
      })
    }
  }


  render() {


    const { type = 'add', editData, match, history } = this.props;

    const { subCategoryName, description, categories, isLoading, apiError, error, categoryId, editCategoryId } = this.state;

    const { errorType } = error;

    if (type === 'edit') {

      if (!editData) {

        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        return null;

      }

    }
    return (

      <div className="form-container">

        {type === 'add' && (

          <FormItem
            label="Categories"
            hasFeedback
            className={cx({ 'has-error': errorType === 'categoryId' })}>

            <Select
              className="w-100"
              name="categoryId"
              value={categoryId}
              placeholder="Select Category"
              onChange={e => this.handleSelect('categoryId', e)}
            >


              {categories.map((data, index) => {
                return (
                  <Option key={index} value={data._id}>
                    {data.categoryName}
                  </Option>
                );
              })}


            </Select>

            {this.errorShow('categoryId')}
          </FormItem>
        )}


        {type === 'edit' && (

          <FormItem
            label="Categories"
            hasFeedback
            className={cx({ 'has-error': errorType === 'categoryId' })}>

            <Select
              className="w-100"
              name="editCategoryId"
              disabled={type === 'edit' ? true : false}
              defaultValue={editCategoryId}
              placeholder="Select Category"
              onChange={e => this.handleSelect('editCategoryId', e)}
            >


              {categories.map((data, index) => {

                return (
                  <Option key={index} value={data._id}>
                    {data.categoryName}
                  </Option>
                );
              })}


            </Select>

            {this.errorShow('categoryId')}
          </FormItem>
        )}


        <FormItem
          label="Sub Category Name"
          className={cx({ 'has-error': errorType === 'subCategoryName' })}
          hasFeedback
        >

          <Input maxLength={20} name="subCategoryName" value={subCategoryName} onChange={this.handleInput} />
          {this.errorShow('subCategoryName')}

        </FormItem>

        <FormItem label="Description" className={cx({ 'has-error': errorType === 'description' })}>

          <Input.TextArea name="description" value={description} onChange={this.handleInput} />
          {this.errorShow('description')}

        </FormItem>

        {isLoading && (
          <div className="mb-2 text-center">
            <Spin />
          </div>
        )}


        <div className="text-right pt-3">


          {type === 'add' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              onClick={this.handleAddData}
            >
              <Icon type="plus-circle" theme="filled" />
              Add
            </Button>
          )}


          {type === 'edit' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              onClick={this.handleEditData}
            >
              <Icon type="edit" theme="filled" /> Save
            </Button>
          )}


        </div>


        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}


      </div>
    );
  }
}

const mapStateToProps = state => ({
  category: state.category.category
})

const mapDispatchToProps = dispatch => ({
  getCategoriesDetails: () => dispatch(getAllCategoryDetail())
})


const WrappedFormComponent = Form.create()(withRouter(FormComponent));

export default withToastManager(connect(mapStateToProps, mapDispatchToProps)(WrappedFormComponent));
