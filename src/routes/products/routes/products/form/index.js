import React, { Fragment } from 'react';
import { Form, Input, Icon, Button, Select, Spin, Card } from 'antd';
import { Row, Col } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import { backendUrl } from '../../../../../common/credentials'
import axios from 'axios';
import cx from 'classnames';
import { createGlobalStyle } from 'styled-components';
import { connect } from 'react-redux';
import {
  getAllSubCategoryDetail,
  getAllSubCategoryDetailCategoryWise,
} from '../../../../../middleware/subCategoryMiddleware';
import getAllCategoryDetail from '../../../../../middleware/categoryMiddleware';
import Dragger from 'antd/lib/upload/Dragger';
import { withToastManager } from 'react-toast-notifications';


// eslint-disable
const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;

const defaultState = {
  expirydeatails: '',
  productStatus: '',
  howtouse: '',
  quantity: '',
  productcode: '',
  subcategory_id: '',
  category_id: '',
  name: '',
  description: '',
  weight: '',
  price: '',
  ingredients: '',
  directions_to_use: '',
  productImages: [],
  s3Images: [],
  productImagesUrl:[],
  documentTitleArray:[],
  documentImgesArray:[]
};

class FormComponent extends React.Component {
  state = {
    documentTitle: '',
    productStatus:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.productStatus
        : '',
    quantity: '',
    confirmDirty: false,
    productImages: [],
    categories: [],
    subcategories: [],
    productImagesUrl: this.props && this.props.editData && this.props.editData.productImages,
    documentTitleArray: this.props && this.props.editData && this.props.editData.productDocument,
    store: [],
    editProductImages:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.productImages
        : [],

    editProductImagesObjects: null,
    editProductDocument:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.productDocument
        : [],
    editProductDocumentObjects: null,
    SubCategoryId:
      this.props.type === 'edit'
        ? this.props &&
        this.props.editData &&
        this.props.editData.subCategoryId &&
        this.props.editData.subCategoryId._id
        : '',
    CategoryId:
      this.props.type === 'edit'
        ? this.props &&
        this.props.editData &&
        this.props.editData.categoryId &&
        this.props.editData.categoryId._id
        : '',
    ProductName:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.productName
        : '',
    productcode:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.productCode
        : '',
    howtouse:
      this.props.type === 'edit'
        ? this.props && this.props.editData && this.props.editData.howToUse
        : '',
    ...(this.props.type === 'edit' ? this.props.editData : defaultState),
    isLoading: false,
    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
    productDocument: [
      {
        imageUrl: '',
        title:''
      }
    ]
  };

  

  handleTagsChange = (state, value) => {
    this.clearError();
    this.setState({ [state]: value });
    console.log(`rrrr selected  ${value} `);
  };

  handleEditor = (state, content) => {
    this.clearError();
    this.setState({ [state]: content });
  };

  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };

  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };

  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('signup1-password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  handleInput = e => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSelect = (name, value) => {
    this.clearError();
    this.setState({ [name]: value });
  };

  addNewApi = async () => {
    const { toastManager } = this.props
    const {
      productDocument,
      subcategory_id = '',
      category_id = '',
      name,
      price,
      quantity,
      productcode,
      howtouse,
      description,
      ingredients,
      productImages,
      productStatus
    } = this.state;

    const { match, history } = this.props;

    const authToken = localStorage.getItem('access_token');

    this.setState({ isLoading: true });

    const self = this;
    const bodydata = new FormData()
    bodydata.append("categoryId", category_id)
    bodydata.append("subCategoryId", subcategory_id)
    bodydata.append("productName", name)
    bodydata.append("price", price)
    bodydata.append("quantity", quantity)
    bodydata.append("productCode", productcode)
    bodydata.append("howToUse", howtouse)
    bodydata.append("description", description)
    bodydata.append("ingredients", ingredients)
    bodydata.append("productStatus", productStatus)
    // let array = []
    productImages.map(data =>
      bodydata.append("productImages", data.originFileObj)
    )
    productDocument.map((item, i) =>
      bodydata.append(`productDocument`, item.file)
    )
    let array =[]
    productDocument.map((item, i) =>
      array.push(item.documntTitle)
    )
    bodydata.append(`documentTitle`,JSON.stringify(array))

    // productDocument.map((item, i) => {
    //   console.log(JSON.stringify(item.file), "JSON.stringify(item)")

    //   return bodydata.append(`productDocument`, JSON.stringify(item))
    // }
    // )
    console.log(bodydata, "bodydata")
    axios
      .post(
        `${backendUrl}/api/products`,
        bodydata,
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        console.log('Add products', response);
        toastManager.add(
          response.data.data.customMessage, {
          appearance: "success",
          autoDismiss: true
        });
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        console.log('Add products error', error);
        toastManager.add(
          error && error.data && error.data.message, {
          appearance: "warning",
          autoDismiss: true
        });
        self.setState({

          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error && error.data && error.data.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 8080);
        console.log('rrrr err', error);
      });
  };

  editApi = () => {
    const { toastManager } = this.props
    const {
      SubCategoryId,
      CategoryId,
      ProductName,
      price,
      quantity,
      productcode,
      howtouse,
      description,
      ingredients,
      productDocument,
      productStatus,
      editProductImagesObjects,
      editProductImages,
      documentTitle,
      productImagesUrl,
      documentTitleArray
    } = this.state;


    const { match, history } = this.props;

    const { _id } = this.props.editData;

    const authToken = localStorage.getItem('access_token');

    this.setState({ isLoading: true });

    const self = this;
    

    let bodydata = {
      categoryId: CategoryId.toString(),
      subCategoryId: SubCategoryId,
      productName: ProductName,
      price: price,
      quantity: quantity,
      productCode: productcode,
      howToUse: howtouse,
      ingredients: ingredients,
      description: description,
      productStatus: productStatus,
      productDocument: documentTitleArray,
      // documentTitle: documentTitle,
      productImages:productImagesUrl
    };


    console.log('-----------------_WWWW_-------------------')
    console.log(bodydata)
    console.log(this.documentImgesArray)
    // let imageArray = []
    // if (productImagesUrl) {
    //   const formData = new FormData()

    //   formData.append("image", editProductImagesObjects)
    //   console.log("edit product image object", editProductImagesObjects)
    //   axios
    //     .post(
    //       `${backendUrl}/api/upload`,
    //       formData,
    //       {
    //         headers: {
    //           Authorization: `Bearer ${authToken}`,
    //         }
    //       }
    //     )
    //     .then(function (response) {
    //       console.log('response productimage s3link', response)
    //       imageArray.push(response.data && response.data.data && response.data.data.original)
    //       bodydata.productImages = imageArray
    //       axios
    //         .post(
    //           `${backendUrl}/api/products/${_id}`,
    //           bodydata,
    //           {
    //             headers: {
    //               'Content-Type': 'application/json',
    //               Accept: '*/*',
    //               Authorization: `Bearer ${authToken}`,
    //             },
    //           }
    //         )
    //         .then(function (response) {

    //           console.log('rrrr res', response);
    //           toastManager.add(
    //             response.data.data.customMessage, {
    //             appearance: "success",
    //             autoDismiss: true
    //           });
    //           history.push(
    //             match.url
    //               .split('/')
    //               .slice(0, -1)
    //               .join('/')
    //           );
    //           self.setState({ isLoading: false });
    //         })
    //         .catch(function (error) {
    //           toastManager.add(
    //             error && error.response && error.response.data && error.response.data.message, {
    //             appearance: "warning",
    //             autoDismiss: true
    //           });
    //           self.setState({
    //             isLoading: false,
    //             apiError: error.toString().includes('Network Error')
    //               ? 'Network Error'
    //               : error.response.data.message,
    //           });
    //           setTimeout(() => self.setState({ apiError: '' }), 8080);
    //           console.log('rrrr err', error);
    //         });
    //     })
    //     .catch((error) => {
    //       console.log("error error", error)
    //     })
    // }
    // else {
      // bodydata.productImages = editProductImages
      axios
        .post(
          `${backendUrl}/api/products/${_id}`,
          bodydata,
          {
            headers: {
              'Content-Type': 'application/json',
              Accept: '*/*',
              Authorization: `Bearer ${authToken}`,
            },
          }
        )
        .then(function (response) {

          console.log('rrrr res', response);
          toastManager.add(
            response.data.data.customMessage, {
            appearance: "success",
            autoDismiss: true
          });
          history.push(
            match.url
              .split('/')
              .slice(0, -1)
              .join('/')
          );
          self.setState({ isLoading: false });
        })
        .catch(function (error) {
          console.log(error, "Error 40423");
          toastManager.add(
            error && error.response && error.response.data && error.response.data.message, {
            appearance: "warning",
            autoDismiss: true
          });
          self.setState({
            isLoading: false,
            apiError: 'Network Error'
              
          });
          setTimeout(() => self.setState({ apiError: '' }), 8080);
          console.log('rrrr err', error);
        });
    // }

  };

  getStoreApi = () => {
    var self = this;
    const { type = 'add' } = this.props;
    const authToken = localStorage.getItem('access_token');

    axios
      .get(`${backendUrl}/get-store`, {
        headers: {
          Authorization: authToken,
        },
      })
      .then(function (response) {
        console.log('rrrr res ss', response.data.data);
        if (response) {
          self.setState({
            store: response.data.data,
            store_id:
              type === 'add'
                ? localStorage.getItem('store_id')
                  ? localStorage.getItem('store_id')
                  : ''
                : self.props.editData.store_id
                  ? self.props.editData.store_id
                  : '',
          });
        }
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };

  getCategoriesApi = () => {
    var self = this;
    const authToken = localStorage.getItem('access_token');
    axios
      .get(`${backendUrl}/get-category`, {
        headers: {
          Authorization: authToken,
        },
      })
      .then(function (response) {
        console.log('rrrr res', response.data.data);
        if (response) {
          self.setState({
            categories: response.data.data,
          });
        }
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };

  validateForm = () => {
    const {
      category_id,
      name,
      price,
      quantity,
      productcode,
      howtouse,
      description,
      ingredients,
      productStatus,
      productImagesUrl
    } = this.state;

    if (category_id === '') {
      this.setState({
        error: {
          errorText: 'Please select a category',
          errorType: 'category_id',
        },
      });
      return false;
    }

    if (productStatus === '') {
      this.setState({
        error: {
          errorText: 'Please select a Product Status',
          errorType: 'productStatus',
        },
      });
      return false;
    }
    if (name === '') {
      this.setState({
        error: {
          errorText: 'Please enter product name',
          errorType: 'name',
        },
      });
      return false;
    }
    if (name === '') {
      this.setState({
        error: {
          errorText: 'Please enter vaild product name',
          errorType: 'name',
        },
      });
      return false;
    }
    if (price === '') {
      this.setState({
        error: {
          errorText: 'Please enter price',
          errorType: 'price',
        },
      });
      return false;
    }

    if (quantity === '') {
      this.setState({
        error: {
          errorText: 'Please select a Quantity',
          errorType: 'quantity',
        },
      });
      return false;
    }

    if (productcode === '') {
      this.setState({
        error: {
          errorText: 'Please select a Product Code',
          errorType: 'productcode',
        },
      });
      return false;
    }

    if (howtouse === '') {
      this.setState({
        error: {
          errorText: 'Please select a How To Use',
          errorType: 'howtouse',
        },
      });
      return false;
    }
    if (description === '') {
      this.setState({
        error: {
          errorText: 'Please enter description',
          errorType: 'description',
        },
      });
      return false;
    }
    if (ingredients === '') {
      this.setState({
        error: {
          errorText: 'Please add ingredient',
          errorType: 'ingredients',
        },
      });
      return false;
    }

    if (this.props.type === 'edit') {
      if (
        this.state.productImagesUrl && this.state.productImagesUrl.length <= 0 &&
        this.state.productImagesUrl.length <= 0
      ) {
        console.log('inside image of invalida');
        this.setState({
          error: {
            errorText: 'Please Select Valid product image',
            errorType: 'edit_image',
          },
        });
        return false;
      }
    } else {
      if (
        (this.state && this.state.productImages && this.state.productImages.length <= 0) ||
        this.state.productImages === undefined ||
        this.state.productImages === null
      ) {
        this.setState({
          error: {
            errorText: 'Please Select Valid product image',
            errorType: 'add_image',
          },
        });
        return false;
      }
    }
    return true;
  };

  componentDidMount() {
    const { getSubCategoriesDetails, getCategoriesDetails } = this.props;
    const { type } = this.props;

    if (type === 'edit') {
      getCategoriesDetails();
      this.props.getAllSubCategoryDetailCategoryWiseFunction(this.state.CategoryId);
    } else {
      getCategoriesDetails();
      getSubCategoriesDetails();
    }
  }
  handleAddData = (event) => {
    this.clearError();
    if (this.validateForm()) {
      this.addNewApi();
      
    }
  };

  handleEditData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.editApi();
    }
  };

  handleEditProductImage = index => {
    const { productImagesUrl } = this.state;

    console.log(
      productImagesUrl,
      'edit pro imahes dedit pro imahes dedit pro imahes dedit pro imahes d'
    );

    console.log(
      index,
      'index of arra index of arra index of arra index of arra index of arra index of arra index of arra index of arra index of arra index of arra '
    );

    const tempArr = [...productImagesUrl];

    tempArr.splice(index, 1);

    this.setState({
      productImagesUrl: tempArr,
    });

    console.log(
      tempArr,
      'tempArr imahestempArr dedittempArr protempArr imahes dedit protempArrtempArrtempArrtempArr imahestempArrtempArrtempArr dedit pro imahes d'
    );
  };

  handleEditProductDocument = index => {
    const { editProductDocument } = this.state;
  
    const tempArr = [...editProductDocument];

    tempArr.splice(index, 1);

    this.setState({
      editProductDocument: tempArr,
    });

    
  };

  getSubCategoryById = catId => {
    // const { startDate, endDate, listData } = this.state;
    const { subCategory } = this.props
    console.log(catId, 'category id category id category id category id category id ');
    // console.log(subCategory, "array of subCategory array of subCategory array of subCategory array of subCategory")
    const filteredSubCategories = [
      ...subCategory.filter(singleSubCat => {
        return singleSubCat.category_id === catId;
      }),
    ];
    console.log(
      filteredSubCategories,
      'filteredSubCategories filteredSubCategories filteredSubCategories filteredSubCategories filteredSubCategories filteredSubCategories '
    );
    this.setState({
      subcategories: filteredSubCategories,
    });
  };



  onDragChange = (info, event) => {

    const status = info.file.status;
    if (status !== 'uploading') {
      this.setState({
        productImages: info.fileList,
      });
    }
  };

  onEditDragImageChange = (info, event) => {
    const status = info.file.status;
    const authToken = localStorage.getItem('access_token');
    const {productImagesUrl} = this.state
    const { toastManager } = this.props
    if (status !== 'uploading') {
      let abc = info.fileList
      let formData = new FormData()
      let temp = []
      abc.map((i,index)=>{        
        temp.push(i.originFileObj)
      }) 
      formData.append("image", info.file.originFileObj)
      axios
      .post(
        `${backendUrl}/api/uploadProductImage`,
        formData,
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: '*/*',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then((response) => {
        let arr = productImagesUrl;
        arr.push(response.data.data.imageUrl)
        this.setState({ productImagesUrl: arr });
      })
      .catch(function (error) {
        toastManager.add(
          error && error.response && error.response.data && error.response.data.message, {
          appearance: "warning",
          autoDismiss: true
        });
      });
      
      // this.setState({
      //   productImages: info.fileList,
      // });
    }
  };

  onEditDragDocumentChange = (info, event) => {
    const status = info.file.status;
    const authToken = localStorage.getItem('access_token');
    const {documentTitleArray} = this.state
    const { toastManager } = this.props
    if (status !== 'uploading') {
      let abc = info.fileList
      let formData = new FormData()
      let temp = []
      abc.map((i,index)=>{        
        temp.push(i.originFileObj)
      })
      formData.append("image", info.file.originFileObj)
      axios
      .post(
        `${backendUrl}/api/uploadProductImage`,
        formData,
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: '*/*',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then((response) => {
        let arr = documentTitleArray;
         let obj = {
          imageUrl:response.data.data.imageUrl
        }
        arr.push(response.data.data.imageUrl)
       
        this.documentImgesArray.push(obj)
        this.setState({ documentTitleArray: arr });
        console.log()
        console.log(documentTitleArray, "productDocumentproductDocumentproductDocumentproductDocument ---Link")
      })
      .catch(function (error) {
        toastManager.add(
          error && error.response && error.response.data && error.response.data.message, {
          appearance: "warning",
          autoDismiss: true
        });
      });
      
      // this.setState({
      //   productImages: info.fileList,
      // });
    }
  };
  onDocumentDragChange = (info, index) => {
    const status = info.file.status;
    let duplicatearray = this.state.productDocument
    if (info.fileList.length > 0) {
      // eslint-disable-next-line
      duplicatearray.map((item, i) => {
        if (i === index) {
          return item.file = info.fileList[0].originFileObj
        }
      })
    }
    else {
      // eslint-disable-next-line
      duplicatearray.map((item, i) => {
        if (i === index) {
          return item.file = null
        }
      })
    }

    if (status !== 'uploading') {
      this.setState({
        productDocument: duplicatearray,
      });
    }
  }

  onDocumentEditDragChange = (info, index) => {
    // const status = info.file.status;
    // let duplicatearray = this.state.productDocument
    // if (info.fileList.length > 0) {
    //   // eslint-disable-next-line
    //   duplicatearray.map((item, i) => {
    //     if (i === index) {
    //       return item.file = info.fileList[0].originFileObj
    //     }
    //   })
    // }
    // else {
    //   // eslint-disable-next-line
    //   duplicatearray.map((item, i) => {
    //     if (i === index) {
    //       return item.file = null
    //     }
    //   })
    // }

    // if (status !== 'uploading') {
    //   this.setState({
    //     productDocument: duplicatearray,
    //   });
    // }
  }

  
  handleDocumentInput = (value, count) => {
    let duplicatearray = this.state.productDocument
    // eslint-disable-next-line
    duplicatearray.map((item, index) => {
      if (index === count) {
        return item.documntTitle = value
      }
    })
    this.setState({
      productDocument: duplicatearray,
    });
  }
  addMoreDoc = i => {
    const { productDocument } = this.state
    this.setState({
      productDocument: [...productDocument, {
        imageUrl: '',
        title: ""
      }],
    });
  }
  removeDoc = i => {
    const duplicatearray = this.state.productDocument.filter((item, arrayi) => {
      return (arrayi !== i)
    }
    )
    this.setState({
      productDocument: duplicatearray,
    });
  }
  beforeUpload = file => {
    const { type } = this.props;
    const isLt2M = file.size / 1024 / 1024 < 5;
    if (!isLt2M) {
      if (type === 'edit') {
        this.setState({
          error: {
            errorText: 'Image must smaller than 5MB!',
            errorType: 'edit_image',
          },
        });
        return false;
      } else {
        this.setState({
          error: {
            errorText: 'Image must smaller than 5MB!',
            errorType: 'add_image',
          },
        });
        return false;
      }
    } else {
      this.setState({
        error: {
          errorText: '',
          errorType: '',
        },
      });
    }
    return isLt2M;
  };

  onEditDragChange = info => {
    const status = info.file.status;
    console.log("1")
    if (status !== 'uploading') {
      if (info.fileList.length >= 4) {
        console.log('inside filelist lengt <= 4');
        this.setState({
          productImages: info.fileList,
        });
      } else if (info.fileList.length > 4) {
        console.log('inside filelist lengt > 4');
        this.setState({
          productImages: info.fileList.splice(4),
        });
      }
      this.setState({
        editProductImagesObjects: info.file.originFileObj,
      });
    }
  };


  componentDidUpdate(prevProps, prevState) {
    const {

      type,
      getAllSubCategoryDetailCategoryWiseFunction,
    } = this.props;
    const {

      category_id,
      subcategory_id,
      CategoryId,
      SubCategoryId,
    } = this.state;

    if (type === 'edit') {

      if (prevState.CategoryId !== CategoryId) {

        if (SubCategoryId !== '') {
          this.setState({
            SubCategoryId: '',
          });
          getAllSubCategoryDetailCategoryWiseFunction(CategoryId);
        } else {
          getAllSubCategoryDetailCategoryWiseFunction(CategoryId);
        }
        ;
      }

    } else {
      if (prevState.category_id !== category_id) {
        if (subcategory_id !== '') {
          this.setState({
            subcategory_id: '',
          });
          getAllSubCategoryDetailCategoryWiseFunction(category_id);
        } else {
          getAllSubCategoryDetailCategoryWiseFunction(category_id);
        }
      }
    }
  }

  render() {
    const {
      category_id,
      subcategory_id,
      name,
      price,
      quantity,
      productcode,
      howtouse,
      description,
      ingredients,
      CategoryId,
      SubCategoryId,
      ProductName,
      isLoading,
      apiError,
      error,
      editProductImages,
      productStatus,
      productDocument,
      editProductDocument,
      productImagesUrl
    } = this.state;
    const { category, subCategory } = this.props;
    const { type = 'add', editData, match, history } = this.props;
    console.log(editProductDocument, "URL((((((())))))))")
    const { errorType } = error;
    if (type === 'edit') {
      if (!editData) {
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        return null;
      }
    }

    return (
      <div className="form-container">
        <GlobalStyle />


        <Row form>
          <Col md={6}>
            <FormItem
              label="Select Category"
              hasFeedback
              className={cx({ 'has-error': errorType === 'category_id' })}
            >
              <Select
                className="w-100"
                name="category_id"
                value={type === 'edit' ? CategoryId : category_id}
                placeholder="Select Category"
                onChange={e => this.handleSelect(type === 'edit' ? 'CategoryId' : 'category_id', e)}
              >
                {category && category.map((data, index) => {
                  /* if (data.status === 1) {
                        return null;
                      } */
                  return (
                    <Option key={index} value={data._id}>
                      {data.categoryName}
                    </Option>
                  );
                })}
              </Select>
              {this.errorShow(type === 'edit' ? 'CategoryId' : 'category_id')}
            </FormItem>
          </Col>

          <Col md={6}>
            <FormItem
              label="Select Sub Category"
              hasFeedback
              className={cx({ 'has-error': errorType === 'subcategory_id' })}
            >
              <Select
                className="w-100"
                name="subcategory_id"
                value={type === 'edit' ? SubCategoryId : subcategory_id}
                disabled={category_id === '' ? true : false}
                placeholder="Select Sub Category"
                onChange={e =>
                  this.handleSelect(type === 'edit' ? 'SubCategoryId' : 'subcategory_id', e)
                }
              >
                {type === 'edit' &&
                  CategoryId !== '' &&
                  subCategory.map((data, index) => {
                    if (data.status === 1) {
                      return null;
                    }
                    return (
                      <Option key={index} value={data._id}>
                        {data.subCategoryName}
                      </Option>
                    );
                  })}
                {type !== 'edit' &&
                  category_id !== '' &&
                  subCategory.map((data, index) => {
                    return (
                      <Option key={index} value={data._id}>
                        {data.subCategoryName}
                      </Option>
                    );
                  })}
              </Select>
              {this.errorShow(type === 'edit' ? 'SubCategoryId' : 'subcategory_id')}
            </FormItem>
          </Col>
        </Row>
        <Row form>
          <Col md={12}>
            <FormItem
              label="Select Status"
              hasFeedback
              className={cx({ 'has-error': errorType === 'productStatus' })}
            >
              <Select
                className="w-100"
                defaultValue={{ value: 'Others' }}
                name="productStatus"
                value={type === 'edit' ? productStatus : productStatus}
                placeholder="Select Category"
                onChange={e => this.handleSelect(type === 'edit' ? 'productStatus' : 'productStatus', e)}
              >

                <Option value="Sale">
                  Sale
                </Option>
                <Option value="New">
                  New
                </Option>
                <Option value="Popular">
                  Popular
                </Option>
                <Option value="Others">
                  Others
                </Option>

              </Select>
              {this.errorShow(type === 'edit' ? 'CategoryId' : 'category_id')}
            </FormItem>
          </Col>
        </Row>
        <Row form>
          <Col md={6}>
            <FormItem
              label="Product Name"
              hasFeedback
              className={cx({ 'has-error': errorType === 'name' })}
            >
              <Input
                maxLength={70}
                name={type === 'edit' ? 'ProductName' : 'name'}
                value={type === 'edit' ? ProductName : name}
                onChange={this.handleInput}
              />
              {this.errorShow(type === 'edit' ? 'ProductName' : 'name')}
            </FormItem>
          </Col>

          <Col md={6}>
            <FormItem
              label="Price"
              hasFeedback
              className={cx({ 'has-error': errorType === 'price' })}
            >
              <Input
                type={'number'}
                maxLength={8}
                min="0"
                name="price"
                value={price}
                onInput={e => (e.target.value = e.target.value.slice(0, 8))}
                onChange={this.handleInput}
              />
              {this.errorShow('price')}
            </FormItem>
          </Col>
        </Row>

        <Row form>
          <Col md={6}>
            <FormItem
              maxLength={20}
              type={'number'}
              label="Quantity"
              hasFeedback
              className={cx({ 'has-error': errorType === 'quantity' })}
            >
              <Input
                type={'text'}
                maxLength={20}
                name="quantity"
                min="0"
                value={quantity}
                onInput={e => (e.target.value = e.target.value.slice(0, 20))}
                onChange={this.handleInput}
              />
              {this.errorShow('quantity')}
            </FormItem>
          </Col>

          <Col md={6}>
            <FormItem
              label="Product Code"
              hasFeedback
              className={cx({ 'has-error': errorType === 'productcode' })}
            >
              <Input
                maxLength={20}
                min="0"
                name="productcode"
                value={productcode}
                onInput={e => (e.target.value = e.target.value.slice(0, 20))}
                onChange={this.handleInput}
              />
              {this.errorShow('productcode')}
            </FormItem>
          </Col>
        </Row>
        <Row form>
          <Col md={12}>
            <FormItem
              label="How To Use"
              hasFeedback
              className={cx({ 'has-error': errorType === 'howtouse' })}
            >
              <TextArea
                maxLength={500}
                name={type === 'edit' ? 'howtouse' : 'howtouse'}
                value={type === 'edit' ? howtouse : howtouse}
                onChange={this.handleInput}
              />
              {this.errorShow(type === 'edit' ? 'howtouse' : 'howtouse')}
            </FormItem>
          </Col>
        </Row>
        <Row form>
          <Col md={12}>
            <FormItem
              label="Description"
              hasFeedback
              className={cx({ 'has-error': errorType === 'description' })}
            >
              <TextArea
                maxLength={1200}
                name={type === 'edit' ? 'description' : 'description'}
                value={type === 'edit' ? description : description}
                onChange={this.handleInput}
              />
              {this.errorShow(type === 'edit' ? 'description' : 'description')}
            </FormItem>
          </Col>
        </Row>
        <Row form>
          <Col md={12}>
            <FormItem
              label="Product - Ingredients"
              hasFeedback
              className={cx({ 'has-error': errorType === 'ingredients' })}
            >
              <TextArea
                maxLength={500}
                name={type === 'edit' ? 'ingredients' : 'ingredients'}
                value={type === 'edit' ? ingredients : ingredients}
                onChange={this.handleInput}
              />
              {this.errorShow(type === 'edit' ? 'ingredients' : 'ingredients')}
            </FormItem>
          </Col>
        </Row>


        {type === 'add' && (
          <FormItem label="Add Product Image">
            <Dragger
              listType="picture"
              onChange={this.onDragChange}
              showUploadList={errorType === 'add_image' ? false : true}
              beforeUpload={this.beforeUpload}
              name={'file'}
              multiple={true}
              accept=".png, .jpg, .jpeg"
            >
              <p className="ant-upload-drag-icon">
                <Icon type="inbox" />
              </p>
              <p className="ant-upload-text">Click or drag file to this area to upload</p>
              <p className="ant-upload-hint">
                Support for a single or bulk upload. Strictly prohibit from uploading company data
                or other band files
              </p>
            </Dragger>
            Note: For better user experience upload image of 270X360 pixels image.
            {this.errorShow('add_image')}
          </FormItem>
        )}

        {type === 'edit' && (
          <>
            <FormItem className="mt-5" label="Product Images">
              <Row className="pt-5 text-center" justify={'space-between'}>
                {productImagesUrl && productImagesUrl.length > 0 ?
                  productImagesUrl &&
                  productImagesUrl.map((item, index) => {
                    return (

                      <Col span={4} className="text-center">
                        <Button
                          type="danger"
                          className="ml-2"
                          shape="circle"
                          onClick={() => this.handleEditProductImage(index)}
                        >
                          <Icon type="close-circle" theme="filled" />
                        </Button>
                        <br />
                        <img
                          src={item}
                          alt=""
                          style={{ width: 200, height: 200, objectFit: 'contain' }}
                        />
                        <br />
                      </Col>
                    );
                  })
                 : (
                  <Col span={12} className="text-center">
                    <FormItem label="Add Product Image">
                      <Dragger
                        listType="picture"
                        onChange={this.onEditDragImageChange}
                        showUploadList={errorType === 'edit_image' ? false : true}
                        beforeUpload={this.beforeUpload}
                        name={'file'}
                        multiple={true}
                        accept=".png, .jpg, .jpeg"
                      >
                        <p className="ant-upload-drag-icon">
                          <Icon type="inbox" />
                        </p>
                        <p className="ant-upload-text">Click or drag file to this area to upload</p>
                        <p className="ant-upload-hint">
                          Support for a single or bulk upload. Strictly prohibit from uploading company data
                          or other band files
                        </p>
                      </Dragger>
                      Note: For better user experience upload image of 270X360 pixels image.
                      {this.errorShow('add_image')}
                    </FormItem>
                  </Col>
                )}
              </Row>  
            </FormItem>
          </>
        )}

        


        {type === 'edit' && (
          <>
          <label className="font-weight-bold">Product Documents</label>
          {editProductDocument && editProductDocument.length > 0 ? (
                  editProductDocument &&
                  editProductDocument.map((item, index) => {
                    console.log(item, "00000000000000000000")
                    return (
                      <Col span={4} className="text-center">
                        <Button
                          type="danger"
                          className="ml-2"
                          shape="circle"
                          onClick={() => this.handleEditProductDocument(index)}
                        >
                          <Icon type="close-circle" theme="filled" />
                        </Button>
                        <br />
                        
                        <embed  src={item.imageUrl} width= "200" height= "200"></embed>
                        <Input
                        maxLength={70}
                        name={type === 'edit' ? 'documentTitle' : 'documentTitle'}
                        value={item.title}
                        onChange={(e) => this.handleDocumentInput(e.target.value)}
                      />
                      </Col>
                    );
                  }))
                  : 
          //   <FormItem label="Upload File Document">
          //     <Dragger
          //       listType="picture"
          //       onChange={this.onDocumentDragChange}
          //       showUploadList={errorType === 'edit_image' ? false : true}
          //       beforeUpload={this.beforeUpload}
          //       name={'file'}
          //       accept=".pdf"
          //       multiple={true}
          //     >
          //       <p className="ant-upload-drag-icon">
          //         <Icon type="inbox" />
          //       </p>
          //       <p className="ant-upload-text">Click or drag file to this area to upload</p>
          //       <p className="ant-upload-hint">
          //         Support for a single or bulk upload. Strictly prohibit from uploading company data
          //         or other band files
          //       </p>
          //     </Dragger>
          //     Note: For better user experience upload image of 1280*1280 pixels (square shape) image.
          //     {this.errorShow('edit_image')}
          //  </FormItem>
          productDocument && productDocument.map((item, i) => (
            <Fragment key={i}>
              <Card>
                <FormItem label="Add Product Document">
                  <Dragger
                    listType="picture"
                    // onChange={(e) => this.onDocumentDragChange(e, i)}
                    onChange={this.onEditDragDocumentChange}
                    showUploadList={errorType === 'productDocument' ? false : true}
                    beforeUpload={this.beforeUpload}
                    name={'file'}
                    multiple={true}
                    accept=".pdf"
                  >
                    <p className="ant-upload-drag-icon">
                      <Icon type="inbox" />
                    </p>
                    <p className="ant-upload-text">Click or drag file to this area to upload</p>
                    <p className="ant-upload-hint">
                      Support for a single or bulk upload. Strictly prohibit from uploading company data
                      or other band files
                    </p>
                  </Dragger>
                  Note: For better user experience upload Document of  File.
                  {this.errorShow('productDocument')}
                </FormItem>
                <Row form className="align-center justify-between" >
                  <Col md={10}>
                    <FormItem
                      label="Document File Name"
                      hasFeedback
                      className={cx({ 'has-error': errorType === 'documentTitle' })}
                    >
                      <Input
                        maxLength={70}
                        name={type === 'edit' ? 'documentTitle' : 'documentTitle'}
                        value={item.documntTitle}
                        onChange={(e) => this.handleDocumentInput(e.target.value, i)}
                      />
                      {this.errorShow('documentTitle')}
                    </FormItem>
                  </Col>
                  <Col md={1}>
                    {
                      productDocument.length === i + 1 && <Icon type="plus-circle" style={{ fontSize: '25px' }} onClick={() => this.addMoreDoc(i)} />
                    }

                  </Col>
                  {
                    productDocument.length > 1 && <Col md={1}>
                      <Icon type="minus-circle" style={{ fontSize: '25px' }} onClick={() => this.removeDoc(i)} />
                    </Col>
                  }

                </Row>
              </Card>
            </Fragment>
          ))
          }
          </>

        )}


        {type === 'add' &&
          productDocument && productDocument.map((item, i) => (
            <Fragment key={i}>
              <Card>
                <FormItem label="Add Product Document">
                  <Dragger
                    listType="picture"
                    onChange={(e) => this.onDocumentDragChange(e, i)}
                    showUploadList={errorType === 'productDocument' ? false : true}
                    beforeUpload={this.beforeUpload}
                    name={'file'}
                    multiple={true}
                    accept=".pdf"
                  >
                    <p className="ant-upload-drag-icon">
                      <Icon type="inbox" />
                    </p>
                    <p className="ant-upload-text">Click or drag file to this area to upload</p>
                    <p className="ant-upload-hint">
                      Support for a single or bulk upload. Strictly prohibit from uploading company data
                      or other band files
                    </p>
                  </Dragger>
                  Note: For better user experience upload Document of  File.
                  {this.errorShow('productDocument')}
                </FormItem>
                <Row form className="align-center justify-between" >
                  <Col md={10}>
                    <FormItem
                      label="Document File Name"
                      hasFeedback
                      className={cx({ 'has-error': errorType === 'documentTitle' })}
                    >
                      <Input
                        maxLength={70}
                        name={type === 'edit' ? 'documentTitle' : 'documentTitle'}
                        value={item.documntTitle}
                        onChange={(e) => this.handleDocumentInput(e.target.value, i)}
                      />
                      {this.errorShow('documentTitle')}
                    </FormItem>
                  </Col>
                  <Col md={1}>
                    {
                      productDocument.length === i + 1 && <Icon type="plus-circle" style={{ fontSize: '25px' }} onClick={() => this.addMoreDoc(i)} />
                    }

                  </Col>
                  {
                    productDocument.length > 1 && <Col md={1}>
                      <Icon type="minus-circle" style={{ fontSize: '25px' }} onClick={() => this.removeDoc(i)} />
                    </Col>
                  }

                </Row>
              </Card>
            </Fragment>
          ))}

        {isLoading && (
          <div className="mb-2 text-center">
            <Spin />
          </div>
        )}

        <div className="text-right pt-3">
          {type === 'add' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              onClick={this.handleAddData}
            >
              <Icon type="plus-circle" theme="filled" />
              Add
            </Button>
          )}
          {type === 'edit' && (
            <Button
              type="primary"
              htmlType="submit"
              onClick={this.handleEditData}
              className="btn-cta"
            >
              <Icon type="edit" theme="filled" /> Save
            </Button>
          )}
        </div>

        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  subCategory: state.subCategory.subCategory,
  category: state.category.category,
});

const mapDispatchToProps = dispatch => ({
  getSubCategoriesDetails: () => dispatch(getAllSubCategoryDetail()),
  getCategoriesDetails: () => dispatch(getAllCategoryDetail()),
  getAllSubCategoryDetailCategoryWiseFunction: category =>
    dispatch(getAllSubCategoryDetailCategoryWise(category)),
});

const WrappedFormComponent = Form.create()(withRouter(FormComponent));

export default withToastManager(connect(mapStateToProps, mapDispatchToProps)(WrappedFormComponent));

const GlobalStyle = createGlobalStyle`.ant-select-dropdown--multiple{display:none}`;
