import React from 'react';
import { ProductDetailCard } from '../../../../../components/Cards';
import { Link, withRouter } from 'react-router-dom';
import { Icon } from 'antd';
import './product-preview.scss'
const ProductPreview = props => {

  const { match, location } = props;
  const { state } = location;

  const backLink = match.url
    .split('/')
    .slice(0, -1)
    .join('/');


  return (
    <section>
      <div className="form-card__body p-lg-5 p-4">
        <Link to={backLink} className="ant-btn ant-btn-primary">
          <Icon type="caret-left" /> Back
        </Link>
        <ProductDetailCard {...state} />
      </div>
    </section>
  );
};

export default withRouter(ProductPreview);
