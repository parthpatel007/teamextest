import React from 'react';
import { Table, Tooltip, Icon, Form, Input, DatePicker, Button, Select } from 'antd';
import Skeleton from 'react-loading-skeleton';
import { Link } from 'react-router-dom';
import productImage from '../../../../../assets/images/productImage.png';
import classes from './style.module.scss';
import DeleteProduct from '../deleteProduct';
import axios from 'axios';
import { backendUrl } from '../../../../../common/credentials';
import { BlockModal } from '../../../../../components/Modal';
import { AccountStatistics } from '../../../../../components/Cards';
import styled from 'styled-components';
import getAllProductDetail from '../../../../../middleware/productMiddleware';
import { connect } from 'react-redux';
import {
  getAllSubCategoryDetail,
  getAllSubCategoryDetailCategoryWise,
} from '../../../../../middleware/subCategoryMiddleware';
import getAllCategoryDetail from '../../../../../middleware/categoryMiddleware';
import getAllProductCategoryWiseDetail from '../../../../../middleware/productCategoryWIseMiddleware';
import getAllProductSubCategoryWiseDetail from '../../../../../middleware/productSubCategoryWiseMiddleware';
import ReactHtmlParser from 'react-html-parser';
/* eslint-disable */

const FormItem = Form.Item;
const Option = Select.Option;
const { RangePicker } = DatePicker;

class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: '',
      endDate: '',
      loader: true,
      store: [],
      store_id: localStorage.getItem('store_id') ? localStorage.getItem('store_id') : '',
      listData: [],
      categories: [],
      subcategories: [],
      search: '',
      filterData: [],
      stockFilter: 0,
      statusFilter: 0,
      categoryId: undefined,
      subCategoryId: undefined,
    };
  }

  // getCategoryApi = () => {
  //   var self = this;
  //   const { store_id } = this.state;
  //   const authToken = localStorage.getItem('access_token');
  //   axios
  //     .get(`${backendUrl}/get-category/${store_id}`, {
  //       headers: {
  //         Authorization: authToken,
  //       },
  //     })
  //     .then(function (response) {
  //       if (response) {
  //         self.setState({
  //           categories: response.data.data,
  //         });
  //       }
  //     })
  //     .catch(function (error) {
  //       console.log('rrrr category err', error);
  //     });
  // };

  // getStoreApi = () => {
  //   var self = this;
  //   const authToken = localStorage.getItem('access_token');
  //   axios
  //     .get(`${backendUrl}/get-store`, {
  //       headers: {
  //         Authorization: authToken,
  //       },
  //     })
  //     .then(function (response) {
  //       if (response) {
  //         self.setState({
  //           store: response.data.data,
  //           store_id: localStorage.getItem('store_id') ? localStorage.getItem('store_id') : '',
  //         });
  //       }
  //     })
  //     .catch(function (error) {
  //       console.log('rrrr err', error);
  //     });
  // };

  handleStatusFilter = stock => {
    const { listData, statusFilter } = this.state;
    let filterDataHolder = [];
    if (statusFilter === stock) {
      filterDataHolder = [...listData];
    } else {
      filterDataHolder = [
        ...listData.filter(
          ({ status }) => (stock === 0 && status === 0) || (stock === 1 && status === 1)
        ),
      ];
    }
    this.setState({
      search: '',
      statusFilter: stock,
      stockFilter: stock,
      filterData: filterDataHolder,
    });
  };

  // getDataApi = () => {
  //   var self = this;
  //   const authToken = localStorage.getItem('access_token');
  //   axios
  //     .get(`${backendUrl}/api/products`, {
  //       headers: {
  //         Authorization: `Bearer ${authToken}`,
  //       },
  //     })
  //     .then(function (response) {
  //       if (response) {
  //         self.setState({
  //           listData: response.data.data,
  //           filterData: response.data.data.filter(
  //             data => self.store_id !== '' && self.state.store_id === data.store_id
  //           ),
  //         });
  //         console.log(listData, "listDatalistDatalistDatalistDatalistData New")
  //       }
  //     })
  //     .catch(function (error) {
  //       console.log('rrrr err', error);
  //     });
  // };

  getDataRangeApi = () => {
    const { startDate, endDate, listData } = this.state;
    const filterDataHolder = [
      ...listData.filter(
        ({ createdAt }) =>
          new Date(createdAt).getTime() > new Date(startDate).getTime() &&
          new Date(createdAt).getTime() < new Date(endDate).getTime()
      ),
    ];
    this.setState({
      filterData: filterDataHolder,
      statusFilter: 0,
    });
  };

  getDataApiWithStoreId = id => {
    var self = this;
    const { store_id } = this.state;
    this.setState({ listData: [], startDate: '', endDate: '' });
    const authToken = localStorage.getItem('access_token');
    axios
      .get(`${backendUrl}/api/get-product/${id}`, {
        headers: {
          Authorization: authToken,
        },
      })
      .then(function (response) {
        if (response) {
          self.setState({
            listData: response.data.data,
            filterData: response.data.data.filter(
              data => self.store_id !== '' && self.state.store_id === data.store_id
            ),
          });
        }
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };

  componentDidMount() {
    const { getProductsDetails, product, productLoader } = this.props;
    getProductsDetails();
    this.props.getCategoriesDetails();
    this.setState({
      filterData: product,
      listData: product,
      loader: productLoader,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { product } = this.props;

    const {
      getProductsCategoryWise,
      getProductsSubCategoryWise,
      getAllSubCategoryDetailCategoryWiseFunction,
    } = this.props;
    const { categoryId, subCategoryId } = this.state;

    if (prevState.categoryId !== categoryId) {
      getAllSubCategoryDetailCategoryWiseFunction(categoryId);
      if (categoryId !== '' && subCategoryId == '') {
        getProductsCategoryWise(categoryId);
        this.setState({
          filterData: product,
          listData: product,
        });
      }
      if (categoryId !== '' && subCategoryId !== '') {
        this.setState({
          subCategoryId: '',
          filterData: product,
          listData: product,
        });
        getProductsCategoryWise(categoryId);
      }
    }
    if (prevState.subCategoryId !== subCategoryId) {
      if (categoryId !== '' && subCategoryId !== '') {
        getProductsSubCategoryWise(categoryId, subCategoryId);
        this.setState({
          filterData: product,
          listData: product,
        });
      }
    }
    if (prevProps.product !== product) {
      this.setState({
        filterData: product,
        listData: product,
        loader: product && product.length >= 0 ? false : true,
      });
    }
  }

  handleSearch = e => {
    const { listData } = this.state;
    const filterDataHolder = [
      ...listData.filter(({ productName }) =>
        productName.toLowerCase().startsWith(e.target.value.trim().toLowerCase())
      ),
    ];
    this.setState({
      [e.target.name]: e.target.value,
      filterData: filterDataHolder,
      stockFilter: 0,
    });
  };

  handleStockFilter = stock => {
    const { listData, stockFilter } = this.state;
    let filterDataHolder = [];
    if (stockFilter === stock) {
      filterDataHolder = [...listData.filter(({ store_id }) => store_id === this.state.store_id)];
    } else {
      filterDataHolder = [
        ...listData.filter(
          ({ status, store_id }) =>
            ((stock === 1 && status === 0) || (stock === 2 && status === 1)) &&
            store_id === this.state.store_id
        ),
      ];
    }
    this.setState({
      search: '',
      stockFilter: stock !== stockFilter ? stock : 0,
      filterData: filterDataHolder,
    });
  };

  handleSelect = (name, value) => {
    const { listData } = this.state;
    const filterDataHolder = [...listData.filter(({ store_id }) => store_id === value)];
    this.setState({ [name]: value, stockFilter: 0 });
    this.getDataApiWithStoreId(value);
    if (!localStorage.getItem('store_id')) {
      localStorage.setItem('store_id', value);
    }
  };
  selectedStore() {
    const { store, store_id } = this.state;
    return (
      <SelectStore>
        <FormItem label="Choose Store" hasFeedback className="mb-0 d-flex align-items-center">
          <Select
            name="store_id"
            value={store_id}
            placeholder="Select Store"
            onChange={e => this.handleSelect('store_id', e)}
          >
            {store.map((data, index) => (
              <Option key={index} value={data._id}>
                {data.name}
              </Option>
            ))}
          </Select>
        </FormItem>
      </SelectStore>
    );
  }

  onDateChange = (date, dateString) => {
    const { listData } = this.state;
    // if(startDate !== ''){
    //   this.setState({

    //   });
    // }
    if (date.length == 0) {
      this.setState({ filterData: listData });
    }
    console.log('eee', date, 'dddd', dateString);
    this.setState({ startDate: dateString[0], endDate: dateString[1] });
  };

  onDateClick = (startDate, product, date, dateString) => {
    if (startDate !== '') {
      this.getDataRangeApi();
    }
    console.log('vvv', date, 'bbbbbbbbbbbnnnnnnnnnn', dateString);
  };

  onRangeSearch = () => {
    this.getDataRangeApi();
  };
  resetSearchFilter = () => {
    const { getProductsDetails } = this.props
    this.setState({
      categoryId: undefined,
      subCategoryId: undefined,
      search: ''
    })
    getProductsDetails()
  }

  render() {
    const { match, category, subCategory, getProductsDetails, productLoader } = this.props;
    // const { listData, filterData, startDate, search } = this.state;
    const dateFormat = 'YYYY-MM-DD';

    const {
      listData,
      categories,
      store,
      filterData,
      categoryId,
      subCategoryId,
      search,
      stockFilter,
      startDate,
    } = this.state;
    console.log(listData, "listData--------------------")
    const columns = [
      {
        title: 'Index',
        render: (text, record, index) => <> {index + 1} </>,
        width: 80,
      },
      // {
      //   title: 'Date',
      //   dataIndex: 'createdAt',
      //   // key: 'createdAt',
      //   render: text => (
      //     moment(text).format('DD-MM-YYYY')
      //     // console.log(text, moment(text.createdAt).format('DD-MM-YYYY'), "with moment" )
      //   ),
      //   width: 120,
      // },

      {
        title: 'Product-Code',
        dataIndex: 'productCode',
        Key: 'productCode',
        width: 120,
      },
      {
        title: 'Product Image',
        dataIndex: 'productImages',
        key: 'image',
        render: src => {
          src = src.filter(Boolean)
          return (
            <div>
              {src && src.length > 0 ? (
                <img className={classes.productImage} src={src[0].original ? src[0].original : src[0]} />
              ) : (
                <img className={classes.productImage} src={productImage} />
              )}
            </div>
          );
        },
        width: 120,
      },
      {
        title: 'Product-Name',
        dataIndex: 'productName',
        key: 'productName',
        // render: text => <a href={DEMO.link}>{text}</a>,
        width: 120,
      },
      {
        title: 'Category',

        render: (text, row) => {
          return row && row.categoryId && row.categoryId.categoryName;
        },
        width: 100,
      },
      {
        title: 'Sub-Category',

        render: (text, row) => {
          return row && row.subCategoryId && row.subCategoryId.subCategoryName;
        },
        width: 150,
      },
      {
        title: 'Product Status',
        dataIndex: 'productStatus',
        key: 'productStatus',
        width: 100,
      },
      {
        title: 'Ingredients',
        // dataIndex: 'ingredients',
        key: 'ingredients',
        render: (text, record) => {
          if ((text && text.ingredients && text.ingredients.length) >= 120) {
            return ReactHtmlParser(`${text.ingredients.substring(0, 120)}...`);
          } else {
            return ReactHtmlParser(`${text.ingredients}`);
          }
        },
        // render: (dataList = []) => dataList.join(', '),
        width: 300,
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        render: (text, row) => '₹' + text,
        width: 80,
      },
      {
        title: 'Quantity',
        dataIndex: 'quantity',
        Key: 'quantity',
        width: 100,
      },
      {
        title: 'Description',
        key: 'description',
        render: (text, record) => {
          if ((text && text.description && text.description.length) >= 35) {
            return ReactHtmlParser(`${text.description.substring(0, 35)}...`);
          } else {
            return ReactHtmlParser(`${text.description}`);
          }
        },
        width: 200,
      },
      {
        title: 'How to use',
        // dataIndex: 'howToUse',
        key: 'howToUse',
        render: (text, record) => {
          if ((text && text.howToUse && text.howToUse.length) >= 35) {
            return ReactHtmlParser(`${text.howToUse}...`);
          } else {
            return ReactHtmlParser(`${text.howToUse}`);
          }
        },
        // render: (dataList = []) => dataList.join(', '),
        width: 300,
      },

      {
        title: 'Action',
        key: 'action',
        render: (text, record) => {
          const filterStore = store.filter(data => data._id === text.store_id);
          const storeName = filterStore.length > 0 && filterStore[0].name;

          const filterCategories = categories.filter(data => data._id === text.category_id);
          const categoryName =
            filterCategories.length > 0 && filterCategories[0].title ? (
              filterCategories[0].title
            ) : (
              <center>-</center>
            );

          const filterData = {
            ...text,
            image: productImage,
            viewData: true,
            category: categoryName,
            store: storeName,
          };
          return (
            <span className="d-flex">
              <Tooltip placement="bottomLeft" title={'view'}>
                <Link
                  to={{ pathname: `${match.url}/preview`, state: text }}
                  className="ant-btn ant-btn-primary ant-btn-circle"
                >
                  <Icon type="eye" theme="filled" />
                </Link>
              </Tooltip>

              <Tooltip placement="bottomLeft" title={'edit'}>
                <Link
                  to={{ pathname: `${match.url}/edit`, state: text }}
                  className="ml-2 ant-btn ant-btn-primary ant-btn-circle"
                >
                  <Icon type="edit" theme="filled" />
                </Link>
              </Tooltip>

              <Tooltip placement="bottomLeft" title="delete">
                <span>
                  <DeleteProduct recordData={text} getProductsDetails={getProductsDetails} />
                </span>
              </Tooltip>

              <Tooltip placement="bottomLeft" title={text.status === 0 ? 'block' : 'unblock'}>
                <span>
                  <BlockModal
                    recordData={text}
                    recordName={text.productName}
                    api={'/api/changeProductStatus'}
                    type={text.status === 0 ? 'block' : 'unblock'}
                    btnText
                    blockTitleText="Block Product"
                    unblockTitleText="Unblock Product"
                    blockText="block Product"
                    unblockText="unblock Product"
                    blockBtnText="Block Product"
                    unblockBtnText="Unblock Product"
                  />
                </span>
              </Tooltip>
            </span>
          );
        },
        width: 170,
      },
    ];

    return (
      <>
        <AccountStatistics
          dateFilter={
            <div>
              <RangePicker
                style={{ width: 270 }}
                onChange={this.onDateChange}
                format={dateFormat}
                onClick={this.onDateClick}
              />
              <Button
                className="ml-2"
                onClick={() => this.onRangeSearch()}
                type="primary"
                disabled={startDate === ''}
              >
                Search
              </Button>
              {startDate !== '' && (
                <Button className="ml-2" onClick={() => window.location.reload()} type="danger">
                  Reset
                </Button>
              )}
            </div>
          }
          total={listData && listData.length}
          active={listData && listData.filter(({ status }) => status === 0).length}
          inactive={listData && listData.filter(({ status }) => status === 1).length}
          title="Products"
        />

        <section className="container-fluid container-mw-xxl no-breadcrumb chapter pt-0">
          <article className="article">
            <div className="d-flex justify-content-between">
              <div className="d-flex">
                <FormItem>
                  <Input
                    placeholder="Search Product"
                    prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                    name="search"
                    value={search}
                    onChange={this.handleSearch}
                  />
                </FormItem>
                 
                <FormItem>
                  
                  <Select
                  
                    className="ml-1"
                    style={{ width: 160 }}
                    name="categoryId"
                    value={categoryId}
                    optionFilterProp="children"
                    placeholder="Select Category"
                    onChange={e => this.handleSelect('categoryId', e)}
                  >
                    {category && category.map((data, index) => {
                      return (
                        <Option key={index} value={data._id}>
                          {data.categoryName}
                        </Option>
                      );
                    })}
                    
                    
                  </Select>
                </FormItem>

                <FormItem>
                  <Select
                    className="ml-1"
                    style={{ width: 160 }}
                    name="subCategoryId"
                    disabled={categoryId === '' ? true : false}
                    value={subCategoryId}
                    optionFilterProp="children"
                    placeholder="Select Sub Category"
                    onChange={e => this.handleSelect('subCategoryId', e)}
                  >
                    {subCategory && subCategory.map((data, index) => {
                      return (
                        <Option key={index} value={data._id}>
                          {data.subCategoryName}
                        </Option>
                      );
                    })}
                  </Select>
                </FormItem>

                {(subCategoryId !== '' || categoryId !== '') && (
                  <Button
                    className="ml-2 mt-1"
                    onClick={() => this.resetSearchFilter()}
                    type="danger"
                  >
                    Reset
                  </Button>
                )}
              </div>
              <div className="d-flex">
                <div>
                  <Button
                    className="mx-2"
                    onClick={() => this.handleStatusFilter(0)}
                    type={stockFilter === 0 ? 'primary' : 'secondary'}
                  >
                    Active
                  </Button>
                  <Button
                    className="mr-2"
                    onClick={() => this.handleStatusFilter(1)}
                    type={stockFilter === 1 ? 'primary' : 'secondary'}
                  >
                    Inactive
                  </Button>

                  <Link to={`${match.url}/add`} className="ant-btn ant-btn-primary">
                    <Icon type="plus-circle" theme="filled" /> Add
                  </Link>
                </div>
              </div>
            </div>
            {productLoader ? <Skeleton height={50} count={10} /> : (
              <Table
                columns={columns}
                dataSource={filterData}
                pagination={{ defaultPageSize: 15 }}
                className="ant-table-v1"
                scroll={{ x: 700 }}
                loading={productLoader}
              />
            )}
          </article>
        </section>
      </>
    );
  }
}

const mapStateToProps = state => ({
  product: state.product.product,
  productLoader: state.product.productLoader,
  subCategory: state.subCategory.subCategory,
  category: state.category.category,
});

const mapDispatchToProps = dispatch => ({
  getProductsDetails: () => dispatch(getAllProductDetail()),
  getProductsCategoryWise: categoryId => dispatch(getAllProductCategoryWiseDetail(categoryId)),
  getProductsSubCategoryWise: (categoryId, subCategoryId) =>
    dispatch(getAllProductSubCategoryWiseDetail(categoryId, subCategoryId)),
  getSubCategoriesDetails: () => dispatch(getAllSubCategoryDetail()),
  getCategoriesDetails: () => dispatch(getAllCategoryDetail()),
  getAllSubCategoryDetailCategoryWiseFunction: category =>
    dispatch(getAllSubCategoryDetailCategoryWise(category)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);

const SelectStore = styled('div')`
  .ant-select {
    min-width: 200px;
  }
`;
