import React from 'react';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';

import { Table, Modal, Icon, Button, Select, Form, Input, DatePicker, Tooltip } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import DEMO from 'constants/demoData';
// import productImage from '../../../../../assets/images/productImage.png';
import classes from './style.module.scss';
// import ViewDetails from './viewDetail';
import mockData from './mockData';
import getAllProductDetail from '../../../../../middleware/productMiddleware';
import { OrderInfoCard } from '../../../../../components/Cards';
import getAllOrderDetail from '../../../../../middleware/orderMiddleware';
import { connect } from 'react-redux';
import axios from 'axios';
import { backendUrl } from '../../../../../common/credentials';
import { UserInvoice } from '../../../../../components/Cards/OrderDetailCard/invoice';
import { PDFViewer } from '@react-pdf/renderer';
const Option = Select.Option;
const FormItem = Form.Item;
const { RangePicker } = DatePicker;

const filterFields = [
  {
    id: 1,
    value: 'on going',
    name: 'On Going',
  },
  {
    id: 2,
    value: 'past',
    name: 'Past',
  },
  {
    id: 3,
    value: 'cancelled',
    name: 'Cancelled',
  }
];

class Article extends React.Component {
  state = {
    selectedFilter: 1,
    listData: [],
    search: '',
    filterData: [],
    loader: true,
    countFilter: 'all',
    startDate: '',
    endDate: '',
    visible: false,
    orderStatus: null,
    orderRecord: null,
    orderId: null
  };
  updateListing = id => this.setState({ selectedFilter: id });
  handleSearch = e => {
    const { listData, countFilter, search } = this.state;

    let filterDataHolder = [];

    if (countFilter === 'all') {
      console.log("inside all csn inside all csn inside all csn inside all csn inside all csn inside all csn inside all csn inside all csn inside all csn ")
      filterDataHolder = [
        ...listData.filter(({ addedBy }) =>
          addedBy && addedBy.userName.toLowerCase().startsWith(e.target.value.trim().toLowerCase())
        ),
      ];
      console.log(filterDataHolder, 'filterDataHolder filterDataHolder filterDataHolder filterDataHolder filterDataHolder filterDataHolder filterDataHolder filterDataHolder filterDataHolder filterDataHolder ')
    } else {
      filterDataHolder = [
        ...listData.filter(
          ({ name, status }) =>
            status === countFilter &&
            name.toLowerCase().startsWith(e.target.value.trim().toLowerCase())
        ),
      ];
    }

    this.setState({
      [e.target.name]: e.target.value,
      filterData: filterDataHolder,
      // stockFilter: 0,
    });
  };

  showModal = (text) => {
    console.log(text.orderStatus, 'asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj asdasfdasdj ')
    this.setState({
      visible: true,
      orderStatus: text.orderStatus,
      orderId: text._id
    });
  };
  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };


  changeOrderStatus = (value) => {
    var self = this;
    const { orderStatus, orderId } = this.state
    const { api, type, getStoreDetail } = this.props;

    const authToken = localStorage.getItem('access_token');
    const current_store = JSON.parse(localStorage.getItem('current_store'))

    console.log(authToken, 'auth token')
    console.log(backendUrl, 'backendUrl')
    axios.post(`${backendUrl}/order/changeorderstatus`, {
      storeid: current_store._id,
      orderid: orderId,
      order_status: orderStatus
    },
      {
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
      }
    )
      .then(async function (response) {
        console.log('rrrr res', response.data.data);
        if (response) {
          await getStoreDetail()
          window.location.reload();
        }
      })
      .catch(function (error) {
        self.setState({
          visible: false,
        });
        // ErrorNotify(error.response)
        console.log('rrrr err', error);
      });
  }


  componentDidMount() {

    const { getOrdersDetails, order } = this.props;

    getOrdersDetails()

    this.setState({
      filterData: order,
      listData: order,
      loader: order.length >= 0 ? false : true
    });

    
    if(this.props.orderSuccess){
      this.setState({
        loader: false
      });
    }
    else{
      this.setState({
        loader: true
      });
    }

  }


  componentDidUpdate(prevProps, prevState) {

    const { order } = this.props;

    console.log(prevState, "state state state state state state state ")

    console.log(this.props, "PRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndksPRposdmmkdsndks ")

    if (prevProps.order !== order) {
      this.setState({
        filterData: order,
        listData: order,
        loader: order.length >= 0 ? false : true
      })
    }

  }


  showPreviewModal = (text) => {
    this.setState({
      preview: true,
      orderRecord: { ...text },
    });
  }

  onDateChange = (date, dateString) => {
    const { listData } = this.state
    // if(startDate !== ''){
    //   this.setState({

    //   });
    // }
    if (date.length == 0) {
      this.setState({ filterData: listData })
    }
    console.log('eee', date, 'dddd', dateString);
    this.setState({ startDate: dateString[0], endDate: dateString[1] });
  };

  onDateClick = (startDate, product, date, dateString) => {
    if (startDate !== '') {
      this.getDataRangeApi();
    }
    console.log('vvv', date, 'bbbbbbbbbbbnnnnnnnnnn', dateString);
  }

  handleChange = (value) => {
    this.setState({ orderStatus: value })
  }
  getDataRangeApi = () => {
    const { startDate, endDate, listData } = this.state;
    const filterDataHolder = [
      ...listData.filter(
        ({ createdAt }) =>
          (new Date(createdAt).getTime() > new Date(startDate).getTime() && new Date(createdAt).getTime() < new Date(endDate).getTime())
      )
    ];
    this.setState({
      filterData: filterDataHolder,
      statusFilter: 0,
    });
  };
  onRangeSearch = () => {
    this.getDataRangeApi();
  };


  handleCountFilter = e => {
    const { listData, countFilter, search } = this.state;

    let filterDataHolder = [];
    if (e === 'all') {
      filterDataHolder = [
        ...listData.filter(({ addedBy }) => addedBy.userName.toLowerCase().includes(search.trim().toLowerCase())),
      ];
    } else {
      console.log(e, "order status order status order status order status order status order status order status ")
      filterDataHolder = [
        ...listData.filter(
          ({ addedBy, orderStatus }) =>
            orderStatus === e && addedBy && addedBy.userName.toLowerCase().includes(search.trim().toLowerCase())
        ),
      ];
    }

    this.setState({
      countFilter: e,
      filterData: filterDataHolder,
    });
  };





  changeOrderStatus = (value) => {
    var self = this;
    const { orderStatus, orderId } = this.state
    const { api, type, getStoreDetail } = this.props;

    const authToken = localStorage.getItem('access_token');
    const current_store = JSON.parse(localStorage.getItem('current_store'))

    console.log(authToken, 'auth token')
    console.log(backendUrl, 'backendUrl')
    console.log("orderstatus chaange orderstatus chaange orderstatus chaange orderstatus chaange orderstatus chaange orderstatus chaange orderstatus chaange orderstatus chaange orderstatus chaange orderstatus chaange orderstatus chaange orderstatus chaange ")
    axios.post(`${backendUrl}/api/changeOrderStatus`, {
      orderId: orderId,
      orderStatus: orderStatus
    },
      {
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
      }
    )
      .then(async function (response) {
        console.log('rrrr res', response.data.data);
        if (response) {
          // await getStoreDetail()
          window.location.reload();
        }
      })
      .catch(function (error) {
        self.setState({
          visible: false,
        });
        // ErrorNotify(error.response)
        console.log('rrrr err', error);
      });
  }



  render() {
    const { orderStatus, loader } = this.state;
    const { match } = this.props;
    const columns = [
      // {
      //   title: 'Product Image',
      //   dataIndex: 'image',
      //   key: 'image',
      //   render: src => (
      //     <div className="text-center">
      //       <img className={classes.productImage} src={src} />
      //     </div>
      //   ),
      // },
      {
        title: 'Order Number',
        dataIndex: 'orderNo',
        key: 'orderNumber',
        // render: (text, row) => {
        //   return text && text.orderNo;
        // }

      },
      {
        title: 'Customer',
        dataIndex: 'addedBy',
        key: 'addedBy',
        render: (text, row) => {
          return text && text.userName;
        },
        // render: (text) => {
        //   return text && text.addedBy && text.addedBy.email
        // }
      },
      {
        title: 'Total Cost',
        key: 'totalCost',
        render: (text) => {
          if (text.isSubscription) {
            return '-'
          }
          return '₹' +  text.discountedTotal
        }
      },
      {
        title: 'Delivery Address',
        key: 'deliveryAddress',
        render: (text) => {
          console.log(text.addressId, "addressindex addressindex addressindex addressindex addressindex addressindex addressindex addressindex addressindex ")
          if (text.addressId === undefined) {
            return
          }
          const add = `${text && text.addressId && text.addressId.flatNo} ${' '}  ${text && text.addressId && text.addressId.street} ${' '}  ${text && text.addressId && text.addressId.landmark} ${' '} ${text && text.addressId && text.addressId.city} ${' '}  ${text && text.addressId && text.addressId.state} ${' '} ${text && text.addressId && text.addressId.postalCode}`
          return add
        }
      },
      {
        title: 'Phone',
        dataIndex: 'addedBy',
        key: 'customerMobile',
        render: (text, row) => {
          return text && text.phone;
        },
      },

      {
        title: 'Email',
        dataIndex: 'addedBy',
        key: 'customerEmail',
        render: (text, row) => {
          return text && text.email;
        },
      },
      {
        title: 'Payment Method',
        dataIndex: 'paymentMode',
        key: 'method',

      },
      {
        title: 'Status',
        dataIndex: 'orderStatus',
        key: 'method',

      },
      {
        title: 'Invoice',
        dataIndex: 'invoice',
        key: 'invoice',
        render: (text, record) => {
          console.log(text, 'textasdadasdasd')
          console.log(record, 'recordasdadasdasd')
          const { name, customer, image, method, status, dec, order_status, payment_mode, createdAt, first_name, match, order_number, last_name, email, phone, address, address2, city, country, postal_code, original_total, discounted_total, item } = record
          // const current_store = JSON.parse(localStorage.getItem('current_store'))
          // Font.register({
          //   family: 'Oswald',
          //   src: font
          // });
          return (
            <>
              <Button
                onClick={() => this.showPreviewModal(record)}
                className="ant-btn ant-btn-primary"
              >
                <label>Preview</label>
              </Button>

            </>
          )

        },
        width: 130
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span className="d-flex">
            {/* <Link to={`${match.url}/details`} className="ant-btn ant-btn-primary">
              View Details
          </Link> */}
          <Tooltip placement="bottomLeft" title={"view"}>
  <span>
            <Link
              to={{ pathname: `${match.url}/details`, state: text }}
              className="ant-btn ant-btn-primary ant-btn-circle"
            >
              <Icon type="eye" theme="filled" />
            </Link>
            </span>
            </Tooltip>

            <Tooltip placement="bottomLeft" title="Status change">
              <span> 
            <Button type="primary" shape="circle" className="ml-2 mr-2" onClick={() => this.showModal(record)}>
              <Icon type={'api'} theme={'block' && 'filled'} />
            </Button>
            </span>
            </Tooltip>
          </span>
        ),
      },
    ];
    const { selectedFilter, filterData, search, countFilter, startDate, endDate } = this.state;
    const dateFormat = 'YYYY-MM-DD';
    return (
      <>
      <OrderInfoCard
      order={this.props.order}
      orderCardInfo={this.props.orderCardInfo}
      filter={
        <div style={{ textAlign: 'right' }}>
          <RangePicker
            style={{ width: 270 }}
            onChange={this.onDateChange}
            format={dateFormat}
            onClick={this.onDateClick}
          />
          <Button
            className="ml-2"
            onClick={() => this.onRangeSearch()}
            type="primary"
            disabled={startDate === ''}
          >
            Search
              </Button>
          {startDate !== '' && (
            <Button className="ml-2" onClick={() => window.location.reload()} type="danger">
              Reset
            </Button>
          )}
        </div>
      }
        />
        <article className="article">
          <div className="d-flex align-items-center mb-3">
            <FormItem className="mb-0">
              <Input
                placeholder="Search order "
                prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                name="search"
                value={search}
                onChange={this.handleSearch}
              />
            </FormItem>
            <div>
              {filterFields.map(({ id, value, name }) => {
                return (
                  <Button
                    className="ml-2"
                    key={id}
                    onClick={() => this.handleCountFilter(value)}
                    type={countFilter === value ? 'primary' : 'secondary'}
                  >
                    {name}
                  </Button>
                );
              })}
            </div>
          </div>

          {loader&&
              <Skeleton height={50} count={10}/> 
              }

          {!loader&&   

          <Table
            columns={columns}
            dataSource={filterData}
            className="ant-table-v1"
            scroll={{ x: 700 }}
          />}
        </article>


        {/* <Modal
          title={`Change Order Status`}
          visible={this.state.visible}
          onOk={() => this.changeOrderStatus(orderStatus)}
          okText={'Change Status'}
          onCancel={this.handleCancel}
          centered
        ></Modal> */}



        <Modal
          title={`Change Order Status`}
          visible={this.state.visible}
          onOk={() => this.changeOrderStatus(orderStatus)}
          okText={'Change Status'}
          onCancel={this.handleCancel}
          centered
        >
          <section className="form-card">
            <div className="form-card__body ">
              <label style={{ fontSize: 16 }}>
                Do you want to change order status? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Select defaultValue={orderStatus} style={{ width: 120 }} onChange={this.handleChange}>
                  <Option value="on going">On Going</Option>
                  <Option value="past">Past</Option>
                  <Option value="cancelled">
                    Cancelled
                  </Option>
                </Select>
              </label>
            </div>
          </section>
        </Modal>



        <Modal
          width={900}
          title={``}
        footer={null}
          visible={this.state.preview}
          onOk={() => this.setState({ preview: !this.state.preview })}
          // okText={type === 'block' ? blockBtnText : unblockBtnText}
          onCancel={() => this.setState({ preview: !this.state.preview })}
          // footer={null}
          centered
        >
          <div className="w-100">
            <PDFViewer width={'100%'} height={600}>
              <UserInvoice {...this.state.orderRecord} />
            </PDFViewer>
          </div>
        </Modal>

      </>
    );
  }
}


const mapStateToProps = state => ({
  order: state.order.order,
  orderSuccess: state.order.orderSuccess,
  orderCardInfo: state.order.orderCardInfo
})

const mapDispatchToProps = dispatch => ({
  getOrdersDetails: () => dispatch(getAllOrderDetail())
})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Article));
