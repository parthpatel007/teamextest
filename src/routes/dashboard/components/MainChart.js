// import React from 'react';
// import ReactEcharts from 'echarts-for-react';
// import 'echarts/theme/macarons';

// const COLOR = {
//   success:    'rgba(139,195,74,.7)',
//   info:       'rgba(1,188,212,.7)',
//   text:       '#3D4051',
//   gray:       '#EDF0F1'
// }

// let combo = {};

// combo.option = {
//   legend: {
//     show: true,
//     x: 'right',
//     y: 'top',
//     data: ['WOM', 'Viral', 'Paid']
//   },
//   grid: {
//     x: 40,
//     y: 60,
//     x2: 40,
//     y2: 30,
//     borderWidth: 0
//   },
//   tooltip: {
//     show: true,
//     trigger: 'axis',
//     axisPointer: {
//       lineStyle: {
//         color: COLOR.gray
//       }
//     }
//   },
//   xAxis: [
//     {
//       type : 'category',
//       axisLine: {
//         show: false
//       },
//       axisTick: {
//         show: false
//       },
//       axisLabel: {
//         textStyle: {
//           color: '#607685'
//         }
//       },
//       splitLine: {
//         show: false,
//         lineStyle: {
//           color: '#f3f3f3'
//         }
//       },
//       data : [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
//     }
//   ],
//   yAxis: [
//     {
//       type : 'value',
//       axisLine: {
//         show: false
//       },
//       axisTick: {
//         show: false
//       },
//       axisLabel: {
//         textStyle: {
//           color: '#607685'
//         }
//       },
//       splitLine: {
//         show: true,
//         lineStyle: {
//           color: '#f3f3f3'
//         }
//       }
//     }
//   ],
//   series: [
//     {
//       name:'WOM',
//       type:'bar',
//       clickable: false,
//       itemStyle: {
//         normal: {
//           color: COLOR.gray
//         },
//         emphasis: {
//           color: 'rgba(237,240,241,.7)'
//         }
//       },
//       barCategoryGap: '50%',
//       data:[75,62,45,60,73,50,31,56,70,63,49,72,76,67,46,51,69,59,85,67,56],
//       legendHoverLink: false,
//       z: 2
//     },
//     {
//       name:'Viral',
//       type:'line',
//       animation: false,
//       smooth:true,
//       itemStyle: {
//         normal: {
//           color: COLOR.success,
//           areaStyle: {
//             color: COLOR.success,
//             type: 'default'
//           }
//         }
//       },
//       data:[0,0,0,5,20,15,30,28,25,40,60,40,43,32,36,23,12,15,2,0,0],
//       symbol: 'none',
//       legendHoverLink: false,
//       z: 3
//     },
//     {
//       name:'Paid',
//       type:'line',
//       animation: false,
//       smooth:true,
//       itemStyle: {
//         normal: {
//           color: COLOR.info,
//           areaStyle: {
//             color: COLOR.info,
//             type: 'default'
//           }
//         }
//       },
//       data:[0,0,0,0,1,6,15,8,16,9,25,12,50,20,25,12,2,1,0,0,0],
//       symbol: 'none',
//       legendHoverLink: false,
//       z: 4
//     }
//   ]
// };

// const Chart = () => (
//   <div className="box box-default mb-4">
//     <div className="box-body">
//       <ReactEcharts option={combo.option} theme={"macarons"}  style={{height: '450px'}} />
//     </div>
//   </div>
// );

// export default Chart;

/* eslint-disable */




import React, { Component } from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import moment from 'moment'

const COLOR = {
  success: 'rgba(139,195,74,.7)',
  info: 'rgba(1,188,212,.7)',
  text: '#3D4051',
  gray: '#EDF0F1'
}

let combo = {};

class Chart extends Component {




  render() {
    const { dashboardData, date, dateString } = this.props
    // console.log(dateString, dashboardData, " dashboardData dashboardData dashboardData dashboardData ")
    let xAxisData = []
    let salesData = []
    let orderData = []

    dashboardData && dashboardData.orders.map((item) => {
      return (
        orderData.push(item.totalSale)
      )
    })
    dashboardData && dashboardData.sales.map((item) => {
      return (
        salesData.push(item.totalSale)
      )
    })
    // console.log(salesData,"salesData salesDatasalesDatasalesData salesDatasalesData salesData")
    // console.log(orderData,"salesData salesDatasalesDatasalesData salesDatasalesData salesData")

    const d1 = new Date(dateString !== null ? dateString.length && dateString[0] : new Date((new Date()).getMonth() - 1)),
      d2 = new Date(dateString != null ? dateString.length && dateString[1] : moment(d1).add(1, 'M')),
      diff = (d2 - d1) / 864e5,
      dates = Array.from(
        { length: diff + 1 },
        (_, i) => {
          const date = new Date()
          date.setDate(d1.getDate() + i)
          const [dates] = moment(date).format("DD-MMM").split(', ')
          return dates
        }
      )
      // console.log("statdate: date123 ",d1);
      // console.log("enddate:  date123",d2);
    // const d1 = new Date((new Date()).getMonth() - 1),
    //   d2 = moment(d1).add(1, 'M'),
    //   diff = (d2 - d1) / 864e5,
    //   dates = Array.from(
    //     { length: diff + 1 },
    //     (_, i) => {
    //       const date = new Date()
    //       date.setDate(d1.getDate() + i)
    //       const [dates] = moment(date).format("DD-MMM").split(', ')
    //       return dates
    //     }
    //   )

    // console.log(dates, "dvsnjrbdsjkvbjkdsbvjb")

    combo.option = {
      legend: {
        show: true,
        x: 'right',
        y: 'top',
        data: ['Order', 'Sales']
      },
      grid: {
        x: 40,
        y: 60,
        x2: 40,
        y2: 30,
        borderWidth: 0
      },
      tooltip: {
        show: true,
        trigger: 'axis',
        axisPointer: {
          lineStyle: {
            color: COLOR.gray
          }
        }
      },
      xAxis: [
        {
          type: 'category',
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            textStyle: {
              color: '#607685'
            }
          },
          splitLine: {
            show: false,
            lineStyle: {
              color: '#f3f3f3'
            }
          },
          data: dates
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            textStyle: {
              color: '#607685'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#f3f3f3'
            }
          }
        }
      ],
      series: [
        {
          name: 'Order',
          type: 'bar',
          clickable: false,
          itemStyle: {
            normal: {
              color: '#3498db'
            },
            emphasis: {
              color: '#2980b9'
            }
          },
          barCategoryGap: '50%',
          data: orderData,
          legendHoverLink: false,
          z: 2
        },
        {
          name: 'Sales',
          type: 'line',
          animation: false,
          smooth: true,
          itemStyle: {
            normal: {
              color: COLOR.success,
              areaStyle: {
                color: COLOR.success,
                type: 'default'
              }
            }
          },
          data: salesData,
          symbol: 'none',
          legendHoverLink: false,
          z: 3
        },
        // {
        //   name:'Paid',
        //   type:'line',
        //   animation: false,
        //   smooth:true,
        //   itemStyle: {
        //     normal: {
        //       color: COLOR.info,
        //       areaStyle: {
        //         color: COLOR.info,
        //         type: 'default'
        //       }
        //     }
        //   },
        //   data:[0,0,0,0,1,6,15,8,16,9,25,12,50,20,25,12,2,1,0,0,0],
        //   symbol: 'none',
        //   legendHoverLink: false,
        //   z: 4
        // }
      ]
    };
    return (
      <div className="box box-default mb-4" >
          {
            dashboardData && dashboardData.sales && dashboardData.sales.length > 0 &&
        <div className="box-body">
            <ReactEcharts option={combo.option} theme={"macarons"} style={{ height: '450px' }} />
            
          {/*  // <div>
            // <span style={{display: 'block', textAlign: 'center', color: '#643e98', fontSize: '20px', fontWeight: 500}}>No data found</span>
          // <div>*/}
        </div>
          }
      </div>
    )
  }
}

export default Chart;



