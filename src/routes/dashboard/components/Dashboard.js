import React from 'react';
import QueueAnim from 'rc-queue-anim';
import MainChart from './MainChart';
import NumberCards from './NumberCards';
import axios from 'axios'
import { backendUrl } from '../../../common/credentials';
import TableData from './TableData';

class Dashboard extends React.Component {

  state = {
    store_id: null,
    dashboardData: null,
    date: null,
    dateString: null
  }

  selectDateRangeString = (date, dateString) => {
    this.setState({ date: date, dateString: dateString })
  }

  componentDidMount() {
    const { dateString } = this.state
    const authToken = localStorage.getItem('access_token')
    const headers = {
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    }
    let endpoint;
    if (dateString == null) {
      endpoint = `${backendUrl}/api/dashboardData`
    }
    else {
      endpoint = `${backendUrl}/api/dashboardData?startDate=${dateString && dateString[0].toISOString()}&endDate=${dateString && dateString[1].toISOString()}`
    }
    axios.get(endpoint, headers)
      .then((response) => {
        this.setState({ dashboardData: response.data.data })
      })
      .catch((error) => {

      })
  }


  componentDidUpdate(prevProps, prevState) {
    const { dateString } = this.state
    if (prevState.dateString !== dateString) {
      const authToken = localStorage.getItem('access_token')
      const headers = {
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
      }
      let endpoint;
      if (dateString == null) {
        endpoint = `${backendUrl}/api/dashboardData`
      }
      else { }

      console.log(endpoint, 'endpoint')
      axios.get(endpoint, headers)
        .then((response) => {
          this.setState({ dashboardData: response.data.data })
        })
        .catch((error) => {

        })

    }

  }


  render() {
    return (
      <div className="container-fluid no-breadcrumb page-dashboard">
        {/* <Filter
          dateRangestring={this.selectDateRangeString}
        /> */}
        <QueueAnim type="bottom" className="ui-animate">
          <div key="1">
            {' '}
            <NumberCards dashboardData={this.state.dashboardData} />{' '}
          </div>
          {/* <div key="2">
            {' '}
            <div className="d-flex justify-content-between">
              <div>
                <h2 className="small-title pt-0">Recent Orders</h2>
              </div>
            </div>
            <OrdersTable />{' '}
          </div> */}
          <div key="3" className={'mb-1'}>
            {' '}
            {/* <ProjectTable dashboardData={dashboardData} />{' '} */}
            <TableData dashboardData={this.state.dashboardData} />{' '}
          </div>
          <div key="4">
            {' '}
            <MainChart
              dashboardData={this.state.dashboardData}
              date={this.state.date}
              dateString={this.state.dateString}
            />{' '}
          </div>
        </QueueAnim>
      </div>)
  }
}
export default Dashboard;
