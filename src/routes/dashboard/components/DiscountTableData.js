import React from 'react';
import { Table, Icon, Select, Form, DatePicker, Button, Input } from 'antd';
import styled from 'styled-components';
import { connect } from 'react-redux';
// import getAllDiscountDetail from '../../../../middleware/discountMiddleware';
import moment from 'moment'
import getAllDiscountDetail from '../../../middleware/discountMiddleware';


/* eslint-disable */

const Option = Select.Option;
const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const expDateFormat = 'DD/MM/YYYY';

class DiscountTableData extends React.Component {
    state = {
        startDate: '',
        endDate: '',
        listData: [],
        store: [],
        categories: [],
        search: '',
        subcategories: [],
        store: [],
        filterData: [],
        defaultSuperCatName: '',
        defaultSuperCatId: '',
        defaultStoreName: '',
        defaultStoreId: '',
        categoryId: '',
        subCategoryId: '',
        superCategoryId: '',
        orderType: ''
    };


    handleSelect = (name, value) => {
        console.log("on handel select callon handel select callon handel select callon handel select callon handel select callon handel select call")
        console.log("name is", name, value, "valu issdsddsd")
        this.setState({ [name]: value });

    };

    getDataRangeApi = () => {
        const { startDate, endDate, listData } = this.state;
        const filterDataHolder = [
            ...listData.filter(
                ({ createdAt }) =>
                    (new Date(createdAt).getTime() > new Date(startDate).getTime() && new Date(createdAt).getTime() < new Date(endDate).getTime())
            )
        ];

        this.setState({
            filterData: filterDataHolder,
            statusFilter: 0,
        });
    };

    handleSelect = (name, value) => {

        this.setState({ [name]: value });

    };


    componentDidMount() {
        const { getDiscountDetails, superCategory, discount } = this.props

        const current_store = JSON.parse(localStorage.getItem('current_store'));
        this.setState({
            filterData: discount,
            listData: discount,
            defaultStoreId: current_store !== null && current_store._id,
            defaultSuperCatId: superCategory && superCategory.length && superCategory[0]._id,
            // defaultStoreName: current_store.name,
            // defaultSuperCatName: superCategory && superCategory.length && superCategory[0].supercategoryname
        });
        getDiscountDetails()
    }

    componentDidUpdate(prevProps, prevState) {
        const {

            getDiscountTypeDetail,
            discount
        } = this.props
        const {
            defaultSuperCatId,
            defaultStoreId,

            orderType
        } = this.state;

        // console.log(prevState, "onstate update onstate update onstate update onstate update onstate update onstate update update onstate update update onstate update update onstate update ")

        if (prevState.orderType !== orderType) {
            getDiscountTypeDetail(defaultStoreId, defaultSuperCatId, orderType)
            this.setState({
                categoryId: '',
                subCategoryId: '',
                filterData: discount,
                listData: discount,
            })
        }

        if (prevState.defaultSuperCatId !== defaultSuperCatId) {


        }
        if (prevProps.discount !== discount) {
            this.setState({
                filterData: discount,
                listData: discount,
            })
        }

    }

    handleSearch = e => {
        const { listData } = this.state;
        console.log(listData, 'list data')
        const filterDataHolder = [
            ...listData.filter(({ title, store_id }) =>
                title.toLowerCase().includes(
                    e.target.value.trim().toLowerCase()
                    // && store_id === this.state.store_id
                )
            ),
        ];
        this.setState({
            [e.target.name]: e.target.value,
            filterData: filterDataHolder,
            stockFilter: 0,
        });
    };

    selectedStore() {
        const { store, store_id } = this.state;
        return (
            <SelectStore>
                <FormItem label="Choose Store" hasFeedback className="mb-0 d-flex align-items-center">
                    <Select
                        name="store_id"
                        value={store_id}
                        placeholder="Select Store"
                        onChange={e => this.handleSelect('store_id', e)}
                    >
                        {store.map((data, index) => (
                            <Option key={index} value={data._id}>
                                {data.name}
                            </Option>
                        ))}
                    </Select>
                </FormItem>
            </SelectStore>
        );
    }
    onDateChange = (date, dateString) => {
        console.log('eee', date, 'dddd', dateString);
        const { listData } = this.state
        if (date.length == 0)
            this.setState({ filterData: listData })
        this.setState({ startDate: dateString[0], endDate: dateString[1] });
    };
    onRangeSearch = () => {
        this.getDataRangeApi();
    };
    render() {
        const {
            discount,
        } = this.props;

        const {
            filterData
        } = this.state;

        // console.log(discount, "get discount ")
        const columns = [
            {
                title: 'Date',
                key: 'createdAt',
                dataIndex: 'createdAt',
                render: text => moment(text).format('DD-MM-YYYY '),
                width: 250,
            },
            {
                title: 'Offer Name',
                dataIndex: 'discountName',
                width: 250,
                key: 'discountName',
            },
            {
                title: 'Offer',
                key: 'offer',
                width: 250,
                render: (text) => {
                    if (text.measure == 'price') {
                        return text.offer + ' Rs.';
                    }
                    else {
                        return text.offer + ' %';
                    }
                }
            },
            {
                title: 'Expiry Date',
                dataIndex: 'expiryDate',
                key: 'expiryDate',
                width: 250,
                render: (text, row) => {
                    return moment(text).format(expDateFormat)
                },
            },
            // {
            //     title: 'Status',
            //     key: 'status',
            //     render: (text, record) => (
            //         <b className={`text-${text.status === 0 ? 'success' : 'danger'}`}>
            //             {text.status === 0 ? 'Active' : 'Inactive'}
            //         </b>
            //     ),
            // },

        ];
        return (
            <>
                <div className="mb-2">
                    <span className="recent-data-title">Recent Discounts</span>
                </div>
                <article className="article pb-0">
                    <Table
                        columns={columns}
                        dataSource={filterData.slice(0, 10)}
                        className="ant-table-v1"
                        scroll={{ x: 1000 }}
                        pagination={false}
                    />
                </article>
            </>
        );
    }
}


// const mapStateToProps = state => ({
//     superCategory: state.superCategory.superCategory,
//     discount: state.discount.discount,
//     category: state.category.category,
//     subCategory: state.subCategory.subCategory,
// })

// const mapDispatchToProps = dispatch => ({

//     getDiscountDetails: (defaultStoreId, defaultSuperCatId) => dispatch(getAllDiscountDetail(defaultStoreId, defaultSuperCatId)),

// })

const mapStateToProps = state => ({
    discount: state.discount.discount
})

const mapDispatchToProps = dispatch => ({
    getDiscountDetails: () => dispatch(getAllDiscountDetail())
})


export default connect(mapStateToProps, mapDispatchToProps)(DiscountTableData);


const SelectStore = styled('div')`
  .ant-select {
    min-width: 200px;
  }
`;
