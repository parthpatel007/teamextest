import React, { Component } from 'react'
import { Row, Col } from 'antd'
import OrderTableData from './OrderTableData'
import UserTableData from './UserTableData'
import DiscountTableData from './DiscountTableData'
import TransactionTableData from './TransactionTableData'

/* eslint-disable */
class TableData extends Component {

    state = {
        selectedValue: 'order'
    }

    render() {
        const { selectedValue } = this.state
        return (
            <div className="dashboard-table-data">
                <Row className="dashboard-data mb-4">
                    <Col
                        sm={6}
                        md={6}
                        className={selectedValue == 'order' ? "tb-header-text-selected" : "tb-header-text"}
                        onClick={() => this.setState({ selectedValue: 'order' })}
                    >
                        Orders
                    </Col>
                    <Col
                        sm={6}
                        md={6}
                        className={selectedValue == 'user' ? "tb-header-text-selected" : "tb-header-text"}
                        onClick={() => this.setState({ selectedValue: 'user' })}
                    >
                        Users
                    </Col>
                    <Col
                        sm={6}
                        md={6}
                        className={selectedValue == 'discount' ? "tb-header-text-selected" : "tb-header-text"}
                        onClick={() => this.setState({ selectedValue: 'discount' })}
                    >
                        Discounts
                    </Col>
                    <Col
                        sm={6}
                        md={6}
                        className={selectedValue == 'transaction' ? "tb-header-text-selected" : "tb-header-text"}
                        onClick={() => this.setState({ selectedValue: 'transaction' })}
                    >
                        Transactions
                    </Col>
                </Row>

               { selectedValue == 'order' && <OrderTableData/>}
               { selectedValue == 'user' && <UserTableData/>}
               { selectedValue == 'discount' && <DiscountTableData/>}
               { selectedValue == 'transaction' && <TransactionTableData/>}

            </div>
        )
    }
}

export default TableData