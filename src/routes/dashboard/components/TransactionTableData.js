import React, { Component } from 'react';
import { Table, Icon } from 'antd';
import DEMO from 'constants/demoData';
import avatarImage from '../../../assets/images/avatar.jpg';
import classes from './style.module.scss';
import getAllPaymentDetail from '../../../middleware/paymentMiddleware';
import { connect } from 'react-redux';

const data = [
    {
        key: '1',
        name: 'Jhone',
        email: 'Jhone@gmail.com',
        phone: '7104505987',
        image: avatarImage,
        method: 'Visa',
    },
    {
        key: '1',
        name: 'Eliate',
        email: 'Guest@gmail.com',
        phone: '9104505987',
        image: avatarImage,
        method: 'COD',
    },
];

class TransactionTableData extends Component {

    componentDidMount(){
        this.props.getPaymentDetails()
    }

    render() {


        const columns = [
            // {
            //   title: 'Person Image',
            //   dataIndex: 'image',
            //   key: 'image',
            //   render: src => (
            //     <div>
            //       <img className={classes.productImage} src={src} />
            //     </div>
            //   ),
            // },
            {
              title: 'Username',
              key: 'name',
              render: text => text && text.addedBy && text.addedBy.userName
            },
            {
              title: 'Email',
              render: text => text && text.addedBy &&  text.addedBy.email,
              key: 'email',
            },
            {
              title: 'Mobile',
              render: text => text && text.addedBy && text.addedBy.phone, 
              key: 'phone',
            },
        
            {
              title: 'Payment method',
              render: text => text && text.cardType, 
              key: 'method',
            },
            {
              title: 'Amount',
              render: text => text && '₹' +text.totalAmount, 
              key: 'method',
            },
            // {
            //   title: 'Action',
            //   key: 'action',
            //   render: (text, record) => (
            //     <span className="d-flex">
            //       {/* <EditCustomer recordData={text} /> */}
            //       <Link to={`${match.url}/edit`} className="ant-btn ant-btn-primary">
            //         <Icon type="edit" theme="filled" /> Edit
            //       </Link>
            //       <DeleteCustomer recordData={text} />
            //     </span>
            //   ),
            //   width: 200,
            // },
          ];
        return (
            <>
                <div className="mb-2">
                    <span className="recent-data-title">Recent Transactions</span>
                </div>
                <article className="article">
                    
                    <Table
                        columns={columns}
                        dataSource={this.props.payment.slice(0, 10)}
                        lassName="ant-table-v1"
                        scroll={{ x: 700 }}
                        pagination={false}
                    />
                </article>
            </>
        );
    };
}


const mapStateToProps = state => ({
    payment: state.payment.payment
  })
  
  const mapDispatchToProps = dispatch => ({
    getPaymentDetails: () => dispatch(getAllPaymentDetail())
  })
  
export default connect(mapStateToProps, mapDispatchToProps)(TransactionTableData);
