import React, { Component } from 'react';
import { Table } from 'antd';
import { connect } from 'react-redux';
import getAllOrderDetail from '../../../middleware/orderMiddleware';


/* eslint-disable */
class OrderTableData extends Component {

    state = {
        selectedFilter: 1,
        listData: [],
        search: '',
        filterData: [],
        countFilter: 'all',
        startDate: '',
        endDate: '',
        dateFormat: 'YYYY-MM-DD',
        visible: false,
        orderStatus: null,
        orderRecord: null,
        orderId: null
    };

    showModal = (text) => {
        console.log(text, 'asdasfdasdj')
        this.setState({
            visible: true,
            orderStatus: text.order_status,
            orderId: text._id
        });
    };

    showPreviewModal = (text) => {
        this.setState({
            preview: true,
            orderRecord: { ...text },
        });
    }

    handleCancel = e => {
        this.setState({
            visible: false,
        });
    };

    componentDidMount() {
        const { getOrdersDetails, superCategory, order } = this.props

        this.setState({
            filterData: order,
            listData: order,
        });
        this.props.getOrdersDetails()
    }



    componentDidUpdate(prevProps, prevState) {
        const { getCateggetOrderDetail, order } = this.props
        const { defaultSuperCatId, defaultStoreId } = this.state;

        if (prevProps.order !== order) {
            this.setState({
                filterData: order,
                listData: order,
            })
        }

    }

    getDataRangeApi = () => {
        const { startDate, endDate, listData } = this.state;
        const filterDataHolder = [
            ...listData.filter(
                ({ createdAt }) =>
                    (new Date(createdAt).getTime() > new Date(startDate).getTime() && new Date(createdAt).getTime() < new Date(endDate).getTime())
            )
        ];

        this.setState({
            filterData: filterDataHolder,
            statusFilter: 0,
        });
    };

    updateListing = id => this.setState({ selectedFilter: id });

    handleSearch = e => {
        const { listData, countFilter, search } = this.state;
        console.log(listData, 'list dara')

        let filterDataHolder = [];

        if (countFilter === 'all') {
            filterDataHolder = [
                ...listData.filter(({ first_name }) =>
                    first_name.toLowerCase().includes(e.target.value.trim().toLowerCase())
                ),
            ];
        } else {
            filterDataHolder = [
                ...listData.filter(
                    ({ first_name, status }) =>
                        status === countFilter &&
                        first_name.toLowerCase().includes(e.target.value.trim().toLowerCase())
                ),
            ];
        }

        this.setState({
            [e.target.name]: e.target.value,
            filterData: filterDataHolder,
            // stockFilter: 0,
        });
    };
    handleCountFilter = e => {
        const { listData, countFilter, search } = this.state;
        let filterDataHolder = [];
        console.log(listData, 'list data')
        console.log(e, 'e')
        if (e === 'all') {
            filterDataHolder = [
                ...listData.filter(({ order_status }) => order_status.toLowerCase().includes(search.trim().toLowerCase())),
            ];
        } else {
            filterDataHolder = listData.filter(item => item.order_status.toLowerCase() == e.toLowerCase() && item.order_status.toLowerCase().includes(search.trim().toLowerCase()))

        }

        this.setState({
            countFilter: e,
            filterData: filterDataHolder,
        });
    };
    onDateChange = (date, dateString) => {
        console.log('eee', date, 'dddd', dateString);
        const { listData } = this.state
        if (date.length == 0)
            this.setState({ filterData: listData })
        this.setState({ startDate: dateString[0], endDate: dateString[1] });
    };
    onRangeSearch = () => {
        this.getDataRangeApi();
    };
    handleChange = (value) => {
        this.setState({ orderStatus: value })
    }



    render() {
        const columns = [
            // {
            //   title: 'Product Image',
            //   dataIndex: 'image',
            //   key: 'image',
            //   render: src => (
            //     <div className="text-center">
            //       <img className={classes.productImage} src={src} />
            //     </div>
            //   ),
            // },
            {
                title: 'Order Number',
                dataIndex: 'orderNo',
                key: 'orderNumber',
                // render: (text, row) => {
                //   return text && text.orderNo;
                // }

            },
            {
                title: 'Customer',
                dataIndex: 'addedBy',
                key: 'addedBy',
                render: (text, row) => {
                    return text && text.userName;
                },
                // render: (text) => {
                //   return text && text.addedBy && text.addedBy.email
                // }
            },
            {
                title: 'Total Cost',
                key: 'totalCost',
                render: (text) => {
                    if (text.isSubscription) {
                        return '-'
                    }
                    return text.discountedTotal
                }
            },
            {
                title: 'Delivery Address',
                key: 'deliveryAddress',
                render: (text) => {
                    console.log(text.addressId, "addressindex addressindex addressindex addressindex addressindex addressindex addressindex addressindex addressindex ")
                    if (text.addressId === undefined) {
                        return
                    }
                    const add = `${text && text.addressId && text.addressId.flatNo} ${' '}  ${text && text.addressId && text.addressId.street} ${' '}  ${text && text.addressId && text.addressId.landmark} ${' '} ${text && text.addressId && text.addressId.city} ${' '}  ${text && text.addressId && text.addressId.state} ${' '} ${text && text.addressId && text.addressId.postalCode}`
                    return add
                }
            },
            {
                title: 'Phone',
                dataIndex: 'addedBy',
                key: 'customerMobile',
                render: (text, row) => {
                    return text && text.phone;
                },
            },

            {
                title: 'Email',
                dataIndex: 'addedBy',
                key: 'customerEmail',
                render: (text, row) => {
                    return text && text.email;
                },
            },
            // {
            //     title: 'Payment Method',
            //     dataIndex: 'paymentMode',
            //     key: 'method',

            // },
            {
                title: 'Status',
                dataIndex: 'orderStatus',
                key: 'method',

            },
        ];

        const { selectedFilter, filterData, search, countFilter, dateFormat, startDate, listData, orderStatus } = this.state;
        return (
            <>
                <div className="mb-2">
                    <span className="recent-data-title">Recent Orders</span>
                </div>
                <article className="article">
                    <Table
                        columns={columns}
                        dataSource={filterData.slice(0, 10)}
                        className="ant-table-v1"
                        scroll={{ x: 700 }}
                        pagination={false}
                    />
                </article>

            </>
        );
    }
}

const mapStateToProps = state => ({
    order: state.order.order
})

const mapDispatchToProps = dispatch => ({
    getOrdersDetails: () => dispatch(getAllOrderDetail())
})


export default connect(mapStateToProps, mapDispatchToProps)(OrderTableData);
