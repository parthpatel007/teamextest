import React from 'react';
import { Table, Icon, DatePicker, Form, Input, Button } from 'antd';

import moment from 'moment';
import { connect } from 'react-redux';
import getAllCustomersListing from '../../../middleware/customersMiddleware';

const { RangePicker } = DatePicker;
const FormItem = Form.Item;
class UserTableData extends React.Component {
    state = { startDate: '', endDate: '', search: '', listData: [], filterData: [] };

    componentDidUpdate(prevProps, PrevState) {
        const { getCustomersDetail, customer } = this.props

        const { defaultSuperCatId, defaultStoreId } = this.state;

        if (prevProps.customer !== customer) {
            this.setState({
                filterData: customer,
                listData: customer,
            })
        }

        if (PrevState.defaultSuperCatId !== defaultSuperCatId) {
            getCustomersDetail(defaultStoreId, defaultSuperCatId);
            this.setState({
                filterData: customer,
                listData: customer,
            })

        }
    }

    getDataRangeApi = () => {
        const { startDate, endDate, listData } = this.state;
        const filterDataHolder = [
            ...listData.filter(
                ({ createdAt }) =>
                    (new Date(createdAt).getTime() > new Date(startDate).getTime() && new Date(createdAt).getTime() < new Date(endDate).getTime())
            )
        ];

        this.setState({
            filterData: filterDataHolder,
            statusFilter: 0,
        });
    };

    handleSearch = e => {
        const { listData } = this.state;
        console.log(listData, 'list data')
        const filterDataHolder = [
            ...listData.filter(
                ({ first_name, last_name, email, phone }) =>
                    first_name.toLowerCase().includes(e.target.value.trim().toLowerCase()) ||
                    last_name.toLowerCase().includes(e.target.value.trim().toLowerCase()) ||
                    email.toLowerCase().includes(e.target.value.trim().toLowerCase())),
        ];
        this.setState({
            [e.target.name]: e.target.value,
            filterData: filterDataHolder,
            statusFilter: 0,
        });
    };
    componentDidMount() {
        // this.getDataApi();
        const { getCustomersDetail, superCategory, customer } = this.props
        // const { defaultStoreId, defaultSuperCatId } = this.state;
        const current_store = JSON.parse(localStorage.getItem('current_store'))

        this.setState({
            filterData: customer,
            listData: customer,
            defaultStoreId: current_store !== null && current_store._id,
            defaultStoreName: current_store !== null && current_store.name,
            defaultSuperCatId: superCategory && superCategory.length && superCategory[0]._id,
            defaultSuperCatName: superCategory && superCategory.length && superCategory[0].supercategoryname
        });

        // const { getCustomersDetail } = this.props
        getCustomersDetail()
    }
    onDateChange = (date, dateString) => {
        console.log('eee', date, 'dddd', dateString);
        const { listData } = this.state
        if (date.length == 0)
            this.setState({ filterData: listData })
        this.setState({ startDate: dateString[0], endDate: dateString[1] });
    };
    onRangeSearch = () => {
        this.getDataRangeApi();
    };
    render() {
        const { match, customer } = this.props;
        const { listData, filterData, startDate, search } = this.state;
        // const dateFormat = 'YYYY-MM-DDTHH:mm:ss.SSS[Z]';
        const dateFormat = 'YYYY-MM-DD';
        // console.log(customer, " get customer")
        const columns = [
            // { 
            //   title: 'Person Image',
            //   dataIndex: 'image',
            //   key: 'image',
            //   width: 120,
            //   render: src => (
            //     <div className="text-center">
            //       <img className={classes.productImage} src={src || avatarImage} />
            //     </div>
            //   ),
            // },
      
            {
              title: 'Registration Date',
              key: 'createdAt',
              dataIndex: 'createdAt',
              render: text => moment(text).format('DD-MM-YYYY '),
              width: 135,
            },
            {
              title: 'User Name',
              key: 'userName',
              dataIndex: 'userName',
              width: 135,
            },
            // {
            //   title: 'Address',
            //   key: 'address',
            //   dataIndex: 'address',
            // },
            // {
            //   title: 'Status',
            //   key: 'name',
            //   width: 120,
            //   render: (text, record) => (
            //     <b className={`text-${text.status === 0 ? 'success' : 'danger'}`}>
            //       {text.status === 0 ? 'Active' : 'Inactive'}
            //     </b>
            //   ),
            // },
            {
              title: 'Email',
              dataIndex: 'email',
              key: 'email',
              width: 160,
            },
            {
              title: 'Phone',
              dataIndex: 'phone',
              key: 'phone',
              width: 120,
            },
        ];
        return (
            <>
                <div className="mb-2">
                    <span className="recent-data-title">Recent Users</span>
                </div>

                <article className="article">

                    <Table
                        columns={columns}
                        dataSource={filterData.slice(0, 10)}
                        className="ant-table-v1"
                        scroll={{ x: 700 }}
                        pagination={false}                        
                    />
                </article>
            </>
        );
    }
}


const mapStateToProps = state => ({
    customer: state.customers.customers
})

const mapDispatchToProps = dispatch => ({
    getCustomersDetail: () => dispatch(getAllCustomersListing())
})
export default connect(mapStateToProps, mapDispatchToProps)(UserTableData);
