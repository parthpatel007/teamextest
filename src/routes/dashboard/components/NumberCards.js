import React from 'react';
import { Icon } from 'antd';

const Section = (props) => (
  <div className="row">
    <div className="col-xl-3 mb-4">
      <div className="number-card-v1">
        <div className="card-top">
          <Icon type="shopping-cart" className="text-success" />
        </div>
        <div className="card-info">
          <span>Total Orders</span>
        </div>
        <div className="card-bottom">
          <span>
          {(props && props.dashboardData && props.dashboardData.totalOrders) ? parseInt(props && props.dashboardData && props.dashboardData.totalOrders) : '0' }
          </span>
        </div>
      </div>
    </div>

    <div className="col-xl-3 mb-4">
      <div className="number-card-v1">
        <div className="card-top">
          <span>
          {(props && props.dashboardData && props.dashboardData.totalSales) ? parseInt(props && props.dashboardData && props.dashboardData.totalSales) : '0' }
          </span>
        </div>
        <div className="card-info">
          <span>Total sales</span>
        </div>
        <div className="card-bottom">        
          <Icon type="shopping-cart" className="text-primary" />
        </div>
      </div>
    </div>

    <div className="col-xl-3 mb-4">
      <div className="number-card-v1">
        <div className="card-top">
          <Icon type="usergroup-add" className="text-success" />
        </div>
        <div className="card-info">
          <span>All users</span>
        </div>
        <div className="card-bottom">
          <span>
          {(props && props.dashboardData && props.dashboardData.totalUsers) ? parseInt(props && props.dashboardData && props.dashboardData.totalUsers) : '0' }
          {/* <span className="h5">k</span> */}
          </span>
        </div>
      </div>
    </div>

    <div className="col-xl-3 mb-4">
      <div className="number-card-v1">
        <div className="card-top">
          <span>
          {(props && props.dashboardData && props.dashboardData.totalProducts) ? parseInt(props && props.dashboardData && props.dashboardData.totalProducts) : '0' }
          </span>
        </div>
        <div className="card-info">
          <span>All Products</span>
        </div>
        <div className="card-bottom">
          <Icon type="appstore" className="text-warning" />
        </div>
      </div>
    </div>
  </div>
);

export default Section;
