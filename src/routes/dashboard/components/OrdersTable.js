import React from 'react';
import { Table } from 'antd';
import styles from './style.module.scss';

const columns = [{
  title: '#',
  dataIndex: 'number',
}, {
  title: 'Order Id',
  dataIndex: 'order_id',
}, {
  title: 'Customer Name',
  dataIndex: 'customer_name',
}, {
    title: 'Status',
    dataIndex: 'status',
    render: (text, row, index) => {
      const tag = text.split('-');
      const status = tag[0];
      const type = tag[1] ? 'ant-tag-type-' + tag[1] : 'ant-tag-type-primary';
      const tagClasses = 'ant-tag ant-tag-has-color ' + type;
      return (
        <div data-show="true" className={tagClasses}>
          <span className="ant-tag-text">{status}</span>
        </div>
      )
    }
  },{
    title: 'Order Total',
    dataIndex: 'order_total',
  },
];

const data = [{
  key: '1',
  number: '1',
  order_id:' #OD123 ',
  customer_name: 'Amery Lee',
  status: 'Pending-info',
  order_total: '₹499'
}, {
  key: '2',
  number: '2',
  order_id:' #OD124 ',
  customer_name: 'Romayne Carlyn',
  status: 'Due',
  order_total: '₹599'
}, {
  key: '3',
  number: '3',
  order_id:' #OD135 ',
  customer_name: 'Jane Swift',
  status: 'Due-success',
  order_total: '₹399'
}, {
  key: '4',
  number: '4',
  order_id:' #OD156 ',
  customer_name: 'Marybeth Joanna',
  status: 'Blocked-danger',
  order_total: '₹1099'
}, {
  key: '5',
  number: '5',
  order_id: ' #OD167 ',
  customer_name: 'Jonah Benny',
  status: 'Suspended-warning',
  order_total: '₹230'
}, {
  key: '6',
  number: '6',
  order_id:' #OD450 ',
  customer_name: 'Daly Royle',
  status: 'Suspended-info',
  order_total: '₹320'
}];


const Box = () => (
  <Table
    size={"default"}
    className={`${styles.order_id_table} mb-4`}
    columns={columns}
    dataSource={data}
    pagination={false}
    bordered={false}
  />
)

export default Box;
