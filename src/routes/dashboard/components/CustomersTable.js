import React from 'react';
import { Table } from 'antd';
import styles from './style.module.scss';

const columns = [{
  title: '#',
  dataIndex: 'number',
}, {
  title: 'Registration Date',
  dataIndex: 'registration_date',
},  {
  title: 'Customer Name',
  dataIndex: 'customer_name',
},  {
  title: 'Email',
  dataIndex: 'cust_email',
}, {
    title: 'Phone',
    dataIndex: 'cust_phone',
}, {
    title: 'Status',
    dataIndex: 'status',
    render: (text, row, index) => {
      const tag = text.split('-');
      const status = tag[0];
      const type = tag[1] ? 'ant-tag-type-' + tag[1] : 'ant-tag-type-primary';
      const tagClasses = 'ant-tag ant-tag-has-color ' + type;
      return (
        <div data-show="true" className={tagClasses}>
          <span className="ant-tag-text">{status}</span>
        </div>
      )
    }
}, 
];

const data = [{
  key: '1',
  number: '1',
  registration_date:' 26 sep, 2020 ',
  customer_name: 'Amery Lee',
  cust_email: 'test@gmail.com',
  cust_phone: '9587863450',
  status: 'Unblocked-success',
}, {
  key: '2',
  number: '2',
  registration_date:' 12 oct, 2020 ',
  customer_name: 'Romayne Carlyn',
  cust_email: 'test@gmail.com',
  cust_phone: '9587863450',
  status: 'Unblocked-success',
}, {
  key: '3',
  number: '3',
  registration_date:' 10 Jan, 2020 ',
  customer_name: 'Jane Swift',
  cust_email: 'test@gmail.com',
  cust_phone: '9587863450',
  status: 'Blocked-danger',
}, {
  key: '4',
  number: '4',
  registration_date:' 02 Feb, 2020 ',
  customer_name: 'Marybeth Joanna',
  cust_email: 'test@gmail.com',
  cust_phone: '9587863450',
  status: 'Unblocked-success',
}, {
  key: '5',
  number: '5',
  registration_date: ' 22 Nov, 2020 ',
  customer_name: 'Jonah Benny',
  cust_email: 'test@gmail.com',
  cust_phone: '9587863450',
  status: 'Blocked-danger',
}, {
  key: '6',
  number: '6',
  registration_date:' 21 Aug, 2020 ',
  customer_name: 'Daly Royle',
  cust_email: 'test@gmail.com',
  cust_phone: '9587863450',
  status: 'Unblocked-success',
}];


const Box = () => (
  <Table
    size={"default"}
    className={`${styles.order_id_table} mb-4`}
    columns={columns}
    dataSource={data}
    pagination={false}
    bordered={false}
  />
)

export default Box;
