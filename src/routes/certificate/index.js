import React from 'react';
import { Route } from 'react-router-dom';
import Certificate from './route';
import AddCertificate from "./route/add/index"
import EditCertificate from "./route/edit/index"

const Shop = ({ match }) => (
  <>
    <Route exact path={`${match.url}`} component={Certificate} />
    <Route path={`${match.url}/add`} component={AddCertificate} />
    <Route path={`${match.url}/edit`} component={EditCertificate} />
  </>
);

export default Shop;