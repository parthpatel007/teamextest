import React from 'react';
import { Table, Icon, Form, Input, Select, Tooltip, Skeleton } from 'antd';
import { CertificateCard } from '../../../components/Cards';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import getAllCertificateDetail from '../../../middleware/certificateMiddleware';
import Delete from './delete';
const FormItem = Form.Item;
const Option = Select.Option;


/* eslint-disable */

class Certificate extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            startDate: '',
            endDate: '',
            loader: true,
            search: '',
            store: [],
            store_id: localStorage.getItem('store_id') ? localStorage.getItem('store_id') : '',
            listData: [],
            filterData: [],
            stockFilter: 0
        };
        this.selectedStore = this.selectedStore.bind(this);
    }

    async componentDidMount() {
        const { getCertificateDetail } = this.props
        await getCertificateDetail()
    }

    componentDidUpdate(prevProps, prevState) {
        const { certificate, certificateSuccess } = this.props;
        if (prevProps.certificate !== certificate) {
            this.setState({
                filterData: certificate,
                listData: certificate,
                loader: certificate.length > 0 ? false : true
            })
        }
        if (prevProps.certificateSuccess !== certificateSuccess) {
            this.setState({
                loader: !certificateSuccess
            });
        }
    }

    handleSearch = e => {
        const { listData } = this.state;
        const filterDataHolder = [
            ...listData.filter(
                ({ name }) =>
                    name.toLowerCase().startsWith(e.target.value.trim().toLowerCase())
            ),
        ];
        this.setState({
            [e.target.name]: e.target.value,
            filterData: filterDataHolder,
        });
    };

    handleSelect = (name, value) => {
        const { listData } = this.state;

        console.log(
            'rrrr handleSelect',
            listData,
            listData.filter(({ store_id }) => store_id === value)
        );
        const filterDataHolder = [...listData.filter(({ store_id }) => store_id === value)];
        this.setState({ [name]: value, stockFilter: 0 });
        this.getDataApiWithStoreId(value);
        if (!localStorage.getItem('store_id')) {
            localStorage.setItem('store_id', value);
        }
    };
    selectedStore() {
        const { store, store_id } = this.state;
        return (
            <SelectStore>
                <FormItem label="Choose Store" hasFeedback className="mb-0 d-flex align-items-center">
                    <Select
                        name="store_id"
                        value={store_id}
                        placeholder="Select Store"
                        onChange={e => this.handleSelect('store_id', e)}
                    >
                        {store.map((data, index) => (
                            <Option key={index} value={data._id}>
                                {data.name}
                            </Option>
                        ))}
                    </Select>
                </FormItem>
            </SelectStore>
        );
    }
    render() {
        const { match } = this.props;
        const { filterData, loader, search } = this.state;
        const columns = [
            {
                title: 'Index',
                render: (text, record, index) => <> {index + 1} </>,
                width: 80,
            },
            {
                title: 'Name',
                dataIndex: 'name'
            },
            {
                title: 'Image',
                dataIndex: 'image',
                key: 'image',
                render: (src) => {
                    return (
                        <div>{
                            <img height="40" width="40" src={src && src.original} alt="dowmloadimg" />
                        }
                        </div>)
                },
                width: 120,
            },

            {
                title: 'Action',
                key: 'action',
                render: (text, record) => {
                    return (
                        <span className="d-flex">
                            <Tooltip placement="bottomLeft" title={text.status === 0 ? 'block' : 'unblock'}>
                                <span className="d-flex">
                                    <Delete recordData={text} />
                                </span>
                            </Tooltip>
                            <Tooltip placement="bottomLeft" title={'edit'}>
                                <Link
                                    to={{ pathname: `${match.url}/edit`, state: text }}
                                    className="ml-2 ant-btn ant-btn-primary ant-btn-circle"
                                >
                                    <Icon type="edit" theme="filled" />
                                </Link>
                            </Tooltip>
                        </span>
                    );
                },
                width: 166,
            },
        ];
        return (
            <>
                <CertificateCard />
                <section className="container-fluid  no-breadcrumb chapter">
                    <article className="article">
                        <div className="d-flex justify-content-between mb-2">
                            <div className="d-flex">
                                <FormItem>
                                    <Input
                                        placeholder="Search Certificate"
                                        prefix={<Icon type="search" style={{ fontSize: 13 }} />}
                                        name="search"
                                        value={search}
                                        style={{ width: 275 }}
                                        onChange={this.handleSearch}
                                    />
                                </FormItem>
                            </div>
                            <div className="d-flex">
                                <div>
                                    <Link to={`${match.url}/add`} className="ant-btn ant-btn-primary">
                                        <Icon type="plus-circle" theme="filled" /> Add
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {loader ?
                            <Skeleton height={50} count={10} /> :
                            <Table
                                pagination={{ defaultPageSize: 15 }}
                                columns={columns}
                                dataSource={filterData}
                                className="ant-table-v1"
                            />
                        }
                    </article>
                </section>
            </>
        );
    }
}

const mapStateToProps = state => ({
    certificate: state.certificate.certificate,
    certificateSuccess: state.certificate.certificateSuccess
})

const mapDispatchToProps = dispatch => ({
    getCertificateDetail: () => dispatch(getAllCertificateDetail())
})
export default connect(mapStateToProps, mapDispatchToProps)(Certificate)

const SelectStore = styled('div')`
  .ant-select {
    min-width: 200px;
  }
`;