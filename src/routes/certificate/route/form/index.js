
import React from 'react';
import { Form, Input, Icon, Button, Spin } from 'antd';
import { Row, Col } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { backendUrl } from '../../../../common/credentials'
import cx from 'classnames';
import { createGlobalStyle } from 'styled-components';
import { connect } from 'react-redux';
import Dragger from 'antd/lib/upload/Dragger';
import getAllCertificateDetail from '../../../../middleware/certificateMiddleware';

/* eslint-disable */

const FormItem = Form.Item;

const defaultState = {
  CertificateName: '',
  certificateImage: [],
  productImages: [],
};


class FormComponent extends React.Component {


  state = {

    CertificateName: this.props.type === 'edit' ? this.props && this.props.editData && this.props.editData.name : "",
    certificateImage: [],
    productImages: [],
    store: [],
    editProductImages: this.props.type === 'edit' ? this.props && this.props.editData && this.props.editData.image : null,
    isLoading: false,
    apiError: '',
    error: {
      errorText: '',
      errorType: '',
    },
  };

  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };


  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };

  handleInput = e => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };

  addNewApi = async () => {
    const {
      CertificateName,
      certificateImage,
    } = this.state;
    const { match, history } = this.props;
    const authToken = localStorage.getItem('access_token');
    this.setState({ isLoading: true });
    const self = this;
    const formData = new FormData()
    formData.append("image", certificateImage)
    formData.append("name", CertificateName)

    axios
      .post(
        `${backendUrl}/api/certificateUpload`,
        formData,
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
      });
  };


  editApi = async () => {
    const {
      CertificateName,
      certificateImage,
      editProductImages,
    } = this.state;
    const { match, history, editData } = this.props;
    const { _id } = editData;
    const authToken = localStorage.getItem('access_token');
    this.setState({ isLoading: true });
    const self = this;
    const formData = new FormData()
    if (!editProductImages) {
      formData.append("image", certificateImage)
    }
    else {
      formData.append("image", editProductImages.original)
    }
    formData.append("name", CertificateName)
    axios
      .post(
        `${backendUrl}/api/certificateUpdate/${_id}`,
        formData,
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: '*/*',
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        console.log('rrrr res', response);
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        self.setState({
          isLoading: false,
          apiError:'Network Error'
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
        console.log('rrrr err', error);
      });
  };

  validateForm = () => {
    const {
      CertificateName,
      certificateImage,
    } = this.state;

    if (CertificateName === '') {
      this.setState({
        error: {
          errorText: 'Please select a Certificate Name',
          errorType: 'CertificateName',
        },
      });
      return false;
    }
    if (this.props.type === 'edit') {
      if (!this.state.editProductImages && certificateImage.length === 0) {
        this.setState({
          error: {
            errorText: 'Add certificate image',
            errorType: 'edit_image',
          },
        });
        return false;
      }
    }
    else {
      if (certificateImage.length === 0) {
        this.setState({
          error: {
            errorText: 'Please select a certificate Image',
            errorType: 'add_image',
          },
        });
        return false;
      }
    }
    return true;
  };

  componentDidMount() {
    const { getCertificateDetail } = this.props
    getCertificateDetail()

  }
  handleAddData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.addNewApi();
    }
  };

  handleEditData = () => {
    this.clearError();
    if (this.validateForm()) {
      this.editApi();
    }
  };

  handleEditProductImage = () => {
    this.setState({
      editProductImages: null
    })
  }
  onDragChange = (info) => {
    const status = info.file.status;
    if (status !== 'uploading') {
      this.setState({
        certificateImage: info.file.originFileObj
      });
    }
  }

  beforeUpload = (file) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    const { type } = this.props
    const isLt2M = file.size / 1024 / 1024 < 5;
    if (!isJpgOrPng) {
      if (type == "edit") {
        this.setState({
          error: {
            errorText: 'Please select vaild edit image (JPG,PNG,JPEG )',
            errorType: 'edit_image',
          },
        });
        return false
      }
      else {
        this.setState({
          error: {
            errorText: 'Please select vaild add image (JPG,PNG,JPEG )',
            errorType: 'add_image',
          },
        });
        return false
      }
    }
    else if (!isLt2M) {
      if (type == "edit") {
        this.setState({
          error: {
            errorText: 'Image must smaller than 5MB!',
            errorType: 'edit_image',
          },
        });
        return false
      }
      else {
        this.setState({
          error: {
            errorText: 'Image must smaller than 5MB!',
            errorType: 'add_image',
          },
        });
        return false
      }
    }
    else {
      this.setState({
        error: {
          errorText: '',
          errorType: '',
        },
      });
    }
    return isJpgOrPng && isLt2M;
  }

  onEditDragChange = (info) => {
    const status = info.file.status;
    if (status !== 'uploading') {
      this.setState({
        certificateImage: info.file.originFileObj,
        editProductImages: null
      });
    }
  }

  render() {
    const {
      CertificateName,
      isLoading,
      apiError,
      error,
      editProductImages
    } = this.state;
    const { type = 'add', editData, match, history } = this.props;
    console.log(editData, "Edit certificate")
    const { errorType } = error;
    if (type === 'edit') {
      if (!editData) {
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        return null;
      }
    }

    return (
      <div className="form-container">
        <GlobalStyle />
        <Row form>
          <Col md={12}>
            <FormItem
              label="Certificate Name"
              hasFeedback
              className={cx({ 'has-error': errorType === 'CertificateName' })}
            >
              <Input
                maxLength={50}
                name={type === 'edit' ? 'CertificateName' : 'CertificateName'}
                value={type === 'edit' ? CertificateName : CertificateName}
                onChange={this.handleInput} />
              {this.errorShow(type === 'edit' ? 'CertificateName' : 'CertificateName')}
            </FormItem>
          </Col>
        </Row>
        {type === 'add' && (
          <FormItem label="Add Certificate Image">
            <Dragger
              listType="picture"
              onChange={this.onDragChange}
              showUploadList={errorType == 'add_image' ? false : true}
              beforeUpload={this.beforeUpload}
              name={'file'}
              multiple={false}
            >
              <p className="ant-upload-drag-icon">
                <Icon type="inbox" />
              </p>
              <p className="ant-upload-text">Click or drag file to this area to upload</p>
              <p className="ant-upload-hint">
                Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                band files
              </p>
            </Dragger>
            Note: For better user experience upload image of 370*523 pixels (square shape) image.
            {this.errorShow('add_image')}
          </FormItem>
        )}
        {type === 'edit' && (
          <>
            <FormItem
              className="mt-5"
              label="Certificate Images">
              <Row className="pt-5" className="text-center" justify={'space-between'}>
                {editProductImages ?
                  <Col span={4} className="text-center">
                    <Button type="danger" className="ml-2" shape="circle" onClick={() => this.handleEditProductImage()}>
                      <Icon type="close-circle" theme="filled" />
                    </Button>
                    <br />
                    <img
                      src={editProductImages.original}
                      style={{ width: 100, height: 100, objectFit: 'contain' }}
                    />
                    <br />
                  </Col>

                  : <Col span={12} className="text-center">No Image</Col>}
              </Row>
            </FormItem>
          </>
        )}
        {type === 'edit' && (
          <FormItem label="Upload Image">
            <Dragger
              listType="picture"
              onChange={this.onEditDragChange}
              showUploadList={errorType == 'edit_image' ? false : true}
              beforeUpload={this.beforeUpload}
              name={'file'}
              multiple={false}
            >
              <p className="ant-upload-drag-icon">
                <Icon type="inbox" />
              </p>
              <p className="ant-upload-text">Click or drag file to this area to upload</p>
              <p className="ant-upload-hint">
                Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                band files
              </p>
            </Dragger>
            Note: For better user experience upload image of 370*523 pixels (square shape) image.
            {this.errorShow('edit_image')}
          </FormItem>
        )}
        {isLoading && (
          <div className="mb-2 text-center">
            <Spin />
          </div>
        )}
        <div className="text-right pt-3">
          {type === 'add' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              onClick={this.handleAddData}
            >
              <Icon type="plus-circle" theme="filled" />
              Save
            </Button>
          )}
          {type === 'edit' && (
            <Button
              type="primary"
              htmlType="submit"
              onClick={this.handleEditData}
              className="btn-cta"
            >
              <Icon type="edit" theme="filled" /> Save
            </Button>
          )}
        </div>
        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  certificate: state.certificate.certificate
})

const mapDispatchToProps = dispatch => ({
  getCertificateDetail: () => dispatch(getAllCertificateDetail()),

})

const WrappedFormComponent = Form.create()(withRouter(FormComponent));

export default connect(mapStateToProps, mapDispatchToProps)(WrappedFormComponent);

const GlobalStyle = createGlobalStyle`.ant-select-dropdown--multiple{display:none}`;