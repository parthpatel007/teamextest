import React from 'react';
import { Form, Input, Tooltip, Icon, Button, Select, Spin } from 'antd';

import { Row, Col, Table, Collapse } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import DragAndDrop from '../../../../form/routes/form-control/routes/upload/components/DragAndDrop';
import axios from 'axios';
import { backendUrl } from '../../../../../common/credentials';
import cx from 'classnames';
import { connect } from 'react-redux';
import getAllUtilityDetail from '../../../../../middleware/utilityMiddleware';
import CKEditor from '../../../../../components/CKEditor';
import getAllUtilityCategoryDetail from '../../../../../middleware/utilityCategoryMiddleware';
import getAllUtilitySubCategoryDetail from '../../../../../middleware/utilitySubCategoryMiddleware';
import { Upload, message } from 'antd';
import ImageUploader from 'react-images-upload';
import { s3BucketCredentials } from '../../../../../s3Config';
import S3FileUpload from 'react-s3';


const Dragger = Upload.Dragger;



const Option = Select.Option;
const FormItem = Form.Item;
const defaultExtraIngredients = [{ id: 1, name: '', quantity: '', price: '' }];
const defaultState = {
  name: '',
  description: '',
  price: '',
  unit: '',
  ingredients: '',
  subcategory_id: '',
  category_id: '',
  store_id: '',
  quantity: '',
  product_image: '',
  super_category_id: '',
  utility_category_id: '',
  extra_ingredients: [],
  isAddExtraIngredients: false,
};
class FormComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      store: [],
      categories: [],
      utilitiesImages: [],
      images: [],
      editUtilityImages: this.props.type === 'edit' ? this.props && this.props.editData && this.props.editData.utility_image : [],
      editUtilityImagesObjects: [],
      subcategories: [],
      ...(this.props.type === 'edit' ? this.props.editData : defaultState),
      isLoading: false,
      superCategoryId: this.props.type === 'edit' ? this.props && this.props.editData && this.props.editData.supercategoryid : '',
      utilityCategoryId: this.props.type === 'edit' ? this.props && this.props.editData && this.props.editData.utilitycategoryid && this.props.editData.utilitycategoryid._id : '',
      utilitySubCategoryId: this.props.type === 'edit' ? this.props && this.props.editData && this.props.editData.utilitysubcategoryid && this.props.editData.utilitysubcategoryid._id : '',
      SmothieName: this.props.type === 'edit' ? this.props && this.props.editData && this.props.editData.title : '',
      apiError: '',
      error: {
        errorText: '',
        errorType: '',
      },
    };
    this.addExtraIngredientsForm = this.addExtraIngredientsForm.bind(this);
  }
  errorShow = type => {
    const { error } = this.state;
    const { errorType, errorText } = error;
    return errorType === type ? (
      <span className="d-block text-danger" style={{ marginTop: -8 }}>
        {errorText}
      </span>
    ) : null;
  };
  clearError = () => {
    this.setState({
      error: {
        errorText: '',
        errorType: '',
      },
    });
  };
  handleTagsChange = (state, value) => {
    this.clearError();
    this.setState({ [state]: value });
    // console.log(`rrrr selected  ${value} `);
  };
  handleInput = e => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };


  handleSelect = (name, value) => {
    this.setState({ [name]: value });
  };


  addNewApi = async () => {
    const {
      name,
      description,
      price,
      unit,
      ingredients,
      extra_ingredients,
      subcategory_id,
      category_id,
      store_id,
      quantity,
      super_category_id,
      utility_category_id,
      product_image,
      utilitiesImages,
      images
    } = this.state;
    const { match, history } = this.props;
    const authToken = localStorage.getItem('access_token');
    this.setState({ isLoading: true });
    const self = this;
    let imgArray = []

    for (let i = 0; i < utilitiesImages.length; i++) {
      let amazoneParam = Object.assign({}, s3BucketCredentials, { name: utilitiesImages[i].name, dirName: 'CapitolDrug' })
      let response = await S3FileUpload.uploadFile(utilitiesImages[i].originFileObj, amazoneParam)
      imgArray.push(response.location)
      this.setState({ images: imgArray })
    }

    axios
      .post(
        `${backendUrl}/utility/addutilities`,
        {
          title: name,
          description: description,
          price: price,
          unit: unit,
          ingredients: ingredients,
          extra_ingredients: extra_ingredients,
          // categoryid: category_id,
          storeid: store_id,
          quantity: quantity,
          supercategoryid: super_category_id,
          utilitycategoryid: category_id,
          utility_image: imgArray,
        },
        {
          headers: {
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        // console.log('rrrr res', response);
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        console.log('rrrr err', error);
        self.setState({ isLoading: false });
        self.setState({
          isLoading: false,
          apiError: error.toString().includes('Network Error')
            ? 'Network Error'
            : error.response.data.message,
        });
        setTimeout(() => self.setState({ apiError: '' }), 3000);
      });
  };

  uploadImage = () => {
    // console.log('djdjhdjhgjdhgdjh')
    const { utilitiesImages } = this.state
    let imgArray = []
    utilitiesImages.map((data) => {
      // console.log(data, "datadatadatadatadatadatadatadatavdatadatadatadatadatadata")
      let amazoneParam = Object.assign({}, s3BucketCredentials, { name: data.name, dirName: 'CapitolDrug' })
      S3FileUpload.uploadFile(data.originFileObj, amazoneParam)
        .then(res => {
          imgArray.push(res.location)
          this.setState({ images: imgArray })
          console.log(res, "res pjbgjrvjrgjergersegkrj")
        })
    })
  }

  onDragChange = (info) => {

    const status = info.file.status;

    if (status !== 'uploading') {
      this.setState({
        utilitiesImages: info.fileList
      });
    }
  }



  onEditDragChange = (info) => {
    const status = info.file.status;
    if (status !== 'uploading') {
      this.setState({
        editUtilityImagesObjects: info.fileList
      });
    }
  }

  editApi = async () => {
    const {
      name,
      description,
      price,
      extra_ingredients,
      unit,
      ingredients,
      store_id,
      editUtilityImagesObjects,
      editUtilityImages,
      quantity,
      product_image,
      superCategoryId,
      utilitySubCategoryId,
      utilityCategoryId,
      SmothieName
    } = this.state;



    let editImgArray = []

    for (let i = 0; i < editUtilityImagesObjects.length; i++) {
      let amazoneParam = Object.assign({}, s3BucketCredentials, { name: editUtilityImagesObjects[i].name, dirName: 'CapitolDrug' })
      let response = await S3FileUpload.uploadFile(editUtilityImagesObjects[i].originFileObj, amazoneParam)
      editImgArray.push(response.location)
    }
    // let editImgArray = [...editUtilityImages]



    const { _id } = this.props.editData;
    const { match, history } = this.props;
    const authToken = localStorage.getItem('access_token');
    axios
      .put(
        `${backendUrl}/utility/updateutilities?utilityid=${_id}`,
        {
          title: SmothieName,
          description: description,
          price: price,
          unit: unit,
          ingredients: ingredients,
          extra_ingredients: extra_ingredients,
          utilitysubcategoryid: utilitySubCategoryId,
          utilitycategoryid: utilityCategoryId,
          storeid: store_id,
          quantity: quantity,
          supercategoryid: superCategoryId,
          utility_image: editUtilityImagesObjects.length > 0 ? editImgArray : [...editUtilityImages],
        },
        {
          headers: {
            Authorization: `Bearer ${authToken}`,
          },
        }
      )
      .then(function (response) {
        console.log('rrrr res', response);
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
      })
      .catch(function (error) {
        console.log('rrrr err', error);
      });
  };


  componentDidMount() {
    const current_store = JSON.parse(localStorage.getItem('current_store'))

    this.setState({
      store_id: current_store._id,
    })
    const { type, editData, getUtilityCategoryDetail, getUtilitySubCategoryDetail } = this.props


    if (type === 'edit') {
      console.log("EDITED DATA FROM PROPSSSSSSSSSSSSSSSSSSSSSSSSSSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNAAAAAAAAAAAAA :", editData)
      getUtilityCategoryDetail(current_store._id, editData && editData.supercategoryid);
      getUtilitySubCategoryDetail(current_store._id, editData && editData.supercategoryid);
      console.log("edit dataa did mountedit dataa did mountedit dataa did mountedit dataa did mountedit dataa did mountedit dataa did mountedit dataa did mountedit dataa did mount")
      this.getSubCategoryById(editData && editData.utilitycategoryid && editData.utilitycategoryid._id)
    }
  }

  getSubCategoryById = (catId) => {
    const { startDate, endDate, listData } = this.state;
    const { utilityCategory, utilitySubCategory } = this.props;

    console.log(catId, "category id category id category id category id category id ")

    console.log(utilitySubCategory, "array of subCategory array of subCategory array of subCategory array of subCategory")

    const filteredSubCategories = [
      ...utilitySubCategory.filter((singleSubCat) => {
        return singleSubCat.utilitycategoryid._id === catId
      })
    ];

    console.log(filteredSubCategories, "filteredSubCategories filteredSubCategories filteredSubCategories filteredSubCategories filteredSubCategories filteredSubCategories ")

    this.setState({
      subcategories: filteredSubCategories
    });
  };


  handleEditProductImage = (index) => {

    const { editUtilityImages } = this.state

    console.log(editUtilityImages, "edit pro imahes dedit pro imahes dedit pro imahes dedit pro imahes d")

    console.log(index, "index of arra index of arra index of arra index of arra index of arra index of arra index of arra index of arra index of arra index of arra ")

    const tempArr = [...editUtilityImages]

    tempArr.splice(index, 1)

    this.setState({
      editUtilityImages: tempArr
    })

    console.log(tempArr, "tempArr imahestempArr dedittempArr protempArr imahes dedit protempArrtempArrtempArrtempArr imahestempArrtempArrtempArr dedit pro imahes d")

  }



  componentDidUpdate(prevProps, prevState) {
    const { super_category_id, store_id, superCategoryId, category_id, utilityCategoryId } = this.state;
    const { getUtilityCategoryDetail, getUtilitySubCategoryDetail, type, editData } = this.props
    const current_store = JSON.parse(localStorage.getItem('current_store'))

    console.log(prevState, "prevstate prevstate prevstate prevstate prevstate prevstate prevstate prevstate prevstate prevstate prevstate prevstate prevstate prevstate prevstate prevstate prevstate prevstate ")

    if (type === 'edit') {
      if (prevState.utilityCategoryId !== utilityCategoryId) {
        console.log("going to get utitliy sub ctgoing to get utitliy sub ctgoing to get utitliy sub ctgoing to get utitliy sub ctgoing to get utitliy sub ctgoing to get utitliy sub ct")
        this.getSubCategoryById(utilityCategoryId)
      }

      if (prevProps.editData !== editData) {
        getUtilityCategoryDetail(current_store._id, superCategoryId);
        getUtilitySubCategoryDetail(current_store._id, superCategoryId);
      }

      if (prevState.superCategoryId !== superCategoryId) {
        getUtilityCategoryDetail(store_id, superCategoryId);
        getUtilitySubCategoryDetail(store_id, superCategoryId);
      }

      // if(prevState.utilityCategoryId !== utilityCategoryId){
      //   console.log("going to get utitliy sub ctgoing to get utitliy sub ctgoing to get utitliy sub ctgoing to get utitliy sub ctgoing to get utitliy sub ctgoing to get utitliy sub ct")
      //   this.getSubCategoryById(utilityCategoryId)
      // }

    }
    else {

      // if(prevState.category_id !== category_id){
      //   this.getSubCategoryById(category_id)
      // }

      if (prevState.super_category_id !== super_category_id) {
        getUtilityCategoryDetail(store_id, super_category_id);
        getUtilitySubCategoryDetail(store_id, super_category_id);
      }

      if (prevState.category_id !== category_id) {
        this.getSubCategoryById(category_id)
      }

    }

  }

  validateForm = () => {
    const {
      name,
      description,
      price,
      unit,
      ingredients,
      subcategory_id,
      category_id,
      store_id,
      quantity = 0,
      product_image,
      extra_ingredients,
      isAddExtraIngredients,
      super_category_id
    } = this.state;
    if (super_category_id === '') {
      this.setState({
        error: {
          errorText: 'Please Select Super Category',
          errorType: 'super_category_id',
        },
      });
      return false;
    }

    if (category_id === '') {
      this.setState({
        error: {
          errorText: 'Please Select Category',
          errorType: 'category_id',
        },
      });
      return false;
    }
    // if (subcategory_id === '') {
    //   this.setState({
    //     error: {
    //       errorText: 'Please Select Subcategory',
    //       errorType: 'subcategory',
    //     },
    //   });
    //   return false;
    // }
    if (name === '') {
      this.setState({
        error: {
          errorText: 'Please Enter Name',
          errorType: 'name',
        },
      });
      return false;
    }
    if (description === '') {
      this.setState({
        error: {
          errorText: 'Please Enter Description',
          errorType: 'description',
        },
      });
      return false;
    }
    if (ingredients.length === 0) {
      this.setState({
        error: {
          errorText: 'Please Add Ingredient',
          errorType: 'ingredients',
        },
      });
      return false;
    }

    if (price === '' || parseInt(price) > 1000000 || parseInt(price) < 0) {
      this.setState({
        error: {
          errorText: 'Please Enter Price',
          errorType: 'price',
        },
      });
      return false;
    }
    if (unit === '') {
      this.setState({
        error: {
          errorText: 'Please Enter Unit',
          errorType: 'unit',
        },
      });
      return false;
    }

    if (quantity == 0 || parseInt(quantity) < 0 || parseInt(quantity) > 1000000) {
      this.setState({
        error: {
          errorText: 'Please Enter Quantity',
          errorType: 'quantity',
        },
      });
      return false;
    }
    // if (isAddExtraIngredients) {
    //   const temp = [...extra_ingredients].filter(({ name, quantity, price }) => {
    //     if (name.trim() === '' || quantity.trim() === '' || price.trim() === '') {
    //       return true;
    //     }
    //     return false;
    //   });
    //   if (temp.length > 0) {
    //     this.setState({
    //       error: {
    //         errorText: 'Please Fill Values of Extra Ingredients',
    //         errorType: 'extra_ingredients',
    //       },
    //     });
    //     return false;
    //   }
    // }

    return true;
  };
  handleAddData = () => {
    if (this.validateForm()) {
      this.addNewApi();
    }
  };
  handleEditData = () => {
    if (this.validateForm()) {
      this.editApi();
    }
  };

  extraIngredientsFieldHandle = (type, id) => {
    const { extra_ingredients } = this.state;

    const fields = [{ id: id, name: '', quantity: '', price: '' }];
    if (type === 'add') {
      const temp = [...extra_ingredients, ...fields];
      this.setState({ extra_ingredients: temp });
    }
    if (type === 'remove') {
      const temp = [...extra_ingredients.filter(prop => prop.id !== id)];
      this.setState({ extra_ingredients: temp });
    }
  };

  handleExtraIngredientsInput = (field, value, index) => {
    const { extra_ingredients } = this.state;

    let temp = [...extra_ingredients];
    if (field === 'name') {
      temp[index].name = value;
    }
    if (field === 'quantity') {
      temp[index].quantity = value;
    }
    if (field === 'price') {
      temp[index].price = value;
    }
    this.setState({ extraIngredients: temp });
  };

  handleEditor = (state, content) => {
    this.clearError();
    this.setState({ [state]: content });
  };
  addExtraIngredientsForm() {
    const { extra_ingredients, isAddExtraIngredients } = this.state;

    return (
      <Collapse isOpen={isAddExtraIngredients}>
        <Table className="mb-0">
          <thead>
            <tr>
              <th>Ingredient Name</th>
              <th>Quantity</th>
              <th>Price</th>
              {extra_ingredients.length > 1 && <th>Remove</th>}
            </tr>
          </thead>
          <tbody>
            {extra_ingredients.map((data, index) => {
              const { id, name, quantity, price } = data;

              return (
                <tr>
                  <td className="p-1">
                    <FormItem hasFeedback className={cx('mb-0')}>
                      <Input
                        value={name}
                        maxLength={26}
                        onChange={e =>
                          this.handleExtraIngredientsInput('name', e.target.value, index)
                        }
                      />
                    </FormItem>
                  </td>
                  <td className="p-1">
                    <FormItem hasFeedback className={cx('mb-0')}>
                      <Input
                        type="number"
                        value={quantity}
                        onChange={e =>
                          this.handleExtraIngredientsInput('quantity', e.target.value, index)
                        }
                      />
                    </FormItem>
                  </td>
                  <td className="p-1">
                    <FormItem hasFeedback className={cx('mb-0')}>
                      <Input
                        type="number"
                        value={price}
                        onChange={e =>
                          this.handleExtraIngredientsInput('price', e.target.value, index)
                        }
                      />
                    </FormItem>
                  </td>
                  {extra_ingredients.length > 1 && (
                    <td className="p-1 text-center">
                      <Button
                        type="danger"
                        shape="circle"
                        onClick={() => this.extraIngredientsFieldHandle('remove', id)}
                      >
                        <Icon type="delete" theme="filled" />
                      </Button>
                    </td>
                  )}
                </tr>
              );
            })}
          </tbody>
        </Table>
        {this.errorShow('extra_ingredients')}
        <div className="pt-3">
          <Button
            type="primary"
            htmlType="submit"
            onClick={() => this.extraIngredientsFieldHandle('add', new Date().getTime())}
          >
            <Icon type="plus-circle" theme="filled" />
            Add More
          </Button>
          <Button
            type="danger"
            htmlType="submit"
            className="ml-2"
            onClick={() =>
              this.setState({
                isAddExtraIngredients: false,
                extra_ingredients: [],
              })
            }
          >
            <Icon type="minus-circle" theme="filled" />
            Remove
          </Button>
        </div>
      </Collapse>
    );
  }
  render() {
    const {
      type = 'add',
      editData,
      match,
      history,
      superCategory,
      utilityCategory,
      utilitySubCategory
    } = this.props;

    const {
      name,
      description,
      price,
      unit,
      ingredients,
      subcategory_id,
      category_id,
      store_id,
      quantity,
      editUtilityImages,
      product_image,
      categories,
      subcategories,
      store,
      isLoading,
      apiError,
      error,
      isAddExtraIngredients,
      super_category_id,
      superCategoryId,
      utilityCategoryId,
      utilitySubCategoryId,
      utilitiesImages,
      SmothieName,
      images
    } = this.state;
    console.log(superCategoryId, "superCategoryIdsuperCategoryIdsuperCategoryId")
    const { errorType, errorText } = error;
    if (type === 'edit') {
      if (!editData) {
        history.push(
          match.url
            .split('/')
            .slice(0, -1)
            .join('/')
        );
        return null;
      }
    }
    console.log('rrrr nnn', categories, subcategories, this.state, "super_category_id", super_category_id);
    return (
      <div className="form-container">

        <FormItem
          label="Super Category"
          hasFeedback
          className={cx({ 'has-error': errorType === 'store' })}>
          <Select
            className="w-100"
            name="super_category_id"
            value={type === 'edit' ? superCategoryId : super_category_id}
            placeholder="Select Category"
            onChange={e => this.handleSelect(type === 'edit' ? 'superCategoryId' : 'super_category_id', e)}
            disabled={type === 'edit'}
          >
            {superCategory.map((data, index) => {
              if (data.status === 1) {
                return null;
              }
              return (
                <Option key={index} value={data._id}>
                  {data.supercategoryname}
                </Option>
              );
            })}
          </Select>
          {this.errorShow('super_category_id')}

        </FormItem>

        <Row form>
          <Col md={12}>
            <FormItem
              label="Select Beverage Category"
              hasFeedback
              className={cx({ 'has-error': errorType === 'category_id' })}>
              <Select
                className="w-100"
                name="category_id"
                value={type === 'edit' ? utilityCategoryId : category_id}
                // value={category_id}
                disabled={super_category_id == '' || type === 'edit' ? true : false}
                placeholder="Select Beverage Category"
                onChange={e => this.handleSelect(type === 'edit' ? 'utilityCategoryId' : 'category_id', e)}>

                {super_category_id !== '' && (

                  utilityCategory.map((data, index) => {
                    if (data.status === 1) {
                      return null;
                    }
                    return (
                      <Option key={index} value={data._id}>
                        {data.title}
                      </Option>
                    );
                  })

                )}

              </Select>
              {this.errorShow('category_id')}
            </FormItem>

          </Col>




          {/* <Col md={6}>
            <FormItem
              label="Select Sub Categories"
              hasFeedback
              className={cx({ 'has-error': errorType === 'subcategory' })}
            >
              <Select
                className="w-100"
                name="subcategory_id"
                value={type == 'edit' ? utilitySubCategoryId : subcategory_id}
                placeholder="Select Sub Category"
                disabled={category_id == '' ? true : false}
                onChange={e => this.handleSelect(type == 'edit' ? 'utilitySubCategoryId' : 'subcategory_id', e)}
              >
                {type === 'edit' && (
                  utilityCategoryId !== '' && (
                    subcategories.map((data, index) => (
                      <Option key={index} value={data._id}>
                        {data.title}
                      </Option>
                    ))
                  )
                )}

                {type !== 'edit' && (
                  category_id !== '' && (
                    subcategories.map((data, index) => (
                      <Option key={index} value={data._id}>
                        {data.title}
                      </Option>
                    ))
                  )
                )}
              </Select>
              {this.errorShow('subcategory')}
            </FormItem>
          </Col> */}
        </Row>

        <FormItem
          label="Beverage Name"
          hasFeedback
          className={cx({ 'has-error': errorType === 'name' })}>

          <Input
            maxLength={25}
            name={type == 'edit' ? 'SmothieName' : "name"}
            value={type == 'edit' ? SmothieName : name}
            onChange={this.handleInput} />

          {this.errorShow('name')}
        </FormItem>

        {/* <FormItem label="Description" className={cx({ 'has-error': errorType === 'description' })}>
          <Input.TextArea name="description" value={description} onChange={this.handleInput} />
          {this.errorShow('description')}
        </FormItem> */}

        <FormItem label="Description" className={cx({ 'has-error': errorType === 'description' })}>
          <CKEditor name="description" value={description} onUpdate={this.handleEditor} />
          {this.errorShow('description')}
        </FormItem>

        <FormItem label="Ingredients" className={cx({ 'has-error': errorType === 'ingredients' })}>
          <CKEditor name="ingredients" value={ingredients} onUpdate={this.handleEditor} />
          {/* <Select
            mode="tags"
            name="ingredients"
            style={{ width: '100%' }}
            searchPlaceholder="Ingredients"
            onChange={e => this.handleTagsChange('ingredients', e)}
            defaultValue={ingredients}
          ></Select> */}
          {this.errorShow('ingredients')}
        </FormItem>

        <Row form>
          <Col md={6}>
            <FormItem
              label="Price"
              hasFeedback
              className={cx({ 'has-error': errorType === 'price' })}
            >
              <Input
                name="price"
                value={price}
                // prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                type="number"
                onChange={this.handleInput}
              />
              {this.errorShow('price')}
            </FormItem>
          </Col>



          <Col md={6}>
            <FormItem
              label="Unit"
              hasFeedback
              className={cx({ 'has-error': errorType === 'unit' })}
            >
              <Input
                name="unit"
                // prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                maxLength={26}
                value={unit}
                onChange={this.handleInput}
              />
              {this.errorShow('unit')}
            </FormItem>
          </Col>
        </Row>




        <Row form>
          <Col md={6}>
            <FormItem
              label="Quantity"
              hasFeedback
              className={cx({ 'has-error': errorType === 'quantity' })}
            >
              <Input
                name="quantity"
                value={quantity}
                // prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                type="number"
                onChange={this.handleInput}
              />
              {this.errorShow('quantity')}
            </FormItem>
          </Col>


          {type === 'add' && (
            <FormItem label="Add Product Image">
              {/* <DragAndDrop /> */}
              <Dragger
                listType="picture"
                onChange={this.onDragChange}
                beforeUpload={() => false}
                name={'file'}
                multiple={false}
              >
                <p className="ant-upload-drag-icon">
                  <Icon type="inbox" />
                </p>
                <p className="ant-upload-text">Click or drag file to this area to upload</p>
                <p className="ant-upload-hint">
                  Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                  band files
              </p>
              </Dragger>

            </FormItem>
          )}




          <Col md={6} className={cx({ ['pt-md-4']: !isAddExtraIngredients })}>
            {!isAddExtraIngredients && (
              <Button
                type="primary"
                htmlType="submit"
                onClick={() =>
                  this.setState({
                    isAddExtraIngredients: true,
                    extra_ingredients: defaultExtraIngredients,
                  })
                }
              >
                <Icon type="plus-circle" theme="filled" />
                Add Extra Ingredients
              </Button>
            )}
          </Col>
        </Row>
        {this.addExtraIngredientsForm()}




        {type === 'edit' && (

          <>
            <FormItem
              className="mt-5"
              label="Product Images">
              <Row className="pt-5" className="text-center" justify={'space-between'}>
                {editUtilityImages.length > 0 ? editUtilityImages.map((single, index) => {

                  return (
                    <Col span={4} className="text-center">
                      <Button type="danger" className="ml-2" shape="circle" onClick={() => this.handleEditProductImage(index)}>
                        <Icon type="close-circle" theme="filled" />
                      </Button>
                      <br />
                      <img
                        src={single}
                        alt=""
                        style={{ width: 100, height: 100, objectFit: 'contain' }}
                      />
                      <br />
                    </Col>
                  )

                }) : <Col span={12} className="text-center">No Image</Col>}
              </Row>
            </FormItem>
          </>
        )}


        {type === 'edit' && (
          <FormItem label="Upload Image">
            {/* <DragAndDrop /> */}
            <Dragger
              listType="picture"
              onChange={this.onEditDragChange}
              beforeUpload={() => false}
              name={'file'}
              multiple={false}
            >
              <p className="ant-upload-drag-icon">
                <Icon type="inbox" />
              </p>
              <p className="ant-upload-text">Click or drag file to this area to upload</p>
              <p className="ant-upload-hint">
                Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                band files
              </p>
            </Dragger>

          </FormItem>
        )}





        {isLoading && (
          <div className="mb-2 text-center">
            <Spin />
          </div>
        )}



        <div className="text-right pt-3">
          {type === 'add' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              onClick={this.handleAddData}
            >
              <Icon type="plus-circle" theme="filled" />
              Add
            </Button>
          )}
          {type === 'edit' && (
            <Button
              type="primary"
              htmlType="submit"
              className="btn-cta"
              onClick={this.handleEditData}
            >
              <Icon type="edit" theme="filled" /> Save
            </Button>
          )}
        </div>
        {apiError && (
          <div className="callout callout-danger">
            <p>{apiError}</p>
          </div>
        )}
      </div>
    );
  }
}


const mapStateToProps = state => ({
  utilitySubCategory: state.utilitySubCategory.utilitySubCategory,
  utilityCategory: state.utilityCategory.utilityCategory,
  superCategory: state.superCategory.superCategory
})

const mapDispatchToProps = dispatch => ({
  getUtilityCategoryDetail: (defaultStoreId, defaultSuperCatId) => dispatch(getAllUtilityCategoryDetail(defaultStoreId, defaultSuperCatId)),
  getUtilitySubCategoryDetail: (defaultStoreId, defaultSuperCatId) => dispatch(getAllUtilitySubCategoryDetail(defaultStoreId, defaultSuperCatId))
})

const WrappedFormComponent = Form.create()(withRouter(FormComponent));

export default connect(mapStateToProps, mapDispatchToProps)(WrappedFormComponent);
