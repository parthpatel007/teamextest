import * as types from '../constants/actionTypes'

const initialState = {
    product: [],
    productError: false,
    productSuccess: false,
    productLoader: false
}

export const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_PRODUCTS:
            return {
                ...state,
                product: action.payload,
                productError: false,
                productSuccess: true,
                productLoader: false
            }
        case types.GET_PRODUCTS_ERROR:
            return {
                ...state,
                productsError: true,
                productSuccess: false,
                productLoader: false
            }
        case types.GET_PRODUCTS_START:
            return {
                productLoader: true
            }
        default:
            return state;
    }
}