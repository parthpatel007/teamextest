import * as types from '../constants/actionTypes'

const initialState = {
    download: [],
    downloadError: false,
    downloadSuccess: false
}

export const downloadReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_DOWNLOAD:
            return {
                ...state,
                download: action.payload,
                downloadError: false,
                downloadSuccess: true
            }
        case types.GET_DOWNLOAD_ERROR:
            return {
                ...state,
                downloadError: true,
                downloadSuccess: false
            }

        default:
            return state;
    }
}