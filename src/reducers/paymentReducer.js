import * as types from '../constants/actionTypes'

const initialState = {
    payment: [],
    paymentError: false,
    paymentSuccess: false

}

export const paymentReducer = (state = initialState, action) => {
    switch(action.type){
        case types.GET_PAYMENT: 
            return {
                ...state,
                payment: action.payload,
                paymentError: false,
    paymentSuccess: true

            }
        case types.GET_PAYMENT_ERROR: 
            return {
                ...state,
                paymentError: true,
    paymentSuccess: false

            }

        default :
        return state;
    }
}