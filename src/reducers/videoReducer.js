import * as types from '../constants/actionTypes'

const initialState = {
    Video: [],
    videoError: false,
    videoSuccess: false,
    videoLoader: false
}

export const videoReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_VIDEO:
            return {
                ...state,
                Video: action.payload,
                videoError: false,
                videoSuccess: true,
                videoLoader: false
            }
        case types.GET_VIDEO_ERROR:
            return {
                ...state,
                videoError: true,
                videoSuccess: false,
                videoLoader: false
            }
        case types.GET_VIDEO_ERROR:
            return {
                ...state,
                videoLoader: true
            }
        default:
            return state;
    }
}