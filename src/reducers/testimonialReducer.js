import * as types from '../constants/actionTypes'

const initialState = {
    testimonial: [],
    testimonialError: false,
    testimonialSuccess: false,
    productLoader:false,
}

export const testimonialReducer = (state = initialState, action) => {
    switch (action.type) {
        case  types.GET_TESTIMONIAL:
            return {
                ...state,
                testimonial: action.payload,
                testimonialError: false,
                testimonialSuccess: true,
                productLoader:false,
            }
        case  types.GET_TESTIMONIAL_ERROR:
            return {
                ...state,
                testimonialError: true,
                testimonialSuccess: false,
                productLoader:false,
            }
        case types.GET_TESTIMONIAL_START:
            return {
                productLoader:true
            }
        default:
            return state;
    }
}