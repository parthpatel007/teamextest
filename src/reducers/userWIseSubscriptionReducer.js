import * as types from '../constants/actionTypes'

const initialState = {
    userSubscription: [],
    userSubscriptionError: false,
    userSubscriptionSuccess: false

}

export const userSubscriptionReducer = (state = initialState, action) => {
    switch(action.type){
        
        case types.GET_USER_SUBSCRIPTION: 
            return {
                ...state,
                userSubscription: action.payload,
                userSubscriptionError: false,
    userSubscriptionSuccess: true

            }
        case types.GET_USER_SUBSCRIPTION_ERROR: 
            return {
                ...state,
                userSubscriptionError: true,
    userSubscriptionSuccess: false

            }

        default :
        return state;
    }
}