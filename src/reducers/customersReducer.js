import * as types from '../constants/actionTypes'

const initialState = {
    customers: [],
    customersError: false,
    customersSuccess: false
}

export const customersReducer = (state = initialState, action) => {
    switch(action.type){
        case types.GET_CUSTOMERS: 
            return {
                ...state,
                customers: action.payload,
                customersError: false,
    customersSuccess: true

            }
        case types.GET_CUSTOMERS_ERROR: 
            return {
                ...state,
                customersError: true,
    customersSuccess: false
            }

        default :
        return state;
    }
}