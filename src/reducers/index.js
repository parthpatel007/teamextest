import { combineReducers } from 'redux';
import settingsReducer from './settingsReducer';
import { routerReducer } from 'react-router-redux';
import { categoryReducer } from './categoryReducer';
import { productReducer } from './productReducer';
import { subCategoryReducer } from './subCategoryReducer';
import { discountReducer } from './discountReducer';
import { packageReducer } from './packageReducer';
import { customizedItemReducer } from './customizedMealItemReducer';
import { subscriptionReducer } from './subscriptionReducer';
import { customersReducer } from './customersReducer';
import { orderReducer } from './orderReducer'
import { paymentReducer } from './paymentReducer'
import { userSubscriptionReducer } from './userWIseSubscriptionReducer';
import { videoReducer } from './videoReducer';
import { testimonialReducer } from './testimonialReducer';
import { reviewReducer } from './reviewReducer';
import { franchiseReducer } from './franchiseReducer';
import { certificateReducer } from './certificateReducer';
import { bannerReducer } from './bannerReducer';
import { downloadReducer } from './downloadReducer';
import { imagePopupReducer } from './imagePopupReducer';

const rootReducer = combineReducers({
  settings: settingsReducer,
  routing: routerReducer,
  category: categoryReducer,
  product: productReducer,
  subCategory: subCategoryReducer,
  discount: discountReducer,
  packages: packageReducer,
  subscription: subscriptionReducer,
  userSubscription: userSubscriptionReducer,
  customizedItem: customizedItemReducer,
  customers: customersReducer,
  order: orderReducer,
  payment: paymentReducer,
  video: videoReducer,
  testimonial: testimonialReducer,
  review: reviewReducer,
  franchise: franchiseReducer,
  certificate: certificateReducer,
  banner: bannerReducer,
  download: downloadReducer,
  imagePopup: imagePopupReducer,
});

export default rootReducer;
