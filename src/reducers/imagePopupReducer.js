import * as types from '../constants/actionTypes'

const initialState = {
    imagePopup: [],
    imagePopupError: false,
    imagePopupSuccess: false
}

export const imagePopupReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_IMAGEPOPUP:
            return {
                ...state,
                imagePopup: action.payload,
                imagePopupError: false,
                imagePopupSuccess: true
            }
        case types.GET_IMAGEPOPUP_ERROR:
            return {
                ...state,
                imagePopupError: true,
                imagePopupSuccess: false
            }

        default:
            return state;
    }
}