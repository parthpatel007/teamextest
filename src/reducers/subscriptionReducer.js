import * as types from '../constants/actionTypes'

const initialState = {
    subscription: [],
    subscriptionError: false,
    subscriptionSuccess: false
}

export const subscriptionReducer = (state = initialState, action) => {
    switch(action.type){
        case types.GET_SUBSCRIPTION: 
            return {
                ...state,
                subscription: action.payload,
                subscriptionError: false,
    subscriptionSuccess: true

            }
        case types.GET_SUBSCRIPTION_ERROR: 
            return {
                ...state,
                subscriptionError: true,
    subscriptionSuccess: false

            }

        default :
        return state;
    }
}