import * as types from '../constants/actionTypes'

const initialState = {
    subCategory: [],
    subCategoryError: false,
    subCategorySuccess: false,
    subCategoryLoader: false,
}

export const subCategoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_SUBCATEGORY:
            return {
                ...state,
                subCategory: action.payload,
                subCategoryError: false,
                subCategorySuccess: true,
                subCategoryLoader: false
            }
        case types.GET_SUBCATEGORY_ERROR:
            return {
                ...state,
                subCategoryError: true,
                subCategorySuccess: false,
                subCategoryLoader: false
            }
        case types.GET_SUBCATEGORY_START:
            return {
                ...state,
                subCategoryLoader: true
            }
        default:
            return state;
    }
}