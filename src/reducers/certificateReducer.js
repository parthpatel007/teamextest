import * as types from '../constants/actionTypes'

const initialState = {
    certificate: [],
    certificateError: false,
    certificateSuccess: false
}

export const certificateReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_CERTIFICATE:
            return {
                ...state,
                certificate: action.payload,
                certificateError: false,
                certificateSuccess: true
            }
        case types.GET_CERTIFICATE_ERROR:
            return {
                ...state,
                certificateError: true,
                certificateSuccess: false
            }

        default:
            return state;
    }
}