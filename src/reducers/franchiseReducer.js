import * as types from '../constants/actionTypes'

const initialState = {
    Franchise: [],
    FranchiseError: false,
    FranchiseSuccess: false,
    franchiseLoader: false
}

export const franchiseReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_FRANCHISE:
            return {
                ...state,
                Franchise: action.payload,
                FranchiseError: false,
                FranchiseSuccess: true,
                franchiseLoader: false
            }
        case types.GET_FRANCHISE_ERROR:
            return {
                ...state,
                FranchiseError: true,
                FranchiseSuccess: false,
                franchiseLoader: false
            }
        case types.GET_FRANCHISE_START:
            return {
                ...state,
                franchiseLoader: true,
            }
        default:
            return state;
    }
}