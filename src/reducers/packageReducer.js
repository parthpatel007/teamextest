import * as types from '../constants/actionTypes'

const initialState = {
    packages: [],
    packageError: false,
    packageSuccess: false

}

export const packageReducer = (state = initialState, action) => {
    switch(action.type){
        case types.GET_PACKAGES: 
            return {
                ...state,
                packages: action.payload,
                packageError: false,
    packageSuccess: true
            }
        case types.GET_PACKAGES_ERROR: 
            return {
                ...state,
                packageError: true,
    packageSuccess: false

            }

        default :
        return state;
    }
}