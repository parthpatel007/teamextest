import * as types from '../constants/actionTypes'

const initialState = {
    customizedItem: [],
    customizedItemError: false,
    customizedItemSuccess: false

}

export const customizedItemReducer = (state = initialState, action) => {
    switch(action.type){
        case types.CUSTOMIZED_MEALS_ITEM: 
            return {
                ...state,
                customizedItem: action.payload,
                customizedItemError: false,
    customizedItemSuccess: true

            }
        case types.CUSTOMIZED_MEALS_ITEM_ERROR: 
            return {
                ...state,
                customizedItemError: true,
    customizedItemSuccess: false

            }

        default :
        return state;
    }
}