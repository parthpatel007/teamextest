import * as types from '../constants/actionTypes'

const initialState = {
    order: [],
    orderCardInfo: null,
    orderError: false,
    orderSuccess: false
}

export const orderReducer = (state = initialState, action) => {
    switch(action.type){
        case types.GET_ORDER: 
            return {
                ...state,
                order: action.payload,
                orderError: false,
    orderSuccess: true
            }
        case types.GET_ORDER_CARD_INFO: 
            return {
                ...state,
                orderCardInfo: action.payload,
            }
        case types.GET_ORDER_ERROR: 
            return {
                ...state,
                orderError: true,
    orderSuccess: false
            }

        default :
        return state;
    }
}