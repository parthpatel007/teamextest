import * as types from '../constants/actionTypes'

const initialState = {
    discount: [],
    discountError: false,
    discountSuccess: false,
    discountLoader: false
}

export const discountReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_DISCOUNT:
            return {
                ...state,
                discount: action.payload,
                discountSuccess: true,
                discountError: false,
                discountLoader: false
            }
        case types.GET_DISCOUNT_ERROR:
            return {
                ...state,
                discountError: true,
                discountSuccess: false,
                discountLoader: false
            }
        case types.GET_DISCOUNT_START:
            return {
                ...state,
                discountLoader: true
            }
        default:
            return state;
    }
}