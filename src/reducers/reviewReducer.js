import * as types from '../constants/actionTypes'

const initialState = {
    Review: [],
   ReviewError: false,
   ReviewSuccess: false
}

export const reviewReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_REVIEW:
            return {
                ...state,
               Review: action.payload,
               ReviewError: false,
               ReviewSuccess: true
            }
        case types.GET_REVIEW_ERROR:
            return {
                ...state,
               ReviewError: true,
               ReviewSuccess: false
            }

        default:
            return state;
    }
}