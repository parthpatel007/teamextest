import * as types from '../constants/actionTypes'

const initialState = {
    category: [],
    categoryError: false,
    categorySuccess: false,
    categoryLoader: false
}

export const categoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_CATEGORY:
            return {
                ...state,
                category: action.payload,
                categoryError: false,
                categorySuccess: true,
                categoryLoader: false
            }
        case types.GET_CATEGORY_ERROR:
            return {
                ...state,
                categoryError: true,
                categorySuccess: false,
                categoryLoader: false
            }
        case types.GET_CATEGORY_START:
            return {
                categoryLoader: true
            }
        default:
            return state;
    }
}