import * as types from '../constants/actionTypes'

const initialState = {
    banner: [],
    bannerError: false,
    bannerSuccess: false,
    bannerLoader: false
}

export const bannerReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_HOMEBANNER:
            return {
                ...state,
                banner: action.payload,
                bannerError: false,
                bannerSuccess: true,
                bannerLoader: false
            }
        case types.GET_HOMEBANNER_ERROR:
            return {
                ...state,
                bannerError: true,
                bannerSuccess: false,
                bannerLoader: false
            }
        case types.GET_HOMEBANNER_START:
            return {
                ...state,
                bannerLoader: true
            }
        default:
            return state;
    }
}