import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getDiscountDetail, getDiscountError, getDiscountStart } from '../actions/discountActions';

const getAllDiscountDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        dispatch(getDiscountStart())
        await axios.get(`${backendUrl}/api/discounts`, {
            headers: {
                Authorization: `Bearer ${authToken}`,
            },
        })
            .then((response) => {
                dispatch(getDiscountDetail(response.data.data.data))
            })
            .catch((error) => {
                console.log(error);
                dispatch(getDiscountError())
            })
    }
}

export default getAllDiscountDetail;