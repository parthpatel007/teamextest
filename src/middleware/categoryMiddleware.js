import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getCategoryDetail, getCategoryError, getCategoryStart } from '../actions/categoryActions';

const getAllCategoryDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        dispatch(getCategoryStart())
        await axios.get(`${backendUrl}/api/categories`, {
            headers: {
                Authorization: `Bearer ${authToken}`,
            },
        })
            .then((response) => {
                dispatch(getCategoryDetail(response.data.data.data))
            })
            .catch((error) => {
                console.log(error)
                dispatch(getCategoryError())
            })
    }
}

export default getAllCategoryDetail;