import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getUserSubscriptionDetail, getUserSubscriptionError } from '../actions/userSubscriptionActions';

const getAllUserSubscriptionListing = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}/api/getSubscriptionForAdmin`, {
            headers: {
              Authorization:`Bearer ${authToken}`,
            },
        })
        .then((response) => {
            console.log(" SUBSCRIPTINMDSMKD DSNDSKJSK DNSDKN SJD : ",response.data.data.data)
            return (dispatch(getUserSubscriptionDetail(response.data.data.data)))
        })
        .catch((error) => {
            console.log(error);
            return (dispatch(getUserSubscriptionError()))
        })
    }
} 

export default getAllUserSubscriptionListing;