import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getPackageDetail, getPackageError } from '../actions/packageActions';

const getAllPackageListing = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}/api/packages`, {
            headers: {
              Authorization:`Bearer ${authToken}`,
            },
        })
        .then((response) => {
            console.log(" Categories : ",response.data.data.data)
            return (dispatch(getPackageDetail(response.data.data.data)))
        })
        .catch((error) => {
            console.log(error);
            return (dispatch(getPackageError()))
        })
    }
} 

export default getAllPackageListing;