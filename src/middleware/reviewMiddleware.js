import axios from 'axios'
import {backendUrl} from '../common/credentials'
import { getReviewDetail , getReviewError } from '../actions/reviewActions'

const getAllReviewDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
    return await axios.get(`${backendUrl}api/`,{
        headers:{
            Authorization : `Bearer ${authToken}`,
        },
    })
    .then((response) => {
        return (dispatch(getReviewDetail(response.data.data.data.data)))
    })
    .catch((error) => {
        return(dispatch(getReviewError()))
    })
}
}
export default getAllReviewDetail;