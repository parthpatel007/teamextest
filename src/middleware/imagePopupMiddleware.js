import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getImagePopupDetail, getImagePopupError } from '../actions/imagePopupActions'

const getAllImagePopupDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}/api/getImage`, {
            headers: {
                Authorization: `Bearer ${authToken}`,
            },
        })
            .then((response) => {
                return (dispatch(getImagePopupDetail(response.data.data.data)))
            })
            .catch((error) => {
                console.log(error, "rerer")
                return (dispatch(getImagePopupError()))
            })
    }
}
export default getAllImagePopupDetail;