import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getBannerDetail, getBannerError, getBannerStart } from '../actions/bannerActions'

const getAllBannerDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        dispatch(getBannerStart())
        await axios.get(`${backendUrl}/api/homeBanner`, {
            headers: {
                Authorization: `Bearer ${authToken}`,
            },
        })
            .then((response) => {
                dispatch(getBannerDetail(response.data.data.data))
            })
            .catch((error) => {
                dispatch(getBannerError())
            })
    }
}
export default getAllBannerDetail;