import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getCertificateDetail, getCertificateError } from '../actions/certificateActions'

const getAllCertificateDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}/api/getCertificate`, {
            headers: {
                Authorization: `Bearer ${authToken}`,
            },
        })
            .then((response) => {
                return (dispatch(getCertificateDetail(response.data.data.data)))
            })
            .catch((error) => {
                return (dispatch(getCertificateError()))
            })
    }
}
export default getAllCertificateDetail;