import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getSubCategoryDetail, getSubCategoryError, getSubCategoryStart } from '../actions/subCategoryActions';

export const getAllSubCategoryDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        dispatch(getSubCategoryStart())
        await axios.get(`${backendUrl}/api/subCategories`, {
            headers: {
                Authorization: `Bearer ${authToken}`,
            },
        })
            .then((response) => {
                // console.log("Sub Categories : ", response.data.data.data)
                dispatch(getSubCategoryDetail(response.data.data.data))
            })
            .catch((error) => {
                console.log(error);
                dispatch(getSubCategoryError())
            })
    }
}

export const getAllSubCategoryDetailCategoryWise = (categoryId) => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}/api/subCategories?categoryId=${categoryId}`, {
            headers: {
                Authorization: `Bearer ${authToken}`,
            },
        })
            .then((response) => {
                // console.log("Sub Categories : ", response.data.data.data)
                return (dispatch(getSubCategoryDetail(response.data.data.data)))
            })
            .catch((error) => {
                console.log(error);
                return (dispatch(getSubCategoryError()))
            })
    }
}

