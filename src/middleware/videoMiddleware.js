import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getVideoDetail, getVideoError, getVideoStart } from '../actions/videoAction'

const getAllVidoDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        dispatch(getVideoStart())
        await axios.get(`${backendUrl}api/video`, {
            headers: {
                Authorization: `Bearer ${authToken}`,
            },
        })
            .then((response) => {
                dispatch(getVideoDetail(response.data.data.data))
            })
            .catch((error) => {
                // alert("wei ewererererererereererererererer  ")
                dispatch(getVideoError())
            })
    }
}
export default getAllVidoDetail;