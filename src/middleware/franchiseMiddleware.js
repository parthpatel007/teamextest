import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getFranchiseDetail, getFranchiseError, getFranchiseStart } from '../actions/franchiseActions'

const getAllFranchiseDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        dispatch(getFranchiseStart())
        await axios.get(`${backendUrl}/api/franchise`, {
            headers: {
                Authorization: `Bearer ${authToken}`,
            },
        })
            .then((response) => {
                dispatch(getFranchiseDetail(response.data.data.data))
            })
            .catch((error) => {
                dispatch(getFranchiseError())
            })
    }
}
export default getAllFranchiseDetail;