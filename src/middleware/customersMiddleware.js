import axios from 'axios'
import { backendUrl } from '../common/credentials'
import {getCustomersDetail, getCustomersError } from '../actions/customersActions';

const getAllCustomersListing = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}/api/customer`, {
            headers: {
              Authorization:`Bearer ${authToken}`,
            },
        })
        .then((response) => {
            // console.log(" CUSTOMERS FROM MIDDDEL : ",response.data.data.data)
            return (dispatch(getCustomersDetail(response.data.data.data)))
        })
        .catch((error) => {
            console.log(error);
            return (dispatch(getCustomersError()))
        })
    }
} 

export default getAllCustomersListing;