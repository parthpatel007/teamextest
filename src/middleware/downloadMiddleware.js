import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getDownloadDetail, getDownloadError } from '../actions/downloadActions'

const getAllDownloadDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}api/getDownloadImage`, {
            headers: {
                Authorization: `Bearer ${authToken}`,
            },
        })
            .then((response) => {
                return (dispatch(getDownloadDetail(response.data.data.data)))
            })
            .catch((error) => {
                console.log(error, "rerer")
                return (dispatch(getDownloadError()))
            })
    }
}
export default getAllDownloadDetail;