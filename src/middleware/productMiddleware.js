import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getProductDetail, getProductError, getProductStart } from '../actions/productsActions';

const getAllProductDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        dispatch(getProductStart())
        await axios.get(`${backendUrl}/api/products`, {
            headers: {
                Authorization: `Bearer ${authToken}`,
            },
        })
            .then((response) => {
                dispatch(getProductDetail(response.data.data.data))
            })
            .catch((error) => {
                console.log(error);
                dispatch(getProductError())
            })
    }
}

export default getAllProductDetail;