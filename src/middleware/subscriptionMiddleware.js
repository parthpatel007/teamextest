import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getSubscriptionDetail, getSubscriptionError } from '../actions/subscriptionActions';

const getAllSubscriptionListing = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}/api/subscription`, {
            headers: {
              Authorization:`Bearer ${authToken}`,
            },
        })
        .then((response) => {
            console.log(" SUBSCRIPTINMDSMKD DSNDSKJSK DNSDKN SJD : ",response.data.data.data)
            return (dispatch(getSubscriptionDetail(response.data.data.data)))
        })
        .catch((error) => {
            console.log(error);
            return (dispatch(getSubscriptionError()))
        })
    }
} 

export default getAllSubscriptionListing;