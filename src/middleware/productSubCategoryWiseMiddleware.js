import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getProductDetail, getProductError } from '../actions/productsActions';

const getAllProductSubCategoryWiseDetail = (category,subCategory) => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}/api/products?categoryId=${category}&subCategoryId=${subCategory}`, {
            headers: {
              Authorization:`Bearer ${authToken}`,
            },
        })
        .then((response) => {
            console.log(" Products : ",response.data.data.data)
            return (dispatch(getProductDetail(response.data.data.data)))
        })
        .catch((error) => {
            console.log(error);
            return (dispatch(getProductError()))
        })
    }
} 

export default getAllProductSubCategoryWiseDetail;