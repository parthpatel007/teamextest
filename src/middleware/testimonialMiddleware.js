import axios from 'axios'
import {backendUrl} from '../common/credentials'
import {getTestimonialDetail , getTestimonialError } from '../actions/testimonialActions'

const getAllTestimonialDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
    return await axios.get(`${backendUrl}/api/getTestimonial`,{
        headers:{
            Authorization : `Bearer ${authToken}`,
        },
    })
    .then((response) => {
        console.log(response.data.data.data)
        return (dispatch(getTestimonialDetail(response.data.data.data)))
    })
    .catch((error) => {
        return(dispatch(getTestimonialError()))
    })
}
}
export default getAllTestimonialDetail;