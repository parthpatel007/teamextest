import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getCustomizedItemDetail, getCustomizedItemError } from '../actions/customizedMealItemActions';

const getAllCustomizedItemListing = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}/api/customizeMealItem`, {
            headers: {
              Authorization:`Bearer ${authToken}`,
            },
        })
        .then((response) => {
            console.log(" CUSTOMIZD ITEMS FROM MIDDDEL : ",response.data.data.data)
            return (dispatch(getCustomizedItemDetail(response.data.data.data)))
        })
        .catch((error) => {
            console.log(error);
            return (dispatch(getCustomizedItemError()))
        })
    }
} 

export default getAllCustomizedItemListing;