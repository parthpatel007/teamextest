import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getOrderCardInfoDetail, getOrderDetail, getOrderError } from '../actions/orderAction';

const getAllOrderDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}/api/order`, {
            headers: {
              Authorization:`Bearer ${authToken}`,
            },
        })
        .then((response) => {
            console.log(" Order : ",response.data.data)
            return (
                dispatch(getOrderDetail(response.data.data.data)),
                dispatch(getOrderCardInfoDetail(response.data.data))
                )
        })
        .catch((error) => {
            console.log(error);
            return (dispatch(getOrderError()))
        })
    }
} 

export default getAllOrderDetail;