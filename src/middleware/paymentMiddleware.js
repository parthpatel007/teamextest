import axios from 'axios'
import { backendUrl } from '../common/credentials'
import { getPaymentDetail, getPaymentError } from '../actions/paymentActions';

const getAllPaymentDetail = () => {
    const authToken = localStorage.getItem('access_token')
    return async function (dispatch) {
        return await axios.get(`${backendUrl}/api/payment`, {
            headers: {
              Authorization:`Bearer ${authToken}`,
            },
        })
        .then((response) => {
            console.log(" Categories : ",response.data.data.data)
            return (dispatch(getPaymentDetail(response.data.data.data)))
        })
        .catch((error) => {
            console.log(error);
            return (dispatch(getPaymentError()))
        })
    }
} 

export default getAllPaymentDetail;