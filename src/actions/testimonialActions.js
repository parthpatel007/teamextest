import * as type from '../constants/actionTypes'

export function getTestimonialDetail(data) {
    return{
        type: type.GET_TESTIMONIAL,
        payload:data
    }
}

export function getTestimonialError(){
    return{
        type: type.GET_TESTIMONIAL_ERROR,
    }
}