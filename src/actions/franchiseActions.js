import * as type from '../constants/actionTypes'

export function getFranchiseDetail(data) {
    return {
        type: type.GET_FRANCHISE,
        payload: data
    }
}

export function getFranchiseError() {
    return {
        type: type.GET_FRANCHISE_ERROR,
    }
}
export function getFranchiseStart() {
    return {
        type: type.GET_FRANCHISE_START,
    }
}