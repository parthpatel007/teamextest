import * as types from '../constants/actionTypes'

export function getOrderDetail(data) {
    return{
        type: types.GET_ORDER,
        payload: data
    }
}

export function getOrderError() {
    return{
        type: types.GET_ORDER_ERROR,
    }
}


export function getOrderCardInfoDetail(data) {
    return{
        type: types.GET_ORDER_CARD_INFO,
        payload: data
    }
}
