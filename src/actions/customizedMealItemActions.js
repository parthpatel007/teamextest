import * as types from '../constants/actionTypes'

export function getCustomizedItemDetail(data) {
    return{
        type: types.CUSTOMIZED_MEALS_ITEM,
        payload: data
    }
}

export function getCustomizedItemError() {
    return{
        type: types.CUSTOMIZED_MEALS_ITEM_ERROR,
    }
}