import * as types from '../constants/actionTypes'

export function getImagePopupDetail(data) {
    return {
        type: types.GET_IMAGEPOPUP,
        payload: data
    }
}

export function getImagePopupError() {
    return {
        type: types.GET_IMAGEPOPUP_ERROR,
    }
}