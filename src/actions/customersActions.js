import * as types from '../constants/actionTypes'

export function getCustomersDetail(data) {
    return{
        type: types.GET_CUSTOMERS,
        payload: data
    }
}

export function getCustomersError() {
    return{
        type: types.GET_CUSTOMERS_ERROR,
    }
}