import * as types from '../constants/actionTypes'

export function getPackageDetail(data) {
    return{
        type: types.GET_PACKAGES,
        payload: data
    }
}

export function getPackageError() {
    return{
        type: types.GET_PACKAGES_ERROR,
    }
}