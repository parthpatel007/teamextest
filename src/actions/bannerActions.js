import * as types from '../constants/actionTypes'

export function getBannerDetail(data) {
    return {
        type: types.GET_HOMEBANNER,
        payload: data
    }
}

export function getBannerError() {
    return {
        type: types.GET_HOMEBANNER_ERROR,
    }
}

export function getBannerStart() {
    return {
        type: types.GET_HOMEBANNER_START,
    }
}