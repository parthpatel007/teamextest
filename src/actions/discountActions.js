import * as types from '../constants/actionTypes'

export function getDiscountDetail(data) {
    return {
        type: types.GET_DISCOUNT,
        payload: data
    }
}

export function getDiscountError() {
    return {
        type: types.GET_DISCOUNT_ERROR,
    }
}
export function getDiscountStart() {
    return {
        type: types.GET_DISCOUNT_START,
    }
}