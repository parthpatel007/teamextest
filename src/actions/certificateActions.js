import * as type from '../constants/actionTypes'

export function getCertificateDetail(data) {
    return{
        type: type.GET_CERTIFICATE,
        payload:data
    }
}

export function getCertificateError(){
    return{
        type: type.GET_CERTIFICATE_ERROR,
    }
}