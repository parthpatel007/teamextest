import * as types from '../constants/actionTypes'

export function getDownloadDetail(data) {
    return {
        type: types.GET_DOWNLOAD,
        payload: data
    }
}

export function getDownloadError() {
    return {
        type: types.GET_DOWNLOAD_ERROR,
    }
}