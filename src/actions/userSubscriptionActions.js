import * as types from '../constants/actionTypes'

export function getUserSubscriptionDetail(data) {
    return{
        type: types.GET_USER_SUBSCRIPTION,
        payload: data
    }
}

export function getUserSubscriptionError() {
    return{
        type: types.GET_USER_SUBSCRIPTION_ERROR,
    }
}