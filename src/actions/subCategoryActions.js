import * as types from '../constants/actionTypes'

export function getSubCategoryDetail(data) {
    return {
        type: types.GET_SUBCATEGORY,
        payload: data
    }
}

export function getSubCategoryError() {
    return {
        type: types.GET_SUBCATEGORY_ERROR,
    }
}
export function getSubCategoryStart() {
    return {
        type: types.GET_SUBCATEGORY_START,
    }
}