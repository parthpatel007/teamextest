import * as types from '../constants/actionTypes'

export function getPaymentDetail(data) {
    return{
        type: types.GET_PAYMENT,
        payload: data
    }
}

export function getPaymentError() {
    return{
        type: types.GET_PAYMENT_ERROR,
    }
}