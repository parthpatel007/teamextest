import * as types from '../constants/actionTypes'

export function getCategoryDetail(data) {
    return {
        type: types.GET_CATEGORY,
        payload: data
    }
}

export function getCategoryError() {
    return {
        type: types.GET_CATEGORY_ERROR,
    }
}

export function getCategoryStart() {
    return {
        type: types.GET_CATEGORY_START,
    }
}