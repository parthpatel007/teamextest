import * as type from '../constants/actionTypes'

export function getReviewDetail(data) {
    return{
        type: type.GET_REVIEW,
        payload:data
    }
}

export function getReviewError(){
    return{
        type: type.GET_REVIEW_ERROR,
    }
}