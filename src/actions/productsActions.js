import * as types from '../constants/actionTypes'

export function getProductDetail(data) {
    return {
        type: types.GET_PRODUCTS,
        payload: data
    }
}

export function getProductError() {
    return {
        type: types.GET_PRODUCTS_ERROR,
    }
}
export function getProductStart() {
    return {
        type: types.GET_PRODUCTS_START,
    }
}