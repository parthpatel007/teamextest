import * as type from '../constants/actionTypes'

export function getVideoDetail(data) {
    return {
        type: type.GET_VIDEO,
        payload: data
    }
}

export function getVideoError() {
    return {
        type: type.GET_VIDEO_ERROR,
    }
}

export function getVideoStart() {
    return {
        type: type.GET_VIDEO_START,
    }
}