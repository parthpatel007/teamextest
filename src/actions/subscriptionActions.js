import * as types from '../constants/actionTypes'

export function getSubscriptionDetail(data) {
    return{
        type: types.GET_SUBSCRIPTION,
        payload: data
    }
}

export function getSubscriptionError() {
    return{
        type: types.GET_SUBSCRIPTION_ERROR,
    }
}