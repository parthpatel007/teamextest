export default () => {
  const isLoggedIn = localStorage.getItem('isLoggedIn');
  if (isLoggedIn === null) {
    localStorage.setItem('isLoggedIn', false);
    return false;
  } else {
    if (isLoggedIn === 'true') {
      return true;
    }
    return false;
  }
};
