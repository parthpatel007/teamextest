import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import loadable from 'react-loadable';
import LoadingComponent from 'components/Loading';
import isLoggedIn from '../common/isLoggedIn';
import { ToastProvider } from 'react-toast-notifications';
// 3rd
import 'styles/antd.less';
import 'styles/bootstrap/bootstrap.scss';

// custom
import 'styles/layout.scss';
import 'styles/theme.scss';
import 'styles/ui.scss';
import 'styles/vendors.scss';
import 'styles/custom/main.scss';


let AsyncAppLayout = loadable({
  loader: () => import('components/Layout/AppLayout/'),
  loading: LoadingComponent,
});
let AsyncException = loadable({
  loader: () => import('routes/exception/'),
  loading: LoadingComponent,
});
let AsyncAccount = loadable({
  loader: () => import('routes/user'),
  loading: LoadingComponent,
});

class App extends React.Component {
  render() {
    const { match, location } = this.props;
    const isRoot = location.pathname === '/' ? true : false;
    if (isRoot) {
      if (isLoggedIn()) {
        return <Redirect to={'/app/dashboard'} />;
      } else {
        return <Redirect to={'/login'} />;
      }
    }

    return (
      <div id="app">
        <ToastProvider autoDismissTimeout={6000} placement="top-right">
          <Route path={`${match.url}app`} component={AsyncAppLayout} />
          <Route path={`${match.url}exception`} component={AsyncException} />
          <Route path={`${match.url}`} component={AsyncAccount} />
        </ToastProvider>
      </div>
    );
  }
}

export default App;
