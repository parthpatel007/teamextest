import React from 'react';
import { withRouter } from 'react-router-dom';
import {Row, Col } from 'reactstrap';
import { Card } from 'antd';
import classes from './style.module.scss';
import ReactHtmlParser from 'react-html-parser';


const CustomizedItemDetailCard = props => {


  const {
    itemName,
    weight,
    price,
    status,
    ingredients,
    howToUse,
    mealImages,
    match,
    history,
  } = props;


  if (!props._id) {
    history.push(
      match.url
        .split('/')
        .slice(0, -1)
        .join('/')
    );
    return null;
  }

  const backLink = match.url
    .split('/')
    .slice(0, -1)
    .join('/');
  if (itemName === '') {
    history.push(backLink);
    return null;
  }
  return (
    <section className="chapter pt-3">
      <article className="article pb-0">
        <h2 className="main-title pt-0">Customized Meal Detail</h2>

        <div className="box box-v1 mb-4">
          <Row>
            <Col md={6} lg={4}>
                <img className={classes.productImage} src={mealImages} alt={itemName} />
            </Col>
            
            <Col md={6} lg={8}>
              <h3>{itemName}</h3>
              <p className="mb-2">
                <b>Status: </b>
                <b className={`text-${status === 0 ? 'success' : 'danger'}`}>
                  {status === 0 ? 'Active' : 'Inactive'}
                </b>
              </p>
              {/* <p className="mb-2">
                <b>Category: </b>
                {categoryId && categoryId.categoryName}
              </p>
              <p className="mb-2">
                <b>Subcategory: </b>
                {subCategoryId &&subCategoryId.subCategoryName}
              </p> */}
              <p className="mb-2">
                <b>Weight: </b>
                {weight} gm
              </p>
              {/* <p className="mb-2">
                <b>Quantity: </b>
                {quantity}
              </p> */}
              <p>
                <b>Price: </b>₹{price}
              </p>
            </Col>
          </Row>
          {/* <Card>
            <h4>Description</h4>
            <hr />
            <div>{ReactHtmlParser(description)}</div>
          </Card> */}
          <Card>
            <Row>
              <Col lg={6}>
                <h4>Ingredients</h4>
                <hr />
                <div style={{"wordBreak": "break-all"}}>{ReactHtmlParser(ingredients)}</div>
                {/* <ol>
                  {ingredients.map((data, index) => (
                    <li key={index}>{data}</li>
                  ))}
                </ol> */}
              </Col>
              <Col lg={6}>
                <h4>How to use:</h4>
                <hr />
                <div style={{"wordBreak": "break-all"}}>{ReactHtmlParser(howToUse)}</div>
                {/* <ol>
                  {directions_to_use.map((data, index) => (
                    <li key={index}>{data}</li>
                  ))}
                </ol> */}
              </Col>
            </Row>
          </Card>
        </div>
      </article>
    </section>
  );
};

export default withRouter(CustomizedItemDetailCard);

