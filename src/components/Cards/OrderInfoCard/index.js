import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Icon } from 'antd';
const OrderInfroCard = (props) => {
  const { filter, order, orderCardInfo } = props
  console.log(orderCardInfo, "orderCardInfo incard")
  const infoCardData = [
    {
      color: 'info',
      iconClassName: 'fas fa-shopping-cart',
      title: 'Total number of orders',
      // day: '30 Days',
      // {(props && props.dashboardData && props.dashboardData.totalOrders) ? parseInt(props && props.dashboardData && props.dashboardData.totalOrders) : '' }
      value: (order && order.length) ? order.length : '',
    },
    {
      color: 'muted',
      iconClassName: 'fas fa-tasks',
      title: 'In-progess amount',
      // day: 'Today',
      value:  '₹' + (orderCardInfo && orderCardInfo.pendingOrdersTotal) ? orderCardInfo && orderCardInfo.pendingOrdersTotal : 0,
    },
    {
      color: 'danger',
      iconClassName: 'fas fa-store-slash',
      title: 'Cancelled orders',
      // day: '30 Days',
      value:  '₹' + (orderCardInfo && orderCardInfo.cancelOrdersTotal) ? orderCardInfo && orderCardInfo.cancelOrdersTotal : 0,
    },
    {
      color: 'warning',
      iconClassName: 'far fa-calendar-alt',
      title: 'Average monthly order amount',
      // day: '30 Days',
      value:  '₹' + (orderCardInfo && orderCardInfo.currentMonthSell) ? orderCardInfo && orderCardInfo.currentMonthSell : 0,
    },
    {
      color: 'success',
      iconClassName: 'far fa-money-bill-alt',
      title: 'Totla revenue',
      // day: '30 Days',
      value:  '₹' + (orderCardInfo && orderCardInfo.totalSell) ? orderCardInfo && orderCardInfo.totalSell : 0,
    },
  ];
  return (
    <section className="chapter pt-5" style={{padding: "0 !important"}}>

        <article className="article pb-0">
          <div className="d-flex justify-content-between">
            <div>
              <h2 className="main-title pt-0">Orders</h2>
            </div>
            <div>{filter}</div>
          </div>

          <div className="box box-v1 mb-4">
            <Row style={{ marginTop: '-1rem' }}>
              {infoCardData.map(({ color, title, iconClassName, day, value }) => (
                <Col sm={6} xl={3} key={title} className="mt-3">
                  <div className="order-info-card">
                    <div className="icon text-info">
                      <i className={`text-${color} ${iconClassName}`} />
                    </div>
                    <div className="content">
                      <h2 className="title">{title}</h2>
                      <p className="day">{day}</p>
                      <p className={`value text-${color}`}>{value}</p>
                    </div>
                  </div>
                </Col>
              ))}
            </Row>
          </div>
        </article>
    </section>
  );
};

export default OrderInfroCard;
