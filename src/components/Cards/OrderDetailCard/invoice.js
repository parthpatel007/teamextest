import React from 'react';
import { Page, Image, Text, View, Font, Document, StyleSheet } from '@react-pdf/renderer';
// import font from '../../../styles/custom/fonts/SulSans/SulSans-Medium.ttf'
import font from './SulSans-Light.ttf'
import logo from '../../../assets/_logo.svg';
import moment from 'moment'
// import src from '*.bmp';

// const styles = StyleSheet.create({

//   page: { backgroundColor: 'tomato' },
//   section: { color: 'white', textAlign: 'center', margin: 30 }
// });

// const doc = (
//   <Document>
//     <Page size="A4" style={styles.page}>
//       <View style={styles.section}>
//         <Text>Section #1</Text>
//       </View>
//     </Page>
//   </Document>
// );

// ReactPDF.render(doc);



export const UserInvoice = (props) => {

  const { name, customer, image, method, status, dec, order_status, payment_mode, createdAt, first_name, match, order_number, last_name, email, phone, address, address2, city, country, postal_code, original_total, discounted_total, item } = props

  console.log(props, "this.props this.props this.props this.props this.props this.props this.props this.props this.props this.props this.props this.props this.props this.props this.props this.props ")

  const current_store = JSON.parse(localStorage.getItem('current_store'))

  return (
    <Document>
      <Page style={styles.body}>
        <Text style={styles.header} fixed>
          ~ INVOICE ~
      </Text>
        <View style={styles.mainView}>
          <View style={styles.singleView}>
            <View style={styles.spaceView}>
              <Image src={logo} style={styles.logo}></Image>
              <View>
                <View style={styles.spaceView}>
                  <Text style={styles.textOrderNumber}>Order Number</Text>
                </View>
                <View style={styles.spaceView}>
                  <Text style={styles.textGray}>#{props && props.orderNo}</Text>
                </View>
              </View>
            </View>
            {/* <View style={styles.spaceView}>
              <Text style={styles.textBlackLarge}>{current_store && current_store.name}</Text>
            </View>
            <View style={styles.spaceView}>
              <Text style={styles.textEmail}>{current_store && current_store.email}</Text>
            </View>
            <View style={styles.spaceView}>
              <Text style={styles.textForSmall}>{current_store && current_store.phone}</Text>
            </View> */}
            {/* <View style={styles.spaceView}>
              <Text style={styles.textForSmall}>
                {current_store && current_store.address} {current_store && current_store.address2} {current_store && current_store.city} {current_store && current_store.country} {current_store && current_store.postal_code}
              </Text>
            </View> */}
          </View>
          <View style={styles.singleView}>
            <View style={styles.spaceView}>
              <Text style={styles.textGray}>Billed to</Text>
              <Text style={styles.textGray}>Bill Date</Text>
              <Text style={styles.textGray}>Total Amount</Text>
            </View>
            <View style={styles.spaceView}>
              <Text style={styles.textForBig}>{props && props.addedBy && props.addedBy.userName}</Text>
              <Text style={styles.textBlackLarge}>{moment(createdAt).format('DD/MM/YYYY')}</Text>
              {props.discountedTotal && (
                <Text style={{...styles.textBlackLarge, fontWeight: 500}}>Rs. {props.discountedTotal}</Text>
              )}

              {!props.discountedTotal && (
                <Text style={styles.textBlackLarge}>Rs. 0</Text>
              )}
              {/* <Text style={styles.textBlackLarge}>Rs. {discounted_total}</Text> */}
            </View>
            <View style={styles.spaceView}>
              <Text style={styles.textEmail}>{props && props.addedBy && props.addedBy.email}</Text>
            </View>

            {props.addressId && (
              <>
                <View style={styles.spaceView}>
                  <Text style={styles.textForSmall2}>{props && props.addressId && props.addressId.flatNo} {', '} {props && props.addressId && props.addressId.street} {', '} {props && props.addressId && props.addressId.landmark} {', '} {props && props.addressId && props.addressId.city} {', '} {props && props.addressId && props.addressId.state} {', '} {props && props.addressId && props.addressId.postalCode}</Text>
                </View>
                {/* <View style={styles.spaceView}>
                  <Text style={styles.textForSmall}>{props && props.addressId && props.addressId.street}</Text>
                </View>
                <View style={styles.spaceView}>
                  <Text style={styles.textForSmall}>{props && props.addressId && props.addressId.landmark}</Text>
                </View>
                <View style={styles.spaceView}>
                  <Text style={styles.textForSmall}>{props && props.addressId && props.addressId.city}</Text>
                </View>
                <View style={styles.spaceView}>
                  <Text style={styles.textForSmall}>{props && props.addressId && props.addressId.state}</Text>
                </View>
                <View style={styles.spaceView}>
                  <Text style={styles.textForSmall}>{props && props.addressId && props.addressId.postalCode}</Text>
                </View> */}
              </>
            )}
          </View>
          <View style={styles.singleView}>
            <View style={styles.spaceView}>
              <Text style={styles.textGray}>Name</Text>
              <View style={{ flexDirection: "row", justifyContent: "space-between", width: "60%", marginRight: "30" }}>
                <Text style={styles.textGray}>Price</Text>
                <Text style={styles.textGray}>Quantity</Text>
                <Text style={styles.textGray}>Amount</Text>
              </View>
            </View>

            {props && props.item && props.item.map((singleItem, key) => {
              return (
                <View style={styles.spaceView}>
                  <Text style={styles.textGray} >{singleItem && singleItem.productId && singleItem.productId.productName}</Text>
                  <View style={{ flexDirection: "row", justifyContent: "space-between", width: "60%", marginRight: "30" }}>
                    <Text style={styles.textGray} >Rs. {singleItem && singleItem.productId && singleItem.productId.price}.00</Text>
                    <Text style={styles.textGray}>{singleItem && singleItem.quantity}</Text>
                    <Text style={styles.textGray}>Rs. {singleItem && singleItem.productId && parseInt(singleItem.productId.price) * parseInt(singleItem.quantity)}.00</Text>
                  </View>
                </View>
              );
            })}

            {(props && props.customizeMeal && props.customizeMeal.length) > 0 && props && props.customizeMeal.map((singleItem, key) => {
              return (
                <View style={styles.spaceView}>
                  <Text style={styles.textGray} >{singleItem && singleItem.customizeMealId && singleItem.customizeMealId.customizeMealName}</Text>
                  <View style={{ flexDirection: "row", justifyContent: "space-between", width: "60%", marginRight: "30" }}>
                    <Text style={styles.textGray} >Rs. {singleItem && singleItem.customizeMealId && singleItem.customizeMealId.total}.00</Text>
                    <Text style={styles.textGray}>{singleItem && singleItem.quantity}</Text>
                    <Text style={styles.textGray}>Rs. {singleItem && singleItem.customizeMealId && parseInt(singleItem.customizeMealId.total) * parseInt(singleItem.quantity)}.00</Text>
                  </View>
                </View>
              );
            })}

            {(props && props.package && props.package.length) > 0 && props && props.package.map((singleItem, key) => {
              return (
                <View style={styles.spaceView}>
                  <Text style={styles.textGray} >{singleItem && singleItem.packageId && singleItem.packageId.packageName}</Text>
                  <View style={{ flexDirection: "row", justifyContent: "space-between", width: "60%", marginRight: "30" }}>
                    <Text style={styles.textGray} >Rs. {singleItem && singleItem.originalPrice}.00</Text>
                    <Text style={styles.textGray}>{singleItem && singleItem.quantity}</Text>
                    <Text style={styles.textGray}>Rs. {parseInt((singleItem && singleItem.originalPrice)) * parseInt((singleItem && singleItem.quantity))}.00</Text>
                  </View>
                </View>
              );
            })}

          </View>

          {((props && props.isSubscription) || ((props && props.isSubscription) == false)) ? (
            <>
              <View style={styles.singleView}>
                <View style={styles.sideView}>
                  <Text style={styles.textPrice}>Subtotal</Text>
                  {props.originalTotal && (
                    <Text style={styles.textPrice}>Rs. {props.originalTotal}.0</Text>
                  )}
                  {!props.originalTotal && (
                    <Text style={styles.textPrice}>Rs. 0.0</Text>
                  )}
                </View>
                <View style={styles.sideView}>
                  <Text style={styles.textPrice}>Discount Price</Text>
                  {props.originalTotal && (
                    <Text style={styles.textPrice}>Rs. {props.originalTotal - props.discountedTotal}.0</Text>
                  )}
                  {!props.originalTotal && (
                    <Text style={styles.textPrice}>Rs. 0.0</Text>
                  )}
                </View>
                <View style={styles.sideView}>
                  <Text style={styles.textPrice}>Delivery Charge</Text>
                  {/* {props.deliveryCharge === 0 ? (
                    <Text style={styles.textPrice}>Rs. 0.0</Text>
                  ) : 
                  <Text style={styles.textPrice}>Rs. {props && props.deliveryCharge}.0</Text>
                  } */}
                  <Text style={styles.textPrice}>Rs. {props && props.deliveryCharge == 0 ? '0' : props.deliveryCharge}.0</Text>
                </View>
                <View style={styles.sideView}>

                  <Text style={styles.textPrice}>Payable Amount</Text>
                  {(props && props.discountedTotal) && (
                    ((props && props.deliveryCharge) && ((props && props.deliveryCharge) == 0)) ? (
                      <Text style={styles.textPrice}>Rs. 0.0</Text>
                    )
                      :
                      <Text style={styles.textPrice}>Rs. {props.discountedTotal + props.deliveryCharge}.0</Text>
                  )}

                  {/* <Text style={styles.textPrice}>Payable Amount</Text>
                  {(props && props.deliveryCharge) == 0 ?
                    <Text style={styles.textPrice}>Rs. 0.0</Text>
                    :
                    ((props && props.discountedTotal) && (props && props.originalTotal) && (props && props.deliveryCharge)) && (
                      <Text style={styles.textPrice}>Rs. {props.discountedTotal + props.deliveryCharge}.0</Text>
                    )
                      (!props.deliveryCharge) && (
                        <Text style={styles.textPrice}>Rs. {props.discountedTotal}.00</Text>
                      )
                      (!props.deliveryCharge) && (
                      <Text style={styles.textPrice}>Rs. {props.discountedTotal}.00</Text>
                    )
                  } */}
                  {/* {}
                  {}
                  {!props.originalTotal && (
                    
                  )} */}
                </View>
              </View>
            </>
          )
            :

            (
              <>
                <View style={styles.singleView}>
                  <View style={styles.sideView}>
                    <Text style={styles.textPrice}>Subtotal</Text>
                    <Text style={styles.textPrice}> - </Text>
                  </View>
                  <View style={styles.sideView}>
                    <Text style={styles.textPrice}>Discount Price</Text>
                    <Text style={styles.textPrice}> - </Text>
                  </View>
                  <View style={styles.sideView}>
                    <Text style={styles.textPrice}>Delivery Charge</Text>
                    <Text style={styles.textPrice}> - </Text>
                  </View>
                  <View style={styles.sideView}>
                    <Text style={styles.textPrice}>Payable Amount</Text>
                    <Text style={styles.textPrice}> - </Text>
                  </View>
                </View>
              </>
            )

          }



          <View style={{ paddingVertical: 20 }}>
            <Text style={styles.textBlack}>Note :</Text>
            <Text style={styles.textNote}>All privacy policy and important notes.</Text>
          </View>
        </View>
        {/* <Text style={styles.pageNumber} render={({ pageNumber, totalPages }) => (
          `Rs. {pageNumber} / Rs. {totalPages}`
        )} fixed /> */}
      </Page>
    </Document>
  )
};



Font.register({
  family: 'Sul sans',
  format: "truetype",
  src: font,
  fontStyle: 'normal',
  weight: 100,
});



const styles = StyleSheet.create({
  mainView: {
    borderWidth: 0.5,
    borderColor: "gray",
    paddingVertical: 10,
    paddingHorizontal: 25,
  },
  logo: {
    height: 100,
    width: 100
  },
  singleView: {
    borderBottomWidth: 0.5,
    borderBottomColor: "gray",
    paddingVertical: 20,
  },
  spaceView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  sideView: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 250,
    // paddingTop: 15,
    // paddingBottom: 10,
    alignItems: "center",
  },
  textGray: {
    fontSize: 8,
    fontFamily: 'Sul sans',
    color: "gray",
    marginVertical: "5"
  },
  textOrderNumber: {
    fontSize: 9,
    fontFamily: 'Sul sans',
    color: "gray",
    marginVertical: "5"
  },
  textForSmall: {
    fontSize: 9,
    fontFamily: 'Sul sans',
    color: "blue",
    marginVertical: "5",
  },
  textForSmall2: {
    fontSize: 9,
    fontFamily: 'Sul sans',
    color: "blue",
    width: '170',
    marginVertical: "5",
  },
  textForBig: {
    fontSize: 16,
    fontFamily: 'Sul sans',
    color: "blue",
    marginVertical: "5",

  },
  textEmail: {
    fontSize: 9,
    fontFamily: 'Sul sans',
    color: "black",
    marginVertical: "5"
  },
  textNote: {
    fontSize: 8,
    fontFamily: 'Sul sans',
    color: "gray",
    marginVertical: "5"
  },
  textBlack: {
    fontSize: 14,
    fontFamily: 'Sul sans',
    color: "black",
    marginVertical: "5"
  },
  textBlackLarge: {
    fontSize: 14,
    fontFamily: 'Sul sans',
    paddingLeft: 50,
    color: "black",
    marginVertical: "5"
  },
  textPrice: {
    fontSize: 11,
    fontFamily: 'Sul sans',
    color: "grey",
    marginVertical: "5",
  },
  textBoldBlack: {
    fontSize: 13,
    fontFamily: 'Sul sans',
    color: "black",
    marginVertical: "5",
  },
  textPurple: {
    fontSize: 13,
    fontFamily: 'Sul sans',
    color: "blue",
    marginVertical: "5",
  },
  textUpload: {
    // marginTop:"100"
  },
  name: {
    fontSize: 18,
    fontFamily: 'Sul sans',
    color: "blue",
    marginVertical: "5",
    textDecoration: "underline"
  },
  body: {
    paddingTop: 15,
    paddingBottom: 65,
    paddingHorizontal: 15,
  },
  title: {
    fontSize: 24,
    textAlign: 'center',
    fontFamily: 'Sul sans'
  },
  header: {
    fontSize: 12,
    marginBottom: 20,
    textAlign: 'center',
    color: 'grey',
  },
  pageNumber: {
    position: 'absolute',
    fontSize: 12,
    bottom: 30,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: 'grey',
  },
});
// ReactPDF.render(<UserInvoice />);