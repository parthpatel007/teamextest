
import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Button, Icon, Table } from 'antd';
import OrderDetailInfoCard from '../OrderDetailInfoCard';
import { PDFDownloadLink, Document, Page } from '@react-pdf/renderer'
import { Link, withRouter } from 'react-router-dom';
import { UserInvoice } from './invoice'
import productImage from '../../../assets/images/productImage.png';
import moment from 'moment'



const OrderDetailCard = props => {

  const { name, customer, image, method, status, dec, order_status, createdAt, payment_mode, first_name, match, order_number, last_name, email, phone, address, address2, city, country, postal_code, original_total, discounted_total, item } = props;

  console.log(item, 'order detail')
  // name: 'Elite Xt',
  //       customer: 'Jhon',
  //       image: productImage,
  //       method: 'Visa',
  const backLink = match.url
    .split('/')
    .slice(0, -1)
    .join('/');

  const columns = [
    {
      title: 'Product Image',
      // dataIndex: 'product_image',
      key: 'productid',
      render: (src, text) => {
        return (
          <div>
            <img style={{ width: 50, height: 50, objectFit: 'contain' }} src={text && text.productId && text.productId.productImages && text.productId.productImages[0] || productImage} />
          </div>
        )
      },
      width: 50,
    },
    {
      title: 'Name',
      // dataIndex: 'title',
      key: 'productid',
      render: (src, text) => text && text.productId && text.productId.productName,
      width: 70,
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
      key: 'productid',
      // render: (src, text) => text && text.productid && text.quantity,
      width: 60,
    },
    {
      title: 'Original Price',
      dataIndex: 'originalPrice',
      key: 'productid',
      // render: (src, text) => text && text.productid && text.productid.price,
      width: 60,
    },
    // {
    //   title: 'Discounted Price',
    //   dataIndex: 'discounted_price',
    //   key: 'productid',
    //   // render: (src, text) => text && text.productid && text.productid.discount_price !== undefined ? text.productid.discount_price : '-',
    //   width: 60,
    // }
  ];

  const columns1 = [
    {
      title: 'Package Image',
      // dataIndex: 'product_image',
      key: 'productid',
      render: (src, text) => {
        return (
          <div>
            <img style={{ width: 50, height: 50, objectFit: 'contain' }} src={text && text.packageId && text.packageId.packageImages && text.packageId.packageImages[0] || productImage} />
          </div>
        )
      },
      width: 50,
    },
    {
      title: 'Name',
      // dataIndex: 'title',
      key: 'packageId',
      render: (src, text) => text && text.packageId && text.packageId.packageName,
      width: 70,
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
      key: 'productid',
      // render: (src, text) => text && text.productid && text.quantity,
      width: 60,
    },
    {
      title: 'Original Price',
      dataIndex: 'originalPrice',
      key: 'productid',
      // render: (src, text) => text && text.productid && text.productid.price,
      width: 60,
    },
    // {
    //   title: 'Discounted Price',
    //   dataIndex: 'discounted_price',
    //   key: 'productid',
    //   // render: (src, text) => text && text.productid && text.productid.discount_price !== undefined ? text.productid.discount_price : '-',
    //   width: 60,
    // }
  ];



  const columns2 = [
    // {
    //   title: 'Package Image',
    //   // dataIndex: 'product_image',
    //   key: 'productid',
    //   render: (src, text) => {
    //     return (
    //       <div>
    //         <img style={{ width: 50, height: 50, objectFit: 'contain' }} src={text && text.customizeMealId && text.customizeMealId.packageImages && text.customizeMealId.packageImages[0] || productImage} />
    //       </div>
    //     )
    //   },
    //   width: 50,
    // },
    {
      title: 'Name',
      // dataIndex: 'title',
      key: 'customizeMealId',
      render: (src, text) => text && text.customizeMealId && text.customizeMealId.customizeMealName,
      width: 70,
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
      key: 'productid',
      // render: (src, text) => text && text.productid && text.quantity,
      width: 60,
    },
    {
      title: 'Original Price',
      dataIndex: 'originalPrice',
      key: 'productid',
      // render: (src, text) => text && text.productid && text.productid.price,
      width: 60,
    },
    // {
    //   title: 'Discounted Price',
    //   dataIndex: 'discounted_price',
    //   key: 'productid',
    //   // render: (src, text) => text && text.productid && text.productid.discount_price !== undefined ? text.productid.discount_price : '-',
    //   width: 60,
    // }
  ];


  return (
    <section className="chapter">
      <Container fluid>
        <Link to={backLink} className="ant-btn ant-btn-primary">
          <Icon type="caret-left" /> Back
        </Link>
        <article className="article pb-0 mt-3">
          <div className="d-flex justify-content-between">
            <div>
              <h2 className="main-title pt-0">Order Detail: #{props && props.orderNo}</h2>
            </div>

            <PDFDownloadLink document={<UserInvoice {...props} />} fileName={`MB-#${props.orderNo}-${moment(createdAt).format('MMDDYYYY')}.pdf`} className="ant-btn ant-btn-primary">
              {({ blob, url, loading, error }) => (loading ? 'Generating Pdf..' : 'Download now')}
            </PDFDownloadLink>

            {/* <div>
              <Button
                className="ant-btn ant-btn-primary text-right"
                type="button">
                Generate Invoice
          </Button>
            </div> */}
          </div>
          {/* <OrderDetailInfoCard /> */}
          <div className="box box-v1 mb-4">
            <Row>
              <Col md={6}>
                <p>
                  <b>Customer Name: </b>
                  {props && props.addedBy && props.addedBy.userName}
                </p>
              </Col>
              {/* <Col md={6}>
                <p>
                  <b>Last Name: </b>
                  {last_name}
                </p>
              </Col> */}
              <Col md={6}>
                <p>
                  <b>Email: </b>
                  {props && props.addedBy && props.addedBy.email}
                </p>
              </Col>
              <Col md={6}>
                <p>
                  <b>Phone: </b>
                  {props && props.addedBy && props.addedBy.phone}
                </p>
              </Col>
              <Col md={6}>
                <p>
                  <b>Flat no. : </b>
                  {props && props.addressId && props.addressId.flatNo}
                  {/* {address + ' ' + address2} */}
                </p>
              </Col>
              <Col md={6}>
                <p>
                  <b>Street : </b>
                  {props && props.addressId && props.addressId.street}
                  {/* {address + ' ' + address2} */}
                </p>
              </Col>
              <Col md={6}>
                <p>
                  <b>Landmark : </b>
                  {props && props.addressId && props.addressId.landmark}
                  {/* {address + ' ' + address2} */}
                </p>
              </Col>

              {/* <Col md={6}>
                <p>
                  <b>Address 2: </b>
                  {address2}
                </p>
              </Col> */}
              <Col md={6}>
                <p>
                  <b>City: </b>
                  {props && props.addressId && props.addressId.city}
                </p>
              </Col>
              <Col md={6}>
                <p>
                  <b>State: </b>
                  {props && props.addressId && props.addressId.state}
                </p>
              </Col>
              <Col md={6}>
                <p>
                  <b>Postal Code: </b>
                  {props && props.addressId && props.addressId.postalCode}
                </p>
              </Col>
              <Col md={6}>
                <p>
                  <b>Payment mode: </b>
                  {props && props.paymentMode}
                </p>
              </Col>
              <Col md={6}>
                <p>
                  <b>Original Total: </b>
                  ₹{props && props.originalTotal}
                </p>
              </Col>
              <Col md={6}>
                <p>
                  <b>Discounted Total: </b>
                  ₹{props && props.discountedTotal}
                </p>
              </Col>
              <Col md={6}>
                <p>
                  <b>Status: </b>
                  {props && props.orderStatus}
                </p>
              </Col> 
            </Row>

          </div>
          <div className="box box-v1 mb-4">
            <Row>
              <label className="child-title pt-0 ml-2 mt-3">Items:</label><br />
            </Row>
            {/* <Row> */}
            <Table
              columns={columns}
              dataSource={item}
              className="ant-table-v1"
              scroll={{ x: 1000 }}
            />
            {props && props.package && props.package.length > 0 ?

              <>
                <Row>
                  <label className="child-title pt-0 ml-1 mt-3">Packages :</label><br />
                </Row>
                {/* <Row> */}
                <Table
                  columns={columns1}
                  dataSource={props && props.package}
                  className="ant-table-v1"
                  scroll={{ x: 1000 }}
                />
                {/* </Row> */}
              </> :
              ''
            }

            {props && props.customizeMeal && props.customizeMeal.length > 0 ?

              <>
                <Row>
                  <label className="child-title pt-0 ml-1 mt-3">Customized Meals:</label><br />
                </Row>
                {/* <Row> */}
                <Table
                  columns={columns2}
                  dataSource={props && props.customizeMeal}
                  className="ant-table-v1"
                  scroll={{ x: 1000 }}
                />
                {/* </Row> */}
              </> :
              ''
            }
          </div>
        </article>
      </Container>
    </section>
  );
};

export default withRouter(OrderDetailCard);
