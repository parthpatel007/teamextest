import productImage from '../../../assets/images/productImage.png';
import React from 'react';
import { withRouter } from 'react-router-dom';
import {Row, Col } from 'reactstrap';
import {Card } from 'antd';
import SlickSlider from "react-slick";
import styled from "styled-components";
import ReactHtmlParser from 'react-html-parser';

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true
};


const PackagePreviewDetailCard = props => {


  const {
    description,
    isPopular,
    item,
    packageImages,
    packageName,
    status,
    totalPrice,
    match,
    history,
  } = props;

  if (!props._id) {
    history.push(
      match.url
        .split('/')
        .slice(0, -1)
        .join('/')
    );
    return null;
  }

  const backLink = match.url
    .split('/')
    .slice(0, -1)
    .join('/');
  if (packageName === '') {
    history.push(backLink);
    return null;
  }
  return (
    <section className="chapter pt-3">
      <article className="article pb-0">
        <h2 className="main-title pt-0">Package  Details</h2>

        <div className="box box-v1 mb-4">
          <Row>
            <Col md={6} lg={4}>
              <Slider {...settings}>
                {packageImages && packageImages.length > 0 ? packageImages.map((image, index) => {
                  return <img src={image} alt={packageName} />
                }) : <img src={productImage} alt={packageName} /> }
              </Slider>
            </Col>

            <Col md={6} lg={8}>
              <h3>{packageName}</h3>

              <p className="mb-2">
                <b>Status: </b>
                <b className={`text-${status === 0 ? 'success' : 'danger'}`}>
                  {status === 0 ? 'Active' : 'Inactive'}
                </b>
              </p>
              <p className="mb-2">
                <b>Popular: </b>
                {isPopular ? "true" : "false"}
              </p>
              {/* <p className="mb-2">
                <b>totalPrice: </b>
                {totalPrice}
              </p> */}

              <p className="mb-2">
                <b>Item: </b>
                {item && item.map((el) => {
                  return el && el.productId && el.productId.productName
                }).join(", ")}
              </p>

              <p>
                <b>Price: </b>₹{totalPrice}
              </p>
            </Col>
          </Row>
          <Card>
            <h4>Description</h4>
            <hr />
            <div style={{"wordBreak": "break-all"}}>{ReactHtmlParser(description)}</div>
          </Card>
          {/* <Card>
            <Row>
              <Col lg={6}>
                <h4>Ingredients</h4>
                <hr />
                <div>{ReactHtmlParser(ingredients)}</div>
                {/* <ol>
                  {ingredients.map((data, index) => (
                    <li key={index}>{data}</li>
                  ))}
                </ol> */}
              {/* </Col> */}
              {/* <Col lg={6}> */}
                {/* <h4>How to use:</h4>
                <hr />
                <div>{ReactHtmlParser(howToUse)}</div> */}
                {/* <ol>
                  {directions_to_use.map((data, index) => (
                    <li key={index}>{data}</li>
                  ))}
                </ol> */}
              {/* </Col>
            </Row>
          </Card> */}
        </div>
      </article>
    </section>
  );
};

export default withRouter(PackagePreviewDetailCard);

const Slider = styled(SlickSlider)`
  padding: 0 40px;
  /* 
  &::before {
    content: "";
    position: absolute;
    box-shadow: 0px 0px 50px rgba(0, 0, 0, 0.1);
    box-shadow: 0px 0px 195px 443px rgba(0, 0, 0, 0.11);
    width: 148px;
    height: 21px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 0;
  } */
  .otc-poduct-s-card {
    box-shadow: none;
  }
  .slick {
    &-list {
    }
    &-track {
      padding: 60px 0;
    }
    &-arrow {
      height: 30px;
      width: 30px;

      &::before {
        font-size: 30px;
        color: #efefef;
        opacity: 1 !important;
        z-index: 2;
      }
      &::after {
        content: "";
        position: absolute;
        width: 20px;
        height: 20px;
        background: #000;
        top: 50%;
        left: 50%;
        z-index: -1;
        border-radius: 100%;
        margin-top: -2px;
        transform: translate(-50%, -50%);
      }
      &:hover {
        &::before {
          color: #5487D5;
        }
      }
      &.slick-prev {
        left: 0;
      }
      &.slick-next {
        right: 0;
      }
    }
  }
`;