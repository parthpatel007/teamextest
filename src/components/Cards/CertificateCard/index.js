import React from 'react';
import { Container } from 'reactstrap';


const CertificateCard = props => {
  return (
    <section className="chapter pt-5">
      <Container fluid>
        <article className="article pb-0">
          <div className="d-flex justify-content-between">
            <div>
              <h2 className="main-title pt-0">Certificate</h2>
            </div>
          </div>
        </article>
      </Container>
    </section>
  );
};

export default CertificateCard;