import React, { Component } from 'react'
import productImage from '../../../assets/images/productImage.png';
import { withRouter } from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import { Card } from 'antd';
import SlickSlider from "react-slick";
import styled from "styled-components";
import ReactHtmlParser from 'react-html-parser';


class ProductDetailCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nav1: null,
      nav2: null
    };
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
  }


  render() {
    const {

      categoryId,

      // description,
      howToUse,
      ingredients,

      price,
      productImages,
      productName,
      productCode,
      description,
      status,
      subCategoryId,

      match,
      history,

    } = this.props;
    console.log(history, "productImagesproductImagesproductImages")
    if (!this.props.categoryId) {
      history.push(
        match.url
          .split('/')
          .slice(0, -1)
          .join('/')
      );
      return null;
    }

    const backLink = match.url
      .split('/')
      .slice(0, -1)
      .join('/');
    if (productName === '') {
      history.push(backLink);
      return null;
    }
    return (
      <>
        <section className="chapter pt-3">
          <article className="article pb-0">
            <h2 className="main-title pt-0">Product  Details</h2>

            <div className="box box-v1 mb-4 comman-slider-main">
              <Row>
                <Col md={6} lg={4}>
                  {productImages && productImages.length > 0 ?

                    <>
                      <Slider
                        asNavFor={this.state.nav2}
                        ref={slider => (this.slider1 = slider)}
                        className="above-slider-main"
                      >
                        {productImages.map((image, index) =>
                          <img src={image} alt={productName} />
                        )}
                      </Slider>

                      <Slider
                        asNavFor={this.state.nav1}
                        ref={slider => (this.slider2 = slider)}
                        slidesToShow={2}
                        swipeToSlide={true}
                        focusOnSelect={true}
                        className="below-slider-main"
                      >
                        {productImages.map((image, index) =>
                          <img src={image} alt={productName} />
                        )}
                      </Slider>
                    </>
                    : <img src={productImage} alt={productName} />
                  }
                </Col>

                <Col md={6} lg={8} className="right-side-main">
                  <h3>{productName}</h3>

                  <p className="mb-2">
                    <b>Status: </b>
                    <b className={`text-${status === 0 ? 'success' : 'danger'}`}>
                      {status === 0 ? 'Active' : 'Inactive'}
                    </b>
                  </p>
                  <p className="mb-2">
                    <b>Category: </b>
                    {categoryId && categoryId.categoryName}
                  </p>
                  <p className="mb-2">
                    <b>Subcategory: </b>
                    {subCategoryId && subCategoryId.subCategoryName}
                  </p>
                  <p className="mb-2">
                    <b>Product Code: </b>
                    {productCode}
                  </p>





                  <p>
                    <b>Price: </b>₹{price}
                  </p>
                </Col>
              </Row>
              <Card>
                <h4>Description</h4>
                <hr />
                <div>{ReactHtmlParser(description)}</div>
              </Card>
              <Card>
                <Row>
                  <Col lg={12} className="margin-tops">
                    <h4>Ingredients</h4>
                    <hr />
                    <div style={{ "wordBreak": "break-all" }}>{ReactHtmlParser(ingredients)}</div>

                  </Col>
                  <Col lg={12} className="margin-tops">
                    <h4>How To Use</h4>
                    <hr />
                    <div style={{ "wordBreak": "break-all" }}>{ReactHtmlParser(howToUse)}</div>

                  </Col>


                </Row>
              </Card>
            </div>
          </article>
        </section>
      </>
    )
  }
}


export default withRouter(ProductDetailCard);


const Slider = styled(SlickSlider)`
  padding: 0 40px;
  /* 
  &::before {
    content: "";
    position: absolute;
    box-shadow: 0px 0px 50px rgba(0, 0, 0, 0.1);
    box-shadow: 0px 0px 195px 443px rgba(0, 0, 0, 0.11);
    width: 148px;
    height: 21px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 0;
  } */
  .otc-poduct-s-card {
    box-shadow: none;
  }
  .slick {
    &-list {
    }
    &-track {
      padding: 60px 0;
    }
    &-arrow {
      height: 30px;
      width: 30px;

      &::before {
        font-size: 30px;
        color: #F05A36;
        opacity: 1 !important;
        z-index: 2;
      }
      &::after {
        content: "";
        position: absolute;
        width: 20px;
        height: 20px;
        background: #000;
        top: 50%;
        left: 50%;
        z-index: -1;
        border-radius: 100%;
        margin-top: -2px;
        transform: translate(-50%, -50%);
      }
      &:hover {
        &::before {
          color: #5487D5;
        }
      }
      &.slick-prev {
        left: 0;
      }
      &.slick-next {
        right: 0;
      }
    }
  }
`;
