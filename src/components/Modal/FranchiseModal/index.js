import React from 'react';
import { Modal, Button, Icon, Descriptions } from 'antd';
import ReactHtmlParser from 'react-html-parser';

class FranchiseModal extends React.Component {
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };


  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  render() {
    const { Address, FranchiseName, Email, FranchiseNumber, City, PinCode, District, State, ContactNo } = this.props;
    return (
      <div>
        <Button type="primary" shape="circle" className="ml-2" onClick={this.showModal}>
          <Icon type={'eye'} theme={'filled'} />
          {/* {type === 'block' ? 'Block' : 'Unblock'} */}
        </Button>
        <Modal
          title={"View Franchise Details"}
          visible={this.state.visible}
          // onOk={this.handleOk}          
          footer={null}
          onCancel={this.handleCancel}
          centered
        >
          <Descriptions bordered column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}>
            <>
              <Descriptions.Item label="FranchiseNumber">{`${FranchiseNumber}`}</Descriptions.Item>
              <Descriptions.Item label="FranchiseName">{ReactHtmlParser(FranchiseName)}</Descriptions.Item>
              <Descriptions.Item label="Address">{ReactHtmlParser(Address)}</Descriptions.Item>
              <Descriptions.Item label="City">{`${City}`}</Descriptions.Item>
              <Descriptions.Item label="PinCode">{`${PinCode}`}</Descriptions.Item>
              <Descriptions.Item label="District">{`${District}`}</Descriptions.Item>
              <Descriptions.Item label="State">{ReactHtmlParser(State)}</Descriptions.Item>
              <Descriptions.Item label="ContactNo">{ContactNo}</Descriptions.Item>
              <Descriptions.Item label="Email">{ReactHtmlParser(Email)}</Descriptions.Item>
            </>
          </Descriptions>
        </Modal>
      </div>
    );
  }
}

export default FranchiseModal