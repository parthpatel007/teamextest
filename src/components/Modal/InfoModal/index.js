import React from 'react';
import { Modal, Button, Icon, Descriptions } from 'antd';
import ReactHtmlParser from 'react-html-parser';

export default class InfoModal extends React.Component {
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };


  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };



  render() {
    const { InfoData, Name, Description, Status } = this.props;
    return (
      <div>
        <Button type="primary" shape="circle" className="ml-2" onClick={this.showModal}>
          <Icon type={'eye'} theme={'filled'} />
          {/* {type === 'block' ? 'Block' : 'Unblock'} */}
        </Button>
        <Modal
          title={"View Category"}
          visible={this.state.visible}
          // onOk={this.handleOk}          
          footer={null}
          onCancel={this.handleCancel}
          centered
        >


          <Descriptions bordered column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}>
            {'subCategoryName' in InfoData ?
              <>
                <Descriptions.Item label="SubCategory">{`${InfoData && InfoData.categoryId && InfoData.categoryId.categoryName}`}</Descriptions.Item>
                <Descriptions.Item label="Category">{`${Name}`}</Descriptions.Item>
                <Descriptions.Item label="Status">{`${Status === 0 ? 'Active' : 'Inactive'}`}</Descriptions.Item>
                <Descriptions.Item label="Description">{ReactHtmlParser(Description)}</Descriptions.Item>
              </> :
              <>
                <Descriptions.Item label="Category">{`${Name}`}</Descriptions.Item>
                <Descriptions.Item label="Status">{`${Status === 0 ? 'Active' : 'Inactive'}`}</Descriptions.Item>
                <Descriptions.Item label="Description">{ReactHtmlParser(Description)}</Descriptions.Item>
              </>}
          </Descriptions>
        </Modal>
      </div>
    );
  }
}
