import React from 'react';
import { Modal, Button, Icon } from 'antd';
import axios from 'axios';
import { backendUrl } from '../../../common/credentials';

export default class BlockModal extends React.Component {
  state = { visible: false };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  blockApi = id => {
    const { api = '',  recordData} = this.props;

    const authToken = localStorage.getItem('access_token');
    api &&
      axios
        .post(
          `${backendUrl}/${api}/${id}`,
          recordData,
          {
            headers: {
              Authorization: `Bearer ${authToken}`,
            },
          }
        )
        .then(function (response) {
          if (response) {
            window.location.reload();
          }
        })
        .catch(function (error) {
        });
  };

  render() {
    const {
      recordData,
      recordName,
      type = 'block',
      blockText = 'block',
      unblockText = 'unblock',
      blockTitleText = 'Block',
      unblockTitleText = 'Unblock',
      blockBtnText = 'Block',
      unblockBtnText = 'Unblock',
    } = this.props;
    const { _id } = recordData;

    return (
      <div>
        <Button type="primary" shape="circle" className="ml-2" onClick={this.showModal}>
          <Icon type={type === 'block' ? 'api' : 'link'} theme={type === 'block' && 'filled'} />
          {/* {type === 'block' ? 'Block' : 'Unblock'} */}
        </Button>
        <Modal
          title={`${type === 'block' ? blockTitleText : unblockTitleText}`}
          visible={this.state.visible}
          onOk={() => this.blockApi(_id)}
          okText={type === 'block' ? blockBtnText : unblockBtnText}
          onCancel={this.handleCancel}
          centered
        >
          <section className="form-card">
            <div className="form-card__body ">
              <label style={{ fontSize: 16 }}>
                Do you really want to {type === 'block' ? blockText : unblockText}{' '}
                <b>{recordName}</b>
              </label>
            </div>
          </section>
        </Modal>
      </div>
    );
  }
}
