import React from 'react';
import { Modal, Button, Icon } from 'antd';
import { Descriptions } from 'antd';
import ReactHtmlParser from 'react-html-parser';



class BannerModal extends React.Component {
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };


  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };



  render() {
    const { InfoData, VideoName, VideoUrl, } = this.props;
    return (
      <div>
        <Button type="primary" shape="circle" className="ml-2" onClick={this.showModal}>
          <Icon type={'eye'} theme={'filled'} />
          {/* {type === 'block' ? 'Block' : 'Unblock'} */}
        </Button>
        <Modal
          title={"View Banner Details"}
          visible={this.state.visible}
          footer={null}
          onCancel={this.handleCancel}
          centered
        >


          <Descriptions bordered column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}>

            <>

              <Descriptions.Item label="Banner Name">{`${VideoName}`}</Descriptions.Item>
              <Descriptions.Item label="Video Url">{ReactHtmlParser(VideoUrl)}</Descriptions.Item>
            </>

          </Descriptions>
        </Modal>
      </div>
    );
  }
}

export default BannerModal