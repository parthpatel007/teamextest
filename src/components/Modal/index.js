export { default as BlockModal } from './BlockModal';
export { default as InfoModal } from './InfoModal';
export { default as VideoModal } from './VideoModal'
export { default as FranchiseModal} from './FranchiseModal'
export {default as BannerModal} from './BannerModal'