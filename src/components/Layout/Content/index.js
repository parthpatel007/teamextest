import React from 'react';
import { Route } from 'react-router-dom';
import { withRouter } from 'react-router';
import loadable from 'react-loadable';
import LoadingComponent from 'components/Loading';
import { Layout } from 'antd';
const { Content } = Layout;

let AsyncCalendar = loadable({
  loader: () => import('routes/calendar/'),
  loading: LoadingComponent,
});
let AsyncCard = loadable({
  loader: () => import('routes/card/'),
  loading: LoadingComponent,
});
let AsyncChart = loadable({
  loader: () => import('routes/chart/'),
  loading: LoadingComponent,
});
let AsyncDashboard = loadable({
  loader: () => import('routes/dashboard/'),
  loading: LoadingComponent,
});


// let AsyncPayment = loadable({
//   loader: () => import('routes/Payments/'),
//   loading: LoadingComponent,
// });


// teamex routes //



let AsyncReview = loadable({
  loader: () => import('routes/review/'),
  loading: LoadingComponent,
});
let AsyncTestimonial = loadable({
  loader: () => import('routes/testimonial/'),
  loading: LoadingComponent,
});
let AsyncFrenchies = loadable({
  loader: () => import('routes/frenchies/'),
  loading: LoadingComponent,
});
let AsyncCertificate = loadable({
  loader: () => import('routes/certificate/'),
  loading: LoadingComponent,
});
let AsyncVideo = loadable({
  loader: () => import('routes/video/'),
  loading: LoadingComponent,
});

let AsyncCustomer = loadable({
  loader: () => import('routes/customer/'),
  loading: LoadingComponent,
});

let AsyncOrders = loadable({
  loader: () => import('routes/order/'),
  loading: LoadingComponent,
});

let AsyncHomebanner = loadable({
  loader: () => import('routes/homebanner/'),
  loading: LoadingComponent,
});

let AsyncDownload = loadable({
  loader: () => import('routes/download/'),
  loading: LoadingComponent,
})

let AsyncImagePopup = loadable({
  loader: () => import('routes/imagepopup/'),
  loading: LoadingComponent,
})
// let AsyncPayments = loadable({
//   loader : () => import('routes/Payments/'),
//   loading : LoadingComponent,
// })











//-------------//

let AsyncProducts = loadable({
  loader: () => import('routes/products/'),
  loading: LoadingComponent,
});
let AsyncPayment = loadable({
  loader: () => import('routes/payment/'),
  loading: LoadingComponent,
});
// let AsyncOrder = loadable({
//   loader: () => import('routes/orders/'),
//   loading: LoadingComponent,
// });
let AsyncAccounts = loadable({
  loader: () => import('routes/accounts/'),
  loading: LoadingComponent,
});
let AsyncECommerce = loadable({
  loader: () => import('routes/ecommerce/'),
  loading: LoadingComponent,
});
let AsyncFeedback = loadable({
  loader: () => import('routes/feedback/'),
  loading: LoadingComponent,
});
let AsyncForm = loadable({
  loader: () => import('routes/form/'),
  loading: LoadingComponent,
});
let AsyncLayout = loadable({
  loader: () => import('routes/layout/'),
  loading: LoadingComponent,
});
let AsyncPage = loadable({
  loader: () => import('routes/page/'),
  loading: LoadingComponent,
});
let AsyncTable = loadable({
  loader: () => import('routes/table/'),
  loading: LoadingComponent,
});
let AsyncUI = loadable({
  loader: () => import('routes/ui/'),
  loading: LoadingComponent,
});
let AsyncUIOverview = loadable({
  loader: () => import('routes/ui-overview/'),
  loading: LoadingComponent,
});
let AsyncException = loadable({
  loader: () => import('routes/exception/'),
  loading: LoadingComponent,
});

class AppContent extends React.Component {
  render() {
    const { match } = this.props;

    return (
      <Content id="app-content">
        <Route path={`${match.url}/dashboard`} component={AsyncDashboard} />
        <Route path={`${match.url}/products`} component={AsyncProducts} />
        <Route path={`${match.url}/accounts`} component={AsyncAccounts} />
        {/* <Route path={`${match.url}/payment`} component={AsyncPayment} /> */}
        {/* <Route path={`${match.url}/orders`} component={AsyncOrder} /> */}
        <Route path={`${match.url}/calendar`} component={AsyncCalendar} />
        <Route path={`${match.url}/card`} component={AsyncCard} />
        <Route path={`${match.url}/chart`} component={AsyncChart} />
        <Route path={`${match.url}/ecommerce`} component={AsyncECommerce} />
        <Route path={`${match.url}/feedback`} component={AsyncFeedback} />
        <Route path={`${match.url}/form`} component={AsyncForm} />
        <Route path={`${match.url}/layout`} component={AsyncLayout} />
        <Route path={`${match.url}/page`} component={AsyncPage} />
        <Route path={`${match.url}/table`} component={AsyncTable} />
        <Route path={`${match.url}/ui`} component={AsyncUI} />
        <Route path={`${match.url}/ui-overview`} component={AsyncUIOverview} />
        <Route path={`${match.url}/exception`} component={AsyncException} />
        <Route path={`${match.url}/review`} component={AsyncReview} />
        <Route path={`${match.url}/testimonial`} component={AsyncTestimonial} />
        <Route path={`${match.url}/frenchies`} component={AsyncFrenchies} />
        <Route path={`${match.url}/certificate`} component={AsyncCertificate} />
        <Route path={`${match.url}/video`} component={AsyncVideo} />
        <Route path={`${match.url}/customer`} component={AsyncCustomer} />
        <Route path={`${match.url}/order`} component={AsyncOrders} />
        <Route path={`${match.url}/payment`} component={AsyncPayment} />
        <Route path={`${match.url}/homebanner`} component={AsyncHomebanner} />
        <Route path={`${match.url}/download`} component={AsyncDownload} />
        <Route path={`${match.url}/imagepopup`} component={AsyncImagePopup} />
      </Content>
    );
  }
}

export default withRouter(AppContent);
