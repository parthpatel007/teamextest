import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Menu, Icon } from 'antd';
import APPCONFIG from 'constants/appConfig';
import { toggleOffCanvasMobileNav } from 'actions/settingsActions';
import {
  PRODUCTS,
} from 'constants/uiComponents';

const SubMenu = Menu.SubMenu;

class AppMenu extends React.Component {
  // list for AccordionNav
  rootMenuItemKeys = [
    // without submenu
    '/app/dashboard',
    '/app/ui-overview',
    '/app/calendar',
    '/app/frenchies',
    '/app/review',
    '/app/testimonial',
    '/app/video',
    '/app/certificate',
    '/app/customer',
    '/app/homebanner',
    '/app/download',
    '/app/imagepopup',

  ];
  rootSubmenuKeys = [
    '/app/card',
    '/app/layout',
    '/app/ui',
    '/app/form',
    '/app/feedback',
    '/app/table',
    '/app/chart',
    '/app/page',
    '/app/ecommerce',
    '/app/products',
    '/app/meals',
    '/app/subscriptions',
    '/app/customers',
    '/user',
    '/exception',
    '/app/menu',
  ];

  state = {
    openKeys: ['/app/dashboard', '/app/frenchies', '/app/review', '/app/testimonial', '/app/video', '/app/certificate', 'app/customer', 'app/homebanner', 'app/download', 'app/imagepopup'],
  };

  onOpenChange = openKeys => {

    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  };

  onMenuItemClick = item => {
    // AccordionNav
    const itemKey = item.key;
    if (this.rootMenuItemKeys.indexOf(itemKey) >= 0) {
      this.setState({ openKeys: [itemKey] });
    }

    //
    const { isMobileNav } = this.props;
    if (isMobileNav) {
      this.closeMobileSidenav();
    }
  };

  closeMobileSidenav = () => {
    if (APPCONFIG.AutoCloseMobileNav) {
      const { handleToggleOffCanvasMobileNav } = this.props;
      handleToggleOffCanvasMobileNav(true);
    }
  };

  //
  getSubMenuOrItem = item => {
    if (item.children && item.children.some(child => child.name)) {
      const childrenItems = this.getNavMenuItems(item.children);
      if (childrenItems && childrenItems.length > 0) {
        return (
          <SubMenu title={item.name} key={item.path}>
            {childrenItems}
          </SubMenu>
        );
      }
      return null;
    } else {
      return (
        <Menu.Item key={item.path}>
          <Link to={item.path}>
            <span>{item.menuName || item.name}</span>
          </Link>
        </Menu.Item>
      );
    }
  };

  getNavMenuItems = menusData => {
    if (!menusData) {
      return [];
    }
    return menusData
      .filter(item => !item.hideInMenu)
      .map(item => {
        // make dom
        const ItemDom = this.getSubMenuOrItem(item);
        return ItemDom;
      })
      .filter(item => item);
  };

  render() {
    const { collapsedNav, colorOption, location } = this.props;
    // const mode = collapsedNav ? 'vertical' : 'inline';
    const menuTheme =
      ['31', '32', '33', '34', '35', '36'].indexOf(colorOption) >= 0 ? 'light' : 'dark';
    const currentPathname = location.pathname;

    const menuProps = collapsedNav
      ? {}
      : {
        openKeys: this.state.openKeys,
      };

    return (
      <Menu
        theme={menuTheme}
        mode="inline"
        inlineCollapsed={collapsedNav}
        {...menuProps}
        onOpenChange={this.onOpenChange}
        onClick={this.onMenuItemClick}
        selectedKeys={[currentPathname]}
      >
        <Menu.Item key="/app/dashboard">
          <a href="#/app/dashboard">
            <Icon type="dashboard" />
            <span className="nav-text">Dashboard</span>
          </a>
        </Menu.Item>
        <SubMenu
          key="/app/products"
          title={
            <span>
              <Icon type="shopping-cart" />
              <span className="nav-text">Products</span>
            </span>
          }
        >
          {this.getNavMenuItems(PRODUCTS)}
        </SubMenu>
        <Menu.Item key="/app/homebanner">
          <a href="#/app/homebanner">
            <Icon type="home" />
            <span className="nav-text">Home Banner</span>
          </a>
        </Menu.Item>
        <Menu.Item key="/app/frenchies">
          <a href="#/app/frenchies">
            <Icon type="fork" />
            <span className="nav-text">Franchise</span>
          </a>
        </Menu.Item>
        <Menu.Item key="/app/review">
          <a href="#/app/review">
            <Icon type="form" />
            <span className="nav-text">Review</span>
          </a>
        </Menu.Item>
        <Menu.Item key="/app/testimonial">
          <a href="#/app/testimonial">
            <Icon type="file-text" />
            <span className="nav-text">Testimonial</span>
          </a>
        </Menu.Item>
        <Menu.Item key="/app/video">
          <a href="#/app/video">
            <Icon type="play-square" />
            <span className="nav-text">Video</span>
          </a>
        </Menu.Item>
        <Menu.Item key="/app/certificate">
          <a href="#/app/certificate">
            <Icon type="safety-certificate" />
            <span className="nav-text">Certificate</span>
          </a>
        </Menu.Item>
        <Menu.Item key="/app/customer">
          <a href="#/app/customer">
            <Icon type="user" />
            <span className="nav-text">Customer</span>
          </a>
        </Menu.Item>
        <Menu.Item key="/app/order">
          <a href="#/app/order">
            <Icon type="inbox" />
            <span className="nav-text">Order</span>
          </a>
        </Menu.Item>
        <Menu.Item key="/app/download">
          <a href="#/app/download">
            <Icon type="form" />
            <span className="nav-text">Download</span>
          </a>
        </Menu.Item>
        <Menu.Item key="/app/imagepopup">
          <a href="#/app/imagepopup">
            <Icon type="form" />
            <span className="nav-text">Image Popup</span>
          </a>
        </Menu.Item>
      </Menu>
    );
  }
}

const mapStateToProps = state => {
  return {
    collapsedNav: state.settings.collapsedNav,
    colorOption: state.settings.colorOption,
    location: state.routing.location,
  };
};

const mapDispatchToProps = dispatch => ({
  handleToggleOffCanvasMobileNav: isOffCanvasMobileNav => {
    dispatch(toggleOffCanvasMobileNav(isOffCanvasMobileNav));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(AppMenu);
